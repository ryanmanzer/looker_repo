view: taxclass {
  sql_table_name: public.taxclass ;;

  dimension: taxclass_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.taxclass_id ;;
  }

  dimension: taxclass_code {
    type: string
    sql: ${TABLE}.taxclass_code ;;
  }

  dimension: taxclass_descrip {
    type: string
    sql: ${TABLE}.taxclass_descrip ;;
  }

  dimension: taxclass_sequence {
    type: number
    sql: ${TABLE}.taxclass_sequence ;;
  }

  measure: count {
    type: count
    drill_fields: [taxclass_id]
  }
}
