view: cmhead {
  sql_table_name: public.cmhead ;;

  dimension: cmhead_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.cmhead_id ;;
  }

  dimension: cmhead_billtoaddress1 {
    type: string
    sql: ${TABLE}.cmhead_billtoaddress1 ;;
  }

  dimension: cmhead_billtoaddress2 {
    type: string
    sql: ${TABLE}.cmhead_billtoaddress2 ;;
  }

  dimension: cmhead_billtoaddress3 {
    type: string
    sql: ${TABLE}.cmhead_billtoaddress3 ;;
  }

  dimension: cmhead_billtocity {
    type: string
    sql: ${TABLE}.cmhead_billtocity ;;
  }

  dimension: cmhead_billtocountry {
    type: string
    sql: ${TABLE}.cmhead_billtocountry ;;
  }

  dimension: cmhead_billtoname {
    type: string
    sql: ${TABLE}.cmhead_billtoname ;;
  }

  dimension: cmhead_billtostate {
    type: string
    sql: ${TABLE}.cmhead_billtostate ;;
  }

  dimension: cmhead_billtozip {
    type: string
    sql: ${TABLE}.cmhead_billtozip ;;
  }

  dimension: cmhead_comments {
    type: string
    sql: ${TABLE}.cmhead_comments ;;
  }

  dimension: cmhead_commission {
    type: number
    sql: ${TABLE}.cmhead_commission ;;
  }

  dimension: cmhead_curr_id {
    type: number
    sql: ${TABLE}.cmhead_curr_id ;;
  }

  dimension: cmhead_cust_id {
    type: number
    sql: ${TABLE}.cmhead_cust_id ;;
  }

  dimension: cmhead_custponumber {
    type: string
    sql: ${TABLE}.cmhead_custponumber ;;
  }

  dimension_group: cmhead_docdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cmhead_docdate ;;
  }

  dimension: cmhead_freight {
    type: number
    sql: ${TABLE}.cmhead_freight ;;
  }

  dimension: cmhead_freighttaxtype_id {
    type: number
    sql: ${TABLE}.cmhead_freighttaxtype_id ;;
  }

  dimension_group: cmhead_gldistdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cmhead_gldistdate ;;
  }

  dimension: cmhead_hold {
    type: yesno
    sql: ${TABLE}.cmhead_hold ;;
  }

  dimension: cmhead_invcnumber {
    type: string
    sql: ${TABLE}.cmhead_invcnumber ;;
  }

  dimension: cmhead_misc {
    type: number
    sql: ${TABLE}.cmhead_misc ;;
  }

  dimension: cmhead_misc_accnt_id {
    type: number
    sql: ${TABLE}.cmhead_misc_accnt_id ;;
  }

  dimension: cmhead_misc_descrip {
    type: string
    sql: ${TABLE}.cmhead_misc_descrip ;;
  }

  dimension: cmhead_number {
    type: string
    sql: ${TABLE}.cmhead_number ;;
  }

  dimension: cmhead_posted {
    type: yesno
    sql: ${TABLE}.cmhead_posted ;;
  }

  dimension: cmhead_printed {
    type: yesno
    sql: ${TABLE}.cmhead_printed ;;
  }

  dimension: cmhead_prj_id {
    type: number
    sql: ${TABLE}.cmhead_prj_id ;;
  }

  dimension: cmhead_rahead_id {
    type: number
    sql: ${TABLE}.cmhead_rahead_id ;;
  }

  dimension: cmhead_rsncode_id {
    type: number
    sql: ${TABLE}.cmhead_rsncode_id ;;
  }

  dimension: cmhead_salesrep_id {
    type: number
    sql: ${TABLE}.cmhead_salesrep_id ;;
  }

  dimension: cmhead_saletype_id {
    type: number
    sql: ${TABLE}.cmhead_saletype_id ;;
  }

  dimension: cmhead_shipto_address1 {
    type: string
    sql: ${TABLE}.cmhead_shipto_address1 ;;
  }

  dimension: cmhead_shipto_address2 {
    type: string
    sql: ${TABLE}.cmhead_shipto_address2 ;;
  }

  dimension: cmhead_shipto_address3 {
    type: string
    sql: ${TABLE}.cmhead_shipto_address3 ;;
  }

  dimension: cmhead_shipto_city {
    type: string
    sql: ${TABLE}.cmhead_shipto_city ;;
  }

  dimension: cmhead_shipto_country {
    type: string
    sql: ${TABLE}.cmhead_shipto_country ;;
  }

  dimension: cmhead_shipto_id {
    type: number
    sql: ${TABLE}.cmhead_shipto_id ;;
  }

  dimension: cmhead_shipto_name {
    type: string
    sql: ${TABLE}.cmhead_shipto_name ;;
  }

  dimension: cmhead_shipto_state {
    type: string
    sql: ${TABLE}.cmhead_shipto_state ;;
  }

  dimension: cmhead_shipto_zipcode {
    type: string
    sql: ${TABLE}.cmhead_shipto_zipcode ;;
  }

  dimension: cmhead_shipzone_id {
    type: number
    sql: ${TABLE}.cmhead_shipzone_id ;;
  }

  dimension: cmhead_taxzone_id {
    type: number
    sql: ${TABLE}.cmhead_taxzone_id ;;
  }

  dimension: cmhead_void {
    type: yesno
    sql: ${TABLE}.cmhead_void ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [cmhead_id, cmhead_shipto_name, cmhead_billtoname]
  }
}
