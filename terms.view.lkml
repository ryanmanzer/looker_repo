view: terms {
  sql_table_name: public.terms ;;

  dimension: terms_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.terms_id ;;
  }

  dimension: terms_ap {
    type: yesno
    sql: ${TABLE}.terms_ap ;;
  }

  dimension: terms_ar {
    type: yesno
    sql: ${TABLE}.terms_ar ;;
  }

  dimension: terms_code {
    type: string
    sql: ${TABLE}.terms_code ;;
  }

  dimension: terms_cutoffday {
    type: number
    sql: ${TABLE}.terms_cutoffday ;;
  }

  dimension: terms_descrip {
    type: string
    sql: ${TABLE}.terms_descrip ;;
  }

  dimension: terms_discdays {
    type: number
    sql: ${TABLE}.terms_discdays ;;
  }

  dimension: terms_discprcnt {
    type: number
    sql: ${TABLE}.terms_discprcnt ;;
  }

  dimension: terms_duedays {
    type: number
    sql: ${TABLE}.terms_duedays ;;
  }

  dimension: terms_fincharg {
    type: yesno
    sql: ${TABLE}.terms_fincharg ;;
  }

  dimension: terms_type {
    type: string
    sql: ${TABLE}.terms_type ;;
  }

  measure: count {
    type: count
    drill_fields: [terms_id]
  }
}
