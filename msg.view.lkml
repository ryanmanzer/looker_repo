view: msg {
  sql_table_name: public.msg ;;

  dimension: msg_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.msg_id ;;
  }

  dimension_group: msg_expires {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.msg_expires ;;
  }

  dimension_group: msg_posted {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.msg_posted ;;
  }

  dimension_group: msg_scheduled {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.msg_scheduled ;;
  }

  dimension: msg_text {
    type: string
    sql: ${TABLE}.msg_text ;;
  }

  dimension: msg_username {
    type: string
    sql: ${TABLE}.msg_username ;;
  }

  measure: count {
    type: count
    drill_fields: [msg_id, msg_username]
  }
}
