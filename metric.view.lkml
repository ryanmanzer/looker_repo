view: metric {
  sql_table_name: public.metric ;;

  dimension: metric_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.metric_id ;;
  }

  dimension: metric_module {
    type: string
    sql: ${TABLE}.metric_module ;;
  }

  dimension: metric_name {
    type: string
    sql: ${TABLE}.metric_name ;;
  }

  dimension: metric_value {
    type: string
    sql: ${TABLE}.metric_value ;;
  }

  measure: count {
    type: count
    drill_fields: [metric_id, metric_name]
  }
}
