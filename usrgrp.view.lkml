view: usrgrp {
  sql_table_name: public.usrgrp ;;

  dimension: usrgrp_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.usrgrp_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: usrgrp_grp_id {
    type: number
    sql: ${TABLE}.usrgrp_grp_id ;;
  }

  dimension: usrgrp_username {
    type: string
    sql: ${TABLE}.usrgrp_username ;;
  }

  measure: count {
    type: count
    drill_fields: [usrgrp_id, usrgrp_username]
  }
}
