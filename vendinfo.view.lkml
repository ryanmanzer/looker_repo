view: vendinfo {
  sql_table_name: public.vendinfo ;;

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: vend_1099 {
    type: yesno
    sql: ${TABLE}.vend_1099 ;;
  }

  dimension: vend_accnt_id {
    type: number
    sql: ${TABLE}.vend_accnt_id ;;
  }

  dimension: vend_accntnum {
    type: string
    sql: ${TABLE}.vend_accntnum ;;
  }

  dimension: vend_ach_accntnumber {
    type: string
    sql: ${TABLE}.vend_ach_accntnumber ;;
  }

  dimension: vend_ach_accnttype {
    type: string
    sql: ${TABLE}.vend_ach_accnttype ;;
  }

  dimension: vend_ach_enabled {
    type: yesno
    sql: ${TABLE}.vend_ach_enabled ;;
  }

  dimension: vend_ach_indiv_name {
    type: string
    sql: ${TABLE}.vend_ach_indiv_name ;;
  }

  dimension: vend_ach_indiv_number {
    type: string
    sql: ${TABLE}.vend_ach_indiv_number ;;
  }

  dimension: vend_ach_routingnumber {
    type: string
    sql: ${TABLE}.vend_ach_routingnumber ;;
  }

  dimension: vend_ach_use_vendinfo {
    type: yesno
    sql: ${TABLE}.vend_ach_use_vendinfo ;;
  }

  dimension: vend_active {
    type: yesno
    sql: ${TABLE}.vend_active ;;
  }

  dimension: vend_addr_id {
    type: number
    sql: ${TABLE}.vend_addr_id ;;
  }

  dimension: vend_cntct1_id {
    type: number
    sql: ${TABLE}.vend_cntct1_id ;;
  }

  dimension: vend_cntct2_id {
    type: number
    sql: ${TABLE}.vend_cntct2_id ;;
  }

  dimension: vend_comments {
    type: string
    sql: ${TABLE}.vend_comments ;;
  }

  dimension: vend_curr_id {
    type: number
    sql: ${TABLE}.vend_curr_id ;;
  }

  dimension: vend_edicc {
    type: string
    sql: ${TABLE}.vend_edicc ;;
  }

  dimension: vend_ediemail {
    type: string
    sql: ${TABLE}.vend_ediemail ;;
  }

  dimension: vend_ediemailbody {
    type: string
    sql: ${TABLE}.vend_ediemailbody ;;
  }

  dimension: vend_ediemailhtml {
    type: yesno
    sql: ${TABLE}.vend_ediemailhtml ;;
  }

  dimension: vend_edifilename {
    type: string
    sql: ${TABLE}.vend_edifilename ;;
  }

  dimension: vend_edisubject {
    type: string
    sql: ${TABLE}.vend_edisubject ;;
  }

  dimension: vend_emailpodelivery {
    type: yesno
    sql: ${TABLE}.vend_emailpodelivery ;;
  }

  dimension: vend_expcat_id {
    type: number
    sql: ${TABLE}.vend_expcat_id ;;
  }

  dimension: vend_exported {
    type: yesno
    sql: ${TABLE}.vend_exported ;;
  }

  dimension: vend_fob {
    type: string
    sql: ${TABLE}.vend_fob ;;
  }

  dimension: vend_fobsource {
    type: string
    sql: ${TABLE}.vend_fobsource ;;
  }

  dimension: vend_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.vend_id ;;
  }

  dimension_group: vend_lastpurchdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.vend_lastpurchdate ;;
  }

  dimension: vend_match {
    type: yesno
    sql: ${TABLE}.vend_match ;;
  }

  dimension: vend_name {
    type: string
    sql: ${TABLE}.vend_name ;;
  }

  dimension: vend_number {
    type: string
    sql: ${TABLE}.vend_number ;;
  }

  dimension: vend_po {
    type: yesno
    sql: ${TABLE}.vend_po ;;
  }

  dimension: vend_pocomments {
    type: string
    sql: ${TABLE}.vend_pocomments ;;
  }

  dimension: vend_qualified {
    type: yesno
    sql: ${TABLE}.vend_qualified ;;
  }

  dimension: vend_restrictpurch {
    type: yesno
    sql: ${TABLE}.vend_restrictpurch ;;
  }

  dimension: vend_shipvia {
    type: string
    sql: ${TABLE}.vend_shipvia ;;
  }

  dimension: vend_tax_id {
    type: number
    sql: ${TABLE}.vend_tax_id ;;
  }

  dimension: vend_taxzone_id {
    type: number
    sql: ${TABLE}.vend_taxzone_id ;;
  }

  dimension: vend_terms_id {
    type: number
    sql: ${TABLE}.vend_terms_id ;;
  }

  dimension: vend_vendtype_id {
    type: number
    sql: ${TABLE}.vend_vendtype_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      vend_name,
      vend_edifilename,
      vend_ach_indiv_name,
      vend.vend_id,
      vend.vend_name,
      vend.vend_edifilename
    ]
  }
}
