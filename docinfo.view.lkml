view: docinfo {
  sql_table_name: public.docinfo ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: purpose {
    type: string
    sql: ${TABLE}.purpose ;;
  }

  dimension: source_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.source_id ;;
  }

  dimension: source_type {
    type: string
    sql: ${TABLE}.source_type ;;
  }

  dimension: target_id {
    type: number
    sql: ${TABLE}.target_id ;;
  }

  dimension: target_number {
    type: string
    sql: ${TABLE}.target_number ;;
  }

  dimension: target_type {
    type: string
    sql: ${TABLE}.target_type ;;
  }

  measure: count {
    type: count
    drill_fields: [id, name, source.source_id, source.source_name, source.source_uiform_name]
  }
}
