view: bankadjtype {
  sql_table_name: public.bankadjtype ;;

  dimension: bankadjtype_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.bankadjtype_id ;;
  }

  dimension: bankadjtype_accnt_id {
    type: number
    sql: ${TABLE}.bankadjtype_accnt_id ;;
  }

  dimension: bankadjtype_descrip {
    type: string
    sql: ${TABLE}.bankadjtype_descrip ;;
  }

  dimension: bankadjtype_iscredit {
    type: yesno
    sql: ${TABLE}.bankadjtype_iscredit ;;
  }

  dimension: bankadjtype_name {
    type: string
    sql: ${TABLE}.bankadjtype_name ;;
  }

  measure: count {
    type: count
    drill_fields: [bankadjtype_id, bankadjtype_name]
  }
}
