view: rev {
  sql_table_name: public.rev ;;

  dimension: rev_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.rev_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension_group: rev_created {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.rev_created ;;
  }

  dimension_group: rev {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.rev_date ;;
  }

  dimension_group: rev_effective {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.rev_effective ;;
  }

  dimension_group: rev_expires {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.rev_expires ;;
  }

  dimension: rev_number {
    type: string
    sql: ${TABLE}.rev_number ;;
  }

  dimension: rev_status {
    type: string
    sql: ${TABLE}.rev_status ;;
  }

  dimension: rev_target_id {
    type: number
    sql: ${TABLE}.rev_target_id ;;
  }

  dimension: rev_target_type {
    type: string
    sql: ${TABLE}.rev_target_type ;;
  }

  measure: count {
    type: count
    drill_fields: [rev_id]
  }
}
