view: salesrep {
  sql_table_name: public.salesrep ;;

  dimension: salesrep_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.salesrep_id ;;
  }

  dimension: salesrep_active {
    type: yesno
    sql: ${TABLE}.salesrep_active ;;
  }

  dimension: salesrep_commission {
    type: number
    sql: ${TABLE}.salesrep_commission ;;
  }

  dimension: salesrep_emp_id {
    type: number
    sql: ${TABLE}.salesrep_emp_id ;;
  }

  dimension: salesrep_method {
    type: string
    sql: ${TABLE}.salesrep_method ;;
  }

  dimension: salesrep_name {
    type: string
    sql: ${TABLE}.salesrep_name ;;
  }

  dimension: salesrep_number {
    type: string
    sql: ${TABLE}.salesrep_number ;;
  }

  measure: count {
    type: count
    drill_fields: [salesrep_id, salesrep_name, saleshistory.count, saleshistorymisc.count]
  }
}
