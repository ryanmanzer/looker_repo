view: ccard {
  sql_table_name: public.ccard ;;

  dimension: ccard_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.ccard_id ;;
  }

  dimension: ccard_active {
    type: yesno
    sql: ${TABLE}.ccard_active ;;
  }

  dimension: ccard_added_by_username {
    type: string
    sql: ${TABLE}.ccard_added_by_username ;;
  }

  dimension: ccard_address1 {
    type: string
    sql: ${TABLE}.ccard_address1 ;;
  }

  dimension: ccard_address2 {
    type: string
    sql: ${TABLE}.ccard_address2 ;;
  }

  dimension: ccard_city {
    type: string
    sql: ${TABLE}.ccard_city ;;
  }

  dimension: ccard_country {
    type: string
    sql: ${TABLE}.ccard_country ;;
  }

  dimension: ccard_cust_id {
    type: number
    sql: ${TABLE}.ccard_cust_id ;;
  }

  dimension_group: ccard_date_added {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.ccard_date_added ;;
  }

  dimension: ccard_debit {
    type: yesno
    sql: ${TABLE}.ccard_debit ;;
  }

  dimension: ccard_last_updated_by_username {
    type: string
    sql: ${TABLE}.ccard_last_updated_by_username ;;
  }

  dimension_group: ccard_lastupdated {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.ccard_lastupdated ;;
  }

  dimension: ccard_month_expired {
    type: string
    sql: ${TABLE}.ccard_month_expired ;;
  }

  dimension: ccard_name {
    type: string
    sql: ${TABLE}.ccard_name ;;
  }

  dimension: ccard_number {
    type: string
    sql: ${TABLE}.ccard_number ;;
  }

  dimension: ccard_seq {
    type: number
    sql: ${TABLE}.ccard_seq ;;
  }

  dimension: ccard_state {
    type: string
    sql: ${TABLE}.ccard_state ;;
  }

  dimension: ccard_type {
    type: string
    sql: ${TABLE}.ccard_type ;;
  }

  dimension: ccard_year_expired {
    type: string
    sql: ${TABLE}.ccard_year_expired ;;
  }

  dimension: ccard_zip {
    type: string
    sql: ${TABLE}.ccard_zip ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [ccard_id, ccard_name, ccard_added_by_username, ccard_last_updated_by_username]
  }
}
