view: regtype {
  sql_table_name: public.regtype ;;

  dimension: regtype_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.regtype_id ;;
  }

  dimension: regtype_code {
    type: string
    sql: ${TABLE}.regtype_code ;;
  }

  dimension: regtype_descrip {
    type: string
    sql: ${TABLE}.regtype_descrip ;;
  }

  measure: count {
    type: count
    drill_fields: [regtype_id]
  }
}
