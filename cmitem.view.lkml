view: cmitem {
  sql_table_name: public.cmitem ;;

  dimension: cmitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.cmitem_id ;;
  }

  dimension: cmitem_cmhead_id {
    type: number
    sql: ${TABLE}.cmitem_cmhead_id ;;
  }

  dimension: cmitem_comments {
    type: string
    sql: ${TABLE}.cmitem_comments ;;
  }

  dimension: cmitem_itemsite_id {
    type: number
    sql: ${TABLE}.cmitem_itemsite_id ;;
  }

  dimension: cmitem_linenumber {
    type: number
    sql: ${TABLE}.cmitem_linenumber ;;
  }

  dimension: cmitem_price_invuomratio {
    type: number
    sql: ${TABLE}.cmitem_price_invuomratio ;;
  }

  dimension: cmitem_price_uom_id {
    type: number
    sql: ${TABLE}.cmitem_price_uom_id ;;
  }

  dimension: cmitem_qty_invuomratio {
    type: number
    sql: ${TABLE}.cmitem_qty_invuomratio ;;
  }

  dimension: cmitem_qty_uom_id {
    type: number
    sql: ${TABLE}.cmitem_qty_uom_id ;;
  }

  dimension: cmitem_qtycredit {
    type: number
    sql: ${TABLE}.cmitem_qtycredit ;;
  }

  dimension: cmitem_qtyreturned {
    type: number
    sql: ${TABLE}.cmitem_qtyreturned ;;
  }

  dimension: cmitem_raitem_id {
    type: number
    sql: ${TABLE}.cmitem_raitem_id ;;
  }

  dimension: cmitem_rsncode_id {
    type: number
    sql: ${TABLE}.cmitem_rsncode_id ;;
  }

  dimension: cmitem_taxtype_id {
    type: number
    sql: ${TABLE}.cmitem_taxtype_id ;;
  }

  dimension: cmitem_unitprice {
    type: number
    sql: ${TABLE}.cmitem_unitprice ;;
  }

  dimension: cmitem_updateinv {
    type: yesno
    sql: ${TABLE}.cmitem_updateinv ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [cmitem_id, creditmemoitem.count]
  }
}
