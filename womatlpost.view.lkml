view: womatlpost {
  sql_table_name: public.womatlpost ;;

  dimension: womatlpost_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.womatlpost_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: womatlpost_invhist_id {
    type: number
    sql: ${TABLE}.womatlpost_invhist_id ;;
  }

  dimension: womatlpost_womatl_id {
    type: number
    sql: ${TABLE}.womatlpost_womatl_id ;;
  }

  measure: count {
    type: count
    drill_fields: [womatlpost_id]
  }
}
