view: aropen {
  sql_table_name: public.aropen ;;

  dimension: aropen_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.aropen_id ;;
  }

  dimension: aropen_accnt_id {
    type: number
    sql: ${TABLE}.aropen_accnt_id ;;
  }

  dimension: aropen_amount {
    type: number
    sql: ${TABLE}.aropen_amount ;;
  }

  dimension: aropen_applyto {
    type: string
    sql: ${TABLE}.aropen_applyto ;;
  }

  dimension_group: aropen_closedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.aropen_closedate ;;
  }

  dimension: aropen_cobmisc_id {
    type: number
    sql: ${TABLE}.aropen_cobmisc_id ;;
  }

  dimension: aropen_commission_due {
    type: number
    sql: ${TABLE}.aropen_commission_due ;;
  }

  dimension: aropen_commission_paid {
    type: yesno
    sql: ${TABLE}.aropen_commission_paid ;;
  }

  dimension: aropen_curr_id {
    type: number
    sql: ${TABLE}.aropen_curr_id ;;
  }

  dimension: aropen_curr_rate {
    type: number
    sql: ${TABLE}.aropen_curr_rate ;;
  }

  dimension: aropen_cust_id {
    type: number
    sql: ${TABLE}.aropen_cust_id ;;
  }

  dimension: aropen_discount {
    type: yesno
    sql: ${TABLE}.aropen_discount ;;
  }

  dimension_group: aropen_distdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.aropen_distdate ;;
  }

  dimension_group: aropen_docdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.aropen_docdate ;;
  }

  dimension: aropen_docnumber {
    type: string
    sql: ${TABLE}.aropen_docnumber ;;
  }

  dimension: aropen_doctype {
    type: string
    sql: ${TABLE}.aropen_doctype ;;
  }

  dimension_group: aropen_duedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.aropen_duedate ;;
  }

  dimension: aropen_fincharg_amount {
    type: number
    sql: ${TABLE}.aropen_fincharg_amount ;;
  }

  dimension_group: aropen_fincharg {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.aropen_fincharg_date ;;
  }

  dimension: aropen_journalnumber {
    type: number
    sql: ${TABLE}.aropen_journalnumber ;;
  }

  dimension: aropen_notes {
    type: string
    sql: ${TABLE}.aropen_notes ;;
  }

  dimension: aropen_open {
    type: yesno
    sql: ${TABLE}.aropen_open ;;
  }

  dimension: aropen_ordernumber {
    type: string
    sql: ${TABLE}.aropen_ordernumber ;;
  }

  dimension: aropen_paid {
    type: number
    value_format_name: id
    sql: ${TABLE}.aropen_paid ;;
  }

  dimension: aropen_ponumber {
    type: string
    sql: ${TABLE}.aropen_ponumber ;;
  }

  dimension: aropen_posted {
    type: yesno
    sql: ${TABLE}.aropen_posted ;;
  }

  dimension: aropen_rsncode_id {
    type: number
    sql: ${TABLE}.aropen_rsncode_id ;;
  }

  dimension: aropen_salescat_id {
    type: number
    sql: ${TABLE}.aropen_salescat_id ;;
  }

  dimension: aropen_salesrep_id {
    type: number
    sql: ${TABLE}.aropen_salesrep_id ;;
  }

  dimension: aropen_terms_id {
    type: number
    sql: ${TABLE}.aropen_terms_id ;;
  }

  dimension: aropen_username {
    type: string
    sql: ${TABLE}.aropen_username ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [aropen_id, aropen_username, armemo.count]
  }
}
