view: form {
  sql_table_name: public.form ;;

  dimension: form_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.form_id ;;
  }

  dimension: form_descrip {
    type: string
    sql: ${TABLE}.form_descrip ;;
  }

  dimension: form_key {
    type: string
    sql: ${TABLE}.form_key ;;
  }

  dimension: form_name {
    type: string
    sql: ${TABLE}.form_name ;;
  }

  dimension: form_report_id {
    type: number
    sql: ${TABLE}.form_report_id ;;
  }

  dimension: form_report_name {
    type: string
    sql: ${TABLE}.form_report_name ;;
  }

  measure: count {
    type: count
    drill_fields: [form_id, form_report_name, form_name]
  }
}
