view: toitem {
  sql_table_name: public.toitem ;;

  dimension: toitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.toitem_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: toitem_close_username {
    type: string
    sql: ${TABLE}.toitem_close_username ;;
  }

  dimension_group: toitem_closedate {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.toitem_closedate ;;
  }

  dimension_group: toitem_created {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.toitem_created ;;
  }

  dimension: toitem_creator {
    type: string
    sql: ${TABLE}.toitem_creator ;;
  }

  dimension_group: toitem_duedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.toitem_duedate ;;
  }

  dimension: toitem_freight {
    type: number
    sql: ${TABLE}.toitem_freight ;;
  }

  dimension: toitem_freight_curr_id {
    type: number
    sql: ${TABLE}.toitem_freight_curr_id ;;
  }

  dimension: toitem_freight_received {
    type: number
    sql: ${TABLE}.toitem_freight_received ;;
  }

  dimension: toitem_item_id {
    type: number
    sql: ${TABLE}.toitem_item_id ;;
  }

  dimension_group: toitem_lastupdated {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.toitem_lastupdated ;;
  }

  dimension: toitem_linenumber {
    type: number
    sql: ${TABLE}.toitem_linenumber ;;
  }

  dimension: toitem_notes {
    type: string
    sql: ${TABLE}.toitem_notes ;;
  }

  dimension: toitem_prj_id {
    type: number
    sql: ${TABLE}.toitem_prj_id ;;
  }

  dimension: toitem_qty_ordered {
    type: number
    sql: ${TABLE}.toitem_qty_ordered ;;
  }

  dimension: toitem_qty_received {
    type: number
    sql: ${TABLE}.toitem_qty_received ;;
  }

  dimension: toitem_qty_shipped {
    type: number
    sql: ${TABLE}.toitem_qty_shipped ;;
  }

  dimension_group: toitem_schedrecvdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.toitem_schedrecvdate ;;
  }

  dimension_group: toitem_schedshipdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.toitem_schedshipdate ;;
  }

  dimension: toitem_status {
    type: string
    sql: ${TABLE}.toitem_status ;;
  }

  dimension: toitem_stdcost {
    type: number
    sql: ${TABLE}.toitem_stdcost ;;
  }

  dimension: toitem_tohead_id {
    type: number
    sql: ${TABLE}.toitem_tohead_id ;;
  }

  dimension: toitem_uom {
    type: string
    sql: ${TABLE}.toitem_uom ;;
  }

  measure: count {
    type: count
    drill_fields: [toitem_id, toitem_close_username]
  }
}
