view: shiphead {
  sql_table_name: public.shiphead ;;

  dimension: shiphead_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.shiphead_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: shiphead_freight {
    type: number
    sql: ${TABLE}.shiphead_freight ;;
  }

  dimension: shiphead_freight_curr_id {
    type: number
    sql: ${TABLE}.shiphead_freight_curr_id ;;
  }

  dimension: shiphead_notes {
    type: string
    sql: ${TABLE}.shiphead_notes ;;
  }

  dimension: shiphead_number {
    type: string
    sql: ${TABLE}.shiphead_number ;;
  }

  dimension: shiphead_order_id {
    type: number
    sql: ${TABLE}.shiphead_order_id ;;
  }

  dimension: shiphead_order_type {
    type: string
    sql: ${TABLE}.shiphead_order_type ;;
  }

  dimension: shiphead_sfstatus {
    type: string
    sql: ${TABLE}.shiphead_sfstatus ;;
  }

  dimension: shiphead_shipchrg_id {
    type: number
    sql: ${TABLE}.shiphead_shipchrg_id ;;
  }

  dimension_group: shiphead_shipdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.shiphead_shipdate ;;
  }

  dimension: shiphead_shipform_id {
    type: number
    sql: ${TABLE}.shiphead_shipform_id ;;
  }

  dimension: shiphead_shipped {
    type: yesno
    sql: ${TABLE}.shiphead_shipped ;;
  }

  dimension: shiphead_shipvia {
    type: string
    sql: ${TABLE}.shiphead_shipvia ;;
  }

  dimension: shiphead_tracknum {
    type: string
    sql: ${TABLE}.shiphead_tracknum ;;
  }

  measure: count {
    type: count
    drill_fields: [shiphead_id]
  }
}
