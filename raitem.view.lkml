view: raitem {
  sql_table_name: public.raitem ;;

  dimension: raitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.raitem_id ;;
  }

  dimension: raitem_amtcredited {
    type: number
    sql: ${TABLE}.raitem_amtcredited ;;
  }

  dimension: raitem_coitem_itemsite_id {
    type: number
    sql: ${TABLE}.raitem_coitem_itemsite_id ;;
  }

  dimension: raitem_cos_accnt_id {
    type: number
    sql: ${TABLE}.raitem_cos_accnt_id ;;
  }

  dimension: raitem_custpn {
    type: string
    sql: ${TABLE}.raitem_custpn ;;
  }

  dimension: raitem_disposition {
    type: string
    sql: ${TABLE}.raitem_disposition ;;
  }

  dimension: raitem_itemsite_id {
    type: number
    sql: ${TABLE}.raitem_itemsite_id ;;
  }

  dimension: raitem_linenumber {
    type: number
    sql: ${TABLE}.raitem_linenumber ;;
  }

  dimension: raitem_new_coitem_id {
    type: number
    sql: ${TABLE}.raitem_new_coitem_id ;;
  }

  dimension: raitem_notes {
    type: string
    sql: ${TABLE}.raitem_notes ;;
  }

  dimension: raitem_orig_coitem_id {
    type: number
    sql: ${TABLE}.raitem_orig_coitem_id ;;
  }

  dimension: raitem_price_invuomratio {
    type: number
    sql: ${TABLE}.raitem_price_invuomratio ;;
  }

  dimension: raitem_price_uom_id {
    type: number
    sql: ${TABLE}.raitem_price_uom_id ;;
  }

  dimension: raitem_qty_invuomratio {
    type: number
    sql: ${TABLE}.raitem_qty_invuomratio ;;
  }

  dimension: raitem_qty_uom_id {
    type: number
    sql: ${TABLE}.raitem_qty_uom_id ;;
  }

  dimension: raitem_qtyauthorized {
    type: number
    sql: ${TABLE}.raitem_qtyauthorized ;;
  }

  dimension: raitem_qtycredited {
    type: number
    sql: ${TABLE}.raitem_qtycredited ;;
  }

  dimension: raitem_qtyreceived {
    type: number
    sql: ${TABLE}.raitem_qtyreceived ;;
  }

  dimension: raitem_rahead_id {
    type: number
    sql: ${TABLE}.raitem_rahead_id ;;
  }

  dimension: raitem_rsncode_id {
    type: number
    sql: ${TABLE}.raitem_rsncode_id ;;
  }

  dimension: raitem_saleprice {
    type: number
    sql: ${TABLE}.raitem_saleprice ;;
  }

  dimension_group: raitem_scheddate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.raitem_scheddate ;;
  }

  dimension: raitem_status {
    type: string
    sql: ${TABLE}.raitem_status ;;
  }

  dimension: raitem_subnumber {
    type: number
    sql: ${TABLE}.raitem_subnumber ;;
  }

  dimension: raitem_taxtype_id {
    type: number
    sql: ${TABLE}.raitem_taxtype_id ;;
  }

  dimension: raitem_unitcost {
    type: number
    sql: ${TABLE}.raitem_unitcost ;;
  }

  dimension: raitem_unitprice {
    type: number
    sql: ${TABLE}.raitem_unitprice ;;
  }

  dimension: raitem_warranty {
    type: yesno
    sql: ${TABLE}.raitem_warranty ;;
  }

  measure: count {
    type: count
    drill_fields: [raitem_id]
  }
}
