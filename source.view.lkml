view: source {
  sql_table_name: public.source ;;

  dimension: source_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.source_id ;;
  }

  dimension: source_charass {
    type: string
    sql: ${TABLE}.source_charass ;;
  }

  dimension: source_create_priv {
    type: string
    sql: ${TABLE}.source_create_priv ;;
  }

  dimension_group: source_created {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.source_created ;;
  }

  dimension: source_desc_field {
    type: string
    sql: ${TABLE}.source_desc_field ;;
  }

  dimension: source_descrip {
    type: string
    sql: ${TABLE}.source_descrip ;;
  }

  dimension: source_docass {
    type: string
    sql: ${TABLE}.source_docass ;;
  }

  dimension: source_docass_num {
    type: number
    sql: ${TABLE}.source_docass_num ;;
  }

  dimension: source_joins {
    type: string
    sql: ${TABLE}.source_joins ;;
  }

  dimension: source_key_field {
    type: string
    sql: ${TABLE}.source_key_field ;;
  }

  dimension: source_key_param {
    type: string
    sql: ${TABLE}.source_key_param ;;
  }

  dimension_group: source_last_modified {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.source_last_modified ;;
  }

  dimension: source_module {
    type: string
    sql: ${TABLE}.source_module ;;
  }

  dimension: source_name {
    type: string
    sql: ${TABLE}.source_name ;;
  }

  dimension: source_name_field {
    type: string
    sql: ${TABLE}.source_name_field ;;
  }

  dimension: source_number_field {
    type: string
    sql: ${TABLE}.source_number_field ;;
  }

  dimension: source_table {
    type: string
    sql: ${TABLE}.source_table ;;
  }

  dimension: source_uiform_name {
    type: string
    sql: ${TABLE}.source_uiform_name ;;
  }

  dimension: source_widget {
    type: string
    sql: ${TABLE}.source_widget ;;
  }

  measure: count {
    type: count
    drill_fields: [source_id, source_name, source_uiform_name, docinfo.count]
  }
}
