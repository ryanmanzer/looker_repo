view: itemuomconv {
  sql_table_name: public.itemuomconv ;;

  dimension: itemuomconv_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.itemuomconv_id ;;
  }

  dimension: itemuomconv_active {
    type: yesno
    sql: ${TABLE}.itemuomconv_active ;;
  }

  dimension: itemuomconv_fractional {
    type: yesno
    sql: ${TABLE}.itemuomconv_fractional ;;
  }

  dimension: itemuomconv_from_uom_id {
    type: number
    sql: ${TABLE}.itemuomconv_from_uom_id ;;
  }

  dimension: itemuomconv_from_value {
    type: number
    sql: ${TABLE}.itemuomconv_from_value ;;
  }

  dimension: itemuomconv_item_id {
    type: number
    sql: ${TABLE}.itemuomconv_item_id ;;
  }

  dimension: itemuomconv_to_uom_id {
    type: number
    sql: ${TABLE}.itemuomconv_to_uom_id ;;
  }

  dimension: itemuomconv_to_value {
    type: number
    sql: ${TABLE}.itemuomconv_to_value ;;
  }

  measure: count {
    type: count
    drill_fields: [itemuomconv_id]
  }
}
