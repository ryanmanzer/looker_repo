view: bomhist {
  sql_table_name: public.bomhist ;;

  dimension: bomhist_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.bomhist_id ;;
  }

  dimension: bomhist_actunitcost {
    type: number
    sql: ${TABLE}.bomhist_actunitcost ;;
  }

  dimension: bomhist_char_id {
    type: number
    sql: ${TABLE}.bomhist_char_id ;;
  }

  dimension: bomhist_createwo {
    type: yesno
    sql: ${TABLE}.bomhist_createwo ;;
  }

  dimension_group: bomhist_effective {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.bomhist_effective ;;
  }

  dimension_group: bomhist_expires {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.bomhist_expires ;;
  }

  dimension: bomhist_issuemethod {
    type: string
    sql: ${TABLE}.bomhist_issuemethod ;;
  }

  dimension: bomhist_item_id {
    type: number
    sql: ${TABLE}.bomhist_item_id ;;
  }

  dimension: bomhist_item_type {
    type: string
    sql: ${TABLE}.bomhist_item_type ;;
  }

  dimension: bomhist_level {
    type: number
    sql: ${TABLE}.bomhist_level ;;
  }

  dimension: bomhist_notes {
    type: string
    sql: ${TABLE}.bomhist_notes ;;
  }

  dimension: bomhist_parent_id {
    type: number
    sql: ${TABLE}.bomhist_parent_id ;;
  }

  dimension: bomhist_parent_seqnumber {
    type: number
    sql: ${TABLE}.bomhist_parent_seqnumber ;;
  }

  dimension: bomhist_qtyfxd {
    type: number
    sql: ${TABLE}.bomhist_qtyfxd ;;
  }

  dimension: bomhist_qtyper {
    type: number
    sql: ${TABLE}.bomhist_qtyper ;;
  }

  dimension: bomhist_qtyreq {
    type: number
    sql: ${TABLE}.bomhist_qtyreq ;;
  }

  dimension: bomhist_ref {
    type: string
    sql: ${TABLE}.bomhist_ref ;;
  }

  dimension: bomhist_rev_id {
    type: number
    sql: ${TABLE}.bomhist_rev_id ;;
  }

  dimension: bomhist_scrap {
    type: number
    sql: ${TABLE}.bomhist_scrap ;;
  }

  dimension: bomhist_seq_id {
    type: number
    sql: ${TABLE}.bomhist_seq_id ;;
  }

  dimension: bomhist_seqnumber {
    type: number
    sql: ${TABLE}.bomhist_seqnumber ;;
  }

  dimension: bomhist_status {
    type: string
    sql: ${TABLE}.bomhist_status ;;
  }

  dimension: bomhist_stdunitcost {
    type: number
    sql: ${TABLE}.bomhist_stdunitcost ;;
  }

  dimension: bomhist_value {
    type: string
    sql: ${TABLE}.bomhist_value ;;
  }

  measure: count {
    type: count
    drill_fields: [bomhist_id]
  }
}
