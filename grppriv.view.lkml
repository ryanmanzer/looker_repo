view: grppriv {
  sql_table_name: public.grppriv ;;

  dimension: grppriv_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.grppriv_id ;;
  }

  dimension: grppriv_grp_id {
    type: number
    sql: ${TABLE}.grppriv_grp_id ;;
  }

  dimension: grppriv_priv_id {
    type: number
    sql: ${TABLE}.grppriv_priv_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [grppriv_id]
  }
}
