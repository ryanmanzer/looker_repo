view: evntlog {
  sql_table_name: public.evntlog ;;

  dimension: evntlog_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.evntlog_id ;;
  }

  dimension: evntlog_action {
    type: string
    sql: ${TABLE}.evntlog_action ;;
  }

  dimension_group: evntlog_dispatched {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.evntlog_dispatched ;;
  }

  dimension_group: evntlog_evnttime {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.evntlog_evnttime ;;
  }

  dimension: evntlog_evnttype_id {
    type: number
    sql: ${TABLE}.evntlog_evnttype_id ;;
  }

  dimension_group: evntlog_newdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.evntlog_newdate ;;
  }

  dimension: evntlog_newvalue {
    type: number
    sql: ${TABLE}.evntlog_newvalue ;;
  }

  dimension: evntlog_number {
    type: string
    sql: ${TABLE}.evntlog_number ;;
  }

  dimension_group: evntlog_olddate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.evntlog_olddate ;;
  }

  dimension: evntlog_oldvalue {
    type: number
    sql: ${TABLE}.evntlog_oldvalue ;;
  }

  dimension: evntlog_ord_id {
    type: number
    sql: ${TABLE}.evntlog_ord_id ;;
  }

  dimension: evntlog_ordtype {
    type: string
    sql: ${TABLE}.evntlog_ordtype ;;
  }

  dimension: evntlog_username {
    type: string
    sql: ${TABLE}.evntlog_username ;;
  }

  dimension: evntlog_warehous_id {
    type: number
    sql: ${TABLE}.evntlog_warehous_id ;;
  }

  measure: count {
    type: count
    drill_fields: [evntlog_id, evntlog_username]
  }
}
