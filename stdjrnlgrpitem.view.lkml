view: stdjrnlgrpitem {
  sql_table_name: public.stdjrnlgrpitem ;;

  dimension: stdjrnlgrpitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.stdjrnlgrpitem_id ;;
  }

  dimension: stdjrnlgrpitem_applied {
    type: number
    sql: ${TABLE}.stdjrnlgrpitem_applied ;;
  }

  dimension_group: stdjrnlgrpitem_effective {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.stdjrnlgrpitem_effective ;;
  }

  dimension_group: stdjrnlgrpitem_expires {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.stdjrnlgrpitem_expires ;;
  }

  dimension: stdjrnlgrpitem_stdjrnl_id {
    type: number
    sql: ${TABLE}.stdjrnlgrpitem_stdjrnl_id ;;
  }

  dimension: stdjrnlgrpitem_stdjrnlgrp_id {
    type: number
    sql: ${TABLE}.stdjrnlgrpitem_stdjrnlgrp_id ;;
  }

  dimension: stdjrnlgrpitem_toapply {
    type: number
    sql: ${TABLE}.stdjrnlgrpitem_toapply ;;
  }

  measure: count {
    type: count
    drill_fields: [stdjrnlgrpitem_id]
  }
}
