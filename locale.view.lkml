view: locale {
  sql_table_name: public.locale ;;

  dimension: locale_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.locale_id ;;
  }

  dimension: local_costformat {
    type: string
    sql: ${TABLE}.local_costformat ;;
  }

  dimension: locale_altemphasis_color {
    type: string
    sql: ${TABLE}.locale_altemphasis_color ;;
  }

  dimension: locale_code {
    type: string
    sql: ${TABLE}.locale_code ;;
  }

  dimension: locale_comments {
    type: string
    sql: ${TABLE}.locale_comments ;;
  }

  dimension: locale_cost_scale {
    type: number
    sql: ${TABLE}.locale_cost_scale ;;
  }

  dimension: locale_costformat {
    type: string
    sql: ${TABLE}.locale_costformat ;;
  }

  dimension: locale_country_id {
    type: number
    sql: ${TABLE}.locale_country_id ;;
  }

  dimension: locale_curr_scale {
    type: number
    sql: ${TABLE}.locale_curr_scale ;;
  }

  dimension: locale_currformat {
    type: string
    sql: ${TABLE}.locale_currformat ;;
  }

  dimension: locale_dateformat {
    type: string
    sql: ${TABLE}.locale_dateformat ;;
  }

  dimension: locale_descrip {
    type: string
    sql: ${TABLE}.locale_descrip ;;
  }

  dimension: locale_emphasis_color {
    type: string
    sql: ${TABLE}.locale_emphasis_color ;;
  }

  dimension: locale_error_color {
    type: string
    sql: ${TABLE}.locale_error_color ;;
  }

  dimension: locale_expired_color {
    type: string
    sql: ${TABLE}.locale_expired_color ;;
  }

  dimension: locale_extprice_scale {
    type: number
    sql: ${TABLE}.locale_extprice_scale ;;
  }

  dimension: locale_extpriceformat {
    type: string
    sql: ${TABLE}.locale_extpriceformat ;;
  }

  dimension: locale_future_color {
    type: string
    sql: ${TABLE}.locale_future_color ;;
  }

  dimension: locale_intervalformat {
    type: string
    sql: ${TABLE}.locale_intervalformat ;;
  }

  dimension: locale_lang_file {
    type: string
    sql: ${TABLE}.locale_lang_file ;;
  }

  dimension: locale_lang_id {
    type: number
    sql: ${TABLE}.locale_lang_id ;;
  }

  dimension: locale_percent_scale {
    type: number
    sql: ${TABLE}.locale_percent_scale ;;
  }

  dimension: locale_purchprice_scale {
    type: number
    sql: ${TABLE}.locale_purchprice_scale ;;
  }

  dimension: locale_purchpriceformat {
    type: string
    sql: ${TABLE}.locale_purchpriceformat ;;
  }

  dimension: locale_qty_scale {
    type: number
    sql: ${TABLE}.locale_qty_scale ;;
  }

  dimension: locale_qtyformat {
    type: string
    sql: ${TABLE}.locale_qtyformat ;;
  }

  dimension: locale_qtyper_scale {
    type: number
    sql: ${TABLE}.locale_qtyper_scale ;;
  }

  dimension: locale_qtyperformat {
    type: string
    sql: ${TABLE}.locale_qtyperformat ;;
  }

  dimension: locale_salesprice_scale {
    type: number
    sql: ${TABLE}.locale_salesprice_scale ;;
  }

  dimension: locale_salespriceformat {
    type: string
    sql: ${TABLE}.locale_salespriceformat ;;
  }

  dimension: locale_timeformat {
    type: string
    sql: ${TABLE}.locale_timeformat ;;
  }

  dimension: locale_timestampformat {
    type: string
    sql: ${TABLE}.locale_timestampformat ;;
  }

  dimension: locale_uomratio_scale {
    type: number
    sql: ${TABLE}.locale_uomratio_scale ;;
  }

  dimension: locale_uomratioformat {
    type: string
    sql: ${TABLE}.locale_uomratioformat ;;
  }

  dimension: locale_warning_color {
    type: string
    sql: ${TABLE}.locale_warning_color ;;
  }

  dimension: locale_weight_scale {
    type: number
    sql: ${TABLE}.locale_weight_scale ;;
  }

  measure: count {
    type: count
    drill_fields: [locale_id]
  }
}
