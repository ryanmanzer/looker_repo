view: empgrp {
  sql_table_name: public.empgrp ;;

  dimension: empgrp_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.empgrp_id ;;
  }

  dimension: empgrp_descrip {
    type: string
    sql: ${TABLE}.empgrp_descrip ;;
  }

  dimension: empgrp_name {
    type: string
    sql: ${TABLE}.empgrp_name ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [empgrp_id, empgrp_name]
  }
}
