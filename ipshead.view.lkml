view: ipshead {
  sql_table_name: public.ipshead ;;

  dimension: ipshead_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.ipshead_id ;;
  }

  dimension: ipshead_curr_id {
    type: number
    sql: ${TABLE}.ipshead_curr_id ;;
  }

  dimension: ipshead_descrip {
    type: string
    sql: ${TABLE}.ipshead_descrip ;;
  }

  dimension_group: ipshead_effective {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.ipshead_effective ;;
  }

  dimension_group: ipshead_expires {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.ipshead_expires ;;
  }

  dimension: ipshead_name {
    type: string
    sql: ${TABLE}.ipshead_name ;;
  }

  dimension_group: ipshead_updated {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.ipshead_updated ;;
  }

  measure: count {
    type: count
    drill_fields: [ipshead_id, ipshead_name]
  }
}
