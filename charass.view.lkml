view: charass {
  sql_table_name: public.charass ;;

  dimension: charass_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.charass_id ;;
  }

  dimension: charass_char_id {
    type: number
    sql: ${TABLE}.charass_char_id ;;
  }

  dimension: charass_default {
    type: yesno
    sql: ${TABLE}.charass_default ;;
  }

  dimension: charass_price {
    type: number
    sql: ${TABLE}.charass_price ;;
  }

  dimension: charass_target_id {
    type: number
    sql: ${TABLE}.charass_target_id ;;
  }

  dimension: charass_target_type {
    type: string
    sql: ${TABLE}.charass_target_type ;;
  }

  dimension: charass_value {
    type: string
    sql: ${TABLE}.charass_value ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [charass_id]
  }
}
