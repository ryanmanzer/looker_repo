view: poreject {
  sql_table_name: public.poreject ;;

  dimension: poreject_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.poreject_id ;;
  }

  dimension: poreject_agent_username {
    type: string
    sql: ${TABLE}.poreject_agent_username ;;
  }

  dimension_group: poreject {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.poreject_date ;;
  }

  dimension: poreject_invoiced {
    type: yesno
    sql: ${TABLE}.poreject_invoiced ;;
  }

  dimension: poreject_itemsite_id {
    type: number
    sql: ${TABLE}.poreject_itemsite_id ;;
  }

  dimension: poreject_poitem_id {
    type: number
    sql: ${TABLE}.poreject_poitem_id ;;
  }

  dimension: poreject_ponumber {
    type: string
    sql: ${TABLE}.poreject_ponumber ;;
  }

  dimension: poreject_posted {
    type: yesno
    sql: ${TABLE}.poreject_posted ;;
  }

  dimension: poreject_qty {
    type: number
    sql: ${TABLE}.poreject_qty ;;
  }

  dimension: poreject_recv_id {
    type: number
    sql: ${TABLE}.poreject_recv_id ;;
  }

  dimension: poreject_rjctcode_id {
    type: number
    sql: ${TABLE}.poreject_rjctcode_id ;;
  }

  dimension: poreject_trans_username {
    type: string
    sql: ${TABLE}.poreject_trans_username ;;
  }

  dimension: poreject_value {
    type: number
    sql: ${TABLE}.poreject_value ;;
  }

  dimension: poreject_vend_id {
    type: number
    sql: ${TABLE}.poreject_vend_id ;;
  }

  dimension: poreject_vend_item_descrip {
    type: string
    sql: ${TABLE}.poreject_vend_item_descrip ;;
  }

  dimension: poreject_vend_item_number {
    type: string
    sql: ${TABLE}.poreject_vend_item_number ;;
  }

  dimension: poreject_vend_uom {
    type: string
    sql: ${TABLE}.poreject_vend_uom ;;
  }

  dimension: poreject_vohead_id {
    type: number
    sql: ${TABLE}.poreject_vohead_id ;;
  }

  dimension: poreject_voitem_id {
    type: number
    sql: ${TABLE}.poreject_voitem_id ;;
  }

  measure: count {
    type: count
    drill_fields: [poreject_id, poreject_trans_username, poreject_agent_username]
  }
}
