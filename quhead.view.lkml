view: quhead {
  sql_table_name: public.quhead ;;

  dimension: quhead_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.quhead_id ;;
  }

  dimension: quhead_billto_cntct_email {
    type: string
    sql: ${TABLE}.quhead_billto_cntct_email ;;
  }

  dimension: quhead_billto_cntct_fax {
    type: string
    sql: ${TABLE}.quhead_billto_cntct_fax ;;
  }

  dimension: quhead_billto_cntct_first_name {
    type: string
    sql: ${TABLE}.quhead_billto_cntct_first_name ;;
  }

  dimension: quhead_billto_cntct_honorific {
    type: string
    sql: ${TABLE}.quhead_billto_cntct_honorific ;;
  }

  dimension: quhead_billto_cntct_id {
    type: number
    sql: ${TABLE}.quhead_billto_cntct_id ;;
  }

  dimension: quhead_billto_cntct_last_name {
    type: string
    sql: ${TABLE}.quhead_billto_cntct_last_name ;;
  }

  dimension: quhead_billto_cntct_middle {
    type: string
    sql: ${TABLE}.quhead_billto_cntct_middle ;;
  }

  dimension: quhead_billto_cntct_phone {
    type: string
    sql: ${TABLE}.quhead_billto_cntct_phone ;;
  }

  dimension: quhead_billto_cntct_suffix {
    type: string
    sql: ${TABLE}.quhead_billto_cntct_suffix ;;
  }

  dimension: quhead_billto_cntct_title {
    type: string
    sql: ${TABLE}.quhead_billto_cntct_title ;;
  }

  dimension: quhead_billtoaddress1 {
    type: string
    sql: ${TABLE}.quhead_billtoaddress1 ;;
  }

  dimension: quhead_billtoaddress2 {
    type: string
    sql: ${TABLE}.quhead_billtoaddress2 ;;
  }

  dimension: quhead_billtoaddress3 {
    type: string
    sql: ${TABLE}.quhead_billtoaddress3 ;;
  }

  dimension: quhead_billtocity {
    type: string
    sql: ${TABLE}.quhead_billtocity ;;
  }

  dimension: quhead_billtocountry {
    type: string
    sql: ${TABLE}.quhead_billtocountry ;;
  }

  dimension: quhead_billtoname {
    type: string
    sql: ${TABLE}.quhead_billtoname ;;
  }

  dimension: quhead_billtostate {
    type: string
    sql: ${TABLE}.quhead_billtostate ;;
  }

  dimension: quhead_billtozip {
    type: string
    sql: ${TABLE}.quhead_billtozip ;;
  }

  dimension: quhead_calcfreight {
    type: yesno
    sql: ${TABLE}.quhead_calcfreight ;;
  }

  dimension: quhead_commission {
    type: number
    sql: ${TABLE}.quhead_commission ;;
  }

  dimension: quhead_curr_id {
    type: number
    sql: ${TABLE}.quhead_curr_id ;;
  }

  dimension: quhead_cust_id {
    type: number
    sql: ${TABLE}.quhead_cust_id ;;
  }

  dimension: quhead_custponumber {
    type: string
    sql: ${TABLE}.quhead_custponumber ;;
  }

  dimension_group: quhead_expire {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.quhead_expire ;;
  }

  dimension: quhead_fob {
    type: string
    sql: ${TABLE}.quhead_fob ;;
  }

  dimension: quhead_freight {
    type: number
    sql: ${TABLE}.quhead_freight ;;
  }

  dimension: quhead_imported {
    type: yesno
    sql: ${TABLE}.quhead_imported ;;
  }

  dimension: quhead_misc {
    type: number
    sql: ${TABLE}.quhead_misc ;;
  }

  dimension: quhead_misc_accnt_id {
    type: number
    sql: ${TABLE}.quhead_misc_accnt_id ;;
  }

  dimension: quhead_misc_descrip {
    type: string
    sql: ${TABLE}.quhead_misc_descrip ;;
  }

  dimension: quhead_number {
    type: string
    sql: ${TABLE}.quhead_number ;;
  }

  dimension: quhead_ophead_id {
    type: number
    sql: ${TABLE}.quhead_ophead_id ;;
  }

  dimension: quhead_ordercomments {
    type: string
    sql: ${TABLE}.quhead_ordercomments ;;
  }

  dimension_group: quhead_packdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.quhead_packdate ;;
  }

  dimension: quhead_prj_id {
    type: number
    sql: ${TABLE}.quhead_prj_id ;;
  }

  dimension_group: quhead_quotedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.quhead_quotedate ;;
  }

  dimension: quhead_salesrep_id {
    type: number
    sql: ${TABLE}.quhead_salesrep_id ;;
  }

  dimension: quhead_saletype_id {
    type: number
    sql: ${TABLE}.quhead_saletype_id ;;
  }

  dimension: quhead_shipcomments {
    type: string
    sql: ${TABLE}.quhead_shipcomments ;;
  }

  dimension: quhead_shipto_cntct_email {
    type: string
    sql: ${TABLE}.quhead_shipto_cntct_email ;;
  }

  dimension: quhead_shipto_cntct_fax {
    type: string
    sql: ${TABLE}.quhead_shipto_cntct_fax ;;
  }

  dimension: quhead_shipto_cntct_first_name {
    type: string
    sql: ${TABLE}.quhead_shipto_cntct_first_name ;;
  }

  dimension: quhead_shipto_cntct_honorific {
    type: string
    sql: ${TABLE}.quhead_shipto_cntct_honorific ;;
  }

  dimension: quhead_shipto_cntct_id {
    type: number
    sql: ${TABLE}.quhead_shipto_cntct_id ;;
  }

  dimension: quhead_shipto_cntct_last_name {
    type: string
    sql: ${TABLE}.quhead_shipto_cntct_last_name ;;
  }

  dimension: quhead_shipto_cntct_middle {
    type: string
    sql: ${TABLE}.quhead_shipto_cntct_middle ;;
  }

  dimension: quhead_shipto_cntct_phone {
    type: string
    sql: ${TABLE}.quhead_shipto_cntct_phone ;;
  }

  dimension: quhead_shipto_cntct_suffix {
    type: string
    sql: ${TABLE}.quhead_shipto_cntct_suffix ;;
  }

  dimension: quhead_shipto_cntct_title {
    type: string
    sql: ${TABLE}.quhead_shipto_cntct_title ;;
  }

  dimension: quhead_shipto_id {
    type: number
    sql: ${TABLE}.quhead_shipto_id ;;
  }

  dimension: quhead_shiptoaddress1 {
    type: string
    sql: ${TABLE}.quhead_shiptoaddress1 ;;
  }

  dimension: quhead_shiptoaddress2 {
    type: string
    sql: ${TABLE}.quhead_shiptoaddress2 ;;
  }

  dimension: quhead_shiptoaddress3 {
    type: string
    sql: ${TABLE}.quhead_shiptoaddress3 ;;
  }

  dimension: quhead_shiptocity {
    type: string
    sql: ${TABLE}.quhead_shiptocity ;;
  }

  dimension: quhead_shiptocountry {
    type: string
    sql: ${TABLE}.quhead_shiptocountry ;;
  }

  dimension: quhead_shiptoname {
    type: string
    sql: ${TABLE}.quhead_shiptoname ;;
  }

  dimension: quhead_shiptophone {
    type: string
    sql: ${TABLE}.quhead_shiptophone ;;
  }

  dimension: quhead_shiptostate {
    type: string
    sql: ${TABLE}.quhead_shiptostate ;;
  }

  dimension: quhead_shiptozipcode {
    type: string
    sql: ${TABLE}.quhead_shiptozipcode ;;
  }

  dimension: quhead_shipvia {
    type: string
    sql: ${TABLE}.quhead_shipvia ;;
  }

  dimension: quhead_shipzone_id {
    type: number
    sql: ${TABLE}.quhead_shipzone_id ;;
  }

  dimension: quhead_status {
    type: string
    sql: ${TABLE}.quhead_status ;;
  }

  dimension: quhead_taxtype_id {
    type: number
    sql: ${TABLE}.quhead_taxtype_id ;;
  }

  dimension: quhead_taxzone_id {
    type: number
    sql: ${TABLE}.quhead_taxzone_id ;;
  }

  dimension: quhead_terms_id {
    type: number
    sql: ${TABLE}.quhead_terms_id ;;
  }

  dimension: quhead_warehous_id {
    type: number
    sql: ${TABLE}.quhead_warehous_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      quhead_id,
      quhead_shipto_cntct_first_name,
      quhead_shipto_cntct_last_name,
      quhead_billto_cntct_first_name,
      quhead_billto_cntct_last_name,
      quhead_shiptoname,
      quhead_billtoname
    ]
  }
}
