view: raitemls {
  sql_table_name: public.raitemls ;;

  dimension: raitemls_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.raitemls_id ;;
  }

  dimension: raitemls_ls_id {
    type: number
    sql: ${TABLE}.raitemls_ls_id ;;
  }

  dimension: raitemls_qtyauthorized {
    type: number
    sql: ${TABLE}.raitemls_qtyauthorized ;;
  }

  dimension: raitemls_qtyreceived {
    type: number
    sql: ${TABLE}.raitemls_qtyreceived ;;
  }

  dimension: raitemls_raitem_id {
    type: number
    sql: ${TABLE}.raitemls_raitem_id ;;
  }

  measure: count {
    type: count
    drill_fields: [raitemls_id]
  }
}
