view: stdjrnlgrp {
  sql_table_name: public.stdjrnlgrp ;;

  dimension: stdjrnlgrp_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.stdjrnlgrp_id ;;
  }

  dimension: stdjrnlgrp_descrip {
    type: string
    sql: ${TABLE}.stdjrnlgrp_descrip ;;
  }

  dimension: stdjrnlgrp_name {
    type: string
    sql: ${TABLE}.stdjrnlgrp_name ;;
  }

  measure: count {
    type: count
    drill_fields: [stdjrnlgrp_id, stdjrnlgrp_name]
  }
}
