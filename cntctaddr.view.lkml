view: cntctaddr {
  sql_table_name: public.cntctaddr ;;

  dimension: cntctaddr_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.cntctaddr_id ;;
  }

  dimension: cntctaddr_addr_id {
    type: number
    sql: ${TABLE}.cntctaddr_addr_id ;;
  }

  dimension: cntctaddr_cntct_id {
    type: number
    sql: ${TABLE}.cntctaddr_cntct_id ;;
  }

  dimension: cntctaddr_primary {
    type: yesno
    sql: ${TABLE}.cntctaddr_primary ;;
  }

  dimension: cntctaddr_type {
    type: string
    sql: ${TABLE}.cntctaddr_type ;;
  }

  measure: count {
    type: count
    drill_fields: [cntctaddr_id]
  }
}
