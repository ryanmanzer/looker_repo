view: salescat {
  sql_table_name: public.salescat ;;

  dimension: salescat_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.salescat_id ;;
  }

  dimension: salescat_active {
    type: yesno
    sql: ${TABLE}.salescat_active ;;
  }

  dimension: salescat_ar_accnt_id {
    type: number
    sql: ${TABLE}.salescat_ar_accnt_id ;;
  }

  dimension: salescat_descrip {
    type: string
    sql: ${TABLE}.salescat_descrip ;;
  }

  dimension: salescat_name {
    type: string
    sql: ${TABLE}.salescat_name ;;
  }

  dimension: salescat_prepaid_accnt_id {
    type: number
    sql: ${TABLE}.salescat_prepaid_accnt_id ;;
  }

  dimension: salescat_sales_accnt_id {
    type: number
    sql: ${TABLE}.salescat_sales_accnt_id ;;
  }

  measure: count {
    type: count
    drill_fields: [salescat_id, salescat_name]
  }
}
