view: bomhead {
  sql_table_name: public.bomhead ;;

  dimension: bomhead_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.bomhead_id ;;
  }

  dimension: bomhead_batchsize {
    type: number
    sql: ${TABLE}.bomhead_batchsize ;;
  }

  dimension: bomhead_docnum {
    type: string
    sql: ${TABLE}.bomhead_docnum ;;
  }

  dimension: bomhead_item_id {
    type: number
    sql: ${TABLE}.bomhead_item_id ;;
  }

  dimension: bomhead_requiredqtyper {
    type: number
    sql: ${TABLE}.bomhead_requiredqtyper ;;
  }

  dimension: bomhead_rev_id {
    type: number
    sql: ${TABLE}.bomhead_rev_id ;;
  }

  dimension: bomhead_revision {
    type: string
    sql: ${TABLE}.bomhead_revision ;;
  }

  dimension_group: bomhead_revisiondate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.bomhead_revisiondate ;;
  }

  dimension: bomhead_serial {
    type: number
    sql: ${TABLE}.bomhead_serial ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [bomhead_id]
  }
}
