view: arapply {
  sql_table_name: public.arapply ;;

  dimension: arapply_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.arapply_id ;;
  }

  dimension: arapply_applied {
    type: number
    sql: ${TABLE}.arapply_applied ;;
  }

  dimension: arapply_closed {
    type: yesno
    sql: ${TABLE}.arapply_closed ;;
  }

  dimension: arapply_curr_id {
    type: number
    sql: ${TABLE}.arapply_curr_id ;;
  }

  dimension: arapply_cust_id {
    type: number
    sql: ${TABLE}.arapply_cust_id ;;
  }

  dimension_group: arapply_distdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.arapply_distdate ;;
  }

  dimension: arapply_fundstype {
    type: string
    sql: ${TABLE}.arapply_fundstype ;;
  }

  dimension: arapply_journalnumber {
    type: string
    sql: ${TABLE}.arapply_journalnumber ;;
  }

  dimension_group: arapply_postdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.arapply_postdate ;;
  }

  dimension: arapply_ref_id {
    type: number
    sql: ${TABLE}.arapply_ref_id ;;
  }

  dimension: arapply_refnumber {
    type: string
    sql: ${TABLE}.arapply_refnumber ;;
  }

  dimension: arapply_reftype {
    type: string
    sql: ${TABLE}.arapply_reftype ;;
  }

  dimension: arapply_source_aropen_id {
    type: number
    sql: ${TABLE}.arapply_source_aropen_id ;;
  }

  dimension: arapply_source_docnumber {
    type: string
    sql: ${TABLE}.arapply_source_docnumber ;;
  }

  dimension: arapply_source_doctype {
    type: string
    sql: ${TABLE}.arapply_source_doctype ;;
  }

  dimension: arapply_target_aropen_id {
    type: number
    sql: ${TABLE}.arapply_target_aropen_id ;;
  }

  dimension: arapply_target_docnumber {
    type: string
    sql: ${TABLE}.arapply_target_docnumber ;;
  }

  dimension: arapply_target_doctype {
    type: string
    sql: ${TABLE}.arapply_target_doctype ;;
  }

  dimension: arapply_target_paid {
    type: number
    value_format_name: id
    sql: ${TABLE}.arapply_target_paid ;;
  }

  dimension: arapply_username {
    type: string
    sql: ${TABLE}.arapply_username ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [arapply_id, arapply_username]
  }
}
