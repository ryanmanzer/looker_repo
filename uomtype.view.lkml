view: uomtype {
  sql_table_name: public.uomtype ;;

  dimension: uomtype_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.uomtype_id ;;
  }

  dimension: uomtype_descrip {
    type: string
    sql: ${TABLE}.uomtype_descrip ;;
  }

  dimension: uomtype_multiple {
    type: yesno
    sql: ${TABLE}.uomtype_multiple ;;
  }

  dimension: uomtype_name {
    type: string
    sql: ${TABLE}.uomtype_name ;;
  }

  measure: count {
    type: count
    drill_fields: [uomtype_id, uomtype_name]
  }
}
