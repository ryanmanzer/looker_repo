view: incdtcat {
  sql_table_name: public.incdtcat ;;

  dimension: incdtcat_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.incdtcat_id ;;
  }

  dimension: incdtcat_descrip {
    type: string
    sql: ${TABLE}.incdtcat_descrip ;;
  }

  dimension: incdtcat_ediprofile_id {
    type: number
    sql: ${TABLE}.incdtcat_ediprofile_id ;;
  }

  dimension: incdtcat_name {
    type: string
    sql: ${TABLE}.incdtcat_name ;;
  }

  dimension: incdtcat_order {
    type: number
    sql: ${TABLE}.incdtcat_order ;;
  }

  measure: count {
    type: count
    drill_fields: [incdtcat_id, incdtcat_name]
  }
}
