view: incdtresolution {
  sql_table_name: public.incdtresolution ;;

  dimension: incdtresolution_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.incdtresolution_id ;;
  }

  dimension: incdtresolution_descrip {
    type: string
    sql: ${TABLE}.incdtresolution_descrip ;;
  }

  dimension: incdtresolution_name {
    type: string
    sql: ${TABLE}.incdtresolution_name ;;
  }

  dimension: incdtresolution_order {
    type: number
    sql: ${TABLE}.incdtresolution_order ;;
  }

  measure: count {
    type: count
    drill_fields: [incdtresolution_id, incdtresolution_name]
  }
}
