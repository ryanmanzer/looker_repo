view: womatlvar {
  sql_table_name: public.womatlvar ;;

  dimension: womatlvar_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.womatlvar_id ;;
  }

  dimension: womatlvar_bomitem_id {
    type: number
    sql: ${TABLE}.womatlvar_bomitem_id ;;
  }

  dimension: womatlvar_component_itemsite_id {
    type: number
    sql: ${TABLE}.womatlvar_component_itemsite_id ;;
  }

  dimension: womatlvar_notes {
    type: string
    sql: ${TABLE}.womatlvar_notes ;;
  }

  dimension: womatlvar_number {
    type: number
    sql: ${TABLE}.womatlvar_number ;;
  }

  dimension: womatlvar_parent_itemsite_id {
    type: number
    sql: ${TABLE}.womatlvar_parent_itemsite_id ;;
  }

  dimension_group: womatlvar_posted {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.womatlvar_posted ;;
  }

  dimension: womatlvar_qtyfxd {
    type: number
    sql: ${TABLE}.womatlvar_qtyfxd ;;
  }

  dimension: womatlvar_qtyiss {
    type: number
    sql: ${TABLE}.womatlvar_qtyiss ;;
  }

  dimension: womatlvar_qtyord {
    type: number
    sql: ${TABLE}.womatlvar_qtyord ;;
  }

  dimension: womatlvar_qtyper {
    type: number
    sql: ${TABLE}.womatlvar_qtyper ;;
  }

  dimension: womatlvar_qtyrcv {
    type: number
    sql: ${TABLE}.womatlvar_qtyrcv ;;
  }

  dimension: womatlvar_ref {
    type: string
    sql: ${TABLE}.womatlvar_ref ;;
  }

  dimension: womatlvar_scrap {
    type: number
    sql: ${TABLE}.womatlvar_scrap ;;
  }

  dimension: womatlvar_subnumber {
    type: number
    sql: ${TABLE}.womatlvar_subnumber ;;
  }

  dimension: womatlvar_wipscrap {
    type: number
    sql: ${TABLE}.womatlvar_wipscrap ;;
  }

  measure: count {
    type: count
    drill_fields: [womatlvar_id]
  }
}
