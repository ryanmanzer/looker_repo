view: mpsmrpwork {
  sql_table_name: public.mpsmrpwork ;;

  dimension: mpsmrpwork_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.mpsmrpwork_id ;;
  }

  dimension: mpsmrpwork_allocations {
    type: number
    sql: ${TABLE}.mpsmrpwork_allocations ;;
  }

  dimension: mpsmrpwork_availability {
    type: number
    sql: ${TABLE}.mpsmrpwork_availability ;;
  }

  dimension: mpsmrpwork_available {
    type: number
    sql: ${TABLE}.mpsmrpwork_available ;;
  }

  dimension_group: mpsmrpwork_enddate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.mpsmrpwork_enddate ;;
  }

  dimension: mpsmrpwork_firmed {
    type: number
    sql: ${TABLE}.mpsmrpwork_firmed ;;
  }

  dimension: mpsmrpwork_firmedavailability {
    type: number
    sql: ${TABLE}.mpsmrpwork_firmedavailability ;;
  }

  dimension: mpsmrpwork_order {
    type: number
    sql: ${TABLE}.mpsmrpwork_order ;;
  }

  dimension: mpsmrpwork_orders {
    type: number
    sql: ${TABLE}.mpsmrpwork_orders ;;
  }

  dimension: mpsmrpwork_planned {
    type: number
    sql: ${TABLE}.mpsmrpwork_planned ;;
  }

  dimension: mpsmrpwork_plannedavailability {
    type: number
    sql: ${TABLE}.mpsmrpwork_plannedavailability ;;
  }

  dimension: mpsmrpwork_qoh {
    type: number
    sql: ${TABLE}.mpsmrpwork_qoh ;;
  }

  dimension: mpsmrpwork_set_id {
    type: number
    sql: ${TABLE}.mpsmrpwork_set_id ;;
  }

  dimension_group: mpsmrpwork_startdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.mpsmrpwork_startdate ;;
  }

  measure: count {
    type: count
    drill_fields: [mpsmrpwork_id]
  }
}
