view: cmnttypesource {
  sql_table_name: public.cmnttypesource ;;

  dimension: cmnttypesource_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.cmnttypesource_id ;;
  }

  dimension: cmnttypesource_cmnttype_id {
    type: number
    sql: ${TABLE}.cmnttypesource_cmnttype_id ;;
  }

  dimension: cmnttypesource_source_id {
    type: number
    sql: ${TABLE}.cmnttypesource_source_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [cmnttypesource_id]
  }
}
