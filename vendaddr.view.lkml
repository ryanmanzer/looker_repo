view: vendaddr {
  sql_table_name: public.vendaddr ;;

  dimension: vendaddr_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.vendaddr_id ;;
  }

  dimension: vendaddr_address1 {
    type: string
    sql: ${TABLE}.vendaddr_address1 ;;
  }

  dimension: vendaddr_address2 {
    type: string
    sql: ${TABLE}.vendaddr_address2 ;;
  }

  dimension: vendaddr_address3 {
    type: string
    sql: ${TABLE}.vendaddr_address3 ;;
  }

  dimension: vendaddr_city {
    type: string
    sql: ${TABLE}.vendaddr_city ;;
  }

  dimension: vendaddr_code {
    type: string
    sql: ${TABLE}.vendaddr_code ;;
  }

  dimension: vendaddr_contact1 {
    type: string
    sql: ${TABLE}.vendaddr_contact1 ;;
  }

  dimension: vendaddr_country {
    type: string
    sql: ${TABLE}.vendaddr_country ;;
  }

  dimension: vendaddr_fax1 {
    type: string
    sql: ${TABLE}.vendaddr_fax1 ;;
  }

  dimension: vendaddr_name {
    type: string
    sql: ${TABLE}.vendaddr_name ;;
  }

  dimension: vendaddr_phone1 {
    type: string
    sql: ${TABLE}.vendaddr_phone1 ;;
  }

  dimension: vendaddr_state {
    type: string
    sql: ${TABLE}.vendaddr_state ;;
  }

  dimension: vendaddr_vend_id {
    type: number
    sql: ${TABLE}.vendaddr_vend_id ;;
  }

  dimension: vendaddr_zipcode {
    type: string
    sql: ${TABLE}.vendaddr_zipcode ;;
  }

  measure: count {
    type: count
    drill_fields: [vendaddr_id, vendaddr_name, vendaddrinfo.count]
  }
}
