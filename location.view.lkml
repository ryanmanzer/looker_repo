view: location {
  sql_table_name: public.location ;;

  dimension: location_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.location_id ;;
  }

  dimension: location_aisle {
    type: string
    sql: ${TABLE}.location_aisle ;;
  }

  dimension: location_bin {
    type: string
    sql: ${TABLE}.location_bin ;;
  }

  dimension: location_descrip {
    type: string
    sql: ${TABLE}.location_descrip ;;
  }

  dimension: location_formatname {
    type: string
    sql: ${TABLE}.location_formatname ;;
  }

  dimension: location_name {
    type: string
    sql: ${TABLE}.location_name ;;
  }

  dimension: location_netable {
    type: yesno
    sql: ${TABLE}.location_netable ;;
  }

  dimension: location_rack {
    type: string
    sql: ${TABLE}.location_rack ;;
  }

  dimension: location_restrict {
    type: yesno
    sql: ${TABLE}.location_restrict ;;
  }

  dimension: location_usable {
    type: yesno
    sql: ${TABLE}.location_usable ;;
  }

  dimension: location_warehous_id {
    type: number
    sql: ${TABLE}.location_warehous_id ;;
  }

  dimension: location_whsezone_id {
    type: number
    sql: ${TABLE}.location_whsezone_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [location_id, location_name, location_formatname]
  }
}
