view: evntnot {
  sql_table_name: public.evntnot ;;

  dimension: evntnot_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.evntnot_id ;;
  }

  dimension: evntnot_evnttype_id {
    type: number
    sql: ${TABLE}.evntnot_evnttype_id ;;
  }

  dimension: evntnot_username {
    type: string
    sql: ${TABLE}.evntnot_username ;;
  }

  dimension: evntnot_warehous_id {
    type: number
    sql: ${TABLE}.evntnot_warehous_id ;;
  }

  measure: count {
    type: count
    drill_fields: [evntnot_id, evntnot_username]
  }
}
