view: tax {
  sql_table_name: public.tax ;;

  dimension: tax_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.tax_id ;;
  }

  dimension: tax_basis_tax_id {
    type: number
    sql: ${TABLE}.tax_basis_tax_id ;;
  }

  dimension: tax_code {
    type: string
    sql: ${TABLE}.tax_code ;;
  }

  dimension: tax_descrip {
    type: string
    sql: ${TABLE}.tax_descrip ;;
  }

  dimension: tax_dist_accnt_id {
    type: number
    sql: ${TABLE}.tax_dist_accnt_id ;;
  }

  dimension: tax_sales_accnt_id {
    type: number
    sql: ${TABLE}.tax_sales_accnt_id ;;
  }

  dimension: tax_taxauth_id {
    type: number
    sql: ${TABLE}.tax_taxauth_id ;;
  }

  dimension: tax_taxclass_id {
    type: number
    sql: ${TABLE}.tax_taxclass_id ;;
  }

  measure: count {
    type: count
    drill_fields: [tax_id, obsolete_tax.count]
  }
}
