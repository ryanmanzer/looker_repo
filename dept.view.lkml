view: dept {
  sql_table_name: public.dept ;;

  dimension: dept_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.dept_id ;;
  }

  dimension: dept_name {
    type: string
    sql: ${TABLE}.dept_name ;;
  }

  dimension: dept_number {
    type: string
    sql: ${TABLE}.dept_number ;;
  }

  measure: count {
    type: count
    drill_fields: [dept_id, dept_name]
  }
}
