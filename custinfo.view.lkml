view: custinfo {
  sql_table_name: public.custinfo ;;

  dimension: cust_active {
    type: yesno
    sql: ${TABLE}.cust_active ;;
  }

  dimension: cust_autoholdorders {
    type: yesno
    sql: ${TABLE}.cust_autoholdorders ;;
  }

  dimension: cust_autoupdatestatus {
    type: yesno
    sql: ${TABLE}.cust_autoupdatestatus ;;
  }

  dimension: cust_backorder {
    type: yesno
    sql: ${TABLE}.cust_backorder ;;
  }

  dimension: cust_balmethod {
    type: string
    sql: ${TABLE}.cust_balmethod ;;
  }

  dimension: cust_blanketpos {
    type: yesno
    sql: ${TABLE}.cust_blanketpos ;;
  }

  dimension: cust_cntct_id {
    type: number
    sql: ${TABLE}.cust_cntct_id ;;
  }

  dimension: cust_comments {
    type: string
    sql: ${TABLE}.cust_comments ;;
  }

  dimension: cust_commprcnt {
    type: number
    sql: ${TABLE}.cust_commprcnt ;;
  }

  dimension: cust_corrcntct_id {
    type: number
    sql: ${TABLE}.cust_corrcntct_id ;;
  }

  dimension: cust_creditlmt {
    type: number
    sql: ${TABLE}.cust_creditlmt ;;
  }

  dimension: cust_creditlmt_curr_id {
    type: number
    sql: ${TABLE}.cust_creditlmt_curr_id ;;
  }

  dimension: cust_creditrating {
    type: string
    sql: ${TABLE}.cust_creditrating ;;
  }

  dimension: cust_creditstatus {
    type: string
    sql: ${TABLE}.cust_creditstatus ;;
  }

  dimension: cust_curr_id {
    type: number
    sql: ${TABLE}.cust_curr_id ;;
  }

  dimension: cust_custtype_id {
    type: number
    sql: ${TABLE}.cust_custtype_id ;;
  }

  dimension_group: cust_dateadded {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cust_dateadded ;;
  }

  dimension: cust_discntprcnt {
    type: number
    sql: ${TABLE}.cust_discntprcnt ;;
  }

  dimension: cust_edicc {
    type: string
    sql: ${TABLE}.cust_edicc ;;
  }

  dimension: cust_ediemail {
    type: string
    sql: ${TABLE}.cust_ediemail ;;
  }

  dimension: cust_ediemailbody {
    type: string
    sql: ${TABLE}.cust_ediemailbody ;;
  }

  dimension: cust_ediemailhtml {
    type: yesno
    sql: ${TABLE}.cust_ediemailhtml ;;
  }

  dimension: cust_edifilename {
    type: string
    sql: ${TABLE}.cust_edifilename ;;
  }

  dimension: cust_ediprofile_id {
    type: number
    sql: ${TABLE}.cust_ediprofile_id ;;
  }

  dimension: cust_edisubject {
    type: string
    sql: ${TABLE}.cust_edisubject ;;
  }

  dimension: cust_emaildelivery {
    type: yesno
    sql: ${TABLE}.cust_emaildelivery ;;
  }

  dimension: cust_exported {
    type: yesno
    sql: ${TABLE}.cust_exported ;;
  }

  dimension: cust_ffbillto {
    type: yesno
    sql: ${TABLE}.cust_ffbillto ;;
  }

  dimension: cust_ffshipto {
    type: yesno
    sql: ${TABLE}.cust_ffshipto ;;
  }

  dimension: cust_financecharge {
    type: yesno
    sql: ${TABLE}.cust_financecharge ;;
  }

  dimension: cust_gracedays {
    type: number
    sql: ${TABLE}.cust_gracedays ;;
  }

  dimension: cust_id {
    type: number
    # hidden: yes
    primary_key: yes
    sql: ${TABLE}.cust_id ;;
  }

  dimension: cust_name {
    type: string
    sql: ${TABLE}.cust_name ;;
  }

  dimension: cust_number {
    type: string
    sql: ${TABLE}.cust_number ;;
  }

  dimension: cust_partialship {
    type: yesno
    sql: ${TABLE}.cust_partialship ;;
  }

  dimension: cust_preferred_warehous_id {
    type: number
    sql: ${TABLE}.cust_preferred_warehous_id ;;
  }

  dimension: cust_salesrep_id {
    type: number
    sql: ${TABLE}.cust_salesrep_id ;;
  }

  dimension: cust_shipchrg_id {
    type: number
    sql: ${TABLE}.cust_shipchrg_id ;;
  }

  dimension: cust_shipform_id {
    type: number
    sql: ${TABLE}.cust_shipform_id ;;
  }

  dimension: cust_shipvia {
    type: string
    sql: ${TABLE}.cust_shipvia ;;
  }

  dimension: cust_soedicc {
    type: string
    sql: ${TABLE}.cust_soedicc ;;
  }

  dimension: cust_soediemail {
    type: string
    sql: ${TABLE}.cust_soediemail ;;
  }

  dimension: cust_soediemailbody {
    type: string
    sql: ${TABLE}.cust_soediemailbody ;;
  }

  dimension: cust_soediemailhtml {
    type: yesno
    sql: ${TABLE}.cust_soediemailhtml ;;
  }

  dimension: cust_soedifilename {
    type: string
    sql: ${TABLE}.cust_soedifilename ;;
  }

  dimension: cust_soediprofile_id {
    type: number
    sql: ${TABLE}.cust_soediprofile_id ;;
  }

  dimension: cust_soedisubject {
    type: string
    sql: ${TABLE}.cust_soedisubject ;;
  }

  dimension: cust_soemaildelivery {
    type: yesno
    sql: ${TABLE}.cust_soemaildelivery ;;
  }

  dimension: cust_statementcycle {
    type: string
    sql: ${TABLE}.cust_statementcycle ;;
  }

  dimension: cust_taxzone_id {
    type: number
    sql: ${TABLE}.cust_taxzone_id ;;
  }

  dimension: cust_terms_id {
    type: number
    sql: ${TABLE}.cust_terms_id ;;
  }

  dimension: cust_usespos {
    type: yesno
    sql: ${TABLE}.cust_usespos ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      cust_soedifilename,
      cust_name,
      cust_edifilename,
      cust.cust_id,
      cust.cust_name,
      cust.cust_edifilename
    ]
  }
}
