view: classcode {
  sql_table_name: public.classcode ;;

  dimension: classcode_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.classcode_id ;;
  }

  dimension: classcode_code {
    type: string
    sql: ${TABLE}.classcode_code ;;
  }

  dimension_group: classcode_created {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.classcode_created ;;
  }

  dimension: classcode_creator {
    type: string
    sql: ${TABLE}.classcode_creator ;;
  }

  dimension: classcode_descrip {
    type: string
    sql: ${TABLE}.classcode_descrip ;;
  }

  dimension: classcode_mfg {
    type: yesno
    sql: ${TABLE}.classcode_mfg ;;
  }

  dimension_group: classcode_modified {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.classcode_modified ;;
  }

  dimension: classcode_modifier {
    type: string
    sql: ${TABLE}.classcode_modifier ;;
  }

  dimension: classcode_type {
    type: string
    sql: ${TABLE}.classcode_type ;;
  }

  measure: count {
    type: count
    drill_fields: [classcode_id, saleshistory.count, saleshistorymisc.count]
  }
}
