view: comment {
  sql_table_name: public.comment ;;

  dimension: comment_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.comment_id ;;
  }

  dimension: comment_cmnttype_id {
    type: number
    sql: ${TABLE}.comment_cmnttype_id ;;
  }

  dimension_group: comment {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.comment_date ;;
  }

  dimension: comment_public {
    type: yesno
    sql: ${TABLE}.comment_public ;;
  }

  dimension: comment_source {
    type: string
    sql: ${TABLE}.comment_source ;;
  }

  dimension: comment_source_id {
    type: number
    sql: ${TABLE}.comment_source_id ;;
  }

  dimension: comment_text {
    type: string
    sql: ${TABLE}.comment_text ;;
  }

  dimension: comment_user {
    type: string
    sql: ${TABLE}.comment_user ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [comment_id]
  }
}
