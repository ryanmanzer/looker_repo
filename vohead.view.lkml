view: vohead {
  sql_table_name: public.vohead ;;

  dimension: vohead_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.vohead_id ;;
  }

  dimension: vohead_1099 {
    type: yesno
    sql: ${TABLE}.vohead_1099 ;;
  }

  dimension: vohead_adjtaxtype_id {
    type: number
    sql: ${TABLE}.vohead_adjtaxtype_id ;;
  }

  dimension: vohead_amount {
    type: number
    sql: ${TABLE}.vohead_amount ;;
  }

  dimension: vohead_curr_id {
    type: number
    sql: ${TABLE}.vohead_curr_id ;;
  }

  dimension_group: vohead_distdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.vohead_distdate ;;
  }

  dimension_group: vohead_docdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.vohead_docdate ;;
  }

  dimension_group: vohead_duedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.vohead_duedate ;;
  }

  dimension: vohead_freighttaxtype_id {
    type: number
    sql: ${TABLE}.vohead_freighttaxtype_id ;;
  }

  dimension_group: vohead_gldistdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.vohead_gldistdate ;;
  }

  dimension: vohead_invcnumber {
    type: string
    sql: ${TABLE}.vohead_invcnumber ;;
  }

  dimension: vohead_misc {
    type: yesno
    sql: ${TABLE}.vohead_misc ;;
  }

  dimension: vohead_notes {
    type: string
    sql: ${TABLE}.vohead_notes ;;
  }

  dimension: vohead_number {
    type: string
    sql: ${TABLE}.vohead_number ;;
  }

  dimension: vohead_pohead_id {
    type: number
    sql: ${TABLE}.vohead_pohead_id ;;
  }

  dimension: vohead_posted {
    type: yesno
    sql: ${TABLE}.vohead_posted ;;
  }

  dimension: vohead_recurring_vohead_id {
    type: number
    sql: ${TABLE}.vohead_recurring_vohead_id ;;
  }

  dimension: vohead_reference {
    type: string
    sql: ${TABLE}.vohead_reference ;;
  }

  dimension: vohead_taxtype_id {
    type: number
    sql: ${TABLE}.vohead_taxtype_id ;;
  }

  dimension: vohead_taxzone_id {
    type: number
    sql: ${TABLE}.vohead_taxzone_id ;;
  }

  dimension: vohead_terms_id {
    type: number
    sql: ${TABLE}.vohead_terms_id ;;
  }

  dimension: vohead_vend_id {
    type: number
    sql: ${TABLE}.vohead_vend_id ;;
  }

  measure: count {
    type: count
    drill_fields: [vohead_id]
  }
}
