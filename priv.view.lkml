view: priv {
  sql_table_name: public.priv ;;

  dimension: priv_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.priv_id ;;
  }

  dimension: priv_descrip {
    type: string
    sql: ${TABLE}.priv_descrip ;;
  }

  dimension: priv_module {
    type: string
    sql: ${TABLE}.priv_module ;;
  }

  dimension: priv_name {
    type: string
    sql: ${TABLE}.priv_name ;;
  }

  dimension: priv_seq {
    type: number
    sql: ${TABLE}.priv_seq ;;
  }

  measure: count {
    type: count
    drill_fields: [priv_id, priv_name]
  }
}
