view: recur {
  sql_table_name: public.recur ;;

  dimension: recur_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.recur_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: recur_data {
    type: string
    sql: ${TABLE}.recur_data ;;
  }

  dimension_group: recur_end {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.recur_end ;;
  }

  dimension: recur_freq {
    type: number
    sql: ${TABLE}.recur_freq ;;
  }

  dimension: recur_max {
    type: number
    sql: ${TABLE}.recur_max ;;
  }

  dimension: recur_parent_id {
    type: number
    sql: ${TABLE}.recur_parent_id ;;
  }

  dimension: recur_parent_type {
    type: string
    sql: ${TABLE}.recur_parent_type ;;
  }

  dimension: recur_period {
    type: string
    sql: ${TABLE}.recur_period ;;
  }

  dimension_group: recur_start {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.recur_start ;;
  }

  measure: count {
    type: count
    drill_fields: [recur_id]
  }
}
