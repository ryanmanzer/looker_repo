view: taxreg {
  sql_table_name: public.taxreg ;;

  dimension: taxreg_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.taxreg_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension_group: taxreg_effective {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.taxreg_effective ;;
  }

  dimension_group: taxreg_expires {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.taxreg_expires ;;
  }

  dimension: taxreg_notes {
    type: string
    sql: ${TABLE}.taxreg_notes ;;
  }

  dimension: taxreg_number {
    type: string
    sql: ${TABLE}.taxreg_number ;;
  }

  dimension: taxreg_rel_id {
    type: number
    sql: ${TABLE}.taxreg_rel_id ;;
  }

  dimension: taxreg_rel_type {
    type: string
    sql: ${TABLE}.taxreg_rel_type ;;
  }

  dimension: taxreg_taxauth_id {
    type: number
    sql: ${TABLE}.taxreg_taxauth_id ;;
  }

  dimension: taxreg_taxzone_id {
    type: number
    sql: ${TABLE}.taxreg_taxzone_id ;;
  }

  measure: count {
    type: count
    drill_fields: [taxreg_id]
  }
}
