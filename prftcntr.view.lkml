view: prftcntr {
  sql_table_name: public.prftcntr ;;

  dimension: prftcntr_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.prftcntr_id ;;
  }

  dimension: prftcntr_descrip {
    type: string
    sql: ${TABLE}.prftcntr_descrip ;;
  }

  dimension: prftcntr_number {
    type: string
    sql: ${TABLE}.prftcntr_number ;;
  }

  measure: count {
    type: count
    drill_fields: [prftcntr_id]
  }
}
