view: privgranted {
  sql_table_name: public.privgranted ;;

  dimension: granted {
    type: yesno
    sql: ${TABLE}.granted ;;
  }

  dimension: privilege {
    type: string
    sql: ${TABLE}.privilege ;;
  }

  dimension: sequence {
    type: number
    sql: ${TABLE}.sequence ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
