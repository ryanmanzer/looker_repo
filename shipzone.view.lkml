view: shipzone {
  sql_table_name: public.shipzone ;;

  dimension: shipzone_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.shipzone_id ;;
  }

  dimension: shipzone_descrip {
    type: string
    sql: ${TABLE}.shipzone_descrip ;;
  }

  dimension: shipzone_name {
    type: string
    sql: ${TABLE}.shipzone_name ;;
  }

  measure: count {
    type: count
    drill_fields: [shipzone_id, shipzone_name, saleshistory.count, saleshistorymisc.count]
  }
}
