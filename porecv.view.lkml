view: porecv {
  sql_table_name: public.porecv ;;

  dimension: porecv_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.porecv_id ;;
  }

  dimension: porecv_agent_username {
    type: string
    sql: ${TABLE}.porecv_agent_username ;;
  }

  dimension: porecv_curr_id {
    type: number
    sql: ${TABLE}.porecv_curr_id ;;
  }

  dimension_group: porecv {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.porecv_date ;;
  }

  dimension_group: porecv_duedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.porecv_duedate ;;
  }

  dimension: porecv_freight {
    type: number
    sql: ${TABLE}.porecv_freight ;;
  }

  dimension_group: porecv_gldistdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.porecv_gldistdate ;;
  }

  dimension: porecv_invoiced {
    type: yesno
    sql: ${TABLE}.porecv_invoiced ;;
  }

  dimension: porecv_item_number {
    type: string
    sql: ${TABLE}.porecv_item_number ;;
  }

  dimension: porecv_itemsite_id {
    type: number
    sql: ${TABLE}.porecv_itemsite_id ;;
  }

  dimension: porecv_linenumber {
    type: number
    sql: ${TABLE}.porecv_linenumber ;;
  }

  dimension: porecv_notes {
    type: string
    sql: ${TABLE}.porecv_notes ;;
  }

  dimension_group: porecv_orderdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.porecv_orderdate ;;
  }

  dimension: porecv_poitem_id {
    type: number
    sql: ${TABLE}.porecv_poitem_id ;;
  }

  dimension: porecv_ponumber {
    type: string
    sql: ${TABLE}.porecv_ponumber ;;
  }

  dimension: porecv_posted {
    type: yesno
    sql: ${TABLE}.porecv_posted ;;
  }

  dimension: porecv_purchcost {
    type: number
    sql: ${TABLE}.porecv_purchcost ;;
  }

  dimension: porecv_qty {
    type: number
    sql: ${TABLE}.porecv_qty ;;
  }

  dimension: porecv_recvcost {
    type: number
    sql: ${TABLE}.porecv_recvcost ;;
  }

  dimension_group: porecv_released {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.porecv_released ;;
  }

  dimension_group: porecv_rlsd_duedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.porecv_rlsd_duedate ;;
  }

  dimension: porecv_trans_usr_id {
    type: number
    sql: ${TABLE}.porecv_trans_usr_id ;;
  }

  dimension: porecv_value {
    type: number
    sql: ${TABLE}.porecv_value ;;
  }

  dimension: porecv_vend_id {
    type: number
    sql: ${TABLE}.porecv_vend_id ;;
  }

  dimension: porecv_vend_item_descrip {
    type: string
    sql: ${TABLE}.porecv_vend_item_descrip ;;
  }

  dimension: porecv_vend_item_number {
    type: string
    sql: ${TABLE}.porecv_vend_item_number ;;
  }

  dimension: porecv_vend_uom {
    type: string
    sql: ${TABLE}.porecv_vend_uom ;;
  }

  dimension: porecv_vohead_id {
    type: number
    sql: ${TABLE}.porecv_vohead_id ;;
  }

  dimension: porecv_voitem_id {
    type: number
    sql: ${TABLE}.porecv_voitem_id ;;
  }

  measure: count {
    type: count
    drill_fields: [porecv_id, porecv_agent_username]
  }
}
