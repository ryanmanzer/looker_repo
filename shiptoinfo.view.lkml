view: shiptoinfo {
  sql_table_name: public.shiptoinfo ;;

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: shipto_active {
    type: yesno
    sql: ${TABLE}.shipto_active ;;
  }

  dimension: shipto_addr_id {
    type: number
    sql: ${TABLE}.shipto_addr_id ;;
  }

  dimension: shipto_cntct_id {
    type: number
    sql: ${TABLE}.shipto_cntct_id ;;
  }

  dimension: shipto_comments {
    type: string
    sql: ${TABLE}.shipto_comments ;;
  }

  dimension: shipto_commission {
    type: number
    sql: ${TABLE}.shipto_commission ;;
  }

  dimension: shipto_cust_id {
    type: number
    sql: ${TABLE}.shipto_cust_id ;;
  }

  dimension: shipto_default {
    type: yesno
    sql: ${TABLE}.shipto_default ;;
  }

  dimension: shipto_ediprofile_id {
    type: number
    sql: ${TABLE}.shipto_ediprofile_id ;;
  }

  dimension: shipto_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.shipto_id ;;
  }

  dimension: shipto_name {
    type: string
    sql: ${TABLE}.shipto_name ;;
  }

  dimension: shipto_num {
    type: string
    sql: ${TABLE}.shipto_num ;;
  }

  dimension: shipto_preferred_warehous_id {
    type: number
    sql: ${TABLE}.shipto_preferred_warehous_id ;;
  }

  dimension: shipto_salesrep_id {
    type: number
    sql: ${TABLE}.shipto_salesrep_id ;;
  }

  dimension: shipto_shipchrg_id {
    type: number
    sql: ${TABLE}.shipto_shipchrg_id ;;
  }

  dimension: shipto_shipcomments {
    type: string
    sql: ${TABLE}.shipto_shipcomments ;;
  }

  dimension: shipto_shipform_id {
    type: number
    sql: ${TABLE}.shipto_shipform_id ;;
  }

  dimension: shipto_shipvia {
    type: string
    sql: ${TABLE}.shipto_shipvia ;;
  }

  dimension: shipto_shipzone_id {
    type: number
    sql: ${TABLE}.shipto_shipzone_id ;;
  }

  dimension: shipto_taxzone_id {
    type: number
    sql: ${TABLE}.shipto_taxzone_id ;;
  }

  measure: count {
    type: count
    drill_fields: [shipto_name, shipto.shipto_id, shipto.shipto_name]
  }
}
