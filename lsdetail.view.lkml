view: lsdetail {
  sql_table_name: public.lsdetail ;;

  dimension: lsdetail_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.lsdetail_id ;;
  }

  dimension_group: lsdetail_created {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.lsdetail_created ;;
  }

  dimension_group: lsdetail_expiration {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.lsdetail_expiration ;;
  }

  dimension: lsdetail_itemsite_id {
    type: number
    sql: ${TABLE}.lsdetail_itemsite_id ;;
  }

  dimension: lsdetail_ls_id {
    type: number
    sql: ${TABLE}.lsdetail_ls_id ;;
  }

  dimension: lsdetail_qtytoassign {
    type: number
    sql: ${TABLE}.lsdetail_qtytoassign ;;
  }

  dimension: lsdetail_source_id {
    type: number
    sql: ${TABLE}.lsdetail_source_id ;;
  }

  dimension: lsdetail_source_number {
    type: string
    sql: ${TABLE}.lsdetail_source_number ;;
  }

  dimension: lsdetail_source_type {
    type: string
    sql: ${TABLE}.lsdetail_source_type ;;
  }

  dimension_group: lsdetail_warrpurc {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.lsdetail_warrpurc ;;
  }

  measure: count {
    type: count
    drill_fields: [lsdetail_id]
  }
}
