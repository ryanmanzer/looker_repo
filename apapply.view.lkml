view: apapply {
  sql_table_name: public.apapply ;;

  dimension: apapply_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.apapply_id ;;
  }

  dimension: apapply_amount {
    type: number
    sql: ${TABLE}.apapply_amount ;;
  }

  dimension: apapply_checkhead_id {
    type: number
    sql: ${TABLE}.apapply_checkhead_id ;;
  }

  dimension: apapply_curr_id {
    type: number
    sql: ${TABLE}.apapply_curr_id ;;
  }

  dimension: apapply_journalnumber {
    type: number
    sql: ${TABLE}.apapply_journalnumber ;;
  }

  dimension_group: apapply_postdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.apapply_postdate ;;
  }

  dimension: apapply_source_apopen_id {
    type: number
    sql: ${TABLE}.apapply_source_apopen_id ;;
  }

  dimension: apapply_source_docnumber {
    type: string
    sql: ${TABLE}.apapply_source_docnumber ;;
  }

  dimension: apapply_source_doctype {
    type: string
    sql: ${TABLE}.apapply_source_doctype ;;
  }

  dimension: apapply_target_apopen_id {
    type: number
    sql: ${TABLE}.apapply_target_apopen_id ;;
  }

  dimension: apapply_target_docnumber {
    type: string
    sql: ${TABLE}.apapply_target_docnumber ;;
  }

  dimension: apapply_target_doctype {
    type: string
    sql: ${TABLE}.apapply_target_doctype ;;
  }

  dimension: apapply_target_paid {
    type: number
    value_format_name: id
    sql: ${TABLE}.apapply_target_paid ;;
  }

  dimension: apapply_username {
    type: string
    sql: ${TABLE}.apapply_username ;;
  }

  dimension: apapply_vend_id {
    type: number
    sql: ${TABLE}.apapply_vend_id ;;
  }

  measure: count {
    type: count
    drill_fields: [apapply_id, apapply_username]
  }
}
