view: tohead {
  sql_table_name: public.tohead ;;

  dimension: tohead_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.tohead_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: tohead_agent_username {
    type: string
    sql: ${TABLE}.tohead_agent_username ;;
  }

  dimension_group: tohead_created {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.tohead_created ;;
  }

  dimension: tohead_creator {
    type: string
    sql: ${TABLE}.tohead_creator ;;
  }

  dimension: tohead_dest_warehous_id {
    type: number
    sql: ${TABLE}.tohead_dest_warehous_id ;;
  }

  dimension: tohead_destaddress1 {
    type: string
    sql: ${TABLE}.tohead_destaddress1 ;;
  }

  dimension: tohead_destaddress2 {
    type: string
    sql: ${TABLE}.tohead_destaddress2 ;;
  }

  dimension: tohead_destaddress3 {
    type: string
    sql: ${TABLE}.tohead_destaddress3 ;;
  }

  dimension: tohead_destcity {
    type: string
    sql: ${TABLE}.tohead_destcity ;;
  }

  dimension: tohead_destcntct_id {
    type: number
    sql: ${TABLE}.tohead_destcntct_id ;;
  }

  dimension: tohead_destcntct_name {
    type: string
    sql: ${TABLE}.tohead_destcntct_name ;;
  }

  dimension: tohead_destcountry {
    type: string
    sql: ${TABLE}.tohead_destcountry ;;
  }

  dimension: tohead_destname {
    type: string
    sql: ${TABLE}.tohead_destname ;;
  }

  dimension: tohead_destphone {
    type: string
    sql: ${TABLE}.tohead_destphone ;;
  }

  dimension: tohead_destpostalcode {
    type: string
    sql: ${TABLE}.tohead_destpostalcode ;;
  }

  dimension: tohead_deststate {
    type: string
    sql: ${TABLE}.tohead_deststate ;;
  }

  dimension: tohead_freight {
    type: number
    sql: ${TABLE}.tohead_freight ;;
  }

  dimension: tohead_freight_curr_id {
    type: number
    sql: ${TABLE}.tohead_freight_curr_id ;;
  }

  dimension_group: tohead_lastupdated {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.tohead_lastupdated ;;
  }

  dimension: tohead_number {
    type: string
    sql: ${TABLE}.tohead_number ;;
  }

  dimension: tohead_ordercomments {
    type: string
    sql: ${TABLE}.tohead_ordercomments ;;
  }

  dimension_group: tohead_orderdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.tohead_orderdate ;;
  }

  dimension_group: tohead_packdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.tohead_packdate ;;
  }

  dimension: tohead_prj_id {
    type: number
    sql: ${TABLE}.tohead_prj_id ;;
  }

  dimension: tohead_shipchrg_id {
    type: number
    sql: ${TABLE}.tohead_shipchrg_id ;;
  }

  dimension: tohead_shipcomments {
    type: string
    sql: ${TABLE}.tohead_shipcomments ;;
  }

  dimension: tohead_shipcomplete {
    type: yesno
    sql: ${TABLE}.tohead_shipcomplete ;;
  }

  dimension: tohead_shipform_id {
    type: number
    sql: ${TABLE}.tohead_shipform_id ;;
  }

  dimension: tohead_shipvia {
    type: string
    sql: ${TABLE}.tohead_shipvia ;;
  }

  dimension: tohead_src_warehous_id {
    type: number
    sql: ${TABLE}.tohead_src_warehous_id ;;
  }

  dimension: tohead_srcaddress1 {
    type: string
    sql: ${TABLE}.tohead_srcaddress1 ;;
  }

  dimension: tohead_srcaddress2 {
    type: string
    sql: ${TABLE}.tohead_srcaddress2 ;;
  }

  dimension: tohead_srcaddress3 {
    type: string
    sql: ${TABLE}.tohead_srcaddress3 ;;
  }

  dimension: tohead_srccity {
    type: string
    sql: ${TABLE}.tohead_srccity ;;
  }

  dimension: tohead_srccntct_id {
    type: number
    sql: ${TABLE}.tohead_srccntct_id ;;
  }

  dimension: tohead_srccntct_name {
    type: string
    sql: ${TABLE}.tohead_srccntct_name ;;
  }

  dimension: tohead_srccountry {
    type: string
    sql: ${TABLE}.tohead_srccountry ;;
  }

  dimension: tohead_srcname {
    type: string
    sql: ${TABLE}.tohead_srcname ;;
  }

  dimension: tohead_srcphone {
    type: string
    sql: ${TABLE}.tohead_srcphone ;;
  }

  dimension: tohead_srcpostalcode {
    type: string
    sql: ${TABLE}.tohead_srcpostalcode ;;
  }

  dimension: tohead_srcstate {
    type: string
    sql: ${TABLE}.tohead_srcstate ;;
  }

  dimension: tohead_status {
    type: string
    sql: ${TABLE}.tohead_status ;;
  }

  dimension: tohead_taxzone_id {
    type: number
    sql: ${TABLE}.tohead_taxzone_id ;;
  }

  dimension: tohead_trns_warehous_id {
    type: number
    sql: ${TABLE}.tohead_trns_warehous_id ;;
  }

  dimension: tohead_trnsname {
    type: string
    sql: ${TABLE}.tohead_trnsname ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      tohead_id,
      tohead_srcname,
      tohead_srccntct_name,
      tohead_trnsname,
      tohead_destname,
      tohead_destcntct_name,
      tohead_agent_username
    ]
  }
}
