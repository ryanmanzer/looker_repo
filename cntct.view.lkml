view: cntct {
  sql_table_name: public.cntct ;;

  dimension: cntct_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.cntct_id ;;
  }

  dimension: cntct_active {
    type: yesno
    sql: ${TABLE}.cntct_active ;;
  }

  dimension: cntct_addr_id {
    type: number
    sql: ${TABLE}.cntct_addr_id ;;
  }

  dimension: cntct_crmacct_id {
    type: number
    sql: ${TABLE}.cntct_crmacct_id ;;
  }

  dimension: cntct_email {
    type: string
    sql: ${TABLE}.cntct_email ;;
  }

  dimension: cntct_fax {
    type: string
    sql: ${TABLE}.cntct_fax ;;
  }

  dimension: cntct_first_name {
    type: string
    sql: ${TABLE}.cntct_first_name ;;
  }

  dimension: cntct_honorific {
    type: string
    sql: ${TABLE}.cntct_honorific ;;
  }

  dimension: cntct_initials {
    type: string
    sql: ${TABLE}.cntct_initials ;;
  }

  dimension: cntct_last_name {
    type: string
    sql: ${TABLE}.cntct_last_name ;;
  }

  dimension: cntct_middle {
    type: string
    sql: ${TABLE}.cntct_middle ;;
  }

  dimension: cntct_name {
    type: string
    sql: ${TABLE}.cntct_name ;;
  }

  dimension: cntct_notes {
    type: string
    sql: ${TABLE}.cntct_notes ;;
  }

  dimension: cntct_number {
    type: string
    sql: ${TABLE}.cntct_number ;;
  }

  dimension: cntct_owner_username {
    type: string
    sql: ${TABLE}.cntct_owner_username ;;
  }

  dimension: cntct_phone {
    type: string
    sql: ${TABLE}.cntct_phone ;;
  }

  dimension: cntct_phone2 {
    type: string
    sql: ${TABLE}.cntct_phone2 ;;
  }

  dimension: cntct_suffix {
    type: string
    sql: ${TABLE}.cntct_suffix ;;
  }

  dimension: cntct_title {
    type: string
    sql: ${TABLE}.cntct_title ;;
  }

  dimension: cntct_webaddr {
    type: string
    sql: ${TABLE}.cntct_webaddr ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [cntct_id, cntct_owner_username, cntct_first_name, cntct_last_name, cntct_name]
  }
}
