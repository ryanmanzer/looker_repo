view: costupdate {
  sql_table_name: public.costupdate ;;

  dimension: costupdate_item_id {
    type: number
    sql: ${TABLE}.costupdate_item_id ;;
  }

  dimension: costupdate_item_type {
    type: string
    sql: ${TABLE}.costupdate_item_type ;;
  }

  dimension: costupdate_lowlevel_code {
    type: number
    sql: ${TABLE}.costupdate_lowlevel_code ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
