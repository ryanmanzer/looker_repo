view: cntctdata {
  sql_table_name: public.cntctdata ;;

  dimension: cntctdata_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.cntctdata_id ;;
  }

  dimension: cntctdata_cntct_id {
    type: number
    sql: ${TABLE}.cntctdata_cntct_id ;;
  }

  dimension: cntctdata_primary {
    type: yesno
    sql: ${TABLE}.cntctdata_primary ;;
  }

  dimension: cntctdata_text {
    type: string
    sql: ${TABLE}.cntctdata_text ;;
  }

  dimension: cntctdata_type {
    type: string
    sql: ${TABLE}.cntctdata_type ;;
  }

  measure: count {
    type: count
    drill_fields: [cntctdata_id]
  }
}
