view: sltrans_backup {
  sql_table_name: public.sltrans_backup ;;

  dimension: sltrans_new_id {
    type: number
    sql: ${TABLE}.sltrans_new_id ;;
  }

  dimension: sltrans_old_id {
    type: number
    sql: ${TABLE}.sltrans_old_id ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
