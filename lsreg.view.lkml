view: lsreg {
  sql_table_name: public.lsreg ;;

  dimension: lsreg_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.lsreg_id ;;
  }

  dimension: lsreg_cntct_id {
    type: number
    sql: ${TABLE}.lsreg_cntct_id ;;
  }

  dimension: lsreg_cohead_id {
    type: number
    sql: ${TABLE}.lsreg_cohead_id ;;
  }

  dimension: lsreg_crmacct_id {
    type: number
    sql: ${TABLE}.lsreg_crmacct_id ;;
  }

  dimension_group: lsreg_expiredate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.lsreg_expiredate ;;
  }

  dimension: lsreg_ls_id {
    type: number
    sql: ${TABLE}.lsreg_ls_id ;;
  }

  dimension: lsreg_notes {
    type: string
    sql: ${TABLE}.lsreg_notes ;;
  }

  dimension: lsreg_number {
    type: string
    sql: ${TABLE}.lsreg_number ;;
  }

  dimension: lsreg_qty {
    type: number
    sql: ${TABLE}.lsreg_qty ;;
  }

  dimension_group: lsreg_regdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.lsreg_regdate ;;
  }

  dimension: lsreg_regtype_id {
    type: number
    sql: ${TABLE}.lsreg_regtype_id ;;
  }

  dimension: lsreg_shiphead_id {
    type: number
    sql: ${TABLE}.lsreg_shiphead_id ;;
  }

  dimension_group: lsreg_solddate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.lsreg_solddate ;;
  }

  measure: count {
    type: count
    drill_fields: [lsreg_id]
  }
}
