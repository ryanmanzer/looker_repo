view: flspec {
  sql_table_name: public.flspec ;;

  dimension: flspec_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.flspec_id ;;
  }

  dimension: flspec_custom_source {
    type: string
    sql: ${TABLE}.flspec_custom_source ;;
  }

  dimension: flspec_flgrp_id {
    type: number
    sql: ${TABLE}.flspec_flgrp_id ;;
  }

  dimension: flspec_flhead_id {
    type: number
    sql: ${TABLE}.flspec_flhead_id ;;
  }

  dimension: flspec_name {
    type: string
    sql: ${TABLE}.flspec_name ;;
  }

  dimension: flspec_order {
    type: number
    sql: ${TABLE}.flspec_order ;;
  }

  dimension: flspec_prcnt_flgrp_id {
    type: number
    sql: ${TABLE}.flspec_prcnt_flgrp_id ;;
  }

  dimension: flspec_showbudget {
    type: yesno
    sql: ${TABLE}.flspec_showbudget ;;
  }

  dimension: flspec_showbudgetprcnt {
    type: yesno
    sql: ${TABLE}.flspec_showbudgetprcnt ;;
  }

  dimension: flspec_showcustom {
    type: yesno
    sql: ${TABLE}.flspec_showcustom ;;
  }

  dimension: flspec_showcustomprcnt {
    type: yesno
    sql: ${TABLE}.flspec_showcustomprcnt ;;
  }

  dimension: flspec_showdelta {
    type: yesno
    sql: ${TABLE}.flspec_showdelta ;;
  }

  dimension: flspec_showdeltaprcnt {
    type: yesno
    sql: ${TABLE}.flspec_showdeltaprcnt ;;
  }

  dimension: flspec_showdiff {
    type: yesno
    sql: ${TABLE}.flspec_showdiff ;;
  }

  dimension: flspec_showdiffprcnt {
    type: yesno
    sql: ${TABLE}.flspec_showdiffprcnt ;;
  }

  dimension: flspec_showend {
    type: yesno
    sql: ${TABLE}.flspec_showend ;;
  }

  dimension: flspec_showendprcnt {
    type: yesno
    sql: ${TABLE}.flspec_showendprcnt ;;
  }

  dimension: flspec_showstart {
    type: yesno
    sql: ${TABLE}.flspec_showstart ;;
  }

  dimension: flspec_showstartprcnt {
    type: yesno
    sql: ${TABLE}.flspec_showstartprcnt ;;
  }

  dimension: flspec_subtract {
    type: yesno
    sql: ${TABLE}.flspec_subtract ;;
  }

  dimension: flspec_type {
    type: string
    sql: ${TABLE}.flspec_type ;;
  }

  measure: count {
    type: count
    drill_fields: [flspec_id, flspec_name]
  }
}
