view: vendaddrinfo {
  sql_table_name: public.vendaddrinfo ;;

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: vendaddr_addr_id {
    type: number
    sql: ${TABLE}.vendaddr_addr_id ;;
  }

  dimension: vendaddr_cntct_id {
    type: number
    sql: ${TABLE}.vendaddr_cntct_id ;;
  }

  dimension: vendaddr_code {
    type: string
    sql: ${TABLE}.vendaddr_code ;;
  }

  dimension: vendaddr_comments {
    type: string
    sql: ${TABLE}.vendaddr_comments ;;
  }

  dimension: vendaddr_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.vendaddr_id ;;
  }

  dimension: vendaddr_name {
    type: string
    sql: ${TABLE}.vendaddr_name ;;
  }

  dimension: vendaddr_taxzone_id {
    type: number
    sql: ${TABLE}.vendaddr_taxzone_id ;;
  }

  dimension: vendaddr_vend_id {
    type: number
    sql: ${TABLE}.vendaddr_vend_id ;;
  }

  measure: count {
    type: count
    drill_fields: [vendaddr_name, vendaddr.vendaddr_id, vendaddr.vendaddr_name]
  }
}
