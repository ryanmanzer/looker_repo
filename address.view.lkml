view: address {
  sql_table_name: public.address ;;

  dimension: addr_active {
    type: yesno
    sql: ${TABLE}.addr_active ;;
  }

  dimension: addr_city {
    type: string
    sql: ${TABLE}.addr_city ;;
  }

  dimension: addr_country {
    type: string
    sql: ${TABLE}.addr_country ;;
  }

  dimension: addr_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.addr_id ;;
  }

  dimension: addr_line1 {
    type: string
    sql: ${TABLE}.addr_line1 ;;
  }

  dimension: addr_line2 {
    type: string
    sql: ${TABLE}.addr_line2 ;;
  }

  dimension: addr_line3 {
    type: string
    sql: ${TABLE}.addr_line3 ;;
  }

  dimension: addr_notes {
    type: string
    sql: ${TABLE}.addr_notes ;;
  }

  dimension: addr_number {
    type: string
    sql: ${TABLE}.addr_number ;;
  }

  dimension: addr_postalcode {
    type: string
    sql: ${TABLE}.addr_postalcode ;;
  }

  dimension: addr_state {
    type: string
    sql: ${TABLE}.addr_state ;;
  }

  dimension: crmacct_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.crmacct_id ;;
  }

  dimension: crmacct_name {
    type: string
    sql: ${TABLE}.crmacct_name ;;
  }

  dimension: crmacct_number {
    type: string
    sql: ${TABLE}.crmacct_number ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      crmacct_name,
      crmacct.crmacct_usr_username,
      crmacct.crmacct_id,
      crmacct.crmacct_name,
      crmacct.crmacct_owner_username,
      addr.addr_id
    ]
  }
}
