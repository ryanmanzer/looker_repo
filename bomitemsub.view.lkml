view: bomitemsub {
  sql_table_name: public.bomitemsub ;;

  dimension: bomitemsub_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.bomitemsub_id ;;
  }

  dimension: bomitemsub_bomitem_id {
    type: number
    sql: ${TABLE}.bomitemsub_bomitem_id ;;
  }

  dimension: bomitemsub_item_id {
    type: number
    sql: ${TABLE}.bomitemsub_item_id ;;
  }

  dimension: bomitemsub_rank {
    type: number
    sql: ${TABLE}.bomitemsub_rank ;;
  }

  dimension: bomitemsub_uomratio {
    type: number
    sql: ${TABLE}.bomitemsub_uomratio ;;
  }

  measure: count {
    type: count
    drill_fields: [bomitemsub_id]
  }
}
