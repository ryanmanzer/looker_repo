view: creditmemoitem {
  sql_table_name: public.creditmemoitem ;;

  dimension: baseextprice {
    type: number
    sql: ${TABLE}.baseextprice ;;
  }

  dimension: cmitem_cmhead_id {
    type: number
    sql: ${TABLE}.cmitem_cmhead_id ;;
  }

  dimension: cmitem_comments {
    type: string
    sql: ${TABLE}.cmitem_comments ;;
  }

  dimension: cmitem_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.cmitem_id ;;
  }

  dimension: cmitem_itemsite_id {
    type: number
    sql: ${TABLE}.cmitem_itemsite_id ;;
  }

  dimension: cmitem_linenumber {
    type: number
    sql: ${TABLE}.cmitem_linenumber ;;
  }

  dimension: cmitem_price_invuomratio {
    type: number
    sql: ${TABLE}.cmitem_price_invuomratio ;;
  }

  dimension: cmitem_price_uom_id {
    type: number
    sql: ${TABLE}.cmitem_price_uom_id ;;
  }

  dimension: cmitem_qty_invuomratio {
    type: number
    sql: ${TABLE}.cmitem_qty_invuomratio ;;
  }

  dimension: cmitem_qty_uom_id {
    type: number
    sql: ${TABLE}.cmitem_qty_uom_id ;;
  }

  dimension: cmitem_qtycredit {
    type: number
    sql: ${TABLE}.cmitem_qtycredit ;;
  }

  dimension: cmitem_qtyreturned {
    type: number
    sql: ${TABLE}.cmitem_qtyreturned ;;
  }

  dimension: cmitem_raitem_id {
    type: number
    sql: ${TABLE}.cmitem_raitem_id ;;
  }

  dimension: cmitem_rsncode_id {
    type: number
    sql: ${TABLE}.cmitem_rsncode_id ;;
  }

  dimension: cmitem_taxtype_id {
    type: number
    sql: ${TABLE}.cmitem_taxtype_id ;;
  }

  dimension: cmitem_unitprice {
    type: number
    sql: ${TABLE}.cmitem_unitprice ;;
  }

  dimension: cmitem_updateinv {
    type: yesno
    sql: ${TABLE}.cmitem_updateinv ;;
  }

  dimension: extprice {
    type: number
    sql: ${TABLE}.extprice ;;
  }

  dimension: item_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.item_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: qty {
    type: number
    sql: ${TABLE}.qty ;;
  }

  dimension: tax {
    type: number
    sql: ${TABLE}.tax ;;
  }

  dimension: unitcost {
    type: number
    sql: ${TABLE}.unitcost ;;
  }

  dimension: unitprice {
    type: number
    sql: ${TABLE}.unitprice ;;
  }

  measure: count {
    type: count
    drill_fields: [cmitem.cmitem_id, item.item_id]
  }
}
