view: shipitemlocrsrv {
  sql_table_name: public.shipitemlocrsrv ;;

  dimension: shipitemlocrsrv_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.shipitemlocrsrv_id ;;
  }

  dimension_group: shipitemlocrsrv_expiration {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.shipitemlocrsrv_expiration ;;
  }

  dimension: shipitemlocrsrv_itemsite_id {
    type: number
    sql: ${TABLE}.shipitemlocrsrv_itemsite_id ;;
  }

  dimension: shipitemlocrsrv_location_id {
    type: number
    sql: ${TABLE}.shipitemlocrsrv_location_id ;;
  }

  dimension: shipitemlocrsrv_ls_id {
    type: number
    sql: ${TABLE}.shipitemlocrsrv_ls_id ;;
  }

  dimension: shipitemlocrsrv_qty {
    type: number
    sql: ${TABLE}.shipitemlocrsrv_qty ;;
  }

  dimension: shipitemlocrsrv_shipitem_id {
    type: number
    sql: ${TABLE}.shipitemlocrsrv_shipitem_id ;;
  }

  dimension_group: shipitemlocrsrv_warrpurc {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.shipitemlocrsrv_warrpurc ;;
  }

  measure: count {
    type: count
    drill_fields: [shipitemlocrsrv_id]
  }
}
