view: itemsite {
  sql_table_name: public.itemsite ;;

  dimension: itemsite_supply_itemsite_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.itemsite_supply_itemsite_id ;;
  }

  dimension: itemsite_abcclass {
    type: string
    sql: ${TABLE}.itemsite_abcclass ;;
  }

  dimension: itemsite_active {
    type: yesno
    sql: ${TABLE}.itemsite_active ;;
  }

  dimension: itemsite_autoabcclass {
    type: yesno
    sql: ${TABLE}.itemsite_autoabcclass ;;
  }

  dimension: itemsite_autoreg {
    type: yesno
    sql: ${TABLE}.itemsite_autoreg ;;
  }

  dimension: itemsite_controlmethod {
    type: string
    sql: ${TABLE}.itemsite_controlmethod ;;
  }

  dimension: itemsite_cosdefault {
    type: string
    sql: ${TABLE}.itemsite_cosdefault ;;
  }

  dimension: itemsite_costcat_id {
    type: number
    sql: ${TABLE}.itemsite_costcat_id ;;
  }

  dimension: itemsite_costmethod {
    type: string
    sql: ${TABLE}.itemsite_costmethod ;;
  }

  dimension: itemsite_createpr {
    type: yesno
    sql: ${TABLE}.itemsite_createpr ;;
  }

  dimension: itemsite_createsopo {
    type: yesno
    sql: ${TABLE}.itemsite_createsopo ;;
  }

  dimension: itemsite_createsopr {
    type: yesno
    sql: ${TABLE}.itemsite_createsopr ;;
  }

  dimension: itemsite_createwo {
    type: yesno
    sql: ${TABLE}.itemsite_createwo ;;
  }

  dimension: itemsite_cyclecountfreq {
    type: number
    sql: ${TABLE}.itemsite_cyclecountfreq ;;
  }

  dimension_group: itemsite_datelastcount {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.itemsite_datelastcount ;;
  }

  dimension_group: itemsite_datelastused {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.itemsite_datelastused ;;
  }

  dimension: itemsite_disallowblankwip {
    type: yesno
    sql: ${TABLE}.itemsite_disallowblankwip ;;
  }

  dimension: itemsite_dropship {
    type: yesno
    sql: ${TABLE}.itemsite_dropship ;;
  }

  dimension: itemsite_eventfence {
    type: number
    sql: ${TABLE}.itemsite_eventfence ;;
  }

  dimension: itemsite_freeze {
    type: yesno
    sql: ${TABLE}.itemsite_freeze ;;
  }

  dimension: itemsite_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.itemsite_id ;;
  }

  dimension: itemsite_issuelocation_dist {
    type: yesno
    sql: ${TABLE}.itemsite_issuelocation_dist ;;
  }

  dimension: itemsite_issuelocation_id {
    type: number
    sql: ${TABLE}.itemsite_issuelocation_id ;;
  }

  dimension: itemsite_issuemethod {
    type: string
    sql: ${TABLE}.itemsite_issuemethod ;;
  }

  dimension: itemsite_item_id {
    type: number
    sql: ${TABLE}.itemsite_item_id ;;
  }

  dimension: itemsite_leadtime {
    type: number
    sql: ${TABLE}.itemsite_leadtime ;;
  }

  dimension: itemsite_location {
    type: string
    sql: ${TABLE}.itemsite_location ;;
  }

  dimension: itemsite_location_comments {
    type: string
    sql: ${TABLE}.itemsite_location_comments ;;
  }

  dimension: itemsite_location_dist {
    type: yesno
    sql: ${TABLE}.itemsite_location_dist ;;
  }

  dimension: itemsite_location_id {
    type: number
    sql: ${TABLE}.itemsite_location_id ;;
  }

  dimension: itemsite_loccntrl {
    type: yesno
    sql: ${TABLE}.itemsite_loccntrl ;;
  }

  dimension: itemsite_lsseq_id {
    type: number
    sql: ${TABLE}.itemsite_lsseq_id ;;
  }

  dimension: itemsite_maxordqty {
    type: number
    sql: ${TABLE}.itemsite_maxordqty ;;
  }

  dimension: itemsite_minordqty {
    type: number
    sql: ${TABLE}.itemsite_minordqty ;;
  }

  dimension: itemsite_mps_timefence {
    type: number
    sql: ${TABLE}.itemsite_mps_timefence ;;
  }

  dimension: itemsite_multordqty {
    type: number
    sql: ${TABLE}.itemsite_multordqty ;;
  }

  dimension: itemsite_notes {
    type: string
    sql: ${TABLE}.itemsite_notes ;;
  }

  dimension: itemsite_ordergroup {
    type: number
    sql: ${TABLE}.itemsite_ordergroup ;;
  }

  dimension: itemsite_ordergroup_first {
    type: yesno
    sql: ${TABLE}.itemsite_ordergroup_first ;;
  }

  dimension: itemsite_ordertoqty {
    type: number
    sql: ${TABLE}.itemsite_ordertoqty ;;
  }

  dimension: itemsite_perishable {
    type: yesno
    sql: ${TABLE}.itemsite_perishable ;;
  }

  dimension: itemsite_plancode_id {
    type: number
    sql: ${TABLE}.itemsite_plancode_id ;;
  }

  dimension: itemsite_planning_type {
    type: string
    sql: ${TABLE}.itemsite_planning_type ;;
  }

  dimension: itemsite_posupply {
    type: yesno
    sql: ${TABLE}.itemsite_posupply ;;
  }

  dimension: itemsite_qtyonhand {
    type: number
    sql: ${TABLE}.itemsite_qtyonhand ;;
  }

  dimension: itemsite_recvlocation_dist {
    type: yesno
    sql: ${TABLE}.itemsite_recvlocation_dist ;;
  }

  dimension: itemsite_recvlocation_id {
    type: number
    sql: ${TABLE}.itemsite_recvlocation_id ;;
  }

  dimension: itemsite_reorderlevel {
    type: number
    sql: ${TABLE}.itemsite_reorderlevel ;;
  }

  dimension: itemsite_safetystock {
    type: number
    sql: ${TABLE}.itemsite_safetystock ;;
  }

  dimension: itemsite_sold {
    type: yesno
    sql: ${TABLE}.itemsite_sold ;;
  }

  dimension: itemsite_soldranking {
    type: number
    sql: ${TABLE}.itemsite_soldranking ;;
  }

  dimension: itemsite_stocked {
    type: yesno
    sql: ${TABLE}.itemsite_stocked ;;
  }

  dimension: itemsite_useparams {
    type: yesno
    sql: ${TABLE}.itemsite_useparams ;;
  }

  dimension: itemsite_useparamsmanual {
    type: yesno
    sql: ${TABLE}.itemsite_useparamsmanual ;;
  }

  dimension: itemsite_value {
    type: number
    sql: ${TABLE}.itemsite_value ;;
  }

  dimension: itemsite_warehous_id {
    type: number
    sql: ${TABLE}.itemsite_warehous_id ;;
  }

  dimension: itemsite_warrpurc {
    type: yesno
    sql: ${TABLE}.itemsite_warrpurc ;;
  }

  dimension: itemsite_wosupply {
    type: yesno
    sql: ${TABLE}.itemsite_wosupply ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      itemsite_supply_itemsite_id,
      itemsite.itemsite_supply_itemsite_id,
      invoiceitem.count,
      itemsite.count,
      saleshistory.count,
      saleshistorymisc.count
    ]
  }
}
