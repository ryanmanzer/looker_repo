view: taxass {
  sql_table_name: public.taxass ;;

  dimension: taxass_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.taxass_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: taxass_tax_id {
    type: number
    sql: ${TABLE}.taxass_tax_id ;;
  }

  dimension: taxass_taxtype_id {
    type: number
    sql: ${TABLE}.taxass_taxtype_id ;;
  }

  dimension: taxass_taxzone_id {
    type: number
    sql: ${TABLE}.taxass_taxzone_id ;;
  }

  measure: count {
    type: count
    drill_fields: [taxass_id]
  }
}
