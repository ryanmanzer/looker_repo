view: batch_old {
  sql_table_name: public.batch_old ;;

  dimension: batch_action {
    type: string
    sql: ${TABLE}.batch_action ;;
  }

  dimension: batch_cc {
    type: string
    sql: ${TABLE}.batch_cc ;;
  }

  dimension_group: batch_completed {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.batch_completed ;;
  }

  dimension: batch_email {
    type: string
    sql: ${TABLE}.batch_email ;;
  }

  dimension: batch_emailhtml {
    type: yesno
    sql: ${TABLE}.batch_emailhtml ;;
  }

  dimension: batch_exitstatus {
    type: string
    sql: ${TABLE}.batch_exitstatus ;;
  }

  dimension: batch_filename {
    type: string
    sql: ${TABLE}.batch_filename ;;
  }

  dimension: batch_fromemail {
    type: string
    sql: ${TABLE}.batch_fromemail ;;
  }

  dimension: batch_id {
    type: number
    sql: ${TABLE}.batch_id ;;
  }

  dimension: batch_parameter {
    type: string
    sql: ${TABLE}.batch_parameter ;;
  }

  dimension: batch_reschedinterval {
    type: string
    sql: ${TABLE}.batch_reschedinterval ;;
  }

  dimension: batch_responsebody {
    type: string
    sql: ${TABLE}.batch_responsebody ;;
  }

  dimension_group: batch_scheduled {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.batch_scheduled ;;
  }

  dimension_group: batch_started {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.batch_started ;;
  }

  dimension: batch_subject {
    type: string
    sql: ${TABLE}.batch_subject ;;
  }

  dimension_group: batch_submitted {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.batch_submitted ;;
  }

  dimension: batch_user {
    type: string
    sql: ${TABLE}.batch_user ;;
  }

  measure: count {
    type: count
    drill_fields: [batch_filename]
  }
}
