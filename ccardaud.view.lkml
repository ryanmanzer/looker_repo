view: ccardaud {
  sql_table_name: public.ccardaud ;;

  dimension: ccardaud_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.ccardaud_id ;;
  }

  dimension: ccardaud_ccard_active_new {
    type: yesno
    sql: ${TABLE}.ccardaud_ccard_active_new ;;
  }

  dimension: ccardaud_ccard_active_old {
    type: yesno
    sql: ${TABLE}.ccardaud_ccard_active_old ;;
  }

  dimension: ccardaud_ccard_address1_new {
    type: string
    sql: ${TABLE}.ccardaud_ccard_address1_new ;;
  }

  dimension: ccardaud_ccard_address1_old {
    type: string
    sql: ${TABLE}.ccardaud_ccard_address1_old ;;
  }

  dimension: ccardaud_ccard_address2_new {
    type: string
    sql: ${TABLE}.ccardaud_ccard_address2_new ;;
  }

  dimension: ccardaud_ccard_address2_old {
    type: string
    sql: ${TABLE}.ccardaud_ccard_address2_old ;;
  }

  dimension: ccardaud_ccard_city_new {
    type: string
    sql: ${TABLE}.ccardaud_ccard_city_new ;;
  }

  dimension: ccardaud_ccard_city_old {
    type: string
    sql: ${TABLE}.ccardaud_ccard_city_old ;;
  }

  dimension: ccardaud_ccard_country_new {
    type: string
    sql: ${TABLE}.ccardaud_ccard_country_new ;;
  }

  dimension: ccardaud_ccard_country_old {
    type: string
    sql: ${TABLE}.ccardaud_ccard_country_old ;;
  }

  dimension: ccardaud_ccard_cust_id_new {
    type: number
    value_format_name: id
    sql: ${TABLE}.ccardaud_ccard_cust_id_new ;;
  }

  dimension: ccardaud_ccard_cust_id_old {
    type: number
    value_format_name: id
    sql: ${TABLE}.ccardaud_ccard_cust_id_old ;;
  }

  dimension: ccardaud_ccard_debit_new {
    type: yesno
    sql: ${TABLE}.ccardaud_ccard_debit_new ;;
  }

  dimension: ccardaud_ccard_debit_old {
    type: yesno
    sql: ${TABLE}.ccardaud_ccard_debit_old ;;
  }

  dimension: ccardaud_ccard_id {
    type: number
    sql: ${TABLE}.ccardaud_ccard_id ;;
  }

  dimension_group: ccardaud_ccard_last_updated {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.ccardaud_ccard_last_updated ;;
  }

  dimension: ccardaud_ccard_last_updated_by_username {
    type: string
    sql: ${TABLE}.ccardaud_ccard_last_updated_by_username ;;
  }

  dimension: ccardaud_ccard_month_expired_new {
    type: string
    sql: ${TABLE}.ccardaud_ccard_month_expired_new ;;
  }

  dimension: ccardaud_ccard_month_expired_old {
    type: string
    sql: ${TABLE}.ccardaud_ccard_month_expired_old ;;
  }

  dimension: ccardaud_ccard_name_new {
    type: string
    sql: ${TABLE}.ccardaud_ccard_name_new ;;
  }

  dimension: ccardaud_ccard_name_old {
    type: string
    sql: ${TABLE}.ccardaud_ccard_name_old ;;
  }

  dimension: ccardaud_ccard_number_new {
    type: string
    sql: ${TABLE}.ccardaud_ccard_number_new ;;
  }

  dimension: ccardaud_ccard_number_old {
    type: string
    sql: ${TABLE}.ccardaud_ccard_number_old ;;
  }

  dimension: ccardaud_ccard_seq_new {
    type: number
    sql: ${TABLE}.ccardaud_ccard_seq_new ;;
  }

  dimension: ccardaud_ccard_seq_old {
    type: number
    sql: ${TABLE}.ccardaud_ccard_seq_old ;;
  }

  dimension: ccardaud_ccard_state_new {
    type: string
    sql: ${TABLE}.ccardaud_ccard_state_new ;;
  }

  dimension: ccardaud_ccard_state_old {
    type: string
    sql: ${TABLE}.ccardaud_ccard_state_old ;;
  }

  dimension: ccardaud_ccard_type_new {
    type: string
    sql: ${TABLE}.ccardaud_ccard_type_new ;;
  }

  dimension: ccardaud_ccard_type_old {
    type: string
    sql: ${TABLE}.ccardaud_ccard_type_old ;;
  }

  dimension: ccardaud_ccard_year_expired_new {
    type: string
    sql: ${TABLE}.ccardaud_ccard_year_expired_new ;;
  }

  dimension: ccardaud_ccard_year_expired_old {
    type: string
    sql: ${TABLE}.ccardaud_ccard_year_expired_old ;;
  }

  dimension: ccardaud_ccard_zip_new {
    type: string
    sql: ${TABLE}.ccardaud_ccard_zip_new ;;
  }

  dimension: ccardaud_ccard_zip_old {
    type: string
    sql: ${TABLE}.ccardaud_ccard_zip_old ;;
  }

  measure: count {
    type: count
    drill_fields: [ccardaud_id, ccardaud_ccard_last_updated_by_username]
  }
}
