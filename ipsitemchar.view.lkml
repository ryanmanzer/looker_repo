view: ipsitemchar {
  sql_table_name: public.ipsitemchar ;;

  dimension: ipsitemchar_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.ipsitemchar_id ;;
  }

  dimension: ipsitemchar_char_id {
    type: number
    sql: ${TABLE}.ipsitemchar_char_id ;;
  }

  dimension: ipsitemchar_ipsitem_id {
    type: number
    sql: ${TABLE}.ipsitemchar_ipsitem_id ;;
  }

  dimension: ipsitemchar_price {
    type: number
    sql: ${TABLE}.ipsitemchar_price ;;
  }

  dimension: ipsitemchar_value {
    type: string
    sql: ${TABLE}.ipsitemchar_value ;;
  }

  measure: count {
    type: count
    drill_fields: [ipsitemchar_id]
  }
}
