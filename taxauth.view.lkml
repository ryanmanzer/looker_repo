view: taxauth {
  sql_table_name: public.taxauth ;;

  dimension: taxauth_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.taxauth_id ;;
  }

  dimension: taxauth_accnt_id {
    type: number
    sql: ${TABLE}.taxauth_accnt_id ;;
  }

  dimension: taxauth_addr_id {
    type: number
    sql: ${TABLE}.taxauth_addr_id ;;
  }

  dimension: taxauth_code {
    type: string
    sql: ${TABLE}.taxauth_code ;;
  }

  dimension: taxauth_county {
    type: string
    sql: ${TABLE}.taxauth_county ;;
  }

  dimension: taxauth_curr_id {
    type: number
    sql: ${TABLE}.taxauth_curr_id ;;
  }

  dimension: taxauth_extref {
    type: string
    sql: ${TABLE}.taxauth_extref ;;
  }

  dimension: taxauth_name {
    type: string
    sql: ${TABLE}.taxauth_name ;;
  }

  measure: count {
    type: count
    drill_fields: [taxauth_id, taxauth_name]
  }
}
