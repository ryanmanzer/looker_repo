view: bomitemcost {
  sql_table_name: public.bomitemcost ;;

  dimension: bomitemcost_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.bomitemcost_id ;;
  }

  dimension: bomitemcost_actcost {
    type: number
    sql: ${TABLE}.bomitemcost_actcost ;;
  }

  dimension: bomitemcost_bomitem_id {
    type: number
    sql: ${TABLE}.bomitemcost_bomitem_id ;;
  }

  dimension: bomitemcost_costelem_id {
    type: number
    sql: ${TABLE}.bomitemcost_costelem_id ;;
  }

  dimension: bomitemcost_curr_id {
    type: number
    sql: ${TABLE}.bomitemcost_curr_id ;;
  }

  dimension: bomitemcost_lowlevel {
    type: yesno
    sql: ${TABLE}.bomitemcost_lowlevel ;;
  }

  dimension_group: bomitemcost_posted {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.bomitemcost_posted ;;
  }

  dimension: bomitemcost_stdcost {
    type: number
    sql: ${TABLE}.bomitemcost_stdcost ;;
  }

  dimension_group: bomitemcost_updated {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.bomitemcost_updated ;;
  }

  measure: count {
    type: count
    drill_fields: [bomitemcost_id]
  }
}
