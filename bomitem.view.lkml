view: bomitem {
  sql_table_name: public.bomitem ;;

  dimension: bomitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.bomitem_id ;;
  }

  dimension: bomitem_booitem_seq_id {
    type: number
    sql: ${TABLE}.bomitem_booitem_seq_id ;;
  }

  dimension: bomitem_char_id {
    type: number
    sql: ${TABLE}.bomitem_char_id ;;
  }

  dimension: bomitem_createwo {
    type: yesno
    sql: ${TABLE}.bomitem_createwo ;;
  }

  dimension: bomitem_ecn {
    type: string
    sql: ${TABLE}.bomitem_ecn ;;
  }

  dimension_group: bomitem_effective {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.bomitem_effective ;;
  }

  dimension_group: bomitem_expires {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.bomitem_expires ;;
  }

  dimension: bomitem_issuemethod {
    type: string
    sql: ${TABLE}.bomitem_issuemethod ;;
  }

  dimension: bomitem_issuewo {
    type: yesno
    sql: ${TABLE}.bomitem_issuewo ;;
  }

  dimension: bomitem_item_id {
    type: number
    sql: ${TABLE}.bomitem_item_id ;;
  }

  dimension_group: bomitem_moddate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.bomitem_moddate ;;
  }

  dimension: bomitem_notes {
    type: string
    sql: ${TABLE}.bomitem_notes ;;
  }

  dimension: bomitem_parent_item_id {
    type: number
    sql: ${TABLE}.bomitem_parent_item_id ;;
  }

  dimension: bomitem_qtyfxd {
    type: number
    sql: ${TABLE}.bomitem_qtyfxd ;;
  }

  dimension: bomitem_qtyper {
    type: number
    sql: ${TABLE}.bomitem_qtyper ;;
  }

  dimension: bomitem_ref {
    type: string
    sql: ${TABLE}.bomitem_ref ;;
  }

  dimension: bomitem_rev_id {
    type: number
    sql: ${TABLE}.bomitem_rev_id ;;
  }

  dimension: bomitem_schedatwooper {
    type: yesno
    sql: ${TABLE}.bomitem_schedatwooper ;;
  }

  dimension: bomitem_scrap {
    type: number
    sql: ${TABLE}.bomitem_scrap ;;
  }

  dimension: bomitem_seqnumber {
    type: number
    sql: ${TABLE}.bomitem_seqnumber ;;
  }

  dimension: bomitem_status {
    type: string
    sql: ${TABLE}.bomitem_status ;;
  }

  dimension: bomitem_subtype {
    type: string
    sql: ${TABLE}.bomitem_subtype ;;
  }

  dimension: bomitem_uom_id {
    type: number
    sql: ${TABLE}.bomitem_uom_id ;;
  }

  dimension: bomitem_value {
    type: string
    sql: ${TABLE}.bomitem_value ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [bomitem_id]
  }
}
