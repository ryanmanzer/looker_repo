view: itemsrcp {
  sql_table_name: public.itemsrcp ;;

  dimension: itemsrcp_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.itemsrcp_id ;;
  }

  dimension: itemsrcp_curr_id {
    type: number
    sql: ${TABLE}.itemsrcp_curr_id ;;
  }

  dimension: itemsrcp_discntprcnt {
    type: number
    sql: ${TABLE}.itemsrcp_discntprcnt ;;
  }

  dimension: itemsrcp_dropship {
    type: yesno
    sql: ${TABLE}.itemsrcp_dropship ;;
  }

  dimension: itemsrcp_fixedamtdiscount {
    type: number
    sql: ${TABLE}.itemsrcp_fixedamtdiscount ;;
  }

  dimension: itemsrcp_itemsrc_id {
    type: number
    sql: ${TABLE}.itemsrcp_itemsrc_id ;;
  }

  dimension: itemsrcp_price {
    type: number
    sql: ${TABLE}.itemsrcp_price ;;
  }

  dimension: itemsrcp_qtybreak {
    type: number
    sql: ${TABLE}.itemsrcp_qtybreak ;;
  }

  dimension: itemsrcp_type {
    type: string
    sql: ${TABLE}.itemsrcp_type ;;
  }

  dimension_group: itemsrcp_updated {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.itemsrcp_updated ;;
  }

  dimension: itemsrcp_warehous_id {
    type: number
    sql: ${TABLE}.itemsrcp_warehous_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [itemsrcp_id]
  }
}
