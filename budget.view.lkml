view: budget {
  sql_table_name: public.budget ;;

  dimension: budget_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.budget_id ;;
  }

  dimension: budget_accnt_id {
    type: number
    sql: ${TABLE}.budget_accnt_id ;;
  }

  dimension: budget_amount {
    type: number
    sql: ${TABLE}.budget_amount ;;
  }

  dimension: budget_period_id {
    type: number
    sql: ${TABLE}.budget_period_id ;;
  }

  measure: count {
    type: count
    drill_fields: [budget_id]
  }
}
