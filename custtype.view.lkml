view: custtype {
  sql_table_name: public.custtype ;;

  dimension: custtype_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.custtype_id ;;
  }

  dimension: custtype_char {
    type: yesno
    sql: ${TABLE}.custtype_char ;;
  }

  dimension: custtype_code {
    type: string
    sql: ${TABLE}.custtype_code ;;
  }

  dimension: custtype_descrip {
    type: string
    sql: ${TABLE}.custtype_descrip ;;
  }

  measure: count {
    type: count
    drill_fields: [custtype_id, saleshistory.count, saleshistorymisc.count]
  }
}
