view: araccnt {
  sql_table_name: public.araccnt ;;

  dimension: araccnt_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.araccnt_id ;;
  }

  dimension: araccnt_ar_accnt_id {
    type: number
    sql: ${TABLE}.araccnt_ar_accnt_id ;;
  }

  dimension: araccnt_custtype {
    type: string
    sql: ${TABLE}.araccnt_custtype ;;
  }

  dimension: araccnt_custtype_id {
    type: number
    sql: ${TABLE}.araccnt_custtype_id ;;
  }

  dimension: araccnt_deferred_accnt_id {
    type: number
    sql: ${TABLE}.araccnt_deferred_accnt_id ;;
  }

  dimension: araccnt_discount_accnt_id {
    type: number
    sql: ${TABLE}.araccnt_discount_accnt_id ;;
  }

  dimension: araccnt_freight_accnt_id {
    type: number
    sql: ${TABLE}.araccnt_freight_accnt_id ;;
  }

  dimension: araccnt_prepaid_accnt_id {
    type: number
    sql: ${TABLE}.araccnt_prepaid_accnt_id ;;
  }

  measure: count {
    type: count
    drill_fields: [araccnt_id]
  }
}
