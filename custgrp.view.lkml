view: custgrp {
  sql_table_name: public.custgrp ;;

  dimension: custgrp_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.custgrp_id ;;
  }

  dimension: custgrp_descrip {
    type: string
    sql: ${TABLE}.custgrp_descrip ;;
  }

  dimension: custgrp_name {
    type: string
    sql: ${TABLE}.custgrp_name ;;
  }

  measure: count {
    type: count
    drill_fields: [custgrp_id, custgrp_name]
  }
}
