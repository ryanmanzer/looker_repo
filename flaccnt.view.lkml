view: flaccnt {
  sql_table_name: public.flaccnt ;;

  dimension: accnt_company {
    type: string
    sql: ${TABLE}.accnt_company ;;
  }

  dimension: accnt_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.accnt_id ;;
  }

  dimension: accnt_number {
    type: string
    sql: ${TABLE}.accnt_number ;;
  }

  dimension: accnt_profit {
    type: string
    sql: ${TABLE}.accnt_profit ;;
  }

  dimension: accnt_sub {
    type: string
    sql: ${TABLE}.accnt_sub ;;
  }

  dimension: accnt_type {
    type: string
    sql: ${TABLE}.accnt_type ;;
  }

  dimension: flhead_type {
    type: string
    sql: ${TABLE}.flhead_type ;;
  }

  dimension: flitem_accnt_id {
    type: number
    sql: ${TABLE}.flitem_accnt_id ;;
  }

  dimension: flitem_company {
    type: string
    sql: ${TABLE}.flitem_company ;;
  }

  dimension: flitem_custom_source {
    type: string
    sql: ${TABLE}.flitem_custom_source ;;
  }

  dimension: flitem_flgrp_id {
    type: number
    sql: ${TABLE}.flitem_flgrp_id ;;
  }

  dimension: flitem_flhead_id {
    type: number
    sql: ${TABLE}.flitem_flhead_id ;;
  }

  dimension: flitem_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.flitem_id ;;
  }

  dimension: flitem_number {
    type: string
    sql: ${TABLE}.flitem_number ;;
  }

  dimension: flitem_order {
    type: number
    sql: ${TABLE}.flitem_order ;;
  }

  dimension: flitem_prcnt_flgrp_id {
    type: number
    sql: ${TABLE}.flitem_prcnt_flgrp_id ;;
  }

  dimension: flitem_profit {
    type: string
    sql: ${TABLE}.flitem_profit ;;
  }

  dimension: flitem_showbudget {
    type: yesno
    sql: ${TABLE}.flitem_showbudget ;;
  }

  dimension: flitem_showbudgetprcnt {
    type: yesno
    sql: ${TABLE}.flitem_showbudgetprcnt ;;
  }

  dimension: flitem_showcustom {
    type: yesno
    sql: ${TABLE}.flitem_showcustom ;;
  }

  dimension: flitem_showcustomprcnt {
    type: yesno
    sql: ${TABLE}.flitem_showcustomprcnt ;;
  }

  dimension: flitem_showdelta {
    type: yesno
    sql: ${TABLE}.flitem_showdelta ;;
  }

  dimension: flitem_showdeltaprcnt {
    type: yesno
    sql: ${TABLE}.flitem_showdeltaprcnt ;;
  }

  dimension: flitem_showdiff {
    type: yesno
    sql: ${TABLE}.flitem_showdiff ;;
  }

  dimension: flitem_showdiffprcnt {
    type: yesno
    sql: ${TABLE}.flitem_showdiffprcnt ;;
  }

  dimension: flitem_showend {
    type: yesno
    sql: ${TABLE}.flitem_showend ;;
  }

  dimension: flitem_showendprcnt {
    type: yesno
    sql: ${TABLE}.flitem_showendprcnt ;;
  }

  dimension: flitem_showstart {
    type: yesno
    sql: ${TABLE}.flitem_showstart ;;
  }

  dimension: flitem_showstartprcnt {
    type: yesno
    sql: ${TABLE}.flitem_showstartprcnt ;;
  }

  dimension: flitem_sub {
    type: string
    sql: ${TABLE}.flitem_sub ;;
  }

  dimension: flitem_subaccnttype_code {
    type: string
    sql: ${TABLE}.flitem_subaccnttype_code ;;
  }

  dimension: flitem_subtract {
    type: yesno
    sql: ${TABLE}.flitem_subtract ;;
  }

  dimension: flitem_type {
    type: string
    sql: ${TABLE}.flitem_type ;;
  }

  dimension: prj_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.prj_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      flitem.flitem_id,
      accnt.accnt_name,
      accnt.accnt_id,
      prj.prj_name,
      prj.prj_owner_username,
      prj.prj_username,
      prj.prj_recurring_prj_id
    ]
  }
}
