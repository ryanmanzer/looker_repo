view: itemgrp {
  sql_table_name: public.itemgrp ;;

  dimension: itemgrp_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.itemgrp_id ;;
  }

  dimension: itemgrp_catalog {
    type: yesno
    sql: ${TABLE}.itemgrp_catalog ;;
  }

  dimension: itemgrp_descrip {
    type: string
    sql: ${TABLE}.itemgrp_descrip ;;
  }

  dimension: itemgrp_name {
    type: string
    sql: ${TABLE}.itemgrp_name ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [itemgrp_id, itemgrp_name]
  }
}
