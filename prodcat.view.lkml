view: prodcat {
  sql_table_name: public.prodcat ;;

  dimension: prodcat_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.prodcat_id ;;
  }

  dimension: prodcat_code {
    type: string
    sql: ${TABLE}.prodcat_code ;;
  }

  dimension: prodcat_descrip {
    type: string
    sql: ${TABLE}.prodcat_descrip ;;
  }

  measure: count {
    type: count
    drill_fields: [prodcat_id, saleshistory.count, saleshistorymisc.count]
  }
}
