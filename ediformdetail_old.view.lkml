view: ediformdetail_old {
  sql_table_name: public.ediformdetail_old ;;

  dimension: ediformdetail_ediform_id {
    type: number
    sql: ${TABLE}.ediformdetail_ediform_id ;;
  }

  dimension: ediformdetail_id {
    type: number
    sql: ${TABLE}.ediformdetail_id ;;
  }

  dimension: ediformdetail_name {
    type: string
    sql: ${TABLE}.ediformdetail_name ;;
  }

  dimension: ediformdetail_notes {
    type: string
    sql: ${TABLE}.ediformdetail_notes ;;
  }

  dimension: ediformdetail_order {
    type: number
    sql: ${TABLE}.ediformdetail_order ;;
  }

  dimension: ediformdetail_query {
    type: string
    sql: ${TABLE}.ediformdetail_query ;;
  }

  measure: count {
    type: count
    drill_fields: [ediformdetail_name]
  }
}
