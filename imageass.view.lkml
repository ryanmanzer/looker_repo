view: imageass {
  sql_table_name: public.imageass ;;

  dimension: imageass_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.imageass_id ;;
  }

  dimension: imageass_image_id {
    type: number
    sql: ${TABLE}.imageass_image_id ;;
  }

  dimension: imageass_purpose {
    type: string
    sql: ${TABLE}.imageass_purpose ;;
  }

  dimension: imageass_source {
    type: string
    sql: ${TABLE}.imageass_source ;;
  }

  dimension: imageass_source_id {
    type: number
    sql: ${TABLE}.imageass_source_id ;;
  }

  measure: count {
    type: count
    drill_fields: [imageass_id]
  }
}
