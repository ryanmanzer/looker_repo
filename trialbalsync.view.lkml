view: trialbalsync {
  sql_table_name: public.trialbalsync ;;

  dimension: trialbal_accnt_id {
    type: number
    sql: ${TABLE}.trialbal_accnt_id ;;
  }

  dimension: trialbal_beginning {
    type: number
    sql: ${TABLE}.trialbal_beginning ;;
  }

  dimension: trialbal_credits {
    type: number
    sql: ${TABLE}.trialbal_credits ;;
  }

  dimension: trialbal_debits {
    type: number
    sql: ${TABLE}.trialbal_debits ;;
  }

  dimension: trialbal_dirty {
    type: yesno
    sql: ${TABLE}.trialbal_dirty ;;
  }

  dimension: trialbal_ending {
    type: number
    sql: ${TABLE}.trialbal_ending ;;
  }

  dimension: trialbal_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.trialbal_id ;;
  }

  dimension: trialbal_period_id {
    type: number
    sql: ${TABLE}.trialbal_period_id ;;
  }

  dimension: trialbal_yearend {
    type: number
    sql: ${TABLE}.trialbal_yearend ;;
  }

  dimension: trialbalsync_curr_beginning {
    type: number
    sql: ${TABLE}.trialbalsync_curr_beginning ;;
  }

  dimension: trialbalsync_curr_credits {
    type: number
    sql: ${TABLE}.trialbalsync_curr_credits ;;
  }

  dimension: trialbalsync_curr_debits {
    type: number
    sql: ${TABLE}.trialbalsync_curr_debits ;;
  }

  dimension: trialbalsync_curr_ending {
    type: number
    sql: ${TABLE}.trialbalsync_curr_ending ;;
  }

  dimension: trialbalsync_curr_id {
    type: number
    sql: ${TABLE}.trialbalsync_curr_id ;;
  }

  dimension: trialbalsync_curr_posted {
    type: yesno
    sql: ${TABLE}.trialbalsync_curr_posted ;;
  }

  dimension: trialbalsync_curr_rate {
    type: number
    sql: ${TABLE}.trialbalsync_curr_rate ;;
  }

  dimension: trialbalsync_curr_yearend {
    type: number
    sql: ${TABLE}.trialbalsync_curr_yearend ;;
  }

  measure: count {
    type: count
    drill_fields: [trialbal.trialbal_id]
  }
}
