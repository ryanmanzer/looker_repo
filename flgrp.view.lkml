view: flgrp {
  sql_table_name: public.flgrp ;;

  dimension: flgrp_prcnt_flgrp_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.flgrp_prcnt_flgrp_id ;;
  }

  dimension: flgrp_altsubtotal {
    type: string
    sql: ${TABLE}.flgrp_altsubtotal ;;
  }

  dimension: flgrp_descrip {
    type: string
    sql: ${TABLE}.flgrp_descrip ;;
  }

  dimension: flgrp_flgrp_id {
    type: number
    sql: ${TABLE}.flgrp_flgrp_id ;;
  }

  dimension: flgrp_flhead_id {
    type: number
    sql: ${TABLE}.flgrp_flhead_id ;;
  }

  dimension: flgrp_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.flgrp_id ;;
  }

  dimension: flgrp_name {
    type: string
    sql: ${TABLE}.flgrp_name ;;
  }

  dimension: flgrp_order {
    type: number
    sql: ${TABLE}.flgrp_order ;;
  }

  dimension: flgrp_showbudget {
    type: yesno
    sql: ${TABLE}.flgrp_showbudget ;;
  }

  dimension: flgrp_showbudgetprcnt {
    type: yesno
    sql: ${TABLE}.flgrp_showbudgetprcnt ;;
  }

  dimension: flgrp_showcustom {
    type: yesno
    sql: ${TABLE}.flgrp_showcustom ;;
  }

  dimension: flgrp_showcustomprcnt {
    type: yesno
    sql: ${TABLE}.flgrp_showcustomprcnt ;;
  }

  dimension: flgrp_showdelta {
    type: yesno
    sql: ${TABLE}.flgrp_showdelta ;;
  }

  dimension: flgrp_showdeltaprcnt {
    type: yesno
    sql: ${TABLE}.flgrp_showdeltaprcnt ;;
  }

  dimension: flgrp_showdiff {
    type: yesno
    sql: ${TABLE}.flgrp_showdiff ;;
  }

  dimension: flgrp_showdiffprcnt {
    type: yesno
    sql: ${TABLE}.flgrp_showdiffprcnt ;;
  }

  dimension: flgrp_showend {
    type: yesno
    sql: ${TABLE}.flgrp_showend ;;
  }

  dimension: flgrp_showendprcnt {
    type: yesno
    sql: ${TABLE}.flgrp_showendprcnt ;;
  }

  dimension: flgrp_showstart {
    type: yesno
    sql: ${TABLE}.flgrp_showstart ;;
  }

  dimension: flgrp_showstartprcnt {
    type: yesno
    sql: ${TABLE}.flgrp_showstartprcnt ;;
  }

  dimension: flgrp_subtotal {
    type: yesno
    sql: ${TABLE}.flgrp_subtotal ;;
  }

  dimension: flgrp_subtract {
    type: yesno
    sql: ${TABLE}.flgrp_subtract ;;
  }

  dimension: flgrp_summarize {
    type: yesno
    sql: ${TABLE}.flgrp_summarize ;;
  }

  dimension: flgrp_usealtsubtotal {
    type: yesno
    sql: ${TABLE}.flgrp_usealtsubtotal ;;
  }

  measure: count {
    type: count
    drill_fields: [flgrp_prcnt_flgrp_id, flgrp_name, flgrp.flgrp_name, flgrp.flgrp_prcnt_flgrp_id, flgrp.count]
  }
}
