view: ipsfreight {
  sql_table_name: public.ipsfreight ;;

  dimension: ipsfreight_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.ipsfreight_id ;;
  }

  dimension: ipsfreight_freightclass_id {
    type: number
    sql: ${TABLE}.ipsfreight_freightclass_id ;;
  }

  dimension: ipsfreight_ipshead_id {
    type: number
    sql: ${TABLE}.ipsfreight_ipshead_id ;;
  }

  dimension: ipsfreight_price {
    type: number
    sql: ${TABLE}.ipsfreight_price ;;
  }

  dimension: ipsfreight_qtybreak {
    type: number
    sql: ${TABLE}.ipsfreight_qtybreak ;;
  }

  dimension: ipsfreight_shipvia {
    type: string
    sql: ${TABLE}.ipsfreight_shipvia ;;
  }

  dimension: ipsfreight_shipzone_id {
    type: number
    sql: ${TABLE}.ipsfreight_shipzone_id ;;
  }

  dimension: ipsfreight_type {
    type: string
    sql: ${TABLE}.ipsfreight_type ;;
  }

  dimension: ipsfreight_warehous_id {
    type: number
    sql: ${TABLE}.ipsfreight_warehous_id ;;
  }

  measure: count {
    type: count
    drill_fields: [ipsfreight_id]
  }
}
