view: cohist_orders {
  derived_table: {
    sql:
      SELECT cohist_ordernumber AS order_number, SUM(cohist_qtyshipped*cohist_unitprice) as order_total
      FROM
      cohist
      GROUP BY
      cohist_ordernumber
    ;;
  }
  dimension: order_number {
    type: string
    sql: ${TABLE}.order_number ;;
    primary_key: yes
  }
  dimension: order_total {
    type: number
    sql: ${TABLE}.order_total ;;
  }
  measure: sum_of_orders {
    type: sum

  }
  measure: avg_of_orders {
    type: average

  }
  }
