view: ipsprodcat_bak {
  sql_table_name: public.ipsprodcat_bak ;;

  dimension: ipsprodcat_discntprcnt {
    type: number
    sql: ${TABLE}.ipsprodcat_discntprcnt ;;
  }

  dimension: ipsprodcat_fixedamtdiscount {
    type: number
    sql: ${TABLE}.ipsprodcat_fixedamtdiscount ;;
  }

  dimension: ipsprodcat_id {
    type: number
    sql: ${TABLE}.ipsprodcat_id ;;
  }

  dimension: ipsprodcat_ipshead_id {
    type: number
    sql: ${TABLE}.ipsprodcat_ipshead_id ;;
  }

  dimension: ipsprodcat_prodcat_id {
    type: number
    sql: ${TABLE}.ipsprodcat_prodcat_id ;;
  }

  dimension: ipsprodcat_qtybreak {
    type: number
    sql: ${TABLE}.ipsprodcat_qtybreak ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
