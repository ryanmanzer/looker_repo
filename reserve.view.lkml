view: reserve {
  sql_table_name: public.reserve ;;

  dimension: reserve_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.reserve_id ;;
  }

  dimension: reserve_demand_id {
    type: number
    sql: ${TABLE}.reserve_demand_id ;;
  }

  dimension: reserve_demand_type {
    type: string
    sql: ${TABLE}.reserve_demand_type ;;
  }

  dimension: reserve_qty {
    type: number
    sql: ${TABLE}.reserve_qty ;;
  }

  dimension: reserve_status {
    type: string
    sql: ${TABLE}.reserve_status ;;
  }

  dimension: reserve_supply_id {
    type: number
    sql: ${TABLE}.reserve_supply_id ;;
  }

  dimension: reserve_supply_type {
    type: string
    sql: ${TABLE}.reserve_supply_type ;;
  }

  measure: count {
    type: count
    drill_fields: [reserve_id]
  }
}
