view: apselect {
  sql_table_name: public.apselect ;;

  dimension: apselect_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.apselect_id ;;
  }

  dimension: apselect_amount {
    type: number
    sql: ${TABLE}.apselect_amount ;;
  }

  dimension: apselect_apopen_id {
    type: number
    sql: ${TABLE}.apselect_apopen_id ;;
  }

  dimension: apselect_bankaccnt_id {
    type: number
    sql: ${TABLE}.apselect_bankaccnt_id ;;
  }

  dimension: apselect_curr_id {
    type: number
    sql: ${TABLE}.apselect_curr_id ;;
  }

  dimension_group: apselect {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.apselect_date ;;
  }

  dimension: apselect_discount {
    type: number
    sql: ${TABLE}.apselect_discount ;;
  }

  measure: count {
    type: count
    drill_fields: [apselect_id]
  }
}
