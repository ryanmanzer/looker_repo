view: ipsiteminfo {
  sql_table_name: public.ipsiteminfo ;;

  dimension: ipsitem_discntprcnt {
    type: number
    sql: ${TABLE}.ipsitem_discntprcnt ;;
  }

  dimension: ipsitem_fixedamtdiscount {
    type: number
    sql: ${TABLE}.ipsitem_fixedamtdiscount ;;
  }

  dimension: ipsitem_id {
    type: number
    sql: ${TABLE}.ipsitem_id ;;
  }

  dimension: ipsitem_ipshead_id {
    type: number
    sql: ${TABLE}.ipsitem_ipshead_id ;;
  }

  dimension: ipsitem_item_id {
    type: number
    sql: ${TABLE}.ipsitem_item_id ;;
  }

  dimension: ipsitem_price {
    type: number
    sql: ${TABLE}.ipsitem_price ;;
  }

  dimension: ipsitem_price_uom_id {
    type: number
    sql: ${TABLE}.ipsitem_price_uom_id ;;
  }

  dimension: ipsitem_prodcat_id {
    type: number
    sql: ${TABLE}.ipsitem_prodcat_id ;;
  }

  dimension: ipsitem_qty_uom_id {
    type: number
    sql: ${TABLE}.ipsitem_qty_uom_id ;;
  }

  dimension: ipsitem_qtybreak {
    type: number
    sql: ${TABLE}.ipsitem_qtybreak ;;
  }

  dimension: ipsitem_type {
    type: string
    sql: ${TABLE}.ipsitem_type ;;
  }

  dimension: ipsitem_warehous_id {
    type: number
    sql: ${TABLE}.ipsitem_warehous_id ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
