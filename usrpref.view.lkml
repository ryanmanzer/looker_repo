view: usrpref {
  sql_table_name: public.usrpref ;;

  dimension: usrpref_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.usrpref_id ;;
  }

  dimension: usrpref_name {
    type: string
    sql: ${TABLE}.usrpref_name ;;
  }

  dimension: usrpref_username {
    type: string
    sql: ${TABLE}.usrpref_username ;;
  }

  dimension: usrpref_value {
    type: string
    sql: ${TABLE}.usrpref_value ;;
  }

  measure: count {
    type: count
    drill_fields: [usrpref_id, usrpref_name, usrpref_username]
  }
}
