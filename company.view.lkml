view: company {
  sql_table_name: public.company ;;

  dimension: company_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.company_id ;;
  }

  dimension: company_curr_id {
    type: number
    sql: ${TABLE}.company_curr_id ;;
  }

  dimension: company_database {
    type: string
    sql: ${TABLE}.company_database ;;
  }

  dimension: company_descrip {
    type: string
    sql: ${TABLE}.company_descrip ;;
  }

  dimension: company_dscrp_accnt_id {
    type: number
    sql: ${TABLE}.company_dscrp_accnt_id ;;
  }

  dimension: company_external {
    type: yesno
    sql: ${TABLE}.company_external ;;
  }

  dimension: company_gainloss_accnt_id {
    type: number
    sql: ${TABLE}.company_gainloss_accnt_id ;;
  }

  dimension: company_number {
    type: string
    sql: ${TABLE}.company_number ;;
  }

  dimension: company_port {
    type: number
    sql: ${TABLE}.company_port ;;
  }

  dimension: company_server {
    type: string
    sql: ${TABLE}.company_server ;;
  }

  dimension: company_unassigned_accnt_id {
    type: number
    sql: ${TABLE}.company_unassigned_accnt_id ;;
  }

  dimension: company_unrlzgainloss_accnt_id {
    type: number
    sql: ${TABLE}.company_unrlzgainloss_accnt_id ;;
  }

  dimension: company_yearend_accnt_id {
    type: number
    sql: ${TABLE}.company_yearend_accnt_id ;;
  }

  measure: count {
    type: count
    drill_fields: [company_id]
  }
}
