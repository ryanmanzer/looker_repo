view: docass {
  sql_table_name: public.docass ;;

  dimension: docass_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.docass_id ;;
  }

  dimension_group: docass_created {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.docass_created ;;
  }

  dimension: docass_purpose {
    type: string
    sql: ${TABLE}.docass_purpose ;;
  }

  dimension: docass_source_id {
    type: number
    sql: ${TABLE}.docass_source_id ;;
  }

  dimension: docass_source_type {
    type: string
    sql: ${TABLE}.docass_source_type ;;
  }

  dimension: docass_target_id {
    type: number
    sql: ${TABLE}.docass_target_id ;;
  }

  dimension: docass_target_type {
    type: string
    sql: ${TABLE}.docass_target_type ;;
  }

  dimension: docass_username {
    type: string
    sql: ${TABLE}.docass_username ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [docass_id, docass_username]
  }
}
