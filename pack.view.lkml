view: pack {
  sql_table_name: public.pack ;;

  dimension: pack_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.pack_id ;;
  }

  dimension: pack_head_id {
    type: number
    sql: ${TABLE}.pack_head_id ;;
  }

  dimension: pack_head_type {
    type: string
    sql: ${TABLE}.pack_head_type ;;
  }

  dimension: pack_printed {
    type: yesno
    sql: ${TABLE}.pack_printed ;;
  }

  dimension: pack_shiphead_id {
    type: number
    sql: ${TABLE}.pack_shiphead_id ;;
  }

  measure: count {
    type: count
    drill_fields: [pack_id]
  }
}
