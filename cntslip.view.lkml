view: cntslip {
  sql_table_name: public.cntslip ;;

  dimension: cntslip_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.cntslip_id ;;
  }

  dimension: cntslip_cnttag_id {
    type: number
    sql: ${TABLE}.cntslip_cnttag_id ;;
  }

  dimension: cntslip_comments {
    type: string
    sql: ${TABLE}.cntslip_comments ;;
  }

  dimension_group: cntslip_entered {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.cntslip_entered ;;
  }

  dimension: cntslip_location_id {
    type: number
    sql: ${TABLE}.cntslip_location_id ;;
  }

  dimension: cntslip_lotserial {
    type: string
    sql: ${TABLE}.cntslip_lotserial ;;
  }

  dimension_group: cntslip_lotserial_expiration {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cntslip_lotserial_expiration ;;
  }

  dimension_group: cntslip_lotserial_warrpurc {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cntslip_lotserial_warrpurc ;;
  }

  dimension: cntslip_number {
    type: string
    sql: ${TABLE}.cntslip_number ;;
  }

  dimension: cntslip_posted {
    type: yesno
    sql: ${TABLE}.cntslip_posted ;;
  }

  dimension: cntslip_qty {
    type: number
    sql: ${TABLE}.cntslip_qty ;;
  }

  dimension: cntslip_username {
    type: string
    sql: ${TABLE}.cntslip_username ;;
  }

  measure: count {
    type: count
    drill_fields: [cntslip_id, cntslip_username]
  }
}
