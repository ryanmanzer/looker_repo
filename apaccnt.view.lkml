view: apaccnt {
  sql_table_name: public.apaccnt ;;

  dimension: apaccnt_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.apaccnt_id ;;
  }

  dimension: apaccnt_ap_accnt_id {
    type: number
    sql: ${TABLE}.apaccnt_ap_accnt_id ;;
  }

  dimension: apaccnt_discount_accnt_id {
    type: number
    sql: ${TABLE}.apaccnt_discount_accnt_id ;;
  }

  dimension: apaccnt_prepaid_accnt_id {
    type: number
    sql: ${TABLE}.apaccnt_prepaid_accnt_id ;;
  }

  dimension: apaccnt_vendtype {
    type: string
    sql: ${TABLE}.apaccnt_vendtype ;;
  }

  dimension: apaccnt_vendtype_id {
    type: number
    sql: ${TABLE}.apaccnt_vendtype_id ;;
  }

  measure: count {
    type: count
    drill_fields: [apaccnt_id]
  }
}
