view: apchk {
  sql_table_name: public.apchk ;;

  dimension: apchk_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.apchk_id ;;
  }

  dimension: apchk_amount {
    type: number
    sql: ${TABLE}.apchk_amount ;;
  }

  dimension: apchk_bankaccnt_id {
    type: number
    sql: ${TABLE}.apchk_bankaccnt_id ;;
  }

  dimension_group: apchk_checkdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.apchk_checkdate ;;
  }

  dimension: apchk_curr_id {
    type: number
    sql: ${TABLE}.apchk_curr_id ;;
  }

  dimension: apchk_deleted {
    type: yesno
    sql: ${TABLE}.apchk_deleted ;;
  }

  dimension: apchk_expcat_id {
    type: number
    sql: ${TABLE}.apchk_expcat_id ;;
  }

  dimension: apchk_for {
    type: string
    sql: ${TABLE}.apchk_for ;;
  }

  dimension: apchk_journalnumber {
    type: number
    sql: ${TABLE}.apchk_journalnumber ;;
  }

  dimension: apchk_misc {
    type: yesno
    sql: ${TABLE}.apchk_misc ;;
  }

  dimension: apchk_notes {
    type: string
    sql: ${TABLE}.apchk_notes ;;
  }

  dimension: apchk_number {
    type: number
    sql: ${TABLE}.apchk_number ;;
  }

  dimension: apchk_posted {
    type: yesno
    sql: ${TABLE}.apchk_posted ;;
  }

  dimension: apchk_printed {
    type: yesno
    sql: ${TABLE}.apchk_printed ;;
  }

  dimension: apchk_rec {
    type: yesno
    sql: ${TABLE}.apchk_rec ;;
  }

  dimension: apchk_replaced {
    type: yesno
    sql: ${TABLE}.apchk_replaced ;;
  }

  dimension: apchk_vend_id {
    type: number
    sql: ${TABLE}.apchk_vend_id ;;
  }

  dimension: apchk_void {
    type: yesno
    sql: ${TABLE}.apchk_void ;;
  }

  measure: count {
    type: count
    drill_fields: [apchk_id]
  }
}
