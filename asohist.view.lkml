view: asohist {
  sql_table_name: public.asohist ;;

  dimension: asohist_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.asohist_id ;;
  }

  dimension: asohist_billtoaddress1 {
    type: string
    sql: ${TABLE}.asohist_billtoaddress1 ;;
  }

  dimension: asohist_billtoaddress2 {
    type: string
    sql: ${TABLE}.asohist_billtoaddress2 ;;
  }

  dimension: asohist_billtoaddress3 {
    type: string
    sql: ${TABLE}.asohist_billtoaddress3 ;;
  }

  dimension: asohist_billtocity {
    type: string
    sql: ${TABLE}.asohist_billtocity ;;
  }

  dimension: asohist_billtoname {
    type: string
    sql: ${TABLE}.asohist_billtoname ;;
  }

  dimension: asohist_billtostate {
    type: string
    sql: ${TABLE}.asohist_billtostate ;;
  }

  dimension: asohist_billtozip {
    type: string
    sql: ${TABLE}.asohist_billtozip ;;
  }

  dimension: asohist_commission {
    type: number
    sql: ${TABLE}.asohist_commission ;;
  }

  dimension: asohist_commissionpaid {
    type: yesno
    sql: ${TABLE}.asohist_commissionpaid ;;
  }

  dimension: asohist_curr_id {
    type: number
    sql: ${TABLE}.asohist_curr_id ;;
  }

  dimension: asohist_cust_id {
    type: number
    sql: ${TABLE}.asohist_cust_id ;;
  }

  dimension: asohist_doctype {
    type: string
    sql: ${TABLE}.asohist_doctype ;;
  }

  dimension_group: asohist_duedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.asohist_duedate ;;
  }

  dimension: asohist_imported {
    type: yesno
    sql: ${TABLE}.asohist_imported ;;
  }

  dimension_group: asohist_invcdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.asohist_invcdate ;;
  }

  dimension: asohist_invcnumber {
    type: string
    sql: ${TABLE}.asohist_invcnumber ;;
  }

  dimension: asohist_itemsite_id {
    type: number
    sql: ${TABLE}.asohist_itemsite_id ;;
  }

  dimension: asohist_misc_descrip {
    type: string
    sql: ${TABLE}.asohist_misc_descrip ;;
  }

  dimension: asohist_misc_id {
    type: number
    sql: ${TABLE}.asohist_misc_id ;;
  }

  dimension: asohist_misc_type {
    type: string
    sql: ${TABLE}.asohist_misc_type ;;
  }

  dimension_group: asohist_orderdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.asohist_orderdate ;;
  }

  dimension: asohist_ordernumber {
    type: string
    sql: ${TABLE}.asohist_ordernumber ;;
  }

  dimension: asohist_ponumber {
    type: string
    sql: ${TABLE}.asohist_ponumber ;;
  }

  dimension_group: asohist_promisedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.asohist_promisedate ;;
  }

  dimension: asohist_qtyshipped {
    type: number
    sql: ${TABLE}.asohist_qtyshipped ;;
  }

  dimension: asohist_salesrep_id {
    type: number
    sql: ${TABLE}.asohist_salesrep_id ;;
  }

  dimension_group: asohist_shipdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.asohist_shipdate ;;
  }

  dimension: asohist_shipto_id {
    type: number
    sql: ${TABLE}.asohist_shipto_id ;;
  }

  dimension: asohist_shiptoaddress1 {
    type: string
    sql: ${TABLE}.asohist_shiptoaddress1 ;;
  }

  dimension: asohist_shiptoaddress2 {
    type: string
    sql: ${TABLE}.asohist_shiptoaddress2 ;;
  }

  dimension: asohist_shiptoaddress3 {
    type: string
    sql: ${TABLE}.asohist_shiptoaddress3 ;;
  }

  dimension: asohist_shiptocity {
    type: string
    sql: ${TABLE}.asohist_shiptocity ;;
  }

  dimension: asohist_shiptoname {
    type: string
    sql: ${TABLE}.asohist_shiptoname ;;
  }

  dimension: asohist_shiptostate {
    type: string
    sql: ${TABLE}.asohist_shiptostate ;;
  }

  dimension: asohist_shiptozip {
    type: string
    sql: ${TABLE}.asohist_shiptozip ;;
  }

  dimension: asohist_shipvia {
    type: string
    sql: ${TABLE}.asohist_shipvia ;;
  }

  dimension: asohist_taxtype_id {
    type: number
    sql: ${TABLE}.asohist_taxtype_id ;;
  }

  dimension: asohist_taxzone_id {
    type: number
    sql: ${TABLE}.asohist_taxzone_id ;;
  }

  dimension: asohist_unitcost {
    type: number
    sql: ${TABLE}.asohist_unitcost ;;
  }

  dimension: asohist_unitprice {
    type: number
    sql: ${TABLE}.asohist_unitprice ;;
  }

  measure: count {
    type: count
    drill_fields: [asohist_id, asohist_billtoname, asohist_shiptoname]
  }
}
