view: taxrate {
  sql_table_name: public.taxrate ;;

  dimension: taxrate_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.taxrate_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: taxrate_amount {
    type: number
    sql: ${TABLE}.taxrate_amount ;;
  }

  dimension: taxrate_curr_id {
    type: number
    sql: ${TABLE}.taxrate_curr_id ;;
  }

  dimension_group: taxrate_effective {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.taxrate_effective ;;
  }

  dimension_group: taxrate_expires {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.taxrate_expires ;;
  }

  dimension: taxrate_percent {
    type: number
    sql: ${TABLE}.taxrate_percent ;;
  }

  dimension: taxrate_tax_id {
    type: number
    sql: ${TABLE}.taxrate_tax_id ;;
  }

  measure: count {
    type: count
    drill_fields: [taxrate_id]
  }
}
