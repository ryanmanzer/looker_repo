view: bankrecimport {
  sql_table_name: public.bankrecimport ;;

  dimension: bankrecimport_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.bankrecimport_id ;;
  }

  dimension: bankrecimport_comment {
    type: string
    sql: ${TABLE}.bankrecimport_comment ;;
  }

  dimension: bankrecimport_credit_amount {
    type: number
    sql: ${TABLE}.bankrecimport_credit_amount ;;
  }

  dimension: bankrecimport_curr_rate {
    type: number
    sql: ${TABLE}.bankrecimport_curr_rate ;;
  }

  dimension: bankrecimport_debit_amount {
    type: number
    sql: ${TABLE}.bankrecimport_debit_amount ;;
  }

  dimension: bankrecimport_descrip {
    type: string
    sql: ${TABLE}.bankrecimport_descrip ;;
  }

  dimension_group: bankrecimport_effdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.bankrecimport_effdate ;;
  }

  dimension: bankrecimport_reference {
    type: string
    sql: ${TABLE}.bankrecimport_reference ;;
  }

  measure: count {
    type: count
    drill_fields: [bankrecimport_id]
  }
}
