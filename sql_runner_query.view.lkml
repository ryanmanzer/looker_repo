view: sql_runner_query {
  derived_table: {
    sql: SELECT AVG(order_total) avg_order,cohist_shiptostate FROM (SELECT SUM(cohist_qtyshipped*cohist_unitprice) AS order_total, cohist_ordernumber, cohist_shiptostate FROM cohist GROUP BY cohist_ordernumber, cohist_shiptostate) AS foo GROUP BY cohist_shiptostate ORDER BY avg_order DESC
      ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: avg_order {
    type: number
    sql: ${TABLE}.avg_order ;;
  }

  dimension: cohist_shiptostate {
    type: string
    sql: ${TABLE}.cohist_shiptostate ;;
  }

  set: detail {
    fields: [avg_order, cohist_shiptostate]
  }
}
