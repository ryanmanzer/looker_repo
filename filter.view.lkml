view: filter {
  sql_table_name: public.filter ;;

  dimension: filter_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.filter_id ;;
  }

  dimension: filter_name {
    type: string
    sql: ${TABLE}.filter_name ;;
  }

  dimension: filter_screen {
    type: string
    sql: ${TABLE}.filter_screen ;;
  }

  dimension: filter_selected {
    type: yesno
    sql: ${TABLE}.filter_selected ;;
  }

  dimension: filter_username {
    type: string
    sql: ${TABLE}.filter_username ;;
  }

  dimension: filter_value {
    type: string
    sql: ${TABLE}.filter_value ;;
  }

  measure: count {
    type: count
    drill_fields: [filter_id, filter_name, filter_username]
  }
}
