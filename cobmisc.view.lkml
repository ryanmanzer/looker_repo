view: cobmisc {
  sql_table_name: public.cobmisc ;;

  dimension: cobmisc_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.cobmisc_id ;;
  }

  dimension: cobmisc_closeorder {
    type: yesno
    sql: ${TABLE}.cobmisc_closeorder ;;
  }

  dimension: cobmisc_cohead_id {
    type: number
    sql: ${TABLE}.cobmisc_cohead_id ;;
  }

  dimension: cobmisc_curr_id {
    type: number
    sql: ${TABLE}.cobmisc_curr_id ;;
  }

  dimension: cobmisc_freight {
    type: number
    sql: ${TABLE}.cobmisc_freight ;;
  }

  dimension_group: cobmisc_invcdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cobmisc_invcdate ;;
  }

  dimension: cobmisc_invchead_id {
    type: number
    sql: ${TABLE}.cobmisc_invchead_id ;;
  }

  dimension: cobmisc_invcnumber {
    type: number
    sql: ${TABLE}.cobmisc_invcnumber ;;
  }

  dimension: cobmisc_misc {
    type: number
    sql: ${TABLE}.cobmisc_misc ;;
  }

  dimension: cobmisc_misc_accnt_id {
    type: number
    sql: ${TABLE}.cobmisc_misc_accnt_id ;;
  }

  dimension: cobmisc_misc_descrip {
    type: string
    sql: ${TABLE}.cobmisc_misc_descrip ;;
  }

  dimension: cobmisc_notes {
    type: string
    sql: ${TABLE}.cobmisc_notes ;;
  }

  dimension: cobmisc_payment {
    type: number
    sql: ${TABLE}.cobmisc_payment ;;
  }

  dimension: cobmisc_paymentref {
    type: string
    sql: ${TABLE}.cobmisc_paymentref ;;
  }

  dimension: cobmisc_posted {
    type: yesno
    sql: ${TABLE}.cobmisc_posted ;;
  }

  dimension_group: cobmisc_shipdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cobmisc_shipdate ;;
  }

  dimension: cobmisc_shipvia {
    type: string
    sql: ${TABLE}.cobmisc_shipvia ;;
  }

  dimension: cobmisc_taxtype_id {
    type: number
    sql: ${TABLE}.cobmisc_taxtype_id ;;
  }

  dimension: cobmisc_taxzone_id {
    type: number
    sql: ${TABLE}.cobmisc_taxzone_id ;;
  }

  measure: count {
    type: count
    drill_fields: [cobmisc_id]
  }
}
