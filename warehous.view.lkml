view: warehous {
  sql_table_name: public.warehous ;;

  dimension: warehous_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.warehous_id ;;
  }

  dimension: warehous_active {
    type: yesno
    sql: ${TABLE}.warehous_active ;;
  }

  dimension: warehous_addr1 {
    type: string
    sql: ${TABLE}.warehous_addr1 ;;
  }

  dimension: warehous_addr2 {
    type: string
    sql: ${TABLE}.warehous_addr2 ;;
  }

  dimension: warehous_addr3 {
    type: string
    sql: ${TABLE}.warehous_addr3 ;;
  }

  dimension: warehous_addr4 {
    type: string
    sql: ${TABLE}.warehous_addr4 ;;
  }

  dimension: warehous_aislealpha {
    type: yesno
    sql: ${TABLE}.warehous_aislealpha ;;
  }

  dimension: warehous_aislesize {
    type: number
    sql: ${TABLE}.warehous_aislesize ;;
  }

  dimension: warehous_binalpha {
    type: yesno
    sql: ${TABLE}.warehous_binalpha ;;
  }

  dimension: warehous_binsize {
    type: number
    sql: ${TABLE}.warehous_binsize ;;
  }

  dimension: warehous_bol_number {
    type: number
    sql: ${TABLE}.warehous_bol_number ;;
  }

  dimension: warehous_bol_prefix {
    type: string
    sql: ${TABLE}.warehous_bol_prefix ;;
  }

  dimension: warehous_city {
    type: string
    sql: ${TABLE}.warehous_city ;;
  }

  dimension: warehous_code {
    type: string
    sql: ${TABLE}.warehous_code ;;
  }

  dimension: warehous_country {
    type: string
    sql: ${TABLE}.warehous_country ;;
  }

  dimension: warehous_counttag_number {
    type: number
    sql: ${TABLE}.warehous_counttag_number ;;
  }

  dimension: warehous_counttag_prefix {
    type: string
    sql: ${TABLE}.warehous_counttag_prefix ;;
  }

  dimension: warehous_default_accnt_id {
    type: number
    sql: ${TABLE}.warehous_default_accnt_id ;;
  }

  dimension: warehous_descrip {
    type: string
    sql: ${TABLE}.warehous_descrip ;;
  }

  dimension: warehous_enforcearbl {
    type: yesno
    sql: ${TABLE}.warehous_enforcearbl ;;
  }

  dimension: warehous_fob {
    type: string
    sql: ${TABLE}.warehous_fob ;;
  }

  dimension: warehous_locationalpha {
    type: yesno
    sql: ${TABLE}.warehous_locationalpha ;;
  }

  dimension: warehous_locationsize {
    type: number
    sql: ${TABLE}.warehous_locationsize ;;
  }

  dimension: warehous_rackalpha {
    type: yesno
    sql: ${TABLE}.warehous_rackalpha ;;
  }

  dimension: warehous_racksize {
    type: number
    sql: ${TABLE}.warehous_racksize ;;
  }

  dimension: warehous_shipping {
    type: yesno
    sql: ${TABLE}.warehous_shipping ;;
  }

  dimension: warehous_shipping_commission {
    type: number
    sql: ${TABLE}.warehous_shipping_commission ;;
  }

  dimension: warehous_sitetype_id {
    type: number
    sql: ${TABLE}.warehous_sitetype_id ;;
  }

  dimension: warehous_state {
    type: string
    sql: ${TABLE}.warehous_state ;;
  }

  dimension: warehous_useslips {
    type: yesno
    sql: ${TABLE}.warehous_useslips ;;
  }

  dimension: warehous_usezones {
    type: yesno
    sql: ${TABLE}.warehous_usezones ;;
  }

  dimension: warehous_zip {
    type: string
    sql: ${TABLE}.warehous_zip ;;
  }

  measure: count {
    type: count
    drill_fields: [warehous_id, saleshistory.count, saleshistorymisc.count, whsinfo.count]
  }
}
