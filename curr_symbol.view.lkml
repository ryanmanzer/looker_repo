view: curr_symbol {
  sql_table_name: public.curr_symbol ;;

  dimension: curr_abbr {
    type: string
    sql: ${TABLE}.curr_abbr ;;
  }

  dimension: curr_base {
    type: yesno
    sql: ${TABLE}.curr_base ;;
  }

  dimension: curr_id {
    type: number
    sql: ${TABLE}.curr_id ;;
  }

  dimension: curr_name {
    type: string
    sql: ${TABLE}.curr_name ;;
  }

  dimension: curr_symbol {
    type: string
    sql: ${TABLE}.curr_symbol ;;
  }

  measure: count {
    type: count
    drill_fields: [curr_name]
  }
}
