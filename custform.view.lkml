view: custform {
  sql_table_name: public.custform ;;

  dimension: custform_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.custform_id ;;
  }

  dimension: custform_creditmemo_report_id {
    type: number
    sql: ${TABLE}.custform_creditmemo_report_id ;;
  }

  dimension: custform_creditmemo_report_name {
    type: string
    sql: ${TABLE}.custform_creditmemo_report_name ;;
  }

  dimension: custform_custtype {
    type: string
    sql: ${TABLE}.custform_custtype ;;
  }

  dimension: custform_custtype_id {
    type: number
    sql: ${TABLE}.custform_custtype_id ;;
  }

  dimension: custform_invoice_report_id {
    type: number
    sql: ${TABLE}.custform_invoice_report_id ;;
  }

  dimension: custform_invoice_report_name {
    type: string
    sql: ${TABLE}.custform_invoice_report_name ;;
  }

  dimension: custform_packinglist_report_id {
    type: number
    sql: ${TABLE}.custform_packinglist_report_id ;;
  }

  dimension: custform_packinglist_report_name {
    type: string
    sql: ${TABLE}.custform_packinglist_report_name ;;
  }

  dimension: custform_quote_report_id {
    type: number
    sql: ${TABLE}.custform_quote_report_id ;;
  }

  dimension: custform_quote_report_name {
    type: string
    sql: ${TABLE}.custform_quote_report_name ;;
  }

  dimension: custform_sopicklist_report_id {
    type: number
    sql: ${TABLE}.custform_sopicklist_report_id ;;
  }

  dimension: custform_sopicklist_report_name {
    type: string
    sql: ${TABLE}.custform_sopicklist_report_name ;;
  }

  dimension: custform_statement_report_id {
    type: number
    sql: ${TABLE}.custform_statement_report_id ;;
  }

  dimension: custform_statement_report_name {
    type: string
    sql: ${TABLE}.custform_statement_report_name ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      custform_id,
      custform_statement_report_name,
      custform_sopicklist_report_name,
      custform_invoice_report_name,
      custform_creditmemo_report_name,
      custform_quote_report_name,
      custform_packinglist_report_name
    ]
  }
}
