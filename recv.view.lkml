view: recv {
  sql_table_name: public.recv ;;

  dimension: recv_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.recv_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: recv_agent_username {
    type: string
    sql: ${TABLE}.recv_agent_username ;;
  }

  dimension_group: recv {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.recv_date ;;
  }

  dimension_group: recv_duedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.recv_duedate ;;
  }

  dimension: recv_freight {
    type: number
    sql: ${TABLE}.recv_freight ;;
  }

  dimension: recv_freight_curr_id {
    type: number
    sql: ${TABLE}.recv_freight_curr_id ;;
  }

  dimension_group: recv_gldistdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.recv_gldistdate ;;
  }

  dimension: recv_invoiced {
    type: yesno
    sql: ${TABLE}.recv_invoiced ;;
  }

  dimension: recv_itemsite_id {
    type: number
    sql: ${TABLE}.recv_itemsite_id ;;
  }

  dimension: recv_notes {
    type: string
    sql: ${TABLE}.recv_notes ;;
  }

  dimension: recv_order_number {
    type: string
    sql: ${TABLE}.recv_order_number ;;
  }

  dimension: recv_order_type {
    type: string
    sql: ${TABLE}.recv_order_type ;;
  }

  dimension: recv_orderitem_id {
    type: number
    sql: ${TABLE}.recv_orderitem_id ;;
  }

  dimension: recv_posted {
    type: yesno
    sql: ${TABLE}.recv_posted ;;
  }

  dimension: recv_purchcost {
    type: number
    sql: ${TABLE}.recv_purchcost ;;
  }

  dimension: recv_purchcost_curr_id {
    type: number
    sql: ${TABLE}.recv_purchcost_curr_id ;;
  }

  dimension: recv_qty {
    type: number
    sql: ${TABLE}.recv_qty ;;
  }

  dimension: recv_recvcost {
    type: number
    sql: ${TABLE}.recv_recvcost ;;
  }

  dimension: recv_recvcost_curr_id {
    type: number
    sql: ${TABLE}.recv_recvcost_curr_id ;;
  }

  dimension_group: recv_rlsd_duedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.recv_rlsd_duedate ;;
  }

  dimension: recv_splitfrom_id {
    type: number
    sql: ${TABLE}.recv_splitfrom_id ;;
  }

  dimension: recv_trans_usr_name {
    type: string
    sql: ${TABLE}.recv_trans_usr_name ;;
  }

  dimension: recv_value {
    type: number
    sql: ${TABLE}.recv_value ;;
  }

  dimension: recv_vend_id {
    type: number
    sql: ${TABLE}.recv_vend_id ;;
  }

  dimension: recv_vend_item_descrip {
    type: string
    sql: ${TABLE}.recv_vend_item_descrip ;;
  }

  dimension: recv_vend_item_number {
    type: string
    sql: ${TABLE}.recv_vend_item_number ;;
  }

  dimension: recv_vend_uom {
    type: string
    sql: ${TABLE}.recv_vend_uom ;;
  }

  dimension: recv_vohead_id {
    type: number
    sql: ${TABLE}.recv_vohead_id ;;
  }

  dimension: recv_voitem_id {
    type: number
    sql: ${TABLE}.recv_voitem_id ;;
  }

  measure: count {
    type: count
    drill_fields: [recv_id, recv_agent_username, recv_trans_usr_name]
  }
}
