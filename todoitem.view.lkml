view: todoitem {
  sql_table_name: public.todoitem ;;

  dimension: todoitem_recurring_todoitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.todoitem_recurring_todoitem_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: todoitem_active {
    type: yesno
    sql: ${TABLE}.todoitem_active ;;
  }

  dimension_group: todoitem_assigned {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.todoitem_assigned_date ;;
  }

  dimension: todoitem_cntct_id {
    type: number
    sql: ${TABLE}.todoitem_cntct_id ;;
  }

  dimension_group: todoitem_completed {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.todoitem_completed_date ;;
  }

  dimension: todoitem_creator_username {
    type: string
    sql: ${TABLE}.todoitem_creator_username ;;
  }

  dimension: todoitem_crmacct_id {
    type: number
    sql: ${TABLE}.todoitem_crmacct_id ;;
  }

  dimension: todoitem_description {
    type: string
    sql: ${TABLE}.todoitem_description ;;
  }

  dimension_group: todoitem_due {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.todoitem_due_date ;;
  }

  dimension: todoitem_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.todoitem_id ;;
  }

  dimension: todoitem_incdt_id {
    type: number
    sql: ${TABLE}.todoitem_incdt_id ;;
  }

  dimension: todoitem_name {
    type: string
    sql: ${TABLE}.todoitem_name ;;
  }

  dimension: todoitem_notes {
    type: string
    sql: ${TABLE}.todoitem_notes ;;
  }

  dimension: todoitem_ophead_id {
    type: number
    sql: ${TABLE}.todoitem_ophead_id ;;
  }

  dimension: todoitem_owner_username {
    type: string
    sql: ${TABLE}.todoitem_owner_username ;;
  }

  dimension: todoitem_priority_id {
    type: number
    sql: ${TABLE}.todoitem_priority_id ;;
  }

  dimension: todoitem_seq {
    type: number
    sql: ${TABLE}.todoitem_seq ;;
  }

  dimension_group: todoitem_start {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.todoitem_start_date ;;
  }

  dimension: todoitem_status {
    type: string
    sql: ${TABLE}.todoitem_status ;;
  }

  dimension: todoitem_username {
    type: string
    sql: ${TABLE}.todoitem_username ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      todoitem_recurring_todoitem_id,
      todoitem_name,
      todoitem_creator_username,
      todoitem_owner_username,
      todoitem_username,
      todoitem.todoitem_name,
      todoitem.todoitem_creator_username,
      todoitem.todoitem_owner_username,
      todoitem.todoitem_username,
      todoitem.todoitem_recurring_todoitem_id,
      todoitem.count
    ]
  }
}
