view: cashrcptitem {
  sql_table_name: public.cashrcptitem ;;

  dimension: cashrcptitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.cashrcptitem_id ;;
  }

  dimension: cashrcptitem_amount {
    type: number
    sql: ${TABLE}.cashrcptitem_amount ;;
  }

  dimension: cashrcptitem_applied {
    type: yesno
    sql: ${TABLE}.cashrcptitem_applied ;;
  }

  dimension: cashrcptitem_aropen_id {
    type: number
    sql: ${TABLE}.cashrcptitem_aropen_id ;;
  }

  dimension: cashrcptitem_cashrcpt_id {
    type: number
    sql: ${TABLE}.cashrcptitem_cashrcpt_id ;;
  }

  dimension: cashrcptitem_discount {
    type: number
    sql: ${TABLE}.cashrcptitem_discount ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [cashrcptitem_id]
  }
}
