view: uiform {
  sql_table_name: public.uiform ;;

  dimension: uiform_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.uiform_id ;;
  }

  dimension: uiform_enabled {
    type: yesno
    sql: ${TABLE}.uiform_enabled ;;
  }

  dimension: uiform_name {
    type: string
    sql: ${TABLE}.uiform_name ;;
  }

  dimension: uiform_notes {
    type: string
    sql: ${TABLE}.uiform_notes ;;
  }

  dimension: uiform_order {
    type: number
    sql: ${TABLE}.uiform_order ;;
  }

  dimension: uiform_source {
    type: string
    sql: ${TABLE}.uiform_source ;;
  }

  measure: count {
    type: count
    drill_fields: [uiform_id, uiform_name]
  }
}
