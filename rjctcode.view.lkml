view: rjctcode {
  sql_table_name: public.rjctcode ;;

  dimension: rjctcode_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.rjctcode_id ;;
  }

  dimension: rjctcode_code {
    type: string
    sql: ${TABLE}.rjctcode_code ;;
  }

  dimension: rjctcode_descrip {
    type: string
    sql: ${TABLE}.rjctcode_descrip ;;
  }

  measure: count {
    type: count
    drill_fields: [rjctcode_id]
  }
}
