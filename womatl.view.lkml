view: womatl {
  sql_table_name: public.womatl ;;

  dimension: womatl_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.womatl_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: womatl_bomitem_id {
    type: number
    sql: ${TABLE}.womatl_bomitem_id ;;
  }

  dimension: womatl_cost {
    type: number
    sql: ${TABLE}.womatl_cost ;;
  }

  dimension: womatl_createwo {
    type: yesno
    sql: ${TABLE}.womatl_createwo ;;
  }

  dimension_group: womatl_duedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.womatl_duedate ;;
  }

  dimension: womatl_imported {
    type: yesno
    sql: ${TABLE}.womatl_imported ;;
  }

  dimension: womatl_issuemethod {
    type: string
    sql: ${TABLE}.womatl_issuemethod ;;
  }

  dimension: womatl_issuewo {
    type: yesno
    sql: ${TABLE}.womatl_issuewo ;;
  }

  dimension: womatl_itemsite_id {
    type: number
    sql: ${TABLE}.womatl_itemsite_id ;;
  }

  dimension_group: womatl_lastissue {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.womatl_lastissue ;;
  }

  dimension_group: womatl_lastreturn {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.womatl_lastreturn ;;
  }

  dimension: womatl_notes {
    type: string
    sql: ${TABLE}.womatl_notes ;;
  }

  dimension: womatl_picklist {
    type: yesno
    sql: ${TABLE}.womatl_picklist ;;
  }

  dimension: womatl_price {
    type: number
    sql: ${TABLE}.womatl_price ;;
  }

  dimension: womatl_qtyfxd {
    type: number
    sql: ${TABLE}.womatl_qtyfxd ;;
  }

  dimension: womatl_qtyiss {
    type: number
    sql: ${TABLE}.womatl_qtyiss ;;
  }

  dimension: womatl_qtyper {
    type: number
    sql: ${TABLE}.womatl_qtyper ;;
  }

  dimension: womatl_qtyreq {
    type: number
    sql: ${TABLE}.womatl_qtyreq ;;
  }

  dimension: womatl_qtywipscrap {
    type: number
    sql: ${TABLE}.womatl_qtywipscrap ;;
  }

  dimension: womatl_ref {
    type: string
    sql: ${TABLE}.womatl_ref ;;
  }

  dimension: womatl_schedatwooper {
    type: yesno
    sql: ${TABLE}.womatl_schedatwooper ;;
  }

  dimension: womatl_scrap {
    type: number
    sql: ${TABLE}.womatl_scrap ;;
  }

  dimension: womatl_scrapvalue {
    type: number
    sql: ${TABLE}.womatl_scrapvalue ;;
  }

  dimension: womatl_status {
    type: string
    sql: ${TABLE}.womatl_status ;;
  }

  dimension: womatl_uom_id {
    type: number
    sql: ${TABLE}.womatl_uom_id ;;
  }

  dimension: womatl_wo_id {
    type: number
    sql: ${TABLE}.womatl_wo_id ;;
  }

  dimension: womatl_wooper_id {
    type: number
    sql: ${TABLE}.womatl_wooper_id ;;
  }

  measure: count {
    type: count
    drill_fields: [womatl_id]
  }
}
