view: pkghead {
  sql_table_name: public.pkghead ;;

  dimension: pkghead_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.pkghead_id ;;
  }

  dimension_group: pkghead_created {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.pkghead_created ;;
  }

  dimension: pkghead_descrip {
    type: string
    sql: ${TABLE}.pkghead_descrip ;;
  }

  dimension: pkghead_developer {
    type: string
    sql: ${TABLE}.pkghead_developer ;;
  }

  dimension: pkghead_indev {
    type: yesno
    sql: ${TABLE}.pkghead_indev ;;
  }

  dimension: pkghead_name {
    type: string
    sql: ${TABLE}.pkghead_name ;;
  }

  dimension: pkghead_notes {
    type: string
    sql: ${TABLE}.pkghead_notes ;;
  }

  dimension_group: pkghead_updated {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.pkghead_updated ;;
  }

  dimension: pkghead_version {
    type: string
    sql: ${TABLE}.pkghead_version ;;
  }

  measure: count {
    type: count
    drill_fields: [pkghead_id, pkghead_name]
  }
}
