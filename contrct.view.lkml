view: contrct {
  sql_table_name: public.contrct ;;

  dimension: contrct_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.contrct_id ;;
  }

  dimension: contrct_descrip {
    type: string
    sql: ${TABLE}.contrct_descrip ;;
  }

  dimension_group: contrct_effective {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.contrct_effective ;;
  }

  dimension_group: contrct_expires {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.contrct_expires ;;
  }

  dimension: contrct_note {
    type: string
    sql: ${TABLE}.contrct_note ;;
  }

  dimension: contrct_number {
    type: string
    sql: ${TABLE}.contrct_number ;;
  }

  dimension: contrct_vend_id {
    type: number
    sql: ${TABLE}.contrct_vend_id ;;
  }

  measure: count {
    type: count
    drill_fields: [contrct_id]
  }
}
