view: subaccnt {
  sql_table_name: public.subaccnt ;;

  dimension: subaccnt_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.subaccnt_id ;;
  }

  dimension: subaccnt_descrip {
    type: string
    sql: ${TABLE}.subaccnt_descrip ;;
  }

  dimension: subaccnt_number {
    type: string
    sql: ${TABLE}.subaccnt_number ;;
  }

  measure: count {
    type: count
    drill_fields: [subaccnt_id]
  }
}
