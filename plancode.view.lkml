view: plancode {
  sql_table_name: public.plancode ;;

  dimension: plancode_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.plancode_id ;;
  }

  dimension: plancode_code {
    type: string
    sql: ${TABLE}.plancode_code ;;
  }

  dimension: plancode_consumefcst {
    type: yesno
    sql: ${TABLE}.plancode_consumefcst ;;
  }

  dimension: plancode_mpsexplosion {
    type: string
    sql: ${TABLE}.plancode_mpsexplosion ;;
  }

  dimension: plancode_mrpexcp_delete {
    type: yesno
    sql: ${TABLE}.plancode_mrpexcp_delete ;;
  }

  dimension: plancode_mrpexcp_resched {
    type: yesno
    sql: ${TABLE}.plancode_mrpexcp_resched ;;
  }

  dimension: plancode_name {
    type: string
    sql: ${TABLE}.plancode_name ;;
  }

  measure: count {
    type: count
    drill_fields: [plancode_id, plancode_name]
  }
}
