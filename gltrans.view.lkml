view: gltrans {
  sql_table_name: public.gltrans ;;

  dimension: gltrans_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.gltrans_id ;;
  }

  dimension: gltrans_accnt_id {
    type: number
    sql: ${TABLE}.gltrans_accnt_id ;;
  }

  dimension: gltrans_amount {
    type: number
    sql: ${TABLE}.gltrans_amount ;;
  }

  dimension_group: gltrans_created {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.gltrans_created ;;
  }

  dimension_group: gltrans {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.gltrans_date ;;
  }

  dimension: gltrans_deleted {
    type: yesno
    sql: ${TABLE}.gltrans_deleted ;;
  }

  dimension: gltrans_docnumber {
    type: string
    sql: ${TABLE}.gltrans_docnumber ;;
  }

  dimension: gltrans_doctype {
    type: string
    sql: ${TABLE}.gltrans_doctype ;;
  }

  dimension: gltrans_exported {
    type: yesno
    sql: ${TABLE}.gltrans_exported ;;
  }

  dimension: gltrans_journalnumber {
    type: number
    sql: ${TABLE}.gltrans_journalnumber ;;
  }

  dimension: gltrans_misc_id {
    type: number
    sql: ${TABLE}.gltrans_misc_id ;;
  }

  dimension: gltrans_notes {
    type: string
    sql: ${TABLE}.gltrans_notes ;;
  }

  dimension: gltrans_posted {
    type: yesno
    sql: ${TABLE}.gltrans_posted ;;
  }

  dimension: gltrans_rec {
    type: yesno
    sql: ${TABLE}.gltrans_rec ;;
  }

  dimension: gltrans_sequence {
    type: number
    sql: ${TABLE}.gltrans_sequence ;;
  }

  dimension: gltrans_source {
    type: string
    sql: ${TABLE}.gltrans_source ;;
  }

  dimension: gltrans_username {
    type: string
    sql: ${TABLE}.gltrans_username ;;
  }

  measure: count {
    type: count
    drill_fields: [gltrans_id, gltrans_username, gltranssync.count]
  }
}
