view: char {
  sql_table_name: public.char ;;

  dimension: char_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.char_id ;;
  }

  dimension: char_addresses {
    type: yesno
    sql: ${TABLE}.char_addresses ;;
  }

  dimension: char_attributes {
    type: yesno
    sql: ${TABLE}.char_attributes ;;
  }

  dimension: char_contacts {
    type: yesno
    sql: ${TABLE}.char_contacts ;;
  }

  dimension: char_crmaccounts {
    type: yesno
    sql: ${TABLE}.char_crmaccounts ;;
  }

  dimension: char_customers {
    type: yesno
    sql: ${TABLE}.char_customers ;;
  }

  dimension: char_employees {
    type: yesno
    sql: ${TABLE}.char_employees ;;
  }

  dimension: char_incidents {
    type: yesno
    sql: ${TABLE}.char_incidents ;;
  }

  dimension: char_invoices {
    type: yesno
    sql: ${TABLE}.char_invoices ;;
  }

  dimension: char_items {
    type: yesno
    sql: ${TABLE}.char_items ;;
  }

  dimension: char_lotserial {
    type: yesno
    sql: ${TABLE}.char_lotserial ;;
  }

  dimension: char_mask {
    type: string
    sql: ${TABLE}.char_mask ;;
  }

  dimension: char_name {
    type: string
    sql: ${TABLE}.char_name ;;
  }

  dimension: char_notes {
    type: string
    sql: ${TABLE}.char_notes ;;
  }

  dimension: char_opportunity {
    type: yesno
    sql: ${TABLE}.char_opportunity ;;
  }

  dimension: char_options {
    type: yesno
    sql: ${TABLE}.char_options ;;
  }

  dimension: char_order {
    type: number
    sql: ${TABLE}.char_order ;;
  }

  dimension: char_projects {
    type: yesno
    sql: ${TABLE}.char_projects ;;
  }

  dimension: char_purchaseorders {
    type: yesno
    sql: ${TABLE}.char_purchaseorders ;;
  }

  dimension: char_quotes {
    type: yesno
    sql: ${TABLE}.char_quotes ;;
  }

  dimension: char_salesorders {
    type: yesno
    sql: ${TABLE}.char_salesorders ;;
  }

  dimension: char_search {
    type: yesno
    sql: ${TABLE}.char_search ;;
  }

  dimension: char_tasks {
    type: yesno
    sql: ${TABLE}.char_tasks ;;
  }

  dimension: char_type {
    type: number
    sql: ${TABLE}.char_type ;;
  }

  dimension: char_validator {
    type: string
    sql: ${TABLE}.char_validator ;;
  }

  dimension: char_vendors {
    type: yesno
    sql: ${TABLE}.char_vendors ;;
  }

  dimension: char_vouchers {
    type: yesno
    sql: ${TABLE}.char_vouchers ;;
  }

  measure: count {
    type: count
    drill_fields: [char_id, char_name]
  }
}
