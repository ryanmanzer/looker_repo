view: shipchrg {
  sql_table_name: public.shipchrg ;;

  dimension: shipchrg_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.shipchrg_id ;;
  }

  dimension: shipchrg_custfreight {
    type: yesno
    sql: ${TABLE}.shipchrg_custfreight ;;
  }

  dimension: shipchrg_descrip {
    type: string
    sql: ${TABLE}.shipchrg_descrip ;;
  }

  dimension: shipchrg_handling {
    type: string
    sql: ${TABLE}.shipchrg_handling ;;
  }

  dimension: shipchrg_name {
    type: string
    sql: ${TABLE}.shipchrg_name ;;
  }

  measure: count {
    type: count
    drill_fields: [shipchrg_id, shipchrg_name]
  }
}
