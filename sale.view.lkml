view: sale {
  sql_table_name: public.sale ;;

  dimension: sale_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.sale_id ;;
  }

  dimension: sale_descrip {
    type: string
    sql: ${TABLE}.sale_descrip ;;
  }

  dimension_group: sale_enddate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.sale_enddate ;;
  }

  dimension: sale_ipshead_id {
    type: number
    sql: ${TABLE}.sale_ipshead_id ;;
  }

  dimension: sale_name {
    type: string
    sql: ${TABLE}.sale_name ;;
  }

  dimension_group: sale_startdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.sale_startdate ;;
  }

  measure: count {
    type: count
    drill_fields: [sale_id, sale_name]
  }
}
