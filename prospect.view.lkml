view: prospect {
  sql_table_name: public.prospect ;;

  dimension: prospect_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.prospect_id ;;
  }

  dimension: prospect_active {
    type: yesno
    sql: ${TABLE}.prospect_active ;;
  }

  dimension: prospect_cntct_id {
    type: number
    sql: ${TABLE}.prospect_cntct_id ;;
  }

  dimension: prospect_comments {
    type: string
    sql: ${TABLE}.prospect_comments ;;
  }

  dimension_group: prospect_created {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.prospect_created ;;
  }

  dimension: prospect_name {
    type: string
    sql: ${TABLE}.prospect_name ;;
  }

  dimension: prospect_number {
    type: string
    sql: ${TABLE}.prospect_number ;;
  }

  dimension: prospect_salesrep_id {
    type: number
    sql: ${TABLE}.prospect_salesrep_id ;;
  }

  dimension: prospect_taxzone_id {
    type: number
    sql: ${TABLE}.prospect_taxzone_id ;;
  }

  dimension: prospect_warehous_id {
    type: number
    sql: ${TABLE}.prospect_warehous_id ;;
  }

  measure: count {
    type: count
    drill_fields: [prospect_id, prospect_name]
  }
}
