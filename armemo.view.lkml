view: armemo {
  sql_table_name: public.armemo ;;

  dimension: aropen_docnumber {
    type: string
    sql: ${TABLE}.aropen_docnumber ;;
  }

  dimension: aropen_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.aropen_id ;;
  }

  measure: count {
    type: count
    drill_fields: [aropen.aropen_username, aropen.aropen_id]
  }
}
