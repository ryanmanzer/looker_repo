view: period {
  sql_table_name: public.period ;;

  dimension: period_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.period_id ;;
  }

  dimension: period_closed {
    type: yesno
    sql: ${TABLE}.period_closed ;;
  }

  dimension_group: period_end {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.period_end ;;
  }

  dimension: period_freeze {
    type: yesno
    sql: ${TABLE}.period_freeze ;;
  }

  dimension: period_initial {
    type: yesno
    sql: ${TABLE}.period_initial ;;
  }

  dimension: period_name {
    type: string
    sql: ${TABLE}.period_name ;;
  }

  dimension: period_number {
    type: number
    sql: ${TABLE}.period_number ;;
  }

  dimension: period_quarter {
    type: number
    sql: ${TABLE}.period_quarter ;;
  }

  dimension_group: period_start {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.period_start ;;
  }

  dimension: period_yearperiod_id {
    type: number
    sql: ${TABLE}.period_yearperiod_id ;;
  }

  measure: count {
    type: count
    drill_fields: [period_id, period_name]
  }
}
