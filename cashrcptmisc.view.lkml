view: cashrcptmisc {
  sql_table_name: public.cashrcptmisc ;;

  dimension: cashrcptmisc_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.cashrcptmisc_id ;;
  }

  dimension: cashrcptmisc_accnt_id {
    type: number
    sql: ${TABLE}.cashrcptmisc_accnt_id ;;
  }

  dimension: cashrcptmisc_amount {
    type: number
    sql: ${TABLE}.cashrcptmisc_amount ;;
  }

  dimension: cashrcptmisc_cashrcpt_id {
    type: number
    sql: ${TABLE}.cashrcptmisc_cashrcpt_id ;;
  }

  dimension: cashrcptmisc_notes {
    type: string
    sql: ${TABLE}.cashrcptmisc_notes ;;
  }

  measure: count {
    type: count
    drill_fields: [cashrcptmisc_id]
  }
}
