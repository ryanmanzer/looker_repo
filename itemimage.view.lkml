view: itemimage {
  sql_table_name: public.itemimage ;;

  dimension: itemimage_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.itemimage_id ;;
  }

  dimension: itemimage_image_id {
    type: number
    sql: ${TABLE}.itemimage_image_id ;;
  }

  dimension: itemimage_item_id {
    type: number
    sql: ${TABLE}.itemimage_item_id ;;
  }

  dimension: itemimage_purpose {
    type: string
    sql: ${TABLE}.itemimage_purpose ;;
  }

  measure: count {
    type: count
    drill_fields: [itemimage_id]
  }
}
