view: invcnt {
  sql_table_name: public.invcnt ;;

  dimension: invcnt_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.invcnt_id ;;
  }

  dimension: invcnt_cnt_username {
    type: string
    sql: ${TABLE}.invcnt_cnt_username ;;
  }

  dimension_group: invcnt_cntdate {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.invcnt_cntdate ;;
  }

  dimension: invcnt_comments {
    type: string
    sql: ${TABLE}.invcnt_comments ;;
  }

  dimension: invcnt_invhist_id {
    type: number
    sql: ${TABLE}.invcnt_invhist_id ;;
  }

  dimension: invcnt_itemsite_id {
    type: number
    sql: ${TABLE}.invcnt_itemsite_id ;;
  }

  dimension: invcnt_location_id {
    type: number
    sql: ${TABLE}.invcnt_location_id ;;
  }

  dimension: invcnt_matcost {
    type: number
    sql: ${TABLE}.invcnt_matcost ;;
  }

  dimension: invcnt_post_username {
    type: string
    sql: ${TABLE}.invcnt_post_username ;;
  }

  dimension_group: invcnt_postdate {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.invcnt_postdate ;;
  }

  dimension: invcnt_posted {
    type: yesno
    sql: ${TABLE}.invcnt_posted ;;
  }

  dimension: invcnt_priority {
    type: yesno
    sql: ${TABLE}.invcnt_priority ;;
  }

  dimension: invcnt_qoh_after {
    type: number
    sql: ${TABLE}.invcnt_qoh_after ;;
  }

  dimension: invcnt_qoh_before {
    type: number
    sql: ${TABLE}.invcnt_qoh_before ;;
  }

  dimension: invcnt_tag_username {
    type: string
    sql: ${TABLE}.invcnt_tag_username ;;
  }

  dimension_group: invcnt_tagdate {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.invcnt_tagdate ;;
  }

  dimension: invcnt_tagnumber {
    type: string
    sql: ${TABLE}.invcnt_tagnumber ;;
  }

  measure: count {
    type: count
    drill_fields: [invcnt_id, invcnt_post_username, invcnt_tag_username, invcnt_cnt_username]
  }
}
