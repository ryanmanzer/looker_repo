view: orderhead {
  sql_table_name: public.orderhead ;;

  dimension: orderhead_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.orderhead_id ;;
  }

  dimension: orderhead_agent_username {
    type: string
    sql: ${TABLE}.orderhead_agent_username ;;
  }

  dimension: orderhead_curr_id {
    type: number
    sql: ${TABLE}.orderhead_curr_id ;;
  }

  dimension: orderhead_from {
    type: string
    sql: ${TABLE}.orderhead_from ;;
  }

  dimension: orderhead_from_id {
    type: number
    sql: ${TABLE}.orderhead_from_id ;;
  }

  dimension: orderhead_linecount {
    type: number
    sql: ${TABLE}.orderhead_linecount ;;
  }

  dimension: orderhead_number {
    type: string
    sql: ${TABLE}.orderhead_number ;;
  }

  dimension_group: orderhead_orderdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.orderhead_orderdate ;;
  }

  dimension: orderhead_shipvia {
    type: string
    sql: ${TABLE}.orderhead_shipvia ;;
  }

  dimension: orderhead_status {
    type: string
    sql: ${TABLE}.orderhead_status ;;
  }

  dimension: orderhead_to {
    type: string
    sql: ${TABLE}.orderhead_to ;;
  }

  dimension: orderhead_to_id {
    type: number
    sql: ${TABLE}.orderhead_to_id ;;
  }

  dimension: orderhead_type {
    type: string
    sql: ${TABLE}.orderhead_type ;;
  }

  measure: count {
    type: count
    drill_fields: [orderhead_id, orderhead_agent_username]
  }
}
