- dashboard: new_products
  title: New Products
  layout: tile
  tile_size: 200
  refresh: 12 hours


  filters:

  elements:
    - name: q12017_newProductsBar
      title: 'Q1 2017 New Products - Sales by Product'
      type: looker_column
      model: lookml_learning_rm
      explore: new_products
      dimensions: [_newproducts_q1.item_name]
      pivots: [_newproducts_q1.item_name]
      measures: [_newproducts_q1.total_sales]
      sorts: [_newproducts_q1.total_sales desc, _newproducts_q1.item_name]
      limit: '500'
      column_limit: '50'
      query_timezone: America/Los_Angeles
      stacking: ''
      show_value_labels: true
      label_density: 25
      legend_position: center
      x_axis_gridlines: false
      y_axis_gridlines: true
      show_view_names: false
      limit_displayed_rows: false
      y_axis_combined: true
      show_y_axis_labels: false
      show_y_axis_ticks: true
      y_axis_tick_density: default
      y_axis_tick_density_custom: 5
      show_x_axis_label: false
      show_x_axis_ticks: true
      x_axis_scale: auto
      y_axis_scale_mode: linear
      ordering: none
      show_null_labels: false
      show_totals_labels: false
      show_silhouette: false
      totals_color: '#808080'
      column_spacing_ratio: .10
      column_group_spacing_ratio: .20
      colors: ['palette: Santa Cruz']
      series_colors: {}
      label_value_format: $#,###
    - name: B_NewProdByDay
      title: 'New Products (Q1 2017) Sales by Day'
      type: looker_column
      model: lookml_learning_rm
      explore: new_products
      dimensions: [_newproducts_q1.invcdate, _newproducts_q1.item_name]
      pivots: [_newproducts_q1.item_name]
      measures: [_newproducts_q1.total_sales]
      filters:
        _newproducts_q1.total_sales: '>0'
      sorts: [_newproducts_q1.invcdate desc]
      limit: '500'
      column_limit: '50'
      query_timezone: America/Los_Angeles
      stacking: ''
      show_value_labels: true
      label_density: 21
      legend_position: center
      x_axis_gridlines: false
      y_axis_gridlines: true
      show_view_names: false
      limit_displayed_rows: false
      y_axis_combined: true
      show_y_axis_labels: true
      show_y_axis_ticks: true
      y_axis_tick_density: default
      y_axis_tick_density_custom: 5
      show_x_axis_label: true
      show_x_axis_ticks: true
      x_axis_scale: auto
      y_axis_scale_mode: linear
      ordering: none
      show_null_labels: false
      show_totals_labels: false
      show_silhouette: false
      totals_color: '#808080'
      show_row_numbers: true
      truncate_column_names: false
      hide_totals: false
      hide_row_totals: false
      table_theme: editable
      series_types:
        __FILE: lookml_learning_rm/new_products.dashboard.lookml
        __LINE_NUM: 90
      y_axis_value_format: $#,###
      column_spacing_ratio: 0.1
      column_group_spacing_ratio: 0.2
      colors: ['palette: Mixed Dark']
      series_colors:
        __FILE: lookml_learning_rm/new_products.dashboard.lookml
        __LINE_NUM: 95
      series_labels:
        Nordic Flora Probiotic Daily, 60ct - Newproducts Q1 Total Sales: Nordic Flora Probiotic Daily
        Nordic Flora Probiotic Comfort, 30ct - Newproducts Q1 Total Sales: Nordic Flora Probiotic Comfort
        Nordic Flora Probiotic Woman, 60ct - Newproducts Q1 Total Sales: Nordic Flora Probiotic Woman
        __FILE: lookml_learning_rm/new_products.dashboard.lookml
        __LINE_NUM: 97
      x_axis_reversed: true
      label_value_format: $#,###
