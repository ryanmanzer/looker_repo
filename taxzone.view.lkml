view: taxzone {
  sql_table_name: public.taxzone ;;

  dimension: taxzone_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.taxzone_id ;;
  }

  dimension: taxzone_code {
    type: string
    sql: ${TABLE}.taxzone_code ;;
  }

  dimension: taxzone_descrip {
    type: string
    sql: ${TABLE}.taxzone_descrip ;;
  }

  measure: count {
    type: count
    drill_fields: [taxzone_id]
  }
}
