view: opstage {
  sql_table_name: public.opstage ;;

  dimension: opstage_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.opstage_id ;;
  }

  dimension: opstage_descrip {
    type: string
    sql: ${TABLE}.opstage_descrip ;;
  }

  dimension: opstage_name {
    type: string
    sql: ${TABLE}.opstage_name ;;
  }

  dimension: opstage_opinactive {
    type: yesno
    sql: ${TABLE}.opstage_opinactive ;;
  }

  dimension: opstage_order {
    type: number
    sql: ${TABLE}.opstage_order ;;
  }

  measure: count {
    type: count
    drill_fields: [opstage_id, opstage_name]
  }
}
