view: status {
  sql_table_name: public.status ;;

  dimension: status_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.status_id ;;
  }

  dimension: status_code {
    type: string
    sql: ${TABLE}.status_code ;;
  }

  dimension: status_color {
    type: string
    sql: ${TABLE}.status_color ;;
  }

  dimension: status_name {
    type: string
    sql: ${TABLE}.status_name ;;
  }

  dimension: status_seq {
    type: number
    sql: ${TABLE}.status_seq ;;
  }

  dimension: status_type {
    type: string
    sql: ${TABLE}.status_type ;;
  }

  measure: count {
    type: count
    drill_fields: [status_id, status_name]
  }
}
