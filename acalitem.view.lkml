view: acalitem {
  sql_table_name: public.acalitem ;;

  dimension: acalitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.acalitem_id ;;
  }

  dimension: acalitem_calhead_id {
    type: number
    sql: ${TABLE}.acalitem_calhead_id ;;
  }

  dimension: acalitem_name {
    type: string
    sql: ${TABLE}.acalitem_name ;;
  }

  dimension: acalitem_periodlength {
    type: number
    sql: ${TABLE}.acalitem_periodlength ;;
  }

  dimension_group: acalitem_periodstart {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.acalitem_periodstart ;;
  }

  measure: count {
    type: count
    drill_fields: [acalitem_id, acalitem_name]
  }
}
