view: glseries {
  sql_table_name: public.glseries ;;

  dimension: glseries_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.glseries_id ;;
  }

  dimension: glseries_accnt_id {
    type: number
    sql: ${TABLE}.glseries_accnt_id ;;
  }

  dimension: glseries_amount {
    type: number
    sql: ${TABLE}.glseries_amount ;;
  }

  dimension_group: glseries_distdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.glseries_distdate ;;
  }

  dimension: glseries_docnumber {
    type: string
    sql: ${TABLE}.glseries_docnumber ;;
  }

  dimension: glseries_doctype {
    type: string
    sql: ${TABLE}.glseries_doctype ;;
  }

  dimension: glseries_misc_id {
    type: number
    sql: ${TABLE}.glseries_misc_id ;;
  }

  dimension: glseries_notes {
    type: string
    sql: ${TABLE}.glseries_notes ;;
  }

  dimension: glseries_sequence {
    type: number
    sql: ${TABLE}.glseries_sequence ;;
  }

  dimension: glseries_source {
    type: string
    sql: ${TABLE}.glseries_source ;;
  }

  measure: count {
    type: count
    drill_fields: [glseries_id]
  }
}
