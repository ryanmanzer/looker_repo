view: grp {
  sql_table_name: public.grp ;;

  dimension: grp_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.grp_id ;;
  }

  dimension: grp_descrip {
    type: string
    sql: ${TABLE}.grp_descrip ;;
  }

  dimension: grp_name {
    type: string
    sql: ${TABLE}.grp_name ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [grp_id, grp_name]
  }
}
