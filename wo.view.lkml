view: wo {
  sql_table_name: public.wo ;;

  dimension: wo_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.wo_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: wo_adhoc {
    type: yesno
    sql: ${TABLE}.wo_adhoc ;;
  }

  dimension: wo_bom_rev_id {
    type: number
    sql: ${TABLE}.wo_bom_rev_id ;;
  }

  dimension: wo_boo_rev_id {
    type: number
    sql: ${TABLE}.wo_boo_rev_id ;;
  }

  dimension: wo_brdvalue {
    type: number
    sql: ${TABLE}.wo_brdvalue ;;
  }

  dimension: wo_cosmethod {
    type: string
    sql: ${TABLE}.wo_cosmethod ;;
  }

  dimension_group: wo_duedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.wo_duedate ;;
  }

  dimension: wo_imported {
    type: yesno
    sql: ${TABLE}.wo_imported ;;
  }

  dimension: wo_itemcfg_series {
    type: number
    sql: ${TABLE}.wo_itemcfg_series ;;
  }

  dimension: wo_itemsite_id {
    type: number
    sql: ${TABLE}.wo_itemsite_id ;;
  }

  dimension: wo_number {
    type: number
    sql: ${TABLE}.wo_number ;;
  }

  dimension: wo_ordid {
    type: number
    value_format_name: id
    sql: ${TABLE}.wo_ordid ;;
  }

  dimension: wo_ordtype {
    type: string
    sql: ${TABLE}.wo_ordtype ;;
  }

  dimension: wo_postedvalue {
    type: number
    sql: ${TABLE}.wo_postedvalue ;;
  }

  dimension: wo_priority {
    type: number
    sql: ${TABLE}.wo_priority ;;
  }

  dimension: wo_prj_id {
    type: number
    sql: ${TABLE}.wo_prj_id ;;
  }

  dimension: wo_prodnotes {
    type: string
    sql: ${TABLE}.wo_prodnotes ;;
  }

  dimension: wo_qtyord {
    type: number
    sql: ${TABLE}.wo_qtyord ;;
  }

  dimension: wo_qtyrcv {
    type: number
    sql: ${TABLE}.wo_qtyrcv ;;
  }

  dimension_group: wo_startdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.wo_startdate ;;
  }

  dimension: wo_status {
    type: string
    sql: ${TABLE}.wo_status ;;
  }

  dimension: wo_subnumber {
    type: number
    sql: ${TABLE}.wo_subnumber ;;
  }

  dimension: wo_username {
    type: string
    sql: ${TABLE}.wo_username ;;
  }

  dimension: wo_wipvalue {
    type: number
    sql: ${TABLE}.wo_wipvalue ;;
  }

  dimension: wo_womatl_id {
    type: number
    sql: ${TABLE}.wo_womatl_id ;;
  }

  measure: count {
    type: count
    drill_fields: [wo_id, wo_username]
  }
}
