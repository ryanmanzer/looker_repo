view: uom {
  sql_table_name: public.uom ;;

  dimension: uom_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.uom_id ;;
  }

  dimension: uom_descrip {
    type: string
    sql: ${TABLE}.uom_descrip ;;
  }

  dimension: uom_item_dimension {
    type: yesno
    sql: ${TABLE}.uom_item_dimension ;;
  }

  dimension: uom_item_weight {
    type: yesno
    sql: ${TABLE}.uom_item_weight ;;
  }

  dimension: uom_name {
    type: string
    sql: ${TABLE}.uom_name ;;
  }

  measure: count {
    type: count
    drill_fields: [uom_id, uom_name]
  }
}
