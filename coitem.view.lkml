view: coitem {
  sql_table_name: public.coitem ;;

  dimension: coitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.coitem_id ;;
  }

  dimension: coitem_close_username {
    type: string
    sql: ${TABLE}.coitem_close_username ;;
  }

  dimension_group: coitem_closedate {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.coitem_closedate ;;
  }

  dimension: coitem_cohead_id {
    type: number
    sql: ${TABLE}.coitem_cohead_id ;;
  }

  dimension: coitem_cos_accnt_id {
    type: number
    sql: ${TABLE}.coitem_cos_accnt_id ;;
  }

  dimension_group: coitem_created {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.coitem_created ;;
  }

  dimension: coitem_creator {
    type: string
    sql: ${TABLE}.coitem_creator ;;
  }

  dimension: coitem_custpn {
    type: string
    sql: ${TABLE}.coitem_custpn ;;
  }

  dimension: coitem_custprice {
    type: number
    sql: ${TABLE}.coitem_custprice ;;
  }

  dimension: coitem_firm {
    type: yesno
    sql: ${TABLE}.coitem_firm ;;
  }

  dimension: coitem_imported {
    type: yesno
    sql: ${TABLE}.coitem_imported ;;
  }

  dimension: coitem_itemsite_id {
    type: number
    sql: ${TABLE}.coitem_itemsite_id ;;
  }

  dimension_group: coitem_lastupdated {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.coitem_lastupdated ;;
  }

  dimension: coitem_linenumber {
    type: number
    sql: ${TABLE}.coitem_linenumber ;;
  }

  dimension: coitem_memo {
    type: string
    sql: ${TABLE}.coitem_memo ;;
  }

  dimension: coitem_order_id {
    type: number
    sql: ${TABLE}.coitem_order_id ;;
  }

  dimension: coitem_order_type {
    type: string
    sql: ${TABLE}.coitem_order_type ;;
  }

  dimension: coitem_prcost {
    type: number
    sql: ${TABLE}.coitem_prcost ;;
  }

  dimension: coitem_price {
    type: number
    sql: ${TABLE}.coitem_price ;;
  }

  dimension: coitem_price_invuomratio {
    type: number
    sql: ${TABLE}.coitem_price_invuomratio ;;
  }

  dimension: coitem_price_uom_id {
    type: number
    sql: ${TABLE}.coitem_price_uom_id ;;
  }

  dimension: coitem_pricemode {
    type: string
    sql: ${TABLE}.coitem_pricemode ;;
  }

  dimension_group: coitem_promdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.coitem_promdate ;;
  }

  dimension: coitem_qty_invuomratio {
    type: number
    sql: ${TABLE}.coitem_qty_invuomratio ;;
  }

  dimension: coitem_qty_uom_id {
    type: number
    sql: ${TABLE}.coitem_qty_uom_id ;;
  }

  dimension: coitem_qtyord {
    type: number
    sql: ${TABLE}.coitem_qtyord ;;
  }

  dimension: coitem_qtyreserved {
    type: number
    sql: ${TABLE}.coitem_qtyreserved ;;
  }

  dimension: coitem_qtyreserved_uom_id {
    type: number
    sql: ${TABLE}.coitem_qtyreserved_uom_id ;;
  }

  dimension: coitem_qtyreturned {
    type: number
    sql: ${TABLE}.coitem_qtyreturned ;;
  }

  dimension: coitem_qtyshipped {
    type: number
    sql: ${TABLE}.coitem_qtyshipped ;;
  }

  dimension: coitem_rev_accnt_id {
    type: number
    sql: ${TABLE}.coitem_rev_accnt_id ;;
  }

  dimension_group: coitem_scheddate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.coitem_scheddate ;;
  }

  dimension: coitem_status {
    type: string
    sql: ${TABLE}.coitem_status ;;
  }

  dimension: coitem_subnumber {
    type: number
    sql: ${TABLE}.coitem_subnumber ;;
  }

  dimension: coitem_substitute_item_id {
    type: number
    sql: ${TABLE}.coitem_substitute_item_id ;;
  }

  dimension: coitem_taxtype_id {
    type: number
    sql: ${TABLE}.coitem_taxtype_id ;;
  }

  dimension: coitem_unitcost {
    type: number
    sql: ${TABLE}.coitem_unitcost ;;
  }

  dimension: coitem_warranty {
    type: yesno
    sql: ${TABLE}.coitem_warranty ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [coitem_id, coitem_close_username]
  }
}
