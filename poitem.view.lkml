view: poitem {
  sql_table_name: public.poitem ;;

  dimension: poitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.poitem_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: poitem_bom_rev_id {
    type: number
    sql: ${TABLE}.poitem_bom_rev_id ;;
  }

  dimension: poitem_boo_rev_id {
    type: number
    sql: ${TABLE}.poitem_boo_rev_id ;;
  }

  dimension: poitem_comments {
    type: string
    sql: ${TABLE}.poitem_comments ;;
  }

  dimension_group: poitem_duedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.poitem_duedate ;;
  }

  dimension: poitem_expcat_id {
    type: number
    sql: ${TABLE}.poitem_expcat_id ;;
  }

  dimension: poitem_freight {
    type: number
    sql: ${TABLE}.poitem_freight ;;
  }

  dimension: poitem_freight_received {
    type: number
    sql: ${TABLE}.poitem_freight_received ;;
  }

  dimension: poitem_freight_vouchered {
    type: number
    sql: ${TABLE}.poitem_freight_vouchered ;;
  }

  dimension: poitem_invvenduomratio {
    type: number
    sql: ${TABLE}.poitem_invvenduomratio ;;
  }

  dimension: poitem_itemsite_id {
    type: number
    sql: ${TABLE}.poitem_itemsite_id ;;
  }

  dimension: poitem_itemsrc_id {
    type: number
    sql: ${TABLE}.poitem_itemsrc_id ;;
  }

  dimension: poitem_linenumber {
    type: number
    sql: ${TABLE}.poitem_linenumber ;;
  }

  dimension: poitem_manuf_item_descrip {
    type: string
    sql: ${TABLE}.poitem_manuf_item_descrip ;;
  }

  dimension: poitem_manuf_item_number {
    type: string
    sql: ${TABLE}.poitem_manuf_item_number ;;
  }

  dimension: poitem_manuf_name {
    type: string
    sql: ${TABLE}.poitem_manuf_name ;;
  }

  dimension: poitem_order_id {
    type: number
    sql: ${TABLE}.poitem_order_id ;;
  }

  dimension: poitem_order_type {
    type: string
    sql: ${TABLE}.poitem_order_type ;;
  }

  dimension: poitem_pohead_id {
    type: number
    sql: ${TABLE}.poitem_pohead_id ;;
  }

  dimension: poitem_prj_id {
    type: number
    sql: ${TABLE}.poitem_prj_id ;;
  }

  dimension: poitem_qty_ordered {
    type: number
    sql: ${TABLE}.poitem_qty_ordered ;;
  }

  dimension: poitem_qty_received {
    type: number
    sql: ${TABLE}.poitem_qty_received ;;
  }

  dimension: poitem_qty_returned {
    type: number
    sql: ${TABLE}.poitem_qty_returned ;;
  }

  dimension: poitem_qty_toreceive {
    type: number
    sql: ${TABLE}.poitem_qty_toreceive ;;
  }

  dimension: poitem_qty_vouchered {
    type: number
    sql: ${TABLE}.poitem_qty_vouchered ;;
  }

  dimension_group: poitem_rlsd_duedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.poitem_rlsd_duedate ;;
  }

  dimension: poitem_status {
    type: string
    sql: ${TABLE}.poitem_status ;;
  }

  dimension: poitem_stdcost {
    type: number
    sql: ${TABLE}.poitem_stdcost ;;
  }

  dimension: poitem_tax_recoverable {
    type: yesno
    sql: ${TABLE}.poitem_tax_recoverable ;;
  }

  dimension: poitem_taxtype_id {
    type: number
    sql: ${TABLE}.poitem_taxtype_id ;;
  }

  dimension: poitem_unitprice {
    type: number
    sql: ${TABLE}.poitem_unitprice ;;
  }

  dimension: poitem_vend_item_descrip {
    type: string
    sql: ${TABLE}.poitem_vend_item_descrip ;;
  }

  dimension: poitem_vend_item_number {
    type: string
    sql: ${TABLE}.poitem_vend_item_number ;;
  }

  dimension: poitem_vend_uom {
    type: string
    sql: ${TABLE}.poitem_vend_uom ;;
  }

  measure: count {
    type: count
    drill_fields: [poitem_id, poitem_manuf_name]
  }
}
