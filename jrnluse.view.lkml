view: jrnluse {
  sql_table_name: public.jrnluse ;;

  dimension: jrnluse_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.jrnluse_id ;;
  }

  dimension_group: jrnluse {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.jrnluse_date ;;
  }

  dimension: jrnluse_number {
    type: number
    sql: ${TABLE}.jrnluse_number ;;
  }

  dimension: jrnluse_use {
    type: string
    sql: ${TABLE}.jrnluse_use ;;
  }

  measure: count {
    type: count
    drill_fields: [jrnluse_id]
  }
}
