view: accnt {
  sql_table_name: public.accnt ;;

  dimension: accnt_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.accnt_id ;;
  }

  dimension: accnt_active {
    type: yesno
    sql: ${TABLE}.accnt_active ;;
  }

  dimension: accnt_comments {
    type: string
    sql: ${TABLE}.accnt_comments ;;
  }

  dimension: accnt_company {
    type: string
    sql: ${TABLE}.accnt_company ;;
  }

  dimension: accnt_curr_id {
    type: number
    sql: ${TABLE}.accnt_curr_id ;;
  }

  dimension: accnt_descrip {
    type: string
    sql: ${TABLE}.accnt_descrip ;;
  }

  dimension: accnt_extref {
    type: string
    sql: ${TABLE}.accnt_extref ;;
  }

  dimension: accnt_forwardupdate {
    type: yesno
    sql: ${TABLE}.accnt_forwardupdate ;;
  }

  dimension: accnt_name {
    type: string
    sql: ${TABLE}.accnt_name ;;
  }

  dimension: accnt_number {
    type: string
    sql: ${TABLE}.accnt_number ;;
  }

  dimension: accnt_profit {
    type: string
    sql: ${TABLE}.accnt_profit ;;
  }

  dimension: accnt_sub {
    type: string
    sql: ${TABLE}.accnt_sub ;;
  }

  dimension: accnt_subaccnttype_code {
    type: string
    sql: ${TABLE}.accnt_subaccnttype_code ;;
  }

  dimension: accnt_type {
    type: string
    sql: ${TABLE}.accnt_type ;;
  }

  measure: count {
    type: count
    drill_fields: [accnt_id, accnt_name, flaccnt.count]
  }
}
