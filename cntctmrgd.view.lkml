view: cntctmrgd {
  sql_table_name: public.cntctmrgd ;;

  dimension: cntctmrgd_cntct_id {
    type: number
    sql: ${TABLE}.cntctmrgd_cntct_id ;;
  }

  dimension: cntctmrgd_error {
    type: yesno
    sql: ${TABLE}.cntctmrgd_error ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
