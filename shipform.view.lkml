view: shipform {
  sql_table_name: public.shipform ;;

  dimension: shipform_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.shipform_id ;;
  }

  dimension: shipform_name {
    type: string
    sql: ${TABLE}.shipform_name ;;
  }

  dimension: shipform_report_id {
    type: number
    sql: ${TABLE}.shipform_report_id ;;
  }

  dimension: shipform_report_name {
    type: string
    sql: ${TABLE}.shipform_report_name ;;
  }

  measure: count {
    type: count
    drill_fields: [shipform_id, shipform_name, shipform_report_name]
  }
}
