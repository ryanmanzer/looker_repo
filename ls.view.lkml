view: ls {
  sql_table_name: public.ls ;;

  dimension: ls_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.ls_id ;;
  }

  dimension: ls_item_id {
    type: number
    sql: ${TABLE}.ls_item_id ;;
  }

  dimension: ls_notes {
    type: string
    sql: ${TABLE}.ls_notes ;;
  }

  dimension: ls_number {
    type: string
    sql: ${TABLE}.ls_number ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [ls_id]
  }
}
