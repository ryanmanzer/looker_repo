view: invdetail {
  sql_table_name: public.invdetail ;;

  dimension: invdetail_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.invdetail_id ;;
  }

  dimension: invdetail_comments {
    type: string
    sql: ${TABLE}.invdetail_comments ;;
  }

  dimension_group: invdetail_expiration {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.invdetail_expiration ;;
  }

  dimension: invdetail_invcitem_id {
    type: number
    sql: ${TABLE}.invdetail_invcitem_id ;;
  }

  dimension: invdetail_invhist_id {
    type: number
    sql: ${TABLE}.invdetail_invhist_id ;;
  }

  dimension: invdetail_location_id {
    type: number
    sql: ${TABLE}.invdetail_location_id ;;
  }

  dimension: invdetail_ls_id {
    type: number
    sql: ${TABLE}.invdetail_ls_id ;;
  }

  dimension: invdetail_qty {
    type: number
    sql: ${TABLE}.invdetail_qty ;;
  }

  dimension: invdetail_qty_after {
    type: number
    sql: ${TABLE}.invdetail_qty_after ;;
  }

  dimension: invdetail_qty_before {
    type: number
    sql: ${TABLE}.invdetail_qty_before ;;
  }

  dimension: invdetail_transtype {
    type: string
    sql: ${TABLE}.invdetail_transtype ;;
  }

  dimension_group: invdetail_warrpurc {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.invdetail_warrpurc ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [invdetail_id]
  }
}
