view: item {
  sql_table_name: public.item ;;

  dimension: item_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.item_id ;;
  }

  dimension: item_active {
    type: yesno
    sql: ${TABLE}.item_active ;;
  }

  dimension: item_classcode_id {
    type: number
    sql: ${TABLE}.item_classcode_id ;;
  }

  dimension: item_comments {
    type: string
    sql: ${TABLE}.item_comments ;;
  }

  dimension: item_config {
    type: yesno
    sql: ${TABLE}.item_config ;;
  }

  dimension: item_descrip1 {
    type: string
    sql: ${TABLE}.item_descrip1 ;;
  }

  dimension: item_descrip2 {
    type: string
    sql: ${TABLE}.item_descrip2 ;;
  }

  dimension: item_exclusive {
    type: yesno
    sql: ${TABLE}.item_exclusive ;;
  }

  dimension: item_extdescrip {
    type: string
    sql: ${TABLE}.item_extdescrip ;;
  }

  dimension: item_fractional {
    type: yesno
    sql: ${TABLE}.item_fractional ;;
  }

  dimension: item_freightclass_id {
    type: number
    sql: ${TABLE}.item_freightclass_id ;;
  }

  dimension: item_height {
    type: number
    sql: ${TABLE}.item_height ;;
  }

  dimension: item_inv_uom_id {
    type: number
    sql: ${TABLE}.item_inv_uom_id ;;
  }

  dimension: item_length {
    type: number
    sql: ${TABLE}.item_length ;;
  }

  dimension: item_listcost {
    type: number
    sql: ${TABLE}.item_listcost ;;
  }

  dimension: item_listprice {
    type: number
    sql: ${TABLE}.item_listprice ;;
  }

  dimension: item_maxcost {
    type: number
    sql: ${TABLE}.item_maxcost ;;
  }

  dimension: item_mrkt_descrip {
    type: string
    sql: ${TABLE}.item_mrkt_descrip ;;
  }

  dimension: item_mrkt_seokey {
    type: string
    sql: ${TABLE}.item_mrkt_seokey ;;
  }

  dimension: item_mrkt_seotitle {
    type: string
    sql: ${TABLE}.item_mrkt_seotitle ;;
  }

  dimension: item_mrkt_subtitle {
    type: string
    sql: ${TABLE}.item_mrkt_subtitle ;;
  }

  dimension: item_mrkt_teaser {
    type: string
    sql: ${TABLE}.item_mrkt_teaser ;;
  }

  dimension: item_mrkt_title {
    type: string
    sql: ${TABLE}.item_mrkt_title ;;
  }

  dimension: item_number {
    type: string
    sql: ${TABLE}.item_number ;;
  }

  dimension: item_pack_height {
    type: number
    sql: ${TABLE}.item_pack_height ;;
  }

  dimension: item_pack_length {
    type: number
    sql: ${TABLE}.item_pack_length ;;
  }

  dimension: item_pack_phy_uom_id {
    type: number
    sql: ${TABLE}.item_pack_phy_uom_id ;;
  }

  dimension: item_pack_width {
    type: number
    sql: ${TABLE}.item_pack_width ;;
  }

  dimension: item_packweight {
    type: number
    sql: ${TABLE}.item_packweight ;;
  }

  dimension: item_phy_uom_id {
    type: number
    sql: ${TABLE}.item_phy_uom_id ;;
  }

  dimension: item_picklist {
    type: yesno
    sql: ${TABLE}.item_picklist ;;
  }

  dimension: item_price_uom_id {
    type: number
    sql: ${TABLE}.item_price_uom_id ;;
  }

  dimension: item_prodcat_id {
    type: number
    sql: ${TABLE}.item_prodcat_id ;;
  }

  dimension: item_prodweight {
    type: number
    sql: ${TABLE}.item_prodweight ;;
  }

  dimension: item_sold {
    type: yesno
    sql: ${TABLE}.item_sold ;;
  }

  dimension: item_tax_recoverable {
    type: yesno
    sql: ${TABLE}.item_tax_recoverable ;;
  }

  dimension: item_type {
    type: string
    sql: ${TABLE}.item_type ;;
  }

  dimension: item_upccode {
    type: string
    sql: ${TABLE}.item_upccode ;;
  }

  dimension: item_warrdays {
    type: number
    sql: ${TABLE}.item_warrdays ;;
  }

  dimension: item_width {
    type: number
    sql: ${TABLE}.item_width ;;
  }

  measure: count {
    type: count
    drill_fields: [item_id, creditmemoeditlist.count, creditmemoitem.count, saleshistory.count, saleshistorymisc.count]
  }
}
