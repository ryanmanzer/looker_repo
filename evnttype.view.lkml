view: evnttype {
  sql_table_name: public.evnttype ;;

  dimension: evnttype_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.evnttype_id ;;
  }

  dimension: evnttype_descrip {
    type: string
    sql: ${TABLE}.evnttype_descrip ;;
  }

  dimension: evnttype_module {
    type: string
    sql: ${TABLE}.evnttype_module ;;
  }

  dimension: evnttype_name {
    type: string
    sql: ${TABLE}.evnttype_name ;;
  }

  measure: count {
    type: count
    drill_fields: [evnttype_id, evnttype_name]
  }
}
