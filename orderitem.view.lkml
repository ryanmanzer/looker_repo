view: orderitem {
  sql_table_name: public.orderitem ;;

  dimension: orderitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.orderitem_id ;;
  }

  dimension: orderitem_freight {
    type: number
    sql: ${TABLE}.orderitem_freight ;;
  }

  dimension: orderitem_freight_curr_id {
    type: number
    sql: ${TABLE}.orderitem_freight_curr_id ;;
  }

  dimension: orderitem_freight_received {
    type: number
    sql: ${TABLE}.orderitem_freight_received ;;
  }

  dimension: orderitem_itemsite_id {
    type: number
    sql: ${TABLE}.orderitem_itemsite_id ;;
  }

  dimension: orderitem_linenumber {
    type: number
    sql: ${TABLE}.orderitem_linenumber ;;
  }

  dimension: orderitem_orderhead_id {
    type: number
    sql: ${TABLE}.orderitem_orderhead_id ;;
  }

  dimension: orderitem_orderhead_type {
    type: string
    sql: ${TABLE}.orderitem_orderhead_type ;;
  }

  dimension: orderitem_qty_invuomratio {
    type: number
    sql: ${TABLE}.orderitem_qty_invuomratio ;;
  }

  dimension: orderitem_qty_ordered {
    type: number
    sql: ${TABLE}.orderitem_qty_ordered ;;
  }

  dimension: orderitem_qty_received {
    type: number
    sql: ${TABLE}.orderitem_qty_received ;;
  }

  dimension: orderitem_qty_shipped {
    type: number
    sql: ${TABLE}.orderitem_qty_shipped ;;
  }

  dimension: orderitem_qty_uom_id {
    type: number
    sql: ${TABLE}.orderitem_qty_uom_id ;;
  }

  dimension_group: orderitem_scheddate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.orderitem_scheddate ;;
  }

  dimension: orderitem_status {
    type: string
    sql: ${TABLE}.orderitem_status ;;
  }

  dimension: orderitem_unitcost {
    type: number
    sql: ${TABLE}.orderitem_unitcost ;;
  }

  dimension: orderitem_unitcost_curr_id {
    type: number
    sql: ${TABLE}.orderitem_unitcost_curr_id ;;
  }

  measure: count {
    type: count
    drill_fields: [orderitem_id]
  }
}
