view: addr {
  sql_table_name: public.addr ;;

  dimension: addr_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.addr_id ;;
  }

  dimension: addr_active {
    type: yesno
    sql: ${TABLE}.addr_active ;;
  }

  dimension: addr_city {
    type: string
    sql: ${TABLE}.addr_city ;;
  }

  dimension: addr_country {
    type: string
    sql: ${TABLE}.addr_country ;;
  }

  dimension: addr_line1 {
    type: string
    sql: ${TABLE}.addr_line1 ;;
  }

  dimension: addr_line2 {
    type: string
    sql: ${TABLE}.addr_line2 ;;
  }

  dimension: addr_line3 {
    type: string
    sql: ${TABLE}.addr_line3 ;;
  }

  dimension: addr_notes {
    type: string
    sql: ${TABLE}.addr_notes ;;
  }

  dimension: addr_number {
    type: string
    sql: ${TABLE}.addr_number ;;
  }

  dimension: addr_postalcode {
    type: string
    sql: ${TABLE}.addr_postalcode ;;
  }

  dimension: addr_state {
    type: string
    sql: ${TABLE}.addr_state ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [addr_id, address.count]
  }
}
