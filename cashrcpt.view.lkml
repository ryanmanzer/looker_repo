view: cashrcpt {
  sql_table_name: public.cashrcpt ;;

  dimension: cashrcpt_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.cashrcpt_id ;;
  }

  dimension: cashrcpt_alt_curr_rate {
    type: number
    sql: ${TABLE}.cashrcpt_alt_curr_rate ;;
  }

  dimension: cashrcpt_amount {
    type: number
    sql: ${TABLE}.cashrcpt_amount ;;
  }

  dimension_group: cashrcpt_applydate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cashrcpt_applydate ;;
  }

  dimension: cashrcpt_bankaccnt_id {
    type: number
    sql: ${TABLE}.cashrcpt_bankaccnt_id ;;
  }

  dimension: cashrcpt_curr_id {
    type: number
    sql: ${TABLE}.cashrcpt_curr_id ;;
  }

  dimension: cashrcpt_curr_rate {
    type: number
    sql: ${TABLE}.cashrcpt_curr_rate ;;
  }

  dimension: cashrcpt_cust_id {
    type: number
    sql: ${TABLE}.cashrcpt_cust_id ;;
  }

  dimension: cashrcpt_discount {
    type: number
    sql: ${TABLE}.cashrcpt_discount ;;
  }

  dimension_group: cashrcpt_distdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cashrcpt_distdate ;;
  }

  dimension_group: cashrcpt_docdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cashrcpt_docdate ;;
  }

  dimension: cashrcpt_docnumber {
    type: string
    sql: ${TABLE}.cashrcpt_docnumber ;;
  }

  dimension: cashrcpt_fundstype {
    type: string
    sql: ${TABLE}.cashrcpt_fundstype ;;
  }

  dimension: cashrcpt_notes {
    type: string
    sql: ${TABLE}.cashrcpt_notes ;;
  }

  dimension: cashrcpt_number {
    type: string
    sql: ${TABLE}.cashrcpt_number ;;
  }

  dimension: cashrcpt_posted {
    type: yesno
    sql: ${TABLE}.cashrcpt_posted ;;
  }

  dimension: cashrcpt_postedby {
    type: string
    sql: ${TABLE}.cashrcpt_postedby ;;
  }

  dimension_group: cashrcpt_posteddate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cashrcpt_posteddate ;;
  }

  dimension: cashrcpt_salescat_id {
    type: number
    sql: ${TABLE}.cashrcpt_salescat_id ;;
  }

  dimension: cashrcpt_usecustdeposit {
    type: yesno
    sql: ${TABLE}.cashrcpt_usecustdeposit ;;
  }

  dimension: cashrcpt_void {
    type: yesno
    sql: ${TABLE}.cashrcpt_void ;;
  }

  measure: count {
    type: count
    drill_fields: [cashrcpt_id]
  }
}
