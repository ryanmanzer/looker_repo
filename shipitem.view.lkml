view: shipitem {
  sql_table_name: public.shipitem ;;

  dimension: shipitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.shipitem_id ;;
  }

  dimension: shipitem_invcitem_id {
    type: number
    sql: ${TABLE}.shipitem_invcitem_id ;;
  }

  dimension: shipitem_invhist_id {
    type: number
    sql: ${TABLE}.shipitem_invhist_id ;;
  }

  dimension: shipitem_invoiced {
    type: yesno
    sql: ${TABLE}.shipitem_invoiced ;;
  }

  dimension: shipitem_orderitem_id {
    type: number
    sql: ${TABLE}.shipitem_orderitem_id ;;
  }

  dimension: shipitem_qty {
    type: number
    sql: ${TABLE}.shipitem_qty ;;
  }

  dimension_group: shipitem_shipdate {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.shipitem_shipdate ;;
  }

  dimension: shipitem_shiphead_id {
    type: number
    sql: ${TABLE}.shipitem_shiphead_id ;;
  }

  dimension: shipitem_shipped {
    type: yesno
    sql: ${TABLE}.shipitem_shipped ;;
  }

  dimension: shipitem_trans_username {
    type: string
    sql: ${TABLE}.shipitem_trans_username ;;
  }

  dimension_group: shipitem_transdate {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.shipitem_transdate ;;
  }

  dimension: shipitem_value {
    type: number
    sql: ${TABLE}.shipitem_value ;;
  }

  measure: count {
    type: count
    drill_fields: [shipitem_id, shipitem_trans_username]
  }
}
