view: labelform {
  sql_table_name: public.labelform ;;

  dimension: labelform_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.labelform_id ;;
  }

  dimension: labelform_name {
    type: string
    sql: ${TABLE}.labelform_name ;;
  }

  dimension: labelform_perpage {
    type: number
    sql: ${TABLE}.labelform_perpage ;;
  }

  dimension: labelform_report_id {
    type: number
    sql: ${TABLE}.labelform_report_id ;;
  }

  dimension: labelform_report_name {
    type: string
    sql: ${TABLE}.labelform_report_name ;;
  }

  measure: count {
    type: count
    drill_fields: [labelform_id, labelform_report_name, labelform_name]
  }
}
