view: bomwork {
  sql_table_name: public.bomwork ;;

  dimension: bomwork_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.bomwork_id ;;
  }

  dimension: bomwork_actunitcost {
    type: number
    sql: ${TABLE}.bomwork_actunitcost ;;
  }

  dimension: bomwork_bomitem_id {
    type: number
    sql: ${TABLE}.bomwork_bomitem_id ;;
  }

  dimension: bomwork_char_id {
    type: number
    sql: ${TABLE}.bomwork_char_id ;;
  }

  dimension: bomwork_createwo {
    type: yesno
    sql: ${TABLE}.bomwork_createwo ;;
  }

  dimension: bomwork_ecn {
    type: string
    sql: ${TABLE}.bomwork_ecn ;;
  }

  dimension_group: bomwork_effective {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.bomwork_effective ;;
  }

  dimension_group: bomwork_expires {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.bomwork_expires ;;
  }

  dimension: bomwork_issuemethod {
    type: string
    sql: ${TABLE}.bomwork_issuemethod ;;
  }

  dimension: bomwork_item_id {
    type: number
    sql: ${TABLE}.bomwork_item_id ;;
  }

  dimension: bomwork_item_type {
    type: string
    sql: ${TABLE}.bomwork_item_type ;;
  }

  dimension: bomwork_level {
    type: number
    sql: ${TABLE}.bomwork_level ;;
  }

  dimension: bomwork_notes {
    type: string
    sql: ${TABLE}.bomwork_notes ;;
  }

  dimension: bomwork_parent_id {
    type: number
    sql: ${TABLE}.bomwork_parent_id ;;
  }

  dimension: bomwork_parent_seqnumber {
    type: number
    sql: ${TABLE}.bomwork_parent_seqnumber ;;
  }

  dimension: bomwork_qtyfxd {
    type: number
    sql: ${TABLE}.bomwork_qtyfxd ;;
  }

  dimension: bomwork_qtyper {
    type: number
    sql: ${TABLE}.bomwork_qtyper ;;
  }

  dimension: bomwork_qtyreq {
    type: number
    sql: ${TABLE}.bomwork_qtyreq ;;
  }

  dimension: bomwork_ref {
    type: string
    sql: ${TABLE}.bomwork_ref ;;
  }

  dimension: bomwork_scrap {
    type: number
    sql: ${TABLE}.bomwork_scrap ;;
  }

  dimension: bomwork_seqnumber {
    type: number
    sql: ${TABLE}.bomwork_seqnumber ;;
  }

  dimension: bomwork_set_id {
    type: number
    sql: ${TABLE}.bomwork_set_id ;;
  }

  dimension: bomwork_status {
    type: string
    sql: ${TABLE}.bomwork_status ;;
  }

  dimension: bomwork_stdunitcost {
    type: number
    sql: ${TABLE}.bomwork_stdunitcost ;;
  }

  dimension: bomwork_value {
    type: string
    sql: ${TABLE}.bomwork_value ;;
  }

  measure: count {
    type: count
    drill_fields: [bomwork_id]
  }
}
