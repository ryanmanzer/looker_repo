view: cntcteml {
  sql_table_name: public.cntcteml ;;

  dimension: cntcteml_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.cntcteml_id ;;
  }

  dimension: cntcteml_cntct_id {
    type: number
    sql: ${TABLE}.cntcteml_cntct_id ;;
  }

  dimension: cntcteml_email {
    type: string
    sql: ${TABLE}.cntcteml_email ;;
  }

  dimension: cntcteml_primary {
    type: yesno
    sql: ${TABLE}.cntcteml_primary ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [cntcteml_id]
  }
}
