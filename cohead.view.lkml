view: cohead {
  sql_table_name: public.cohead ;;

  dimension: cohead_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.cohead_id ;;
  }

  dimension: cohead_billto_cntct_email {
    type: string
    sql: ${TABLE}.cohead_billto_cntct_email ;;
  }

  dimension: cohead_billto_cntct_fax {
    type: string
    sql: ${TABLE}.cohead_billto_cntct_fax ;;
  }

  dimension: cohead_billto_cntct_first_name {
    type: string
    sql: ${TABLE}.cohead_billto_cntct_first_name ;;
  }

  dimension: cohead_billto_cntct_honorific {
    type: string
    sql: ${TABLE}.cohead_billto_cntct_honorific ;;
  }

  dimension: cohead_billto_cntct_id {
    type: number
    sql: ${TABLE}.cohead_billto_cntct_id ;;
  }

  dimension: cohead_billto_cntct_last_name {
    type: string
    sql: ${TABLE}.cohead_billto_cntct_last_name ;;
  }

  dimension: cohead_billto_cntct_middle {
    type: string
    sql: ${TABLE}.cohead_billto_cntct_middle ;;
  }

  dimension: cohead_billto_cntct_phone {
    type: string
    sql: ${TABLE}.cohead_billto_cntct_phone ;;
  }

  dimension: cohead_billto_cntct_suffix {
    type: string
    sql: ${TABLE}.cohead_billto_cntct_suffix ;;
  }

  dimension: cohead_billto_cntct_title {
    type: string
    sql: ${TABLE}.cohead_billto_cntct_title ;;
  }

  dimension: cohead_billtoaddress1 {
    type: string
    sql: ${TABLE}.cohead_billtoaddress1 ;;
  }

  dimension: cohead_billtoaddress2 {
    type: string
    sql: ${TABLE}.cohead_billtoaddress2 ;;
  }

  dimension: cohead_billtoaddress3 {
    type: string
    sql: ${TABLE}.cohead_billtoaddress3 ;;
  }

  dimension: cohead_billtocity {
    type: string
    sql: ${TABLE}.cohead_billtocity ;;
  }

  dimension: cohead_billtocountry {
    type: string
    sql: ${TABLE}.cohead_billtocountry ;;
  }

  dimension: cohead_billtoname {
    type: string
    sql: ${TABLE}.cohead_billtoname ;;
  }

  dimension: cohead_billtostate {
    type: string
    sql: ${TABLE}.cohead_billtostate ;;
  }

  dimension: cohead_billtozipcode {
    type: string
    sql: ${TABLE}.cohead_billtozipcode ;;
  }

  dimension: cohead_calcfreight {
    type: yesno
    sql: ${TABLE}.cohead_calcfreight ;;
  }

  dimension: cohead_commission {
    type: number
    sql: ${TABLE}.cohead_commission ;;
  }

  dimension_group: cohead_created {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.cohead_created ;;
  }

  dimension: cohead_creator {
    type: string
    sql: ${TABLE}.cohead_creator ;;
  }

  dimension: cohead_curr_id {
    type: number
    sql: ${TABLE}.cohead_curr_id ;;
  }

  dimension: cohead_cust_id {
    type: number
    sql: ${TABLE}.cohead_cust_id ;;
  }

  dimension: cohead_custponumber {
    type: string
    sql: ${TABLE}.cohead_custponumber ;;
  }

  dimension: cohead_fob {
    type: string
    sql: ${TABLE}.cohead_fob ;;
  }

  dimension: cohead_freight {
    type: number
    sql: ${TABLE}.cohead_freight ;;
  }

  dimension: cohead_holdtype {
    type: string
    sql: ${TABLE}.cohead_holdtype ;;
  }

  dimension: cohead_imported {
    type: yesno
    sql: ${TABLE}.cohead_imported ;;
  }

  dimension_group: cohead_lastupdated {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.cohead_lastupdated ;;
  }

  dimension: cohead_misc {
    type: number
    sql: ${TABLE}.cohead_misc ;;
  }

  dimension: cohead_misc_accnt_id {
    type: number
    sql: ${TABLE}.cohead_misc_accnt_id ;;
  }

  dimension: cohead_misc_descrip {
    type: string
    sql: ${TABLE}.cohead_misc_descrip ;;
  }

  dimension_group: cohead_miscdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cohead_miscdate ;;
  }

  dimension: cohead_number {
    type: string
    sql: ${TABLE}.cohead_number ;;
  }

  dimension: cohead_ophead_id {
    type: number
    sql: ${TABLE}.cohead_ophead_id ;;
  }

  dimension: cohead_ordercomments {
    type: string
    sql: ${TABLE}.cohead_ordercomments ;;
  }

  dimension_group: cohead_orderdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cohead_orderdate ;;
  }

  dimension: cohead_origin {
    type: string
    sql: ${TABLE}.cohead_origin ;;
  }

  dimension_group: cohead_packdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cohead_packdate ;;
  }

  dimension: cohead_prj_id {
    type: number
    sql: ${TABLE}.cohead_prj_id ;;
  }

  dimension: cohead_quote_number {
    type: string
    sql: ${TABLE}.cohead_quote_number ;;
  }

  dimension: cohead_salesrep_id {
    type: number
    sql: ${TABLE}.cohead_salesrep_id ;;
  }

  dimension: cohead_saletype_id {
    type: number
    sql: ${TABLE}.cohead_saletype_id ;;
  }

  dimension: cohead_shipchrg_id {
    type: number
    sql: ${TABLE}.cohead_shipchrg_id ;;
  }

  dimension: cohead_shipcomments {
    type: string
    sql: ${TABLE}.cohead_shipcomments ;;
  }

  dimension: cohead_shipcomplete {
    type: yesno
    sql: ${TABLE}.cohead_shipcomplete ;;
  }

  dimension: cohead_shipform_id {
    type: number
    sql: ${TABLE}.cohead_shipform_id ;;
  }

  dimension: cohead_shipto_cntct_email {
    type: string
    sql: ${TABLE}.cohead_shipto_cntct_email ;;
  }

  dimension: cohead_shipto_cntct_fax {
    type: string
    sql: ${TABLE}.cohead_shipto_cntct_fax ;;
  }

  dimension: cohead_shipto_cntct_first_name {
    type: string
    sql: ${TABLE}.cohead_shipto_cntct_first_name ;;
  }

  dimension: cohead_shipto_cntct_honorific {
    type: string
    sql: ${TABLE}.cohead_shipto_cntct_honorific ;;
  }

  dimension: cohead_shipto_cntct_id {
    type: number
    sql: ${TABLE}.cohead_shipto_cntct_id ;;
  }

  dimension: cohead_shipto_cntct_last_name {
    type: string
    sql: ${TABLE}.cohead_shipto_cntct_last_name ;;
  }

  dimension: cohead_shipto_cntct_middle {
    type: string
    sql: ${TABLE}.cohead_shipto_cntct_middle ;;
  }

  dimension: cohead_shipto_cntct_phone {
    type: string
    sql: ${TABLE}.cohead_shipto_cntct_phone ;;
  }

  dimension: cohead_shipto_cntct_suffix {
    type: string
    sql: ${TABLE}.cohead_shipto_cntct_suffix ;;
  }

  dimension: cohead_shipto_cntct_title {
    type: string
    sql: ${TABLE}.cohead_shipto_cntct_title ;;
  }

  dimension: cohead_shipto_id {
    type: number
    sql: ${TABLE}.cohead_shipto_id ;;
  }

  dimension: cohead_shiptoaddress1 {
    type: string
    sql: ${TABLE}.cohead_shiptoaddress1 ;;
  }

  dimension: cohead_shiptoaddress2 {
    type: string
    sql: ${TABLE}.cohead_shiptoaddress2 ;;
  }

  dimension: cohead_shiptoaddress3 {
    type: string
    sql: ${TABLE}.cohead_shiptoaddress3 ;;
  }

  dimension: cohead_shiptoaddress4 {
    type: string
    sql: ${TABLE}.cohead_shiptoaddress4 ;;
  }

  dimension: cohead_shiptoaddress5 {
    type: string
    sql: ${TABLE}.cohead_shiptoaddress5 ;;
  }

  dimension: cohead_shiptocity {
    type: string
    sql: ${TABLE}.cohead_shiptocity ;;
  }

  dimension: cohead_shiptocountry {
    type: string
    sql: ${TABLE}.cohead_shiptocountry ;;
  }

  dimension: cohead_shiptoname {
    type: string
    sql: ${TABLE}.cohead_shiptoname ;;
  }

  dimension: cohead_shiptophone {
    type: string
    sql: ${TABLE}.cohead_shiptophone ;;
  }

  dimension: cohead_shiptostate {
    type: string
    sql: ${TABLE}.cohead_shiptostate ;;
  }

  dimension: cohead_shiptozipcode {
    type: string
    sql: ${TABLE}.cohead_shiptozipcode ;;
  }

  dimension: cohead_shipvia {
    type: string
    sql: ${TABLE}.cohead_shipvia ;;
  }

  dimension: cohead_shipzone_id {
    type: number
    sql: ${TABLE}.cohead_shipzone_id ;;
  }

  dimension: cohead_status {
    type: string
    sql: ${TABLE}.cohead_status ;;
  }

  dimension: cohead_taxtype_id {
    type: number
    sql: ${TABLE}.cohead_taxtype_id ;;
  }

  dimension: cohead_taxzone_id {
    type: number
    sql: ${TABLE}.cohead_taxzone_id ;;
  }

  dimension: cohead_terms_id {
    type: number
    sql: ${TABLE}.cohead_terms_id ;;
  }

  dimension: cohead_type {
    type: string
    sql: ${TABLE}.cohead_type ;;
  }

  dimension: cohead_warehous_id {
    type: number
    sql: ${TABLE}.cohead_warehous_id ;;
  }

  dimension: cohead_wasquote {
    type: yesno
    sql: ${TABLE}.cohead_wasquote ;;
  }

  dimension: create_dfltworkflow {
    type: yesno
    sql: ${TABLE}.create_dfltworkflow ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      cohead_id,
      cohead_shipto_cntct_first_name,
      cohead_shipto_cntct_last_name,
      cohead_billto_cntct_first_name,
      cohead_billto_cntct_last_name,
      cohead_shiptoname,
      cohead_billtoname,
      saleshistory.count,
      saleshistorymisc.count
    ]
  }
}
