view: flcol {
  sql_table_name: public.flcol ;;

  dimension: flcol_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.flcol_id ;;
  }

  dimension: flcol_budget {
    type: yesno
    sql: ${TABLE}.flcol_budget ;;
  }

  dimension: flcol_budgetdiff {
    type: yesno
    sql: ${TABLE}.flcol_budgetdiff ;;
  }

  dimension: flcol_budgetdiffprcnt {
    type: yesno
    sql: ${TABLE}.flcol_budgetdiffprcnt ;;
  }

  dimension: flcol_budgetprcnt {
    type: yesno
    sql: ${TABLE}.flcol_budgetprcnt ;;
  }

  dimension: flcol_descrip {
    type: string
    sql: ${TABLE}.flcol_descrip ;;
  }

  dimension: flcol_flhead_id {
    type: number
    sql: ${TABLE}.flcol_flhead_id ;;
  }

  dimension: flcol_month {
    type: yesno
    sql: ${TABLE}.flcol_month ;;
  }

  dimension: flcol_name {
    type: string
    sql: ${TABLE}.flcol_name ;;
  }

  dimension: flcol_prcnt {
    type: yesno
    sql: ${TABLE}.flcol_prcnt ;;
  }

  dimension: flcol_priordiff {
    type: yesno
    sql: ${TABLE}.flcol_priordiff ;;
  }

  dimension: flcol_priordiffprcnt {
    type: yesno
    sql: ${TABLE}.flcol_priordiffprcnt ;;
  }

  dimension: flcol_priormonth {
    type: yesno
    sql: ${TABLE}.flcol_priormonth ;;
  }

  dimension: flcol_priorprcnt {
    type: yesno
    sql: ${TABLE}.flcol_priorprcnt ;;
  }

  dimension: flcol_priorquarter {
    type: yesno
    sql: ${TABLE}.flcol_priorquarter ;;
  }

  dimension: flcol_priortype {
    type: string
    sql: ${TABLE}.flcol_priortype ;;
  }

  dimension: flcol_prioryear {
    type: string
    sql: ${TABLE}.flcol_prioryear ;;
  }

  dimension: flcol_quarter {
    type: yesno
    sql: ${TABLE}.flcol_quarter ;;
  }

  dimension: flcol_report_id {
    type: number
    sql: ${TABLE}.flcol_report_id ;;
  }

  dimension: flcol_showdb {
    type: yesno
    sql: ${TABLE}.flcol_showdb ;;
  }

  dimension: flcol_year {
    type: yesno
    sql: ${TABLE}.flcol_year ;;
  }

  measure: count {
    type: count
    drill_fields: [flcol_id, flcol_name]
  }
}
