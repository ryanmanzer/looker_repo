view: itemloc {
  sql_table_name: public.itemloc ;;

  dimension: itemloc_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.itemloc_id ;;
  }

  dimension: itemloc_consolflag {
    type: yesno
    sql: ${TABLE}.itemloc_consolflag ;;
  }

  dimension_group: itemloc_expiration {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.itemloc_expiration ;;
  }

  dimension: itemloc_itemsite_id {
    type: number
    sql: ${TABLE}.itemloc_itemsite_id ;;
  }

  dimension: itemloc_location_id {
    type: number
    sql: ${TABLE}.itemloc_location_id ;;
  }

  dimension: itemloc_ls_id {
    type: number
    sql: ${TABLE}.itemloc_ls_id ;;
  }

  dimension: itemloc_qty {
    type: number
    sql: ${TABLE}.itemloc_qty ;;
  }

  dimension_group: itemloc_warrpurc {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.itemloc_warrpurc ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [itemloc_id]
  }
}
