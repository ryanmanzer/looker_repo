view: shipdatasum {
  sql_table_name: public.shipdatasum ;;

  dimension: shipdatasum_base_freight {
    type: number
    sql: ${TABLE}.shipdatasum_base_freight ;;
  }

  dimension: shipdatasum_base_freight_curr_id {
    type: number
    sql: ${TABLE}.shipdatasum_base_freight_curr_id ;;
  }

  dimension: shipdatasum_billing_option {
    type: string
    sql: ${TABLE}.shipdatasum_billing_option ;;
  }

  dimension: shipdatasum_cohead_number {
    type: string
    sql: ${TABLE}.shipdatasum_cohead_number ;;
  }

  dimension: shipdatasum_cosmisc_packnum_tracknum {
    type: string
    sql: ${TABLE}.shipdatasum_cosmisc_packnum_tracknum ;;
  }

  dimension: shipdatasum_cosmisc_tracknum {
    type: string
    sql: ${TABLE}.shipdatasum_cosmisc_tracknum ;;
  }

  dimension_group: shipdatasum_lastupdated {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.shipdatasum_lastupdated ;;
  }

  dimension: shipdatasum_package_type {
    type: string
    sql: ${TABLE}.shipdatasum_package_type ;;
  }

  dimension: shipdatasum_shiphead_number {
    type: string
    sql: ${TABLE}.shipdatasum_shiphead_number ;;
  }

  dimension: shipdatasum_shipped {
    type: yesno
    sql: ${TABLE}.shipdatasum_shipped ;;
  }

  dimension: shipdatasum_shipper {
    type: string
    sql: ${TABLE}.shipdatasum_shipper ;;
  }

  dimension: shipdatasum_total_freight {
    type: number
    sql: ${TABLE}.shipdatasum_total_freight ;;
  }

  dimension: shipdatasum_total_freight_curr_id {
    type: number
    sql: ${TABLE}.shipdatasum_total_freight_curr_id ;;
  }

  dimension: shipdatasum_weight {
    type: number
    sql: ${TABLE}.shipdatasum_weight ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
