view: mrgundo {
  sql_table_name: public.mrgundo ;;

  dimension: mrgundo_pkey_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.mrgundo_pkey_id ;;
  }

  dimension: mrgundo_base_id {
    type: number
    sql: ${TABLE}.mrgundo_base_id ;;
  }

  dimension: mrgundo_base_schema {
    type: string
    sql: ${TABLE}.mrgundo_base_schema ;;
  }

  dimension: mrgundo_base_table {
    type: string
    sql: ${TABLE}.mrgundo_base_table ;;
  }

  dimension: mrgundo_col {
    type: string
    sql: ${TABLE}.mrgundo_col ;;
  }

  dimension: mrgundo_pkey_col {
    type: string
    sql: ${TABLE}.mrgundo_pkey_col ;;
  }

  dimension: mrgundo_schema {
    type: string
    sql: ${TABLE}.mrgundo_schema ;;
  }

  dimension: mrgundo_table {
    type: string
    sql: ${TABLE}.mrgundo_table ;;
  }

  dimension: mrgundo_type {
    type: string
    sql: ${TABLE}.mrgundo_type ;;
  }

  dimension: mrgundo_value {
    type: string
    sql: ${TABLE}.mrgundo_value ;;
  }

  measure: count {
    type: count
    drill_fields: [mrgundo_pkey_id]
  }
}
