view: incdthist {
  sql_table_name: public.incdthist ;;

  dimension: incdthist_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.incdthist_id ;;
  }

  dimension: incdthist_change {
    type: string
    sql: ${TABLE}.incdthist_change ;;
  }

  dimension: incdthist_descrip {
    type: string
    sql: ${TABLE}.incdthist_descrip ;;
  }

  dimension: incdthist_incdt_id {
    type: number
    sql: ${TABLE}.incdthist_incdt_id ;;
  }

  dimension: incdthist_target_id {
    type: number
    sql: ${TABLE}.incdthist_target_id ;;
  }

  dimension_group: incdthist_timestamp {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.incdthist_timestamp ;;
  }

  dimension: incdthist_username {
    type: string
    sql: ${TABLE}.incdthist_username ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [incdthist_id, incdthist_username]
  }
}
