view: aropenalloc {
  sql_table_name: public.aropenalloc ;;

  dimension: aropenalloc_amount {
    type: number
    sql: ${TABLE}.aropenalloc_amount ;;
  }

  dimension: aropenalloc_aropen_id {
    type: number
    sql: ${TABLE}.aropenalloc_aropen_id ;;
  }

  dimension: aropenalloc_curr_id {
    type: number
    sql: ${TABLE}.aropenalloc_curr_id ;;
  }

  dimension: aropenalloc_doc_id {
    type: number
    sql: ${TABLE}.aropenalloc_doc_id ;;
  }

  dimension: aropenalloc_doctype {
    type: string
    sql: ${TABLE}.aropenalloc_doctype ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
