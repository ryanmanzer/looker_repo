view: custgrpitem {
  sql_table_name: public.custgrpitem ;;

  dimension: custgrpitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.custgrpitem_id ;;
  }

  dimension: custgrpitem_cust_id {
    type: number
    sql: ${TABLE}.custgrpitem_cust_id ;;
  }

  dimension: custgrpitem_custgrp_id {
    type: number
    sql: ${TABLE}.custgrpitem_custgrp_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [custgrpitem_id]
  }
}
