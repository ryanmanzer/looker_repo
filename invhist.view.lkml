view: invhist {
  sql_table_name: public.invhist ;;

  dimension: invhist_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.invhist_id ;;
  }

  dimension: invhist_acct_id {
    type: number
    sql: ${TABLE}.invhist_acct_id ;;
  }

  dimension: invhist_analyze {
    type: yesno
    sql: ${TABLE}.invhist_analyze ;;
  }

  dimension: invhist_comments {
    type: string
    sql: ${TABLE}.invhist_comments ;;
  }

  dimension: invhist_costmethod {
    type: string
    sql: ${TABLE}.invhist_costmethod ;;
  }

  dimension_group: invhist_created {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.invhist_created ;;
  }

  dimension: invhist_docnumber {
    type: string
    sql: ${TABLE}.invhist_docnumber ;;
  }

  dimension: invhist_hasdetail {
    type: yesno
    sql: ${TABLE}.invhist_hasdetail ;;
  }

  dimension: invhist_imported {
    type: yesno
    sql: ${TABLE}.invhist_imported ;;
  }

  dimension: invhist_invqty {
    type: number
    sql: ${TABLE}.invhist_invqty ;;
  }

  dimension: invhist_invuom {
    type: string
    sql: ${TABLE}.invhist_invuom ;;
  }

  dimension: invhist_itemsite_id {
    type: number
    sql: ${TABLE}.invhist_itemsite_id ;;
  }

  dimension: invhist_ordnumber {
    type: string
    sql: ${TABLE}.invhist_ordnumber ;;
  }

  dimension: invhist_ordtype {
    type: string
    sql: ${TABLE}.invhist_ordtype ;;
  }

  dimension: invhist_posted {
    type: yesno
    sql: ${TABLE}.invhist_posted ;;
  }

  dimension: invhist_qoh_after {
    type: number
    sql: ${TABLE}.invhist_qoh_after ;;
  }

  dimension: invhist_qoh_before {
    type: number
    sql: ${TABLE}.invhist_qoh_before ;;
  }

  dimension: invhist_series {
    type: number
    sql: ${TABLE}.invhist_series ;;
  }

  dimension_group: invhist_transdate {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.invhist_transdate ;;
  }

  dimension: invhist_transtype {
    type: string
    sql: ${TABLE}.invhist_transtype ;;
  }

  dimension: invhist_unitcost {
    type: number
    sql: ${TABLE}.invhist_unitcost ;;
  }

  dimension: invhist_user {
    type: string
    sql: ${TABLE}.invhist_user ;;
  }

  dimension: invhist_value_after {
    type: number
    sql: ${TABLE}.invhist_value_after ;;
  }

  dimension: invhist_value_before {
    type: number
    sql: ${TABLE}.invhist_value_before ;;
  }

  dimension: invhist_xfer_warehous_id {
    type: number
    sql: ${TABLE}.invhist_xfer_warehous_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [invhist_id]
  }
}
