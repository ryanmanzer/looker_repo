view: quitem {
  sql_table_name: public.quitem ;;

  dimension: quitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.quitem_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: quitem_createorder {
    type: yesno
    sql: ${TABLE}.quitem_createorder ;;
  }

  dimension: quitem_custpn {
    type: string
    sql: ${TABLE}.quitem_custpn ;;
  }

  dimension: quitem_custprice {
    type: number
    sql: ${TABLE}.quitem_custprice ;;
  }

  dimension: quitem_dropship {
    type: yesno
    sql: ${TABLE}.quitem_dropship ;;
  }

  dimension: quitem_imported {
    type: yesno
    sql: ${TABLE}.quitem_imported ;;
  }

  dimension: quitem_item_id {
    type: number
    sql: ${TABLE}.quitem_item_id ;;
  }

  dimension: quitem_itemsite_id {
    type: number
    sql: ${TABLE}.quitem_itemsite_id ;;
  }

  dimension: quitem_itemsrc_id {
    type: number
    sql: ${TABLE}.quitem_itemsrc_id ;;
  }

  dimension: quitem_linenumber {
    type: number
    sql: ${TABLE}.quitem_linenumber ;;
  }

  dimension: quitem_memo {
    type: string
    sql: ${TABLE}.quitem_memo ;;
  }

  dimension: quitem_order_warehous_id {
    type: number
    sql: ${TABLE}.quitem_order_warehous_id ;;
  }

  dimension: quitem_prcost {
    type: number
    sql: ${TABLE}.quitem_prcost ;;
  }

  dimension: quitem_price {
    type: number
    sql: ${TABLE}.quitem_price ;;
  }

  dimension: quitem_price_invuomratio {
    type: number
    sql: ${TABLE}.quitem_price_invuomratio ;;
  }

  dimension: quitem_price_uom_id {
    type: number
    sql: ${TABLE}.quitem_price_uom_id ;;
  }

  dimension: quitem_pricemode {
    type: string
    sql: ${TABLE}.quitem_pricemode ;;
  }

  dimension_group: quitem_promdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.quitem_promdate ;;
  }

  dimension: quitem_qty_invuomratio {
    type: number
    sql: ${TABLE}.quitem_qty_invuomratio ;;
  }

  dimension: quitem_qty_uom_id {
    type: number
    sql: ${TABLE}.quitem_qty_uom_id ;;
  }

  dimension: quitem_qtyord {
    type: number
    sql: ${TABLE}.quitem_qtyord ;;
  }

  dimension: quitem_quhead_id {
    type: number
    sql: ${TABLE}.quitem_quhead_id ;;
  }

  dimension_group: quitem_scheddate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.quitem_scheddate ;;
  }

  dimension: quitem_taxtype_id {
    type: number
    sql: ${TABLE}.quitem_taxtype_id ;;
  }

  dimension: quitem_unitcost {
    type: number
    sql: ${TABLE}.quitem_unitcost ;;
  }

  measure: count {
    type: count
    drill_fields: [quitem_id]
  }
}
