view: checkrecip {
  sql_table_name: public.checkrecip ;;

  dimension: checkrecip_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.checkrecip_id ;;
  }

  dimension: checkrecip_accnt_id {
    type: number
    sql: ${TABLE}.checkrecip_accnt_id ;;
  }

  dimension: checkrecip_addr_id {
    type: number
    sql: ${TABLE}.checkrecip_addr_id ;;
  }

  dimension: checkrecip_gltrans_source {
    type: string
    sql: ${TABLE}.checkrecip_gltrans_source ;;
  }

  dimension: checkrecip_name {
    type: string
    sql: ${TABLE}.checkrecip_name ;;
  }

  dimension: checkrecip_number {
    type: string
    sql: ${TABLE}.checkrecip_number ;;
  }

  dimension: checkrecip_type {
    type: string
    sql: ${TABLE}.checkrecip_type ;;
  }

  measure: count {
    type: count
    drill_fields: [checkrecip_id, checkrecip_name]
  }
}
