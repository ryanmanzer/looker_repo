view: trgthist {
  sql_table_name: public.trgthist ;;

  dimension: trgthist_col {
    type: string
    sql: ${TABLE}.trgthist_col ;;
  }

  dimension: trgthist_src_cntct_id {
    type: number
    sql: ${TABLE}.trgthist_src_cntct_id ;;
  }

  dimension: trgthist_trgt_cntct_id {
    type: number
    sql: ${TABLE}.trgthist_trgt_cntct_id ;;
  }

  dimension: trgthist_value {
    type: string
    sql: ${TABLE}.trgthist_value ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
