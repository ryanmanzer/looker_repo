view: fincharg {
  sql_table_name: public.fincharg ;;

  dimension: fincharg_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.fincharg_id ;;
  }

  dimension: fincharg_accnt_id {
    type: number
    sql: ${TABLE}.fincharg_accnt_id ;;
  }

  dimension: fincharg_air {
    type: number
    sql: ${TABLE}.fincharg_air ;;
  }

  dimension: fincharg_assessoverdue {
    type: yesno
    sql: ${TABLE}.fincharg_assessoverdue ;;
  }

  dimension: fincharg_calcfrom {
    type: number
    sql: ${TABLE}.fincharg_calcfrom ;;
  }

  dimension: fincharg_graceperiod {
    type: number
    sql: ${TABLE}.fincharg_graceperiod ;;
  }

  dimension: fincharg_lastfc_custidfrom {
    type: string
    sql: ${TABLE}.fincharg_lastfc_custidfrom ;;
  }

  dimension: fincharg_lastfc_custidto {
    type: string
    sql: ${TABLE}.fincharg_lastfc_custidto ;;
  }

  dimension: fincharg_lastfc_statementcyclefrom {
    type: string
    sql: ${TABLE}.fincharg_lastfc_statementcyclefrom ;;
  }

  dimension: fincharg_lastfc_statementcycleto {
    type: string
    sql: ${TABLE}.fincharg_lastfc_statementcycleto ;;
  }

  dimension: fincharg_markoninvoice {
    type: string
    sql: ${TABLE}.fincharg_markoninvoice ;;
  }

  dimension: fincharg_mincharg {
    type: number
    sql: ${TABLE}.fincharg_mincharg ;;
  }

  dimension: fincharg_salescat_id {
    type: number
    sql: ${TABLE}.fincharg_salescat_id ;;
  }

  measure: count {
    type: count
    drill_fields: [fincharg_id]
  }
}
