view: cust {
  sql_table_name: public.cust ;;

  dimension: cust_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.cust_id ;;
  }

  dimension: cust_active {
    type: yesno
    sql: ${TABLE}.cust_active ;;
  }

  dimension: cust_address1 {
    type: string
    sql: ${TABLE}.cust_address1 ;;
  }

  dimension: cust_address2 {
    type: string
    sql: ${TABLE}.cust_address2 ;;
  }

  dimension: cust_address3 {
    type: string
    sql: ${TABLE}.cust_address3 ;;
  }

  dimension: cust_autoholdorders {
    type: yesno
    sql: ${TABLE}.cust_autoholdorders ;;
  }

  dimension: cust_autoupdatestatus {
    type: yesno
    sql: ${TABLE}.cust_autoupdatestatus ;;
  }

  dimension: cust_backorder {
    type: yesno
    sql: ${TABLE}.cust_backorder ;;
  }

  dimension: cust_balmethod {
    type: string
    sql: ${TABLE}.cust_balmethod ;;
  }

  dimension: cust_blanketpos {
    type: yesno
    sql: ${TABLE}.cust_blanketpos ;;
  }

  dimension: cust_city {
    type: string
    sql: ${TABLE}.cust_city ;;
  }

  dimension: cust_comments {
    type: string
    sql: ${TABLE}.cust_comments ;;
  }

  dimension: cust_commprcnt {
    type: number
    sql: ${TABLE}.cust_commprcnt ;;
  }

  dimension: cust_contact {
    type: string
    sql: ${TABLE}.cust_contact ;;
  }

  dimension: cust_corraddress1 {
    type: string
    sql: ${TABLE}.cust_corraddress1 ;;
  }

  dimension: cust_corraddress2 {
    type: string
    sql: ${TABLE}.cust_corraddress2 ;;
  }

  dimension: cust_corraddress3 {
    type: string
    sql: ${TABLE}.cust_corraddress3 ;;
  }

  dimension: cust_corrcity {
    type: string
    sql: ${TABLE}.cust_corrcity ;;
  }

  dimension: cust_corrcontact {
    type: string
    sql: ${TABLE}.cust_corrcontact ;;
  }

  dimension: cust_corrcountry {
    type: string
    sql: ${TABLE}.cust_corrcountry ;;
  }

  dimension: cust_corremail {
    type: string
    sql: ${TABLE}.cust_corremail ;;
  }

  dimension: cust_corrfax {
    type: string
    sql: ${TABLE}.cust_corrfax ;;
  }

  dimension: cust_corrphone {
    type: string
    sql: ${TABLE}.cust_corrphone ;;
  }

  dimension: cust_corrstate {
    type: string
    sql: ${TABLE}.cust_corrstate ;;
  }

  dimension: cust_corrzipcode {
    type: string
    sql: ${TABLE}.cust_corrzipcode ;;
  }

  dimension: cust_country {
    type: string
    sql: ${TABLE}.cust_country ;;
  }

  dimension: cust_creditlmt {
    type: number
    sql: ${TABLE}.cust_creditlmt ;;
  }

  dimension: cust_creditlmt_curr_id {
    type: number
    sql: ${TABLE}.cust_creditlmt_curr_id ;;
  }

  dimension: cust_creditrating {
    type: string
    sql: ${TABLE}.cust_creditrating ;;
  }

  dimension: cust_creditstatus {
    type: string
    sql: ${TABLE}.cust_creditstatus ;;
  }

  dimension: cust_curr_id {
    type: number
    sql: ${TABLE}.cust_curr_id ;;
  }

  dimension: cust_custtype_id {
    type: number
    sql: ${TABLE}.cust_custtype_id ;;
  }

  dimension_group: cust_dateadded {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cust_dateadded ;;
  }

  dimension: cust_discntprcnt {
    type: number
    sql: ${TABLE}.cust_discntprcnt ;;
  }

  dimension: cust_edicc {
    type: string
    sql: ${TABLE}.cust_edicc ;;
  }

  dimension: cust_ediemail {
    type: string
    sql: ${TABLE}.cust_ediemail ;;
  }

  dimension: cust_ediemailbody {
    type: string
    sql: ${TABLE}.cust_ediemailbody ;;
  }

  dimension: cust_edifilename {
    type: string
    sql: ${TABLE}.cust_edifilename ;;
  }

  dimension: cust_ediprofile_id {
    type: number
    sql: ${TABLE}.cust_ediprofile_id ;;
  }

  dimension: cust_edisubject {
    type: string
    sql: ${TABLE}.cust_edisubject ;;
  }

  dimension: cust_email {
    type: string
    sql: ${TABLE}.cust_email ;;
  }

  dimension: cust_emaildelivery {
    type: yesno
    sql: ${TABLE}.cust_emaildelivery ;;
  }

  dimension: cust_exported {
    type: yesno
    sql: ${TABLE}.cust_exported ;;
  }

  dimension: cust_fax {
    type: string
    sql: ${TABLE}.cust_fax ;;
  }

  dimension: cust_ffbillto {
    type: yesno
    sql: ${TABLE}.cust_ffbillto ;;
  }

  dimension: cust_ffshipto {
    type: yesno
    sql: ${TABLE}.cust_ffshipto ;;
  }

  dimension: cust_financecharge {
    type: yesno
    sql: ${TABLE}.cust_financecharge ;;
  }

  dimension: cust_name {
    type: string
    sql: ${TABLE}.cust_name ;;
  }

  dimension: cust_number {
    type: string
    sql: ${TABLE}.cust_number ;;
  }

  dimension: cust_partialship {
    type: yesno
    sql: ${TABLE}.cust_partialship ;;
  }

  dimension: cust_phone {
    type: string
    sql: ${TABLE}.cust_phone ;;
  }

  dimension: cust_preferred_warehous_id {
    type: number
    sql: ${TABLE}.cust_preferred_warehous_id ;;
  }

  dimension: cust_salesrep_id {
    type: number
    sql: ${TABLE}.cust_salesrep_id ;;
  }

  dimension: cust_shipchrg_id {
    type: number
    sql: ${TABLE}.cust_shipchrg_id ;;
  }

  dimension: cust_shipform_id {
    type: number
    sql: ${TABLE}.cust_shipform_id ;;
  }

  dimension: cust_shipvia {
    type: string
    sql: ${TABLE}.cust_shipvia ;;
  }

  dimension: cust_state {
    type: string
    sql: ${TABLE}.cust_state ;;
  }

  dimension: cust_taxzone_id {
    type: number
    sql: ${TABLE}.cust_taxzone_id ;;
  }

  dimension: cust_terms_id {
    type: number
    sql: ${TABLE}.cust_terms_id ;;
  }

  dimension: cust_usespos {
    type: yesno
    sql: ${TABLE}.cust_usespos ;;
  }

  dimension: cust_zipcode {
    type: string
    sql: ${TABLE}.cust_zipcode ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      cust_id,
      cust_name,
      cust_edifilename,
      custinfo.count,
      saleshistory.count,
      saleshistorymisc.count
    ]
  }
}
