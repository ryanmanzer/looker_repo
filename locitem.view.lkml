view: locitem {
  sql_table_name: public.locitem ;;

  dimension: locitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.locitem_id ;;
  }

  dimension: locitem_item_id {
    type: number
    sql: ${TABLE}.locitem_item_id ;;
  }

  dimension: locitem_location_id {
    type: number
    sql: ${TABLE}.locitem_location_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [locitem_id]
  }
}
