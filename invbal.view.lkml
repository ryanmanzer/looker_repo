view: invbal {
  sql_table_name: public.invbal ;;

  dimension: invbal_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.invbal_id ;;
  }

  dimension: invbal_dirty {
    type: yesno
    sql: ${TABLE}.invbal_dirty ;;
  }

  dimension: invbal_itemsite_id {
    type: number
    sql: ${TABLE}.invbal_itemsite_id ;;
  }

  dimension: invbal_nn_beginning {
    type: number
    sql: ${TABLE}.invbal_nn_beginning ;;
  }

  dimension: invbal_nn_ending {
    type: number
    sql: ${TABLE}.invbal_nn_ending ;;
  }

  dimension: invbal_nn_in {
    type: number
    sql: ${TABLE}.invbal_nn_in ;;
  }

  dimension: invbal_nn_out {
    type: number
    sql: ${TABLE}.invbal_nn_out ;;
  }

  dimension: invbal_nnval_beginning {
    type: number
    sql: ${TABLE}.invbal_nnval_beginning ;;
  }

  dimension: invbal_nnval_ending {
    type: number
    sql: ${TABLE}.invbal_nnval_ending ;;
  }

  dimension: invbal_nnval_in {
    type: number
    sql: ${TABLE}.invbal_nnval_in ;;
  }

  dimension: invbal_nnval_out {
    type: number
    sql: ${TABLE}.invbal_nnval_out ;;
  }

  dimension: invbal_period_id {
    type: number
    sql: ${TABLE}.invbal_period_id ;;
  }

  dimension: invbal_qoh_beginning {
    type: number
    sql: ${TABLE}.invbal_qoh_beginning ;;
  }

  dimension: invbal_qoh_ending {
    type: number
    sql: ${TABLE}.invbal_qoh_ending ;;
  }

  dimension: invbal_qty_in {
    type: number
    sql: ${TABLE}.invbal_qty_in ;;
  }

  dimension: invbal_qty_out {
    type: number
    sql: ${TABLE}.invbal_qty_out ;;
  }

  dimension: invbal_value_beginning {
    type: number
    sql: ${TABLE}.invbal_value_beginning ;;
  }

  dimension: invbal_value_ending {
    type: number
    sql: ${TABLE}.invbal_value_ending ;;
  }

  dimension: invbal_value_in {
    type: number
    sql: ${TABLE}.invbal_value_in ;;
  }

  dimension: invbal_value_out {
    type: number
    sql: ${TABLE}.invbal_value_out ;;
  }

  measure: count {
    type: count
    drill_fields: [invbal_id]
  }
}
