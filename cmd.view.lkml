view: cmd {
  sql_table_name: public.cmd ;;

  dimension: cmd_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.cmd_id ;;
  }

  dimension: cmd_descrip {
    type: string
    sql: ${TABLE}.cmd_descrip ;;
  }

  dimension: cmd_executable {
    type: string
    sql: ${TABLE}.cmd_executable ;;
  }

  dimension: cmd_module {
    type: string
    sql: ${TABLE}.cmd_module ;;
  }

  dimension: cmd_name {
    type: string
    sql: ${TABLE}.cmd_name ;;
  }

  dimension: cmd_privname {
    type: string
    sql: ${TABLE}.cmd_privname ;;
  }

  dimension: cmd_title {
    type: string
    sql: ${TABLE}.cmd_title ;;
  }

  measure: count {
    type: count
    drill_fields: [cmd_id, cmd_privname, cmd_name]
  }
}
