view: apchkitem {
  sql_table_name: public.apchkitem ;;

  dimension: apchkitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.apchkitem_id ;;
  }

  dimension: apchkitem_amount {
    type: number
    sql: ${TABLE}.apchkitem_amount ;;
  }

  dimension: apchkitem_apchk_id {
    type: number
    sql: ${TABLE}.apchkitem_apchk_id ;;
  }

  dimension: apchkitem_apopen_id {
    type: number
    sql: ${TABLE}.apchkitem_apopen_id ;;
  }

  dimension: apchkitem_curr_id {
    type: number
    sql: ${TABLE}.apchkitem_curr_id ;;
  }

  dimension: apchkitem_discount {
    type: number
    sql: ${TABLE}.apchkitem_discount ;;
  }

  dimension_group: apchkitem_docdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.apchkitem_docdate ;;
  }

  dimension: apchkitem_invcnumber {
    type: string
    sql: ${TABLE}.apchkitem_invcnumber ;;
  }

  dimension: apchkitem_ponumber {
    type: string
    sql: ${TABLE}.apchkitem_ponumber ;;
  }

  dimension: apchkitem_vouchernumber {
    type: string
    sql: ${TABLE}.apchkitem_vouchernumber ;;
  }

  measure: count {
    type: count
    drill_fields: [apchkitem_id]
  }
}
