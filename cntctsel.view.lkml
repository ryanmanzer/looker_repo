view: cntctsel {
  sql_table_name: public.cntctsel ;;

  dimension: cntctsel_cntct_id {
    type: number
    sql: ${TABLE}.cntctsel_cntct_id ;;
  }

  dimension: cntctsel_mrg_addr_id {
    type: yesno
    sql: ${TABLE}.cntctsel_mrg_addr_id ;;
  }

  dimension: cntctsel_mrg_crmacct_id {
    type: yesno
    sql: ${TABLE}.cntctsel_mrg_crmacct_id ;;
  }

  dimension: cntctsel_mrg_email {
    type: yesno
    sql: ${TABLE}.cntctsel_mrg_email ;;
  }

  dimension: cntctsel_mrg_fax {
    type: yesno
    sql: ${TABLE}.cntctsel_mrg_fax ;;
  }

  dimension: cntctsel_mrg_first_name {
    type: yesno
    sql: ${TABLE}.cntctsel_mrg_first_name ;;
  }

  dimension: cntctsel_mrg_honorific {
    type: yesno
    sql: ${TABLE}.cntctsel_mrg_honorific ;;
  }

  dimension: cntctsel_mrg_initials {
    type: yesno
    sql: ${TABLE}.cntctsel_mrg_initials ;;
  }

  dimension: cntctsel_mrg_last_name {
    type: yesno
    sql: ${TABLE}.cntctsel_mrg_last_name ;;
  }

  dimension: cntctsel_mrg_middle {
    type: yesno
    sql: ${TABLE}.cntctsel_mrg_middle ;;
  }

  dimension: cntctsel_mrg_notes {
    type: yesno
    sql: ${TABLE}.cntctsel_mrg_notes ;;
  }

  dimension: cntctsel_mrg_owner_username {
    type: yesno
    sql: ${TABLE}.cntctsel_mrg_owner_username ;;
  }

  dimension: cntctsel_mrg_phone {
    type: yesno
    sql: ${TABLE}.cntctsel_mrg_phone ;;
  }

  dimension: cntctsel_mrg_phone2 {
    type: yesno
    sql: ${TABLE}.cntctsel_mrg_phone2 ;;
  }

  dimension: cntctsel_mrg_suffix {
    type: yesno
    sql: ${TABLE}.cntctsel_mrg_suffix ;;
  }

  dimension: cntctsel_mrg_title {
    type: yesno
    sql: ${TABLE}.cntctsel_mrg_title ;;
  }

  dimension: cntctsel_mrg_webaddr {
    type: yesno
    sql: ${TABLE}.cntctsel_mrg_webaddr ;;
  }

  dimension: cntctsel_target {
    type: yesno
    sql: ${TABLE}.cntctsel_target ;;
  }

  measure: count {
    type: count
    drill_fields: [cntctsel_mrg_last_name, cntctsel_mrg_owner_username, cntctsel_mrg_first_name]
  }
}
