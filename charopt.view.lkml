view: charopt {
  sql_table_name: public.charopt ;;

  dimension: charopt_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.charopt_id ;;
  }

  dimension: charopt_char_id {
    type: number
    sql: ${TABLE}.charopt_char_id ;;
  }

  dimension: charopt_order {
    type: number
    sql: ${TABLE}.charopt_order ;;
  }

  dimension: charopt_value {
    type: string
    sql: ${TABLE}.charopt_value ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [charopt_id]
  }
}
