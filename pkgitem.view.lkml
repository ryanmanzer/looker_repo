view: pkgitem {
  sql_table_name: public.pkgitem ;;

  dimension: pkgitem_pkghead_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.pkgitem_pkghead_id ;;
  }

  dimension: pkgitem_descrip {
    type: string
    sql: ${TABLE}.pkgitem_descrip ;;
  }

  dimension: pkgitem_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.pkgitem_id ;;
  }

  dimension: pkgitem_item_id {
    type: number
    sql: ${TABLE}.pkgitem_item_id ;;
  }

  dimension: pkgitem_name {
    type: string
    sql: ${TABLE}.pkgitem_name ;;
  }

  dimension: pkgitem_type {
    type: string
    sql: ${TABLE}.pkgitem_type ;;
  }

  measure: count {
    type: count
    drill_fields: [pkgitem_pkghead_id, pkgitem_name, pkgitem.pkgitem_pkghead_id, pkgitem.pkgitem_name, pkgitem.count]
  }
}
