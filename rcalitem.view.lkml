view: rcalitem {
  sql_table_name: public.rcalitem ;;

  dimension: rcalitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.rcalitem_id ;;
  }

  dimension: rcalitem_calhead_id {
    type: number
    sql: ${TABLE}.rcalitem_calhead_id ;;
  }

  dimension: rcalitem_name {
    type: string
    sql: ${TABLE}.rcalitem_name ;;
  }

  dimension: rcalitem_offsetcount {
    type: number
    sql: ${TABLE}.rcalitem_offsetcount ;;
  }

  dimension: rcalitem_offsettype {
    type: string
    sql: ${TABLE}.rcalitem_offsettype ;;
  }

  dimension: rcalitem_periodcount {
    type: number
    sql: ${TABLE}.rcalitem_periodcount ;;
  }

  dimension: rcalitem_periodtype {
    type: string
    sql: ${TABLE}.rcalitem_periodtype ;;
  }

  measure: count {
    type: count
    drill_fields: [rcalitem_id, rcalitem_name]
  }
}
