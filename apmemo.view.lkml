view: apmemo {
  sql_table_name: public.apmemo ;;

  dimension: apopen_docnumber {
    type: string
    sql: ${TABLE}.apopen_docnumber ;;
  }

  dimension: apopen_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.apopen_id ;;
  }

  measure: count {
    type: count
    drill_fields: [apopen.apopen_id, apopen.apopen_username]
  }
}
