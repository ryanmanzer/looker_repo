view: urlinfo {
  sql_table_name: public.urlinfo ;;

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: url_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.url_id ;;
  }

  dimension: url_title {
    type: string
    sql: ${TABLE}.url_title ;;
  }

  dimension: url_url {
    type: string
    sql: ${TABLE}.url_url ;;
  }

  measure: count {
    type: count
    drill_fields: [url.url_id]
  }
}
