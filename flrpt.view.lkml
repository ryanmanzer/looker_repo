view: flrpt {
  sql_table_name: public.flrpt ;;

  dimension: flrpt_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.flrpt_id ;;
  }

  dimension: flrpt_accnt_id {
    type: number
    sql: ${TABLE}.flrpt_accnt_id ;;
  }

  dimension: flrpt_altname {
    type: string
    sql: ${TABLE}.flrpt_altname ;;
  }

  dimension: flrpt_beginning {
    type: number
    sql: ${TABLE}.flrpt_beginning ;;
  }

  dimension: flrpt_beginningprcnt {
    type: number
    sql: ${TABLE}.flrpt_beginningprcnt ;;
  }

  dimension: flrpt_budget {
    type: number
    sql: ${TABLE}.flrpt_budget ;;
  }

  dimension: flrpt_budgetprcnt {
    type: number
    sql: ${TABLE}.flrpt_budgetprcnt ;;
  }

  dimension: flrpt_credits {
    type: number
    sql: ${TABLE}.flrpt_credits ;;
  }

  dimension: flrpt_creditsprcnt {
    type: number
    sql: ${TABLE}.flrpt_creditsprcnt ;;
  }

  dimension: flrpt_custom {
    type: number
    sql: ${TABLE}.flrpt_custom ;;
  }

  dimension: flrpt_customprcnt {
    type: number
    sql: ${TABLE}.flrpt_customprcnt ;;
  }

  dimension: flrpt_debits {
    type: number
    sql: ${TABLE}.flrpt_debits ;;
  }

  dimension: flrpt_debitsprcnt {
    type: number
    sql: ${TABLE}.flrpt_debitsprcnt ;;
  }

  dimension: flrpt_diff {
    type: number
    sql: ${TABLE}.flrpt_diff ;;
  }

  dimension: flrpt_diffprcnt {
    type: number
    sql: ${TABLE}.flrpt_diffprcnt ;;
  }

  dimension: flrpt_ending {
    type: number
    sql: ${TABLE}.flrpt_ending ;;
  }

  dimension: flrpt_endingprcnt {
    type: number
    sql: ${TABLE}.flrpt_endingprcnt ;;
  }

  dimension: flrpt_flhead_id {
    type: number
    sql: ${TABLE}.flrpt_flhead_id ;;
  }

  dimension: flrpt_interval {
    type: string
    sql: ${TABLE}.flrpt_interval ;;
  }

  dimension: flrpt_level {
    type: number
    sql: ${TABLE}.flrpt_level ;;
  }

  dimension: flrpt_order {
    type: number
    sql: ${TABLE}.flrpt_order ;;
  }

  dimension: flrpt_parent_id {
    type: number
    sql: ${TABLE}.flrpt_parent_id ;;
  }

  dimension: flrpt_period_id {
    type: number
    sql: ${TABLE}.flrpt_period_id ;;
  }

  dimension: flrpt_type {
    type: string
    sql: ${TABLE}.flrpt_type ;;
  }

  dimension: flrpt_type_id {
    type: number
    sql: ${TABLE}.flrpt_type_id ;;
  }

  dimension: flrpt_username {
    type: string
    sql: ${TABLE}.flrpt_username ;;
  }

  measure: count {
    type: count
    drill_fields: [flrpt_id, flrpt_altname, flrpt_username]
  }
}
