view: coship {
  sql_table_name: public.coship ;;

  dimension: coship_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.coship_id ;;
  }

  dimension: coship_coitem_id {
    type: number
    sql: ${TABLE}.coship_coitem_id ;;
  }

  dimension: coship_cosmisc_id {
    type: number
    sql: ${TABLE}.coship_cosmisc_id ;;
  }

  dimension: coship_invcitem_id {
    type: number
    sql: ${TABLE}.coship_invcitem_id ;;
  }

  dimension: coship_invoiced {
    type: yesno
    sql: ${TABLE}.coship_invoiced ;;
  }

  dimension: coship_qty {
    type: number
    sql: ${TABLE}.coship_qty ;;
  }

  dimension_group: coship_shipdate {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.coship_shipdate ;;
  }

  dimension: coship_shipped {
    type: yesno
    sql: ${TABLE}.coship_shipped ;;
  }

  dimension: coship_trans_username {
    type: string
    sql: ${TABLE}.coship_trans_username ;;
  }

  dimension_group: coship_transdate {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.coship_transdate ;;
  }

  measure: count {
    type: count
    drill_fields: [coship_id, coship_trans_username]
  }
}
