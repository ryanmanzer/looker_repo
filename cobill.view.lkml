view: cobill {
  sql_table_name: public.cobill ;;

  dimension: cobill_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.cobill_id ;;
  }

  dimension: cobill_cobmisc_id {
    type: number
    sql: ${TABLE}.cobill_cobmisc_id ;;
  }

  dimension: cobill_coitem_id {
    type: number
    sql: ${TABLE}.cobill_coitem_id ;;
  }

  dimension: cobill_invcitem_id {
    type: number
    sql: ${TABLE}.cobill_invcitem_id ;;
  }

  dimension: cobill_invcnum {
    type: number
    sql: ${TABLE}.cobill_invcnum ;;
  }

  dimension: cobill_qty {
    type: number
    sql: ${TABLE}.cobill_qty ;;
  }

  dimension: cobill_select_username {
    type: string
    sql: ${TABLE}.cobill_select_username ;;
  }

  dimension_group: cobill_selectdate {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.cobill_selectdate ;;
  }

  dimension: cobill_taxtype_id {
    type: number
    sql: ${TABLE}.cobill_taxtype_id ;;
  }

  dimension: cobill_toclose {
    type: yesno
    sql: ${TABLE}.cobill_toclose ;;
  }

  measure: count {
    type: count
    drill_fields: [cobill_id, cobill_select_username]
  }
}
