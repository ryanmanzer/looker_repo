view: budgitem {
  sql_table_name: public.budgitem ;;

  dimension: budgitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.budgitem_id ;;
  }

  dimension: budgitem_accnt_id {
    type: number
    sql: ${TABLE}.budgitem_accnt_id ;;
  }

  dimension: budgitem_amount {
    type: number
    sql: ${TABLE}.budgitem_amount ;;
  }

  dimension: budgitem_budghead_id {
    type: number
    sql: ${TABLE}.budgitem_budghead_id ;;
  }

  dimension: budgitem_period_id {
    type: number
    sql: ${TABLE}.budgitem_period_id ;;
  }

  measure: count {
    type: count
    drill_fields: [budgitem_id]
  }
}
