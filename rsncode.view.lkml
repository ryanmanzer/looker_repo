view: rsncode {
  sql_table_name: public.rsncode ;;

  dimension: rsncode_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.rsncode_id ;;
  }

  dimension: rsncode_code {
    type: string
    sql: ${TABLE}.rsncode_code ;;
  }

  dimension: rsncode_descrip {
    type: string
    sql: ${TABLE}.rsncode_descrip ;;
  }

  dimension: rsncode_doctype {
    type: string
    sql: ${TABLE}.rsncode_doctype ;;
  }

  measure: count {
    type: count
    drill_fields: [rsncode_id]
  }
}
