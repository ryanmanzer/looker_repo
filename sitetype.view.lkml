view: sitetype {
  sql_table_name: public.sitetype ;;

  dimension: sitetype_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.sitetype_id ;;
  }

  dimension: sitetype_descrip {
    type: string
    sql: ${TABLE}.sitetype_descrip ;;
  }

  dimension: sitetype_name {
    type: string
    sql: ${TABLE}.sitetype_name ;;
  }

  measure: count {
    type: count
    drill_fields: [sitetype_id, sitetype_name]
  }
}
