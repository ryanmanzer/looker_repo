view: schemaord {
  sql_table_name: public.schemaord ;;

  dimension: schemaord_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.schemaord_id ;;
  }

  dimension: schemaord_name {
    type: string
    sql: ${TABLE}.schemaord_name ;;
  }

  dimension: schemaord_order {
    type: number
    sql: ${TABLE}.schemaord_order ;;
  }

  measure: count {
    type: count
    drill_fields: [schemaord_id, schemaord_name]
  }
}
