view: _newproducts_q1 {
  derived_table: {
    sql: SELECT

            cust_id,
            cohist_id,
            cust_number,
            cust_name,
            custtype_descrip as custtype_code,
            salesrep_name as salesrep,
            EXTRACT (year FROM cust_dateadded) as custaddedyear,
            EXTRACT (month FROM cust_dateadded) as custaddedmonth,
            cust_dateadded as custaddeddate,
            UPPER(cohist_shiptocity) as to_city,
            UPPER(cohist_shiptostate) as to_state,
            cohist_shiptozip as to_zip,
            EXTRACT (year FROM cohist_invcdate) as invcyear,
            EXTRACT (month FROM cohist_invcdate) as invcmonth,
            cohist_invcdate as invcdate,
            ((cohist_invcdate - cust_dateadded)/365) as account_age_yrs,
            cohist_ordernumber as order_number,
            item_id as item_id,
            item_number as item_num,
            item_descrip1 as item_name,
            round (cohist_qtyshipped) as quantity,
            round(cohist_unitprice,2) as cohist_unitprice,
            round(item_listprice,2) as base_price,
            sum (round(cohist_qtyshipped * cohist_unitprice,2)) as extprice

            FROM cohist

            LEFT JOIN itemsite on itemsite_id = cohist_itemsite_id
            LEFT JOIN item on item_id = itemsite_item_id
            LEFT JOIN custinfo on cust_id = cohist_cust_id
            LEFT JOIN cntct on cust_cntct_id = cntct_id
            LEFT JOIN salesrep on salesrep_id = cust_salesrep_id
            LEFT JOIN addr on cntct_addr_id = addr_id
            LEFT JOIN custtype on cust_custtype_id = custtype_id
            LEFT JOIN charass on charass_target_id = item_id
            LEFT JOIN char on char_id = charass_char_id

            WHERE char.char_name = 'IsCommissioned'
            and charass.charass_value ilike 'TRUE'
            and charass.charass_target_type = 'I'
            and (item_number = 'RUS-01676' or item_number = 'RUS-01677' or item_number = 'RUS-01672')
            and (custtype_descrip ilike '%retail%' or custtype_descrip ilike '%mass%')
            and cohist_invcdate > '2017/01/08'
            GROUP BY

            cust_id,
            cohist_id,
            cust_number,
            cust_name,
            custtype_descrip,
            salesrep,
            custaddedyear,
            custaddedmonth,
            custaddeddate,
            to_city,
            to_state,
            invcyear,
            invcmonth,
            invcdate,
            order_number,
            item_id,
            item_num,
            item_name,
            quantity,
            cohist_unitprice,
            base_price
       ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: cust_id {
    type: string
    sql: ${TABLE}.cust_id ;;
  }

  dimension: cohist_id {
    type: string
    sql: ${TABLE}.cohist_id ;;
  }

  dimension: cust_number {
    type: string
    sql: ${TABLE}.cust_number ;;
  }

  dimension: cust_name {
    type: string
    sql: ${TABLE}.cust_name ;;
  }

  dimension: custtype_code {
    type: string
    sql: ${TABLE}.custtype_code ;;
  }

  dimension: salesrep {
    type: string
    sql: ${TABLE}.salesrep ;;
  }

  dimension: custaddedyear {
    type: number
    sql: ${TABLE}.custaddedyear ;;
  }

  dimension: custaddedmonth {
    type: number
    sql: ${TABLE}.custaddedmonth ;;
  }

  dimension: custaddeddate {
    type: date
    sql: ${TABLE}.custaddeddate ;;
  }

  dimension: to_city {
    type: string
    sql: ${TABLE}.to_city ;;
  }

  dimension: to_state {
    type: string
    sql: ${TABLE}.to_state ;;
  }

  dimension: to_zip {
    type: zipcode
    sql: ${TABLE}.to_zip ;;
  }

  dimension: invcyear {
    type: number
    sql: ${TABLE}.invcyear ;;
  }

  dimension: invcmonth {
    type: number
    sql: ${TABLE}.invcmonth ;;
  }

  dimension: invcdate {
    type: date
    sql: ${TABLE}.invcdate ;;
  }

  dimension: account_age_yrs {
    type: number
    sql: ${TABLE}.account_age_yrs ;;
  }

  dimension: order_number {
    type: string
    sql: ${TABLE}.order_number ;;
  }

  dimension: item_id {
    type: string
    sql: ${TABLE}.item_id ;;
  }

  dimension: item_num {
    type: string
    sql: ${TABLE}.item_num ;;
  }

  dimension: item_name {
    type: string
    sql: ${TABLE}.item_name ;;
  }

  dimension: quantity {
    type: number
    sql: ${TABLE}.quantity ;;
  }

  dimension: cohist_unitprice {
    type: number
    sql: ${TABLE}.cohist_unitprice ;;
  }

  dimension: base_price {
    type: number
    sql: ${TABLE}.base_price ;;
  }

  dimension: extprice {
    type: number
    sql: ${TABLE}.extprice ;;
  }
  measure: total_sales {
    type: sum
    sql: ${extprice} ;;
  }
  measure: total_units {
    type:  sum
    sql: ${quantity} ;;
  }
  set: detail {
    fields: [
      cust_id,
      cohist_id,
      cust_number,
      cust_name,
      custtype_code,
      salesrep,
      custaddedyear,
      custaddedmonth,
      custaddeddate,
      to_city,
      to_state,
      to_zip,
      invcyear,
      invcmonth,
      invcdate,
      account_age_yrs,
      order_number,
      item_id,
      item_num,
      item_name,
      quantity,
      cohist_unitprice,
      base_price,
      extprice
    ]
  }
}
