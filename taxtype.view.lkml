view: taxtype {
  sql_table_name: public.taxtype ;;

  dimension: taxtype_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.taxtype_id ;;
  }

  dimension: taxtype_descrip {
    type: string
    sql: ${TABLE}.taxtype_descrip ;;
  }

  dimension: taxtype_name {
    type: string
    sql: ${TABLE}.taxtype_name ;;
  }

  dimension: taxtype_sys {
    type: yesno
    sql: ${TABLE}.taxtype_sys ;;
  }

  measure: count {
    type: count
    drill_fields: [taxtype_id, taxtype_name]
  }
}
