view: bankaccnt {
  sql_table_name: public.bankaccnt ;;

  dimension: bankaccnt_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.bankaccnt_id ;;
  }

  dimension: bankaccnt_accnt_id {
    type: number
    sql: ${TABLE}.bankaccnt_accnt_id ;;
  }

  dimension: bankaccnt_accntnumber {
    type: string
    sql: ${TABLE}.bankaccnt_accntnumber ;;
  }

  dimension: bankaccnt_ach_dest {
    type: string
    sql: ${TABLE}.bankaccnt_ach_dest ;;
  }

  dimension: bankaccnt_ach_destname {
    type: string
    sql: ${TABLE}.bankaccnt_ach_destname ;;
  }

  dimension: bankaccnt_ach_desttype {
    type: string
    sql: ${TABLE}.bankaccnt_ach_desttype ;;
  }

  dimension: bankaccnt_ach_enabled {
    type: yesno
    sql: ${TABLE}.bankaccnt_ach_enabled ;;
  }

  dimension: bankaccnt_ach_fed_dest {
    type: string
    sql: ${TABLE}.bankaccnt_ach_fed_dest ;;
  }

  dimension: bankaccnt_ach_genchecknum {
    type: yesno
    sql: ${TABLE}.bankaccnt_ach_genchecknum ;;
  }

  dimension_group: bankaccnt_ach_lastdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.bankaccnt_ach_lastdate ;;
  }

  dimension: bankaccnt_ach_lastfileid {
    type: string
    sql: ${TABLE}.bankaccnt_ach_lastfileid ;;
  }

  dimension: bankaccnt_ach_leadtime {
    type: number
    sql: ${TABLE}.bankaccnt_ach_leadtime ;;
  }

  dimension: bankaccnt_ach_origin {
    type: string
    sql: ${TABLE}.bankaccnt_ach_origin ;;
  }

  dimension: bankaccnt_ach_originname {
    type: string
    sql: ${TABLE}.bankaccnt_ach_originname ;;
  }

  dimension: bankaccnt_ach_origintype {
    type: string
    sql: ${TABLE}.bankaccnt_ach_origintype ;;
  }

  dimension: bankaccnt_ap {
    type: yesno
    sql: ${TABLE}.bankaccnt_ap ;;
  }

  dimension: bankaccnt_ar {
    type: yesno
    sql: ${TABLE}.bankaccnt_ar ;;
  }

  dimension: bankaccnt_bankname {
    type: string
    sql: ${TABLE}.bankaccnt_bankname ;;
  }

  dimension: bankaccnt_check_form_id {
    type: number
    sql: ${TABLE}.bankaccnt_check_form_id ;;
  }

  dimension: bankaccnt_curr_id {
    type: number
    sql: ${TABLE}.bankaccnt_curr_id ;;
  }

  dimension: bankaccnt_descrip {
    type: string
    sql: ${TABLE}.bankaccnt_descrip ;;
  }

  dimension: bankaccnt_name {
    type: string
    sql: ${TABLE}.bankaccnt_name ;;
  }

  dimension: bankaccnt_nextchknum {
    type: number
    sql: ${TABLE}.bankaccnt_nextchknum ;;
  }

  dimension: bankaccnt_notes {
    type: string
    sql: ${TABLE}.bankaccnt_notes ;;
  }

  dimension: bankaccnt_prnt_check {
    type: yesno
    sql: ${TABLE}.bankaccnt_prnt_check ;;
  }

  dimension: bankaccnt_rec_accnt_id {
    type: number
    sql: ${TABLE}.bankaccnt_rec_accnt_id ;;
  }

  dimension: bankaccnt_routing {
    type: string
    sql: ${TABLE}.bankaccnt_routing ;;
  }

  dimension: bankaccnt_type {
    type: string
    sql: ${TABLE}.bankaccnt_type ;;
  }

  dimension: bankaccnt_userec {
    type: yesno
    sql: ${TABLE}.bankaccnt_userec ;;
  }

  measure: count {
    type: count
    drill_fields: [bankaccnt_id, bankaccnt_name, bankaccnt_bankname, bankaccnt_ach_originname, bankaccnt_ach_destname]
  }
}
