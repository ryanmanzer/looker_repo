view: itemlocdist {
  sql_table_name: public.itemlocdist ;;

  dimension: itemlocdist_itemlocdist_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.itemlocdist_itemlocdist_id ;;
  }

  dimension: itemlocdist_distlotserial {
    type: yesno
    sql: ${TABLE}.itemlocdist_distlotserial ;;
  }

  dimension_group: itemlocdist_expiration {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.itemlocdist_expiration ;;
  }

  dimension: itemlocdist_flush {
    type: yesno
    sql: ${TABLE}.itemlocdist_flush ;;
  }

  dimension: itemlocdist_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.itemlocdist_id ;;
  }

  dimension: itemlocdist_invhist_id {
    type: number
    sql: ${TABLE}.itemlocdist_invhist_id ;;
  }

  dimension: itemlocdist_itemsite_id {
    type: number
    sql: ${TABLE}.itemlocdist_itemsite_id ;;
  }

  dimension: itemlocdist_ls_id {
    type: number
    sql: ${TABLE}.itemlocdist_ls_id ;;
  }

  dimension: itemlocdist_order_id {
    type: number
    sql: ${TABLE}.itemlocdist_order_id ;;
  }

  dimension: itemlocdist_order_type {
    type: string
    sql: ${TABLE}.itemlocdist_order_type ;;
  }

  dimension: itemlocdist_qty {
    type: number
    sql: ${TABLE}.itemlocdist_qty ;;
  }

  dimension: itemlocdist_reqlotserial {
    type: yesno
    sql: ${TABLE}.itemlocdist_reqlotserial ;;
  }

  dimension: itemlocdist_series {
    type: number
    sql: ${TABLE}.itemlocdist_series ;;
  }

  dimension: itemlocdist_source_id {
    type: number
    sql: ${TABLE}.itemlocdist_source_id ;;
  }

  dimension: itemlocdist_source_type {
    type: string
    sql: ${TABLE}.itemlocdist_source_type ;;
  }

  dimension_group: itemlocdist_warranty {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.itemlocdist_warranty ;;
  }

  measure: count {
    type: count
    drill_fields: [itemlocdist_itemlocdist_id, itemlocdist.itemlocdist_itemlocdist_id, itemlocdist.count]
  }
}
