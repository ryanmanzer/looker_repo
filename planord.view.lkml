view: planord {
  sql_table_name: public.planord ;;

  dimension: planord_planord_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.planord_planord_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: planord_comments {
    type: string
    sql: ${TABLE}.planord_comments ;;
  }

  dimension_group: planord_duedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.planord_duedate ;;
  }

  dimension: planord_firm {
    type: yesno
    sql: ${TABLE}.planord_firm ;;
  }

  dimension: planord_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.planord_id ;;
  }

  dimension: planord_itemsite_id {
    type: number
    sql: ${TABLE}.planord_itemsite_id ;;
  }

  dimension: planord_mps {
    type: yesno
    sql: ${TABLE}.planord_mps ;;
  }

  dimension: planord_number {
    type: number
    sql: ${TABLE}.planord_number ;;
  }

  dimension: planord_pschitem_id {
    type: number
    sql: ${TABLE}.planord_pschitem_id ;;
  }

  dimension: planord_qty {
    type: number
    sql: ${TABLE}.planord_qty ;;
  }

  dimension_group: planord_startdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.planord_startdate ;;
  }

  dimension: planord_subnumber {
    type: number
    sql: ${TABLE}.planord_subnumber ;;
  }

  dimension: planord_supply_itemsite_id {
    type: number
    sql: ${TABLE}.planord_supply_itemsite_id ;;
  }

  dimension: planord_type {
    type: string
    sql: ${TABLE}.planord_type ;;
  }

  measure: count {
    type: count
    drill_fields: [planord_planord_id, planord.planord_planord_id, planord.count]
  }
}
