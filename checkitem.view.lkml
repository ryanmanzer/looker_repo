view: checkitem {
  sql_table_name: public.checkitem ;;

  dimension: checkitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.checkitem_id ;;
  }

  dimension: checkitem_amount {
    type: number
    sql: ${TABLE}.checkitem_amount ;;
  }

  dimension: checkitem_apopen_id {
    type: number
    sql: ${TABLE}.checkitem_apopen_id ;;
  }

  dimension: checkitem_aropen_id {
    type: number
    sql: ${TABLE}.checkitem_aropen_id ;;
  }

  dimension: checkitem_checkhead_id {
    type: number
    sql: ${TABLE}.checkitem_checkhead_id ;;
  }

  dimension: checkitem_cmnumber {
    type: string
    sql: ${TABLE}.checkitem_cmnumber ;;
  }

  dimension: checkitem_curr_id {
    type: number
    sql: ${TABLE}.checkitem_curr_id ;;
  }

  dimension: checkitem_curr_rate {
    type: number
    sql: ${TABLE}.checkitem_curr_rate ;;
  }

  dimension: checkitem_discount {
    type: number
    sql: ${TABLE}.checkitem_discount ;;
  }

  dimension_group: checkitem_docdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.checkitem_docdate ;;
  }

  dimension: checkitem_invcnumber {
    type: string
    sql: ${TABLE}.checkitem_invcnumber ;;
  }

  dimension: checkitem_ponumber {
    type: string
    sql: ${TABLE}.checkitem_ponumber ;;
  }

  dimension: checkitem_ranumber {
    type: string
    sql: ${TABLE}.checkitem_ranumber ;;
  }

  dimension: checkitem_vouchernumber {
    type: string
    sql: ${TABLE}.checkitem_vouchernumber ;;
  }

  measure: count {
    type: count
    drill_fields: [checkitem_id]
  }
}
