view: labeldef {
  sql_table_name: public.labeldef ;;

  dimension: labeldef_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.labeldef_id ;;
  }

  dimension: labeldef_columns {
    type: number
    sql: ${TABLE}.labeldef_columns ;;
  }

  dimension: labeldef_height {
    type: number
    sql: ${TABLE}.labeldef_height ;;
  }

  dimension: labeldef_horizontal_gap {
    type: number
    sql: ${TABLE}.labeldef_horizontal_gap ;;
  }

  dimension: labeldef_name {
    type: string
    sql: ${TABLE}.labeldef_name ;;
  }

  dimension: labeldef_papersize {
    type: string
    sql: ${TABLE}.labeldef_papersize ;;
  }

  dimension: labeldef_rows {
    type: number
    sql: ${TABLE}.labeldef_rows ;;
  }

  dimension: labeldef_start_offset_x {
    type: number
    sql: ${TABLE}.labeldef_start_offset_x ;;
  }

  dimension: labeldef_start_offset_y {
    type: number
    sql: ${TABLE}.labeldef_start_offset_y ;;
  }

  dimension: labeldef_vertical_gap {
    type: number
    sql: ${TABLE}.labeldef_vertical_gap ;;
  }

  dimension: labeldef_width {
    type: number
    sql: ${TABLE}.labeldef_width ;;
  }

  measure: count {
    type: count
    drill_fields: [labeldef_id, labeldef_name]
  }
}
