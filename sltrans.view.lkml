view: sltrans {
  sql_table_name: public.sltrans ;;

  dimension: sltrans_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.sltrans_id ;;
  }

  dimension: sltrans_accnt_id {
    type: number
    sql: ${TABLE}.sltrans_accnt_id ;;
  }

  dimension: sltrans_amount {
    type: number
    sql: ${TABLE}.sltrans_amount ;;
  }

  dimension_group: sltrans_created {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.sltrans_created ;;
  }

  dimension_group: sltrans {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.sltrans_date ;;
  }

  dimension: sltrans_docnumber {
    type: string
    sql: ${TABLE}.sltrans_docnumber ;;
  }

  dimension: sltrans_doctype {
    type: string
    sql: ${TABLE}.sltrans_doctype ;;
  }

  dimension: sltrans_gltrans_journalnumber {
    type: number
    sql: ${TABLE}.sltrans_gltrans_journalnumber ;;
  }

  dimension: sltrans_journalnumber {
    type: number
    sql: ${TABLE}.sltrans_journalnumber ;;
  }

  dimension: sltrans_misc_id {
    type: number
    sql: ${TABLE}.sltrans_misc_id ;;
  }

  dimension: sltrans_notes {
    type: string
    sql: ${TABLE}.sltrans_notes ;;
  }

  dimension: sltrans_posted {
    type: yesno
    sql: ${TABLE}.sltrans_posted ;;
  }

  dimension: sltrans_rec {
    type: yesno
    sql: ${TABLE}.sltrans_rec ;;
  }

  dimension: sltrans_sequence {
    type: number
    sql: ${TABLE}.sltrans_sequence ;;
  }

  dimension: sltrans_source {
    type: string
    sql: ${TABLE}.sltrans_source ;;
  }

  dimension: sltrans_username {
    type: string
    sql: ${TABLE}.sltrans_username ;;
  }

  measure: count {
    type: count
    drill_fields: [sltrans_id, sltrans_username]
  }
}
