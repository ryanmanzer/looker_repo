view: state {
  sql_table_name: public.state ;;

  dimension: state_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.state_id ;;
  }

  dimension: state_abbr {
    type: string
    sql: ${TABLE}.state_abbr ;;
  }

  dimension: state_country_id {
    type: number
    sql: ${TABLE}.state_country_id ;;
  }

  dimension: state_name {
    type: string
    sql: ${TABLE}.state_name ;;
  }

  measure: count {
    type: count
    drill_fields: [state_id, state_name]
  }
}
