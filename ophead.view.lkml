view: ophead {
  sql_table_name: public.ophead ;;

  dimension: ophead_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.ophead_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: ophead_active {
    type: yesno
    sql: ${TABLE}.ophead_active ;;
  }

  dimension_group: ophead_actual {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.ophead_actual_date ;;
  }

  dimension: ophead_amount {
    type: number
    sql: ${TABLE}.ophead_amount ;;
  }

  dimension_group: ophead_assigned {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.ophead_assigned_date ;;
  }

  dimension: ophead_cntct_id {
    type: number
    sql: ${TABLE}.ophead_cntct_id ;;
  }

  dimension: ophead_crmacct_id {
    type: number
    sql: ${TABLE}.ophead_crmacct_id ;;
  }

  dimension: ophead_curr_id {
    type: number
    sql: ${TABLE}.ophead_curr_id ;;
  }

  dimension: ophead_name {
    type: string
    sql: ${TABLE}.ophead_name ;;
  }

  dimension: ophead_notes {
    type: string
    sql: ${TABLE}.ophead_notes ;;
  }

  dimension: ophead_number {
    type: string
    sql: ${TABLE}.ophead_number ;;
  }

  dimension: ophead_opsource_id {
    type: number
    sql: ${TABLE}.ophead_opsource_id ;;
  }

  dimension: ophead_opstage_id {
    type: number
    sql: ${TABLE}.ophead_opstage_id ;;
  }

  dimension: ophead_optype_id {
    type: number
    sql: ${TABLE}.ophead_optype_id ;;
  }

  dimension: ophead_owner_username {
    type: string
    sql: ${TABLE}.ophead_owner_username ;;
  }

  dimension: ophead_priority_id {
    type: number
    sql: ${TABLE}.ophead_priority_id ;;
  }

  dimension: ophead_probability_prcnt {
    type: number
    sql: ${TABLE}.ophead_probability_prcnt ;;
  }

  dimension_group: ophead_start {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.ophead_start_date ;;
  }

  dimension_group: ophead_target {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.ophead_target_date ;;
  }

  dimension: ophead_username {
    type: string
    sql: ${TABLE}.ophead_username ;;
  }

  measure: count {
    type: count
    drill_fields: [ophead_id, ophead_name, ophead_owner_username, ophead_username]
  }
}
