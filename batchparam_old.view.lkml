view: batchparam_old {
  sql_table_name: public.batchparam_old ;;

  dimension: batchparam_batch_id {
    type: number
    sql: ${TABLE}.batchparam_batch_id ;;
  }

  dimension: batchparam_id {
    type: number
    sql: ${TABLE}.batchparam_id ;;
  }

  dimension: batchparam_name {
    type: string
    sql: ${TABLE}.batchparam_name ;;
  }

  dimension: batchparam_order {
    type: number
    sql: ${TABLE}.batchparam_order ;;
  }

  dimension: batchparam_type {
    type: string
    sql: ${TABLE}.batchparam_type ;;
  }

  dimension: batchparam_value {
    type: string
    sql: ${TABLE}.batchparam_value ;;
  }

  measure: count {
    type: count
    drill_fields: [batchparam_name]
  }
}
