view: crmacctsel {
  sql_table_name: public.crmacctsel ;;

  dimension: crmacctsel_dest_crmacct_id {
    type: number
    sql: ${TABLE}.crmacctsel_dest_crmacct_id ;;
  }

  dimension: crmacctsel_mrg_crmacct_active {
    type: yesno
    sql: ${TABLE}.crmacctsel_mrg_crmacct_active ;;
  }

  dimension: crmacctsel_mrg_crmacct_cntct_id_1 {
    type: yesno
    sql: ${TABLE}.crmacctsel_mrg_crmacct_cntct_id_1 ;;
  }

  dimension: crmacctsel_mrg_crmacct_cntct_id_2 {
    type: yesno
    sql: ${TABLE}.crmacctsel_mrg_crmacct_cntct_id_2 ;;
  }

  dimension: crmacctsel_mrg_crmacct_competitor_id {
    type: yesno
    sql: ${TABLE}.crmacctsel_mrg_crmacct_competitor_id ;;
  }

  dimension: crmacctsel_mrg_crmacct_cust_id {
    type: yesno
    sql: ${TABLE}.crmacctsel_mrg_crmacct_cust_id ;;
  }

  dimension: crmacctsel_mrg_crmacct_emp_id {
    type: yesno
    sql: ${TABLE}.crmacctsel_mrg_crmacct_emp_id ;;
  }

  dimension: crmacctsel_mrg_crmacct_name {
    type: yesno
    sql: ${TABLE}.crmacctsel_mrg_crmacct_name ;;
  }

  dimension: crmacctsel_mrg_crmacct_notes {
    type: yesno
    sql: ${TABLE}.crmacctsel_mrg_crmacct_notes ;;
  }

  dimension: crmacctsel_mrg_crmacct_number {
    type: yesno
    sql: ${TABLE}.crmacctsel_mrg_crmacct_number ;;
  }

  dimension: crmacctsel_mrg_crmacct_owner_username {
    type: yesno
    sql: ${TABLE}.crmacctsel_mrg_crmacct_owner_username ;;
  }

  dimension: crmacctsel_mrg_crmacct_parent_id {
    type: yesno
    sql: ${TABLE}.crmacctsel_mrg_crmacct_parent_id ;;
  }

  dimension: crmacctsel_mrg_crmacct_partner_id {
    type: yesno
    sql: ${TABLE}.crmacctsel_mrg_crmacct_partner_id ;;
  }

  dimension: crmacctsel_mrg_crmacct_prospect_id {
    type: yesno
    sql: ${TABLE}.crmacctsel_mrg_crmacct_prospect_id ;;
  }

  dimension: crmacctsel_mrg_crmacct_salesrep_id {
    type: yesno
    sql: ${TABLE}.crmacctsel_mrg_crmacct_salesrep_id ;;
  }

  dimension: crmacctsel_mrg_crmacct_taxauth_id {
    type: yesno
    sql: ${TABLE}.crmacctsel_mrg_crmacct_taxauth_id ;;
  }

  dimension: crmacctsel_mrg_crmacct_type {
    type: yesno
    sql: ${TABLE}.crmacctsel_mrg_crmacct_type ;;
  }

  dimension: crmacctsel_mrg_crmacct_usr_username {
    type: yesno
    sql: ${TABLE}.crmacctsel_mrg_crmacct_usr_username ;;
  }

  dimension: crmacctsel_mrg_crmacct_vend_id {
    type: yesno
    sql: ${TABLE}.crmacctsel_mrg_crmacct_vend_id ;;
  }

  dimension: crmacctsel_src_crmacct_id {
    type: number
    sql: ${TABLE}.crmacctsel_src_crmacct_id ;;
  }

  measure: count {
    type: count
    drill_fields: [crmacctsel_mrg_crmacct_name, crmacctsel_mrg_crmacct_owner_username, crmacctsel_mrg_crmacct_usr_username]
  }
}
