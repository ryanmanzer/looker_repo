view: arcreditapply {
  sql_table_name: public.arcreditapply ;;

  dimension: arcreditapply_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.arcreditapply_id ;;
  }

  dimension: arcreditapply_amount {
    type: number
    sql: ${TABLE}.arcreditapply_amount ;;
  }

  dimension: arcreditapply_curr_id {
    type: number
    sql: ${TABLE}.arcreditapply_curr_id ;;
  }

  dimension: arcreditapply_ref_id {
    type: number
    sql: ${TABLE}.arcreditapply_ref_id ;;
  }

  dimension: arcreditapply_reftype {
    type: string
    sql: ${TABLE}.arcreditapply_reftype ;;
  }

  dimension: arcreditapply_source_aropen_id {
    type: number
    sql: ${TABLE}.arcreditapply_source_aropen_id ;;
  }

  dimension: arcreditapply_target_aropen_id {
    type: number
    sql: ${TABLE}.arcreditapply_target_aropen_id ;;
  }

  measure: count {
    type: count
    drill_fields: [arcreditapply_id]
  }
}
