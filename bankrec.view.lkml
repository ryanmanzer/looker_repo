view: bankrec {
  sql_table_name: public.bankrec ;;

  dimension: bankrec_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.bankrec_id ;;
  }

  dimension: bankrec_bankaccnt_id {
    type: number
    sql: ${TABLE}.bankrec_bankaccnt_id ;;
  }

  dimension_group: bankrec_created {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.bankrec_created ;;
  }

  dimension: bankrec_endbal {
    type: number
    sql: ${TABLE}.bankrec_endbal ;;
  }

  dimension_group: bankrec_enddate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.bankrec_enddate ;;
  }

  dimension: bankrec_openbal {
    type: number
    sql: ${TABLE}.bankrec_openbal ;;
  }

  dimension_group: bankrec_opendate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.bankrec_opendate ;;
  }

  dimension_group: bankrec_postdate {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.bankrec_postdate ;;
  }

  dimension: bankrec_posted {
    type: yesno
    sql: ${TABLE}.bankrec_posted ;;
  }

  dimension: bankrec_username {
    type: string
    sql: ${TABLE}.bankrec_username ;;
  }

  measure: count {
    type: count
    drill_fields: [bankrec_id, bankrec_username]
  }
}
