view: itemtax {
  sql_table_name: public.itemtax ;;

  dimension: itemtax_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.itemtax_id ;;
  }

  dimension: itemtax_item_id {
    type: number
    sql: ${TABLE}.itemtax_item_id ;;
  }

  dimension: itemtax_taxtype_id {
    type: number
    sql: ${TABLE}.itemtax_taxtype_id ;;
  }

  dimension: itemtax_taxzone_id {
    type: number
    sql: ${TABLE}.itemtax_taxzone_id ;;
  }

  measure: count {
    type: count
    drill_fields: [itemtax_id]
  }
}
