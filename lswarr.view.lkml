view: lswarr {
  sql_table_name: public.lswarr ;;

  dimension: lswarr_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.lswarr_id ;;
  }

  dimension: lswarr_crmacct_id {
    type: number
    sql: ${TABLE}.lswarr_crmacct_id ;;
  }

  dimension_group: lswarr_expiration {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.lswarr_expiration ;;
  }

  dimension: lswarr_lastupdated {
    type: string
    sql: ${TABLE}.lswarr_lastupdated ;;
  }

  dimension: lswarr_regtype_id {
    type: number
    sql: ${TABLE}.lswarr_regtype_id ;;
  }

  dimension_group: lswarr_start {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.lswarr_start ;;
  }

  dimension: lswarr_username {
    type: string
    sql: ${TABLE}.lswarr_username ;;
  }

  measure: count {
    type: count
    drill_fields: [lswarr_id, lswarr_username]
  }
}
