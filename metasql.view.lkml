view: metasql {
  sql_table_name: public.metasql ;;

  dimension: metasql_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.metasql_id ;;
  }

  dimension: metasql_grade {
    type: number
    sql: ${TABLE}.metasql_grade ;;
  }

  dimension: metasql_group {
    type: string
    sql: ${TABLE}.metasql_group ;;
  }

  dimension_group: metasql_lastupdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.metasql_lastupdate ;;
  }

  dimension: metasql_lastuser {
    type: string
    sql: ${TABLE}.metasql_lastuser ;;
  }

  dimension: metasql_name {
    type: string
    sql: ${TABLE}.metasql_name ;;
  }

  dimension: metasql_notes {
    type: string
    sql: ${TABLE}.metasql_notes ;;
  }

  dimension: metasql_query {
    type: string
    sql: ${TABLE}.metasql_query ;;
  }

  measure: count {
    type: count
    drill_fields: [metasql_id, metasql_name]
  }
}
