view: orderseq {
  sql_table_name: public.orderseq ;;

  dimension: orderseq_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.orderseq_id ;;
  }

  dimension: orderseq_name {
    type: string
    sql: ${TABLE}.orderseq_name ;;
  }

  dimension: orderseq_number {
    type: number
    sql: ${TABLE}.orderseq_number ;;
  }

  dimension: orderseq_numcol {
    type: string
    sql: ${TABLE}.orderseq_numcol ;;
  }

  dimension: orderseq_seqiss {
    type: string
    sql: ${TABLE}.orderseq_seqiss ;;
  }

  dimension: orderseq_table {
    type: string
    sql: ${TABLE}.orderseq_table ;;
  }

  measure: count {
    type: count
    drill_fields: [orderseq_id, orderseq_name]
  }
}
