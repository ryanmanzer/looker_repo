view: msguser {
  sql_table_name: public.msguser ;;

  dimension: msguser_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.msguser_id ;;
  }

  dimension: msguser_msg_id {
    type: number
    sql: ${TABLE}.msguser_msg_id ;;
  }

  dimension: msguser_username {
    type: string
    sql: ${TABLE}.msguser_username ;;
  }

  dimension_group: msguser_viewed {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.msguser_viewed ;;
  }

  measure: count {
    type: count
    drill_fields: [msguser_id, msguser_username]
  }
}
