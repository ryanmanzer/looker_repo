view: lsseq {
  sql_table_name: public.lsseq ;;

  dimension: lsseq_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.lsseq_id ;;
  }

  dimension: lsseq_descrip {
    type: string
    sql: ${TABLE}.lsseq_descrip ;;
  }

  dimension: lsseq_number {
    type: string
    sql: ${TABLE}.lsseq_number ;;
  }

  dimension: lsseq_prefix {
    type: string
    sql: ${TABLE}.lsseq_prefix ;;
  }

  dimension: lsseq_seqlen {
    type: number
    sql: ${TABLE}.lsseq_seqlen ;;
  }

  dimension: lsseq_suffix {
    type: string
    sql: ${TABLE}.lsseq_suffix ;;
  }

  measure: count {
    type: count
    drill_fields: [lsseq_id]
  }
}
