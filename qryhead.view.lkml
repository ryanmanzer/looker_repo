view: qryhead {
  sql_table_name: public.qryhead ;;

  dimension: qryhead_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.qryhead_id ;;
  }

  dimension: qryhead_descrip {
    type: string
    sql: ${TABLE}.qryhead_descrip ;;
  }

  dimension: qryhead_name {
    type: string
    sql: ${TABLE}.qryhead_name ;;
  }

  dimension: qryhead_notes {
    type: string
    sql: ${TABLE}.qryhead_notes ;;
  }

  dimension_group: qryhead_updated {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.qryhead_updated ;;
  }

  dimension: qryhead_username {
    type: string
    sql: ${TABLE}.qryhead_username ;;
  }

  measure: count {
    type: count
    drill_fields: [qryhead_id, qryhead_username, qryhead_name]
  }
}
