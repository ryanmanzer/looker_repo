view: itemsub {
  sql_table_name: public.itemsub ;;

  dimension: itemsub_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.itemsub_id ;;
  }

  dimension: itemsub_parent_item_id {
    type: number
    sql: ${TABLE}.itemsub_parent_item_id ;;
  }

  dimension: itemsub_rank {
    type: number
    sql: ${TABLE}.itemsub_rank ;;
  }

  dimension: itemsub_sub_item_id {
    type: number
    sql: ${TABLE}.itemsub_sub_item_id ;;
  }

  dimension: itemsub_uomratio {
    type: number
    sql: ${TABLE}.itemsub_uomratio ;;
  }

  measure: count {
    type: count
    drill_fields: [itemsub_id]
  }
}
