view: bankrecitem {
  sql_table_name: public.bankrecitem ;;

  dimension: bankrecitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.bankrecitem_id ;;
  }

  dimension: bankrecitem_amount {
    type: number
    sql: ${TABLE}.bankrecitem_amount ;;
  }

  dimension: bankrecitem_bankrec_id {
    type: number
    sql: ${TABLE}.bankrecitem_bankrec_id ;;
  }

  dimension: bankrecitem_cleared {
    type: yesno
    sql: ${TABLE}.bankrecitem_cleared ;;
  }

  dimension: bankrecitem_curr_rate {
    type: number
    sql: ${TABLE}.bankrecitem_curr_rate ;;
  }

  dimension_group: bankrecitem_effdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.bankrecitem_effdate ;;
  }

  dimension: bankrecitem_source {
    type: string
    sql: ${TABLE}.bankrecitem_source ;;
  }

  dimension: bankrecitem_source_id {
    type: number
    sql: ${TABLE}.bankrecitem_source_id ;;
  }

  measure: count {
    type: count
    drill_fields: [bankrecitem_id]
  }
}
