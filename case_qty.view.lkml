view: case_qty {
  sql_table_name: public.case_qty ;;

  dimension: case_qty {
    type: number
    sql: ${TABLE}.case_qty ;;
  }

  dimension: item_descrip1 {
    type: string
    sql: ${TABLE}.item_descrip1 ;;
  }

  dimension: item_number {
    type: string
    sql: ${TABLE}.item_number ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
