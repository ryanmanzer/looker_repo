view: crmacct {
  sql_table_name: public.crmacct ;;

  dimension: crmacct_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.crmacct_id ;;
  }

  dimension: crmacct_active {
    type: yesno
    sql: ${TABLE}.crmacct_active ;;
  }

  dimension: crmacct_cntct_id_1 {
    type: number
    value_format_name: id
    sql: ${TABLE}.crmacct_cntct_id_1 ;;
  }

  dimension: crmacct_cntct_id_2 {
    type: number
    value_format_name: id
    sql: ${TABLE}.crmacct_cntct_id_2 ;;
  }

  dimension: crmacct_competitor_id {
    type: number
    sql: ${TABLE}.crmacct_competitor_id ;;
  }

  dimension: crmacct_cust_id {
    type: number
    sql: ${TABLE}.crmacct_cust_id ;;
  }

  dimension: crmacct_emp_id {
    type: number
    sql: ${TABLE}.crmacct_emp_id ;;
  }

  dimension: crmacct_name {
    type: string
    sql: ${TABLE}.crmacct_name ;;
  }

  dimension: crmacct_notes {
    type: string
    sql: ${TABLE}.crmacct_notes ;;
  }

  dimension: crmacct_number {
    type: string
    sql: ${TABLE}.crmacct_number ;;
  }

  dimension: crmacct_owner_username {
    type: string
    sql: ${TABLE}.crmacct_owner_username ;;
  }

  dimension: crmacct_parent_id {
    type: number
    sql: ${TABLE}.crmacct_parent_id ;;
  }

  dimension: crmacct_partner_id {
    type: number
    sql: ${TABLE}.crmacct_partner_id ;;
  }

  dimension: crmacct_prospect_id {
    type: number
    sql: ${TABLE}.crmacct_prospect_id ;;
  }

  dimension: crmacct_salesrep_id {
    type: number
    sql: ${TABLE}.crmacct_salesrep_id ;;
  }

  dimension: crmacct_taxauth_id {
    type: number
    sql: ${TABLE}.crmacct_taxauth_id ;;
  }

  dimension: crmacct_type {
    type: string
    sql: ${TABLE}.crmacct_type ;;
  }

  dimension: crmacct_usr_username {
    type: string
    sql: ${TABLE}.crmacct_usr_username ;;
  }

  dimension: crmacct_vend_id {
    type: number
    sql: ${TABLE}.crmacct_vend_id ;;
  }

  measure: count {
    type: count
    drill_fields: [crmacct_id, crmacct_usr_username, crmacct_name, crmacct_owner_username, address.count]
  }
}
