view: prjtask {
  sql_table_name: public.prjtask ;;

  dimension: prjtask_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.prjtask_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: prjtask_anyuser {
    type: yesno
    sql: ${TABLE}.prjtask_anyuser ;;
  }

  dimension_group: prjtask_assigned {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.prjtask_assigned_date ;;
  }

  dimension_group: prjtask_completed {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.prjtask_completed_date ;;
  }

  dimension: prjtask_descrip {
    type: string
    sql: ${TABLE}.prjtask_descrip ;;
  }

  dimension_group: prjtask_due {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.prjtask_due_date ;;
  }

  dimension: prjtask_exp_actual {
    type: number
    sql: ${TABLE}.prjtask_exp_actual ;;
  }

  dimension: prjtask_exp_budget {
    type: number
    sql: ${TABLE}.prjtask_exp_budget ;;
  }

  dimension: prjtask_hours_actual {
    type: number
    sql: ${TABLE}.prjtask_hours_actual ;;
  }

  dimension: prjtask_hours_budget {
    type: number
    sql: ${TABLE}.prjtask_hours_budget ;;
  }

  dimension: prjtask_name {
    type: string
    sql: ${TABLE}.prjtask_name ;;
  }

  dimension: prjtask_number {
    type: string
    sql: ${TABLE}.prjtask_number ;;
  }

  dimension: prjtask_owner_username {
    type: string
    sql: ${TABLE}.prjtask_owner_username ;;
  }

  dimension: prjtask_prj_id {
    type: number
    sql: ${TABLE}.prjtask_prj_id ;;
  }

  dimension_group: prjtask_start {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.prjtask_start_date ;;
  }

  dimension: prjtask_status {
    type: string
    sql: ${TABLE}.prjtask_status ;;
  }

  dimension: prjtask_username {
    type: string
    sql: ${TABLE}.prjtask_username ;;
  }

  measure: count {
    type: count
    drill_fields: [prjtask_id, prjtask_name, prjtask_owner_username, prjtask_username]
  }
}
