view: report {
  sql_table_name: public.report ;;

  dimension: report_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.report_id ;;
  }

  dimension: report_descrip {
    type: string
    sql: ${TABLE}.report_descrip ;;
  }

  dimension: report_grade {
    type: number
    sql: ${TABLE}.report_grade ;;
  }

  dimension_group: report_loaddate {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.report_loaddate ;;
  }

  dimension: report_name {
    type: string
    sql: ${TABLE}.report_name ;;
  }

  dimension: report_source {
    type: string
    sql: ${TABLE}.report_source ;;
  }

  dimension: report_sys {
    type: yesno
    sql: ${TABLE}.report_sys ;;
  }

  measure: count {
    type: count
    drill_fields: [report_id, report_name]
  }
}
