view: expcat {
  sql_table_name: public.expcat ;;

  dimension: expcat_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.expcat_id ;;
  }

  dimension: expcat_active {
    type: yesno
    sql: ${TABLE}.expcat_active ;;
  }

  dimension: expcat_code {
    type: string
    sql: ${TABLE}.expcat_code ;;
  }

  dimension: expcat_descrip {
    type: string
    sql: ${TABLE}.expcat_descrip ;;
  }

  dimension: expcat_exp_accnt_id {
    type: number
    sql: ${TABLE}.expcat_exp_accnt_id ;;
  }

  dimension: expcat_freight_accnt_id {
    type: number
    sql: ${TABLE}.expcat_freight_accnt_id ;;
  }

  dimension: expcat_liability_accnt_id {
    type: number
    sql: ${TABLE}.expcat_liability_accnt_id ;;
  }

  dimension: expcat_purchprice_accnt_id {
    type: number
    sql: ${TABLE}.expcat_purchprice_accnt_id ;;
  }

  measure: count {
    type: count
    drill_fields: [expcat_id]
  }
}
