view: pr {
  sql_table_name: public.pr ;;

  dimension: pr_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.pr_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension_group: pr_createdate {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.pr_createdate ;;
  }

  dimension_group: pr_duedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.pr_duedate ;;
  }

  dimension: pr_itemsite_id {
    type: number
    sql: ${TABLE}.pr_itemsite_id ;;
  }

  dimension: pr_number {
    type: number
    sql: ${TABLE}.pr_number ;;
  }

  dimension: pr_order_id {
    type: number
    sql: ${TABLE}.pr_order_id ;;
  }

  dimension: pr_order_type {
    type: string
    sql: ${TABLE}.pr_order_type ;;
  }

  dimension: pr_poitem_id {
    type: number
    sql: ${TABLE}.pr_poitem_id ;;
  }

  dimension: pr_prj_id {
    type: number
    sql: ${TABLE}.pr_prj_id ;;
  }

  dimension: pr_qtyreq {
    type: number
    sql: ${TABLE}.pr_qtyreq ;;
  }

  dimension: pr_releasenote {
    type: string
    sql: ${TABLE}.pr_releasenote ;;
  }

  dimension: pr_status {
    type: string
    sql: ${TABLE}.pr_status ;;
  }

  dimension: pr_subnumber {
    type: number
    sql: ${TABLE}.pr_subnumber ;;
  }

  measure: count {
    type: count
    drill_fields: [pr_id]
  }
}
