view: flhead {
  sql_table_name: public.flhead ;;

  dimension: flhead_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.flhead_id ;;
  }

  dimension: flhead_active {
    type: yesno
    sql: ${TABLE}.flhead_active ;;
  }

  dimension: flhead_altbegin {
    type: string
    sql: ${TABLE}.flhead_altbegin ;;
  }

  dimension: flhead_altbudget {
    type: string
    sql: ${TABLE}.flhead_altbudget ;;
  }

  dimension: flhead_altcredits {
    type: string
    sql: ${TABLE}.flhead_altcredits ;;
  }

  dimension: flhead_altdebits {
    type: string
    sql: ${TABLE}.flhead_altdebits ;;
  }

  dimension: flhead_altdiff {
    type: string
    sql: ${TABLE}.flhead_altdiff ;;
  }

  dimension: flhead_altend {
    type: string
    sql: ${TABLE}.flhead_altend ;;
  }

  dimension: flhead_alttotal {
    type: string
    sql: ${TABLE}.flhead_alttotal ;;
  }

  dimension: flhead_custom_label {
    type: string
    sql: ${TABLE}.flhead_custom_label ;;
  }

  dimension: flhead_descrip {
    type: string
    sql: ${TABLE}.flhead_descrip ;;
  }

  dimension: flhead_name {
    type: string
    sql: ${TABLE}.flhead_name ;;
  }

  dimension: flhead_notes {
    type: string
    sql: ${TABLE}.flhead_notes ;;
  }

  dimension: flhead_showbudget {
    type: yesno
    sql: ${TABLE}.flhead_showbudget ;;
  }

  dimension: flhead_showcustom {
    type: yesno
    sql: ${TABLE}.flhead_showcustom ;;
  }

  dimension: flhead_showdelta {
    type: yesno
    sql: ${TABLE}.flhead_showdelta ;;
  }

  dimension: flhead_showdiff {
    type: yesno
    sql: ${TABLE}.flhead_showdiff ;;
  }

  dimension: flhead_showend {
    type: yesno
    sql: ${TABLE}.flhead_showend ;;
  }

  dimension: flhead_showstart {
    type: yesno
    sql: ${TABLE}.flhead_showstart ;;
  }

  dimension: flhead_showtotal {
    type: yesno
    sql: ${TABLE}.flhead_showtotal ;;
  }

  dimension: flhead_sys {
    type: yesno
    sql: ${TABLE}.flhead_sys ;;
  }

  dimension: flhead_type {
    type: string
    sql: ${TABLE}.flhead_type ;;
  }

  dimension: flhead_usealtbegin {
    type: yesno
    sql: ${TABLE}.flhead_usealtbegin ;;
  }

  dimension: flhead_usealtbudget {
    type: yesno
    sql: ${TABLE}.flhead_usealtbudget ;;
  }

  dimension: flhead_usealtcredits {
    type: yesno
    sql: ${TABLE}.flhead_usealtcredits ;;
  }

  dimension: flhead_usealtdebits {
    type: yesno
    sql: ${TABLE}.flhead_usealtdebits ;;
  }

  dimension: flhead_usealtdiff {
    type: yesno
    sql: ${TABLE}.flhead_usealtdiff ;;
  }

  dimension: flhead_usealtend {
    type: yesno
    sql: ${TABLE}.flhead_usealtend ;;
  }

  dimension: flhead_usealttotal {
    type: yesno
    sql: ${TABLE}.flhead_usealttotal ;;
  }

  measure: count {
    type: count
    drill_fields: [flhead_id, flhead_name]
  }
}
