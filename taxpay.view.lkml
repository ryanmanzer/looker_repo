view: taxpay {
  sql_table_name: public.taxpay ;;

  dimension: taxpay_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.taxpay_id ;;
  }

  dimension: taxpay_apply_id {
    type: number
    sql: ${TABLE}.taxpay_apply_id ;;
  }

  dimension_group: taxpay_distdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.taxpay_distdate ;;
  }

  dimension: taxpay_tax {
    type: number
    sql: ${TABLE}.taxpay_tax ;;
  }

  dimension: taxpay_taxhist_id {
    type: number
    sql: ${TABLE}.taxpay_taxhist_id ;;
  }

  measure: count {
    type: count
    drill_fields: [taxpay_id]
  }
}
