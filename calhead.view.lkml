view: calhead {
  sql_table_name: public.calhead ;;

  dimension: calhead_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.calhead_id ;;
  }

  dimension: calhead_descrip {
    type: string
    sql: ${TABLE}.calhead_descrip ;;
  }

  dimension: calhead_name {
    type: string
    sql: ${TABLE}.calhead_name ;;
  }

  dimension: calhead_origin {
    type: string
    sql: ${TABLE}.calhead_origin ;;
  }

  dimension: calhead_type {
    type: string
    sql: ${TABLE}.calhead_type ;;
  }

  measure: count {
    type: count
    drill_fields: [calhead_id, calhead_name]
  }
}
