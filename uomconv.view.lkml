view: uomconv {
  sql_table_name: public.uomconv ;;

  dimension: uomconv_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.uomconv_id ;;
  }

  dimension: uomconv_fractional {
    type: yesno
    sql: ${TABLE}.uomconv_fractional ;;
  }

  dimension: uomconv_from_uom_id {
    type: number
    sql: ${TABLE}.uomconv_from_uom_id ;;
  }

  dimension: uomconv_from_value {
    type: number
    sql: ${TABLE}.uomconv_from_value ;;
  }

  dimension: uomconv_to_uom_id {
    type: number
    sql: ${TABLE}.uomconv_to_uom_id ;;
  }

  dimension: uomconv_to_value {
    type: number
    sql: ${TABLE}.uomconv_to_value ;;
  }

  measure: count {
    type: count
    drill_fields: [uomconv_id]
  }
}
