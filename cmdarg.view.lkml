view: cmdarg {
  sql_table_name: public.cmdarg ;;

  dimension: cmdarg_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.cmdarg_id ;;
  }

  dimension: cmdarg_arg {
    type: string
    sql: ${TABLE}.cmdarg_arg ;;
  }

  dimension: cmdarg_cmd_id {
    type: number
    sql: ${TABLE}.cmdarg_cmd_id ;;
  }

  dimension: cmdarg_order {
    type: number
    sql: ${TABLE}.cmdarg_order ;;
  }

  measure: count {
    type: count
    drill_fields: [cmdarg_id]
  }
}
