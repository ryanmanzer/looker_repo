view: apcreditapply {
  sql_table_name: public.apcreditapply ;;

  dimension: apcreditapply_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.apcreditapply_id ;;
  }

  dimension: apcreditapply_amount {
    type: number
    sql: ${TABLE}.apcreditapply_amount ;;
  }

  dimension: apcreditapply_curr_id {
    type: number
    sql: ${TABLE}.apcreditapply_curr_id ;;
  }

  dimension: apcreditapply_source_apopen_id {
    type: number
    sql: ${TABLE}.apcreditapply_source_apopen_id ;;
  }

  dimension: apcreditapply_target_apopen_id {
    type: number
    sql: ${TABLE}.apcreditapply_target_apopen_id ;;
  }

  measure: count {
    type: count
    drill_fields: [apcreditapply_id]
  }
}
