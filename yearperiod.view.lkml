view: yearperiod {
  sql_table_name: public.yearperiod ;;

  dimension: yearperiod_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.yearperiod_id ;;
  }

  dimension: yearperiod_closed {
    type: yesno
    sql: ${TABLE}.yearperiod_closed ;;
  }

  dimension_group: yearperiod_end {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.yearperiod_end ;;
  }

  dimension_group: yearperiod_start {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.yearperiod_start ;;
  }

  measure: count {
    type: count
    drill_fields: [yearperiod_id]
  }
}
