view: image {
  sql_table_name: public.image ;;

  dimension: image_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.image_id ;;
  }

  dimension: image_data {
    type: string
    sql: ${TABLE}.image_data ;;
  }

  dimension: image_descrip {
    type: string
    sql: ${TABLE}.image_descrip ;;
  }

  dimension: image_name {
    type: string
    sql: ${TABLE}.image_name ;;
  }

  measure: count {
    type: count
    drill_fields: [image_id, image_name]
  }
}
