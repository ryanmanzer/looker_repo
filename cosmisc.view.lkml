view: cosmisc {
  sql_table_name: public.cosmisc ;;

  dimension: cosmisc_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.cosmisc_id ;;
  }

  dimension: cosmisc_cohead_id {
    type: number
    sql: ${TABLE}.cosmisc_cohead_id ;;
  }

  dimension: cosmisc_freight {
    type: number
    sql: ${TABLE}.cosmisc_freight ;;
  }

  dimension: cosmisc_notes {
    type: string
    sql: ${TABLE}.cosmisc_notes ;;
  }

  dimension: cosmisc_number {
    type: string
    sql: ${TABLE}.cosmisc_number ;;
  }

  dimension: cosmisc_sfstatus {
    type: string
    sql: ${TABLE}.cosmisc_sfstatus ;;
  }

  dimension: cosmisc_shipchrg_id {
    type: number
    sql: ${TABLE}.cosmisc_shipchrg_id ;;
  }

  dimension_group: cosmisc_shipdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cosmisc_shipdate ;;
  }

  dimension: cosmisc_shipform_id {
    type: number
    sql: ${TABLE}.cosmisc_shipform_id ;;
  }

  dimension: cosmisc_shipped {
    type: yesno
    sql: ${TABLE}.cosmisc_shipped ;;
  }

  dimension: cosmisc_shipvia {
    type: string
    sql: ${TABLE}.cosmisc_shipvia ;;
  }

  dimension: cosmisc_tracknum {
    type: string
    sql: ${TABLE}.cosmisc_tracknum ;;
  }

  measure: count {
    type: count
    drill_fields: [cosmisc_id]
  }
}
