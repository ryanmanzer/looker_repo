view: itemcost {
  sql_table_name: public.itemcost ;;

  dimension: itemcost_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.itemcost_id ;;
  }

  dimension: itemcost_actcost {
    type: number
    sql: ${TABLE}.itemcost_actcost ;;
  }

  dimension: itemcost_costelem_id {
    type: number
    sql: ${TABLE}.itemcost_costelem_id ;;
  }

  dimension: itemcost_curr_id {
    type: number
    sql: ${TABLE}.itemcost_curr_id ;;
  }

  dimension: itemcost_item_id {
    type: number
    sql: ${TABLE}.itemcost_item_id ;;
  }

  dimension: itemcost_lowlevel {
    type: yesno
    sql: ${TABLE}.itemcost_lowlevel ;;
  }

  dimension_group: itemcost_posted {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.itemcost_posted ;;
  }

  dimension: itemcost_stdcost {
    type: number
    sql: ${TABLE}.itemcost_stdcost ;;
  }

  dimension_group: itemcost_updated {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.itemcost_updated ;;
  }

  measure: count {
    type: count
    drill_fields: [itemcost_id]
  }
}
