view: usrpriv {
  sql_table_name: public.usrpriv ;;

  dimension: usrpriv_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.usrpriv_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: usrpriv_priv_id {
    type: number
    sql: ${TABLE}.usrpriv_priv_id ;;
  }

  dimension: usrpriv_username {
    type: string
    sql: ${TABLE}.usrpriv_username ;;
  }

  measure: count {
    type: count
    drill_fields: [usrpriv_id, usrpriv_username]
  }
}
