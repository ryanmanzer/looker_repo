view: hnfc {
  sql_table_name: public.hnfc ;;

  dimension: hnfc_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.hnfc_id ;;
  }

  dimension: hnfc_code {
    type: string
    sql: ${TABLE}.hnfc_code ;;
  }

  measure: count {
    type: count
    drill_fields: [hnfc_id]
  }
}
