view: prjtype {
  sql_table_name: public.prjtype ;;

  dimension: prjtype_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.prjtype_id ;;
  }

  dimension: prjtype_active {
    type: yesno
    sql: ${TABLE}.prjtype_active ;;
  }

  dimension: prjtype_code {
    type: string
    sql: ${TABLE}.prjtype_code ;;
  }

  dimension: prjtype_descr {
    type: string
    sql: ${TABLE}.prjtype_descr ;;
  }

  measure: count {
    type: count
    drill_fields: [prjtype_id]
  }
}
