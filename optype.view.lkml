view: optype {
  sql_table_name: public.optype ;;

  dimension: optype_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.optype_id ;;
  }

  dimension: optype_descrip {
    type: string
    sql: ${TABLE}.optype_descrip ;;
  }

  dimension: optype_name {
    type: string
    sql: ${TABLE}.optype_name ;;
  }

  measure: count {
    type: count
    drill_fields: [optype_id, optype_name]
  }
}
