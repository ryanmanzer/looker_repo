view: payco {
  sql_table_name: public.payco ;;

  dimension: payco_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.payco_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: payco_amount {
    type: number
    sql: ${TABLE}.payco_amount ;;
  }

  dimension: payco_ccpay_id {
    type: number
    sql: ${TABLE}.payco_ccpay_id ;;
  }

  dimension: payco_cohead_id {
    type: number
    sql: ${TABLE}.payco_cohead_id ;;
  }

  dimension: payco_curr_id {
    type: number
    sql: ${TABLE}.payco_curr_id ;;
  }

  measure: count {
    type: count
    drill_fields: [payco_id]
  }
}
