connection: "reporting"

# include all the views
include: "*.view"

# include all the dashboards
include: "*.dashboard"

explore: custinfo {
  view_name: custinfo
  join: cohead {
    type: left_outer
    sql_on: ${cohead.cohead_cust_id} = ${custinfo.cust_id} ;;
    relationship: many_to_one
  }
  join: cohist {
    type: left_outer
    sql_on: ${cohist.cohist_cust_id} = ${custinfo.cust_id} ;;
    relationship: many_to_one
  }
  join: cntct {
    type: left_outer
    sql_on: ${custinfo.cust_cntct_id} = ${cntct.cntct_id} ;;
    relationship: many_to_one
  }
  join:  addr {
    type:  left_outer
    sql_on: ${cntct.cntct_addr_id} = ${addr.addr_id};;
    relationship: many_to_many
  }
  join: cohist_orders {
    type: left_outer
    sql_on: ${cohist.cohist_ordernumber} = ${cohist_orders.order_number} ;;
    relationship: one_to_many
  }
}
explore:sql_runner_query {
  view_name: sql_runner_query {}
}

explore: new_products {
  view_name: _newproducts_q1
}
