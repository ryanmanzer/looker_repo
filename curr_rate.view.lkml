view: curr_rate {
  sql_table_name: public.curr_rate ;;

  dimension: curr_rate_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.curr_rate_id ;;
  }

  dimension_group: curr_effective {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.curr_effective ;;
  }

  dimension_group: curr_expires {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.curr_expires ;;
  }

  dimension: curr_id {
    type: number
    sql: ${TABLE}.curr_id ;;
  }

  dimension: curr_rate {
    type: number
    sql: ${TABLE}.curr_rate ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [curr_rate_id]
  }
}
