view: destination {
  sql_table_name: public.destination ;;

  dimension: destination_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.destination_id ;;
  }

  dimension: destination_city {
    type: string
    sql: ${TABLE}.destination_city ;;
  }

  dimension: destination_comments {
    type: string
    sql: ${TABLE}.destination_comments ;;
  }

  dimension: destination_name {
    type: string
    sql: ${TABLE}.destination_name ;;
  }

  dimension: destination_state {
    type: string
    sql: ${TABLE}.destination_state ;;
  }

  measure: count {
    type: count
    drill_fields: [destination_id, destination_name]
  }
}
