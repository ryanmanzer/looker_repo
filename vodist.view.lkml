view: vodist {
  sql_table_name: public.vodist ;;

  dimension: vodist_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.vodist_id ;;
  }

  dimension: vodist_accnt_id {
    type: number
    sql: ${TABLE}.vodist_accnt_id ;;
  }

  dimension: vodist_amount {
    type: number
    sql: ${TABLE}.vodist_amount ;;
  }

  dimension: vodist_costelem_id {
    type: number
    sql: ${TABLE}.vodist_costelem_id ;;
  }

  dimension: vodist_discountable {
    type: yesno
    sql: ${TABLE}.vodist_discountable ;;
  }

  dimension: vodist_expcat_id {
    type: number
    sql: ${TABLE}.vodist_expcat_id ;;
  }

  dimension: vodist_notes {
    type: string
    sql: ${TABLE}.vodist_notes ;;
  }

  dimension: vodist_poitem_id {
    type: number
    sql: ${TABLE}.vodist_poitem_id ;;
  }

  dimension: vodist_qty {
    type: number
    sql: ${TABLE}.vodist_qty ;;
  }

  dimension: vodist_tax_id {
    type: number
    sql: ${TABLE}.vodist_tax_id ;;
  }

  dimension: vodist_vohead_id {
    type: number
    sql: ${TABLE}.vodist_vohead_id ;;
  }

  measure: count {
    type: count
    drill_fields: [vodist_id]
  }
}
