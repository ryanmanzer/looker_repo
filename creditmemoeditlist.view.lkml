view: creditmemoeditlist {
  sql_table_name: public.creditmemoeditlist ;;

  dimension: account {
    type: string
    sql: ${TABLE}.account ;;
  }

  dimension: billtoname {
    type: string
    sql: ${TABLE}.billtoname ;;
  }

  dimension: cust_number {
    type: string
    sql: ${TABLE}.cust_number ;;
  }

  dimension: documentnumber {
    type: string
    sql: ${TABLE}.documentnumber ;;
  }

  dimension: extprice {
    type: string
    sql: ${TABLE}.extprice ;;
  }

  dimension: item {
    type: string
    sql: ${TABLE}.item ;;
  }

  dimension: itemdescrip {
    type: string
    sql: ${TABLE}.itemdescrip ;;
  }

  dimension: itemid {
    type: number
    value_format_name: id
    # hidden: yes
    sql: ${TABLE}.itemid ;;
  }

  dimension: iteminvuom {
    type: string
    sql: ${TABLE}.iteminvuom ;;
  }

  dimension: linenumber {
    type: number
    sql: ${TABLE}.linenumber ;;
  }

  dimension: orderid {
    type: number
    value_format_name: id
    sql: ${TABLE}.orderid ;;
  }

  dimension: ordernumber {
    type: string
    sql: ${TABLE}.ordernumber ;;
  }

  dimension: price {
    type: string
    sql: ${TABLE}.price ;;
  }

  dimension: qtytobill {
    type: string
    sql: ${TABLE}.qtytobill ;;
  }

  dimension: sence {
    type: string
    sql: ${TABLE}.sence ;;
  }

  measure: count {
    type: count
    drill_fields: [billtoname, item.item_id]
  }
}
