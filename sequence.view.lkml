view: sequence {
  sql_table_name: public.sequence ;;

  dimension: sequence_value {
    type: number
    sql: ${TABLE}.sequence_value ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
