view: ipsprice {
  sql_table_name: public.ipsprice ;;

  dimension: ipsprice_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.ipsprice_id ;;
  }

  dimension: ipsprice_discountfixed {
    type: number
    sql: ${TABLE}.ipsprice_discountfixed ;;
  }

  dimension: ipsprice_discountpercent {
    type: number
    sql: ${TABLE}.ipsprice_discountpercent ;;
  }

  dimension: ipsprice_ipshead_id {
    type: number
    sql: ${TABLE}.ipsprice_ipshead_id ;;
  }

  dimension: ipsprice_item_id {
    type: number
    sql: ${TABLE}.ipsprice_item_id ;;
  }

  dimension: ipsprice_price {
    type: number
    sql: ${TABLE}.ipsprice_price ;;
  }

  dimension: ipsprice_qtybreak {
    type: number
    sql: ${TABLE}.ipsprice_qtybreak ;;
  }

  dimension: ipsprice_source {
    type: string
    sql: ${TABLE}.ipsprice_source ;;
  }

  dimension: ipsprice_type {
    type: string
    sql: ${TABLE}.ipsprice_type ;;
  }

  dimension: ipsprice_uomprice {
    type: number
    sql: ${TABLE}.ipsprice_uomprice ;;
  }

  dimension: ipsprice_uomprice_uom_id {
    type: number
    sql: ${TABLE}.ipsprice_uomprice_uom_id ;;
  }

  dimension: ipsprice_uomqtybreak {
    type: number
    sql: ${TABLE}.ipsprice_uomqtybreak ;;
  }

  dimension: ipsprice_uomqtybreak_uom_id {
    type: number
    sql: ${TABLE}.ipsprice_uomqtybreak_uom_id ;;
  }

  measure: count {
    type: count
    drill_fields: [ipsprice_id]
  }
}
