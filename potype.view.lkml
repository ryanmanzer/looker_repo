view: potype {
  sql_table_name: public.potype ;;

  dimension: potype_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.potype_id ;;
  }

  dimension: potype_descrip {
    type: string
    sql: ${TABLE}.potype_descrip ;;
  }

  dimension: potype_name {
    type: string
    sql: ${TABLE}.potype_name ;;
  }

  measure: count {
    type: count
    drill_fields: [potype_id, potype_name]
  }
}
