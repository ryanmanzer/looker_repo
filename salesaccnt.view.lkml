view: salesaccnt {
  sql_table_name: public.salesaccnt ;;

  dimension: salesaccnt_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.salesaccnt_id ;;
  }

  dimension: salesaccnt_cor_accnt_id {
    type: number
    sql: ${TABLE}.salesaccnt_cor_accnt_id ;;
  }

  dimension: salesaccnt_cos_accnt_id {
    type: number
    sql: ${TABLE}.salesaccnt_cos_accnt_id ;;
  }

  dimension: salesaccnt_cow_accnt_id {
    type: number
    sql: ${TABLE}.salesaccnt_cow_accnt_id ;;
  }

  dimension: salesaccnt_credit_accnt_id {
    type: number
    sql: ${TABLE}.salesaccnt_credit_accnt_id ;;
  }

  dimension: salesaccnt_custtype {
    type: string
    sql: ${TABLE}.salesaccnt_custtype ;;
  }

  dimension: salesaccnt_custtype_id {
    type: number
    sql: ${TABLE}.salesaccnt_custtype_id ;;
  }

  dimension: salesaccnt_prodcat {
    type: string
    sql: ${TABLE}.salesaccnt_prodcat ;;
  }

  dimension: salesaccnt_prodcat_id {
    type: number
    sql: ${TABLE}.salesaccnt_prodcat_id ;;
  }

  dimension: salesaccnt_returns_accnt_id {
    type: number
    sql: ${TABLE}.salesaccnt_returns_accnt_id ;;
  }

  dimension: salesaccnt_sales_accnt_id {
    type: number
    sql: ${TABLE}.salesaccnt_sales_accnt_id ;;
  }

  dimension: salesaccnt_saletype_id {
    type: number
    sql: ${TABLE}.salesaccnt_saletype_id ;;
  }

  dimension: salesaccnt_shipzone_id {
    type: number
    sql: ${TABLE}.salesaccnt_shipzone_id ;;
  }

  dimension: salesaccnt_warehous_id {
    type: number
    sql: ${TABLE}.salesaccnt_warehous_id ;;
  }

  measure: count {
    type: count
    drill_fields: [salesaccnt_id]
  }
}
