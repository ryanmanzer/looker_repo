view: ccpay {
  sql_table_name: public.ccpay ;;

  dimension: ccpay_ccpay_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.ccpay_ccpay_id ;;
  }

  dimension: ccpay_amount {
    type: number
    sql: ${TABLE}.ccpay_amount ;;
  }

  dimension: ccpay_auth {
    type: yesno
    sql: ${TABLE}.ccpay_auth ;;
  }

  dimension: ccpay_auth_charge {
    type: string
    sql: ${TABLE}.ccpay_auth_charge ;;
  }

  dimension: ccpay_by_username {
    type: string
    sql: ${TABLE}.ccpay_by_username ;;
  }

  dimension: ccpay_card_pan_trunc {
    type: string
    sql: ${TABLE}.ccpay_card_pan_trunc ;;
  }

  dimension: ccpay_card_type {
    type: string
    sql: ${TABLE}.ccpay_card_type ;;
  }

  dimension: ccpay_ccard_id {
    type: number
    sql: ${TABLE}.ccpay_ccard_id ;;
  }

  dimension: ccpay_curr_id {
    type: number
    sql: ${TABLE}.ccpay_curr_id ;;
  }

  dimension: ccpay_cust_id {
    type: number
    sql: ${TABLE}.ccpay_cust_id ;;
  }

  dimension: ccpay_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.ccpay_id ;;
  }

  dimension: ccpay_order_number {
    type: string
    sql: ${TABLE}.ccpay_order_number ;;
  }

  dimension: ccpay_order_number_seq {
    type: number
    sql: ${TABLE}.ccpay_order_number_seq ;;
  }

  dimension: ccpay_r_approved {
    type: string
    sql: ${TABLE}.ccpay_r_approved ;;
  }

  dimension: ccpay_r_avs {
    type: string
    sql: ${TABLE}.ccpay_r_avs ;;
  }

  dimension: ccpay_r_code {
    type: string
    sql: ${TABLE}.ccpay_r_code ;;
  }

  dimension: ccpay_r_error {
    type: string
    sql: ${TABLE}.ccpay_r_error ;;
  }

  dimension: ccpay_r_message {
    type: string
    sql: ${TABLE}.ccpay_r_message ;;
  }

  dimension: ccpay_r_ordernum {
    type: string
    sql: ${TABLE}.ccpay_r_ordernum ;;
  }

  dimension: ccpay_r_ref {
    type: string
    sql: ${TABLE}.ccpay_r_ref ;;
  }

  dimension: ccpay_r_shipping {
    type: string
    sql: ${TABLE}.ccpay_r_shipping ;;
  }

  dimension: ccpay_r_tax {
    type: string
    sql: ${TABLE}.ccpay_r_tax ;;
  }

  dimension: ccpay_status {
    type: string
    sql: ${TABLE}.ccpay_status ;;
  }

  dimension_group: ccpay_transaction_datetime {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.ccpay_transaction_datetime ;;
  }

  dimension: ccpay_type {
    type: string
    sql: ${TABLE}.ccpay_type ;;
  }

  dimension: ccpay_yp_r_score {
    type: number
    sql: ${TABLE}.ccpay_yp_r_score ;;
  }

  dimension: ccpay_yp_r_tdate {
    type: string
    sql: ${TABLE}.ccpay_yp_r_tdate ;;
  }

  dimension_group: ccpay_yp_r {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.ccpay_yp_r_time ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [ccpay_ccpay_id, ccpay_by_username, ccpay.ccpay_by_username, ccpay.ccpay_ccpay_id, ccpay.count]
  }
}
