view: planreq {
  sql_table_name: public.planreq ;;

  dimension: planreq_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.planreq_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: planreq_itemsite_id {
    type: number
    sql: ${TABLE}.planreq_itemsite_id ;;
  }

  dimension: planreq_notes {
    type: string
    sql: ${TABLE}.planreq_notes ;;
  }

  dimension: planreq_planoper_seq_id {
    type: number
    sql: ${TABLE}.planreq_planoper_seq_id ;;
  }

  dimension: planreq_qty {
    type: number
    sql: ${TABLE}.planreq_qty ;;
  }

  dimension: planreq_source {
    type: string
    sql: ${TABLE}.planreq_source ;;
  }

  dimension: planreq_source_id {
    type: number
    sql: ${TABLE}.planreq_source_id ;;
  }

  measure: count {
    type: count
    drill_fields: [planreq_id]
  }
}
