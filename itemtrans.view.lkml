view: itemtrans {
  sql_table_name: public.itemtrans ;;

  dimension: itemtrans_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.itemtrans_id ;;
  }

  dimension: itemtrans_source_item_id {
    type: number
    sql: ${TABLE}.itemtrans_source_item_id ;;
  }

  dimension: itemtrans_target_item_id {
    type: number
    sql: ${TABLE}.itemtrans_target_item_id ;;
  }

  measure: count {
    type: count
    drill_fields: [itemtrans_id]
  }
}
