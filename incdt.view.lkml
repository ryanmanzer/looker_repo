view: incdt {
  sql_table_name: public.incdt ;;

  dimension: incdt_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.incdt_id ;;
  }

  dimension: incdt_aropen_id {
    type: number
    sql: ${TABLE}.incdt_aropen_id ;;
  }

  dimension: incdt_assigned_username {
    type: string
    sql: ${TABLE}.incdt_assigned_username ;;
  }

  dimension: incdt_cntct_id {
    type: number
    sql: ${TABLE}.incdt_cntct_id ;;
  }

  dimension: incdt_crmacct_id {
    type: number
    sql: ${TABLE}.incdt_crmacct_id ;;
  }

  dimension: incdt_descrip {
    type: string
    sql: ${TABLE}.incdt_descrip ;;
  }

  dimension: incdt_incdtcat_id {
    type: number
    sql: ${TABLE}.incdt_incdtcat_id ;;
  }

  dimension: incdt_incdtpriority_id {
    type: number
    sql: ${TABLE}.incdt_incdtpriority_id ;;
  }

  dimension: incdt_incdtresolution_id {
    type: number
    sql: ${TABLE}.incdt_incdtresolution_id ;;
  }

  dimension: incdt_incdtseverity_id {
    type: number
    sql: ${TABLE}.incdt_incdtseverity_id ;;
  }

  dimension: incdt_item_id {
    type: number
    sql: ${TABLE}.incdt_item_id ;;
  }

  dimension: incdt_lotserial {
    type: string
    sql: ${TABLE}.incdt_lotserial ;;
  }

  dimension: incdt_ls_id {
    type: number
    sql: ${TABLE}.incdt_ls_id ;;
  }

  dimension: incdt_number {
    type: number
    sql: ${TABLE}.incdt_number ;;
  }

  dimension: incdt_owner_username {
    type: string
    sql: ${TABLE}.incdt_owner_username ;;
  }

  dimension: incdt_prj_id {
    type: number
    sql: ${TABLE}.incdt_prj_id ;;
  }

  dimension: incdt_public {
    type: yesno
    sql: ${TABLE}.incdt_public ;;
  }

  dimension: incdt_recurring_incdt_id {
    type: number
    sql: ${TABLE}.incdt_recurring_incdt_id ;;
  }

  dimension: incdt_status {
    type: string
    sql: ${TABLE}.incdt_status ;;
  }

  dimension: incdt_summary {
    type: string
    sql: ${TABLE}.incdt_summary ;;
  }

  dimension_group: incdt_timestamp {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.incdt_timestamp ;;
  }

  dimension_group: incdt_updated {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.incdt_updated ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [incdt_id, incdt_assigned_username, incdt_owner_username]
  }
}
