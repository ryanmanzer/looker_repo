view: costelem {
  sql_table_name: public.costelem ;;

  dimension: costelem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.costelem_id ;;
  }

  dimension: costelem_active {
    type: yesno
    sql: ${TABLE}.costelem_active ;;
  }

  dimension: costelem_cost_item_id {
    type: number
    sql: ${TABLE}.costelem_cost_item_id ;;
  }

  dimension: costelem_exp_accnt_id {
    type: number
    sql: ${TABLE}.costelem_exp_accnt_id ;;
  }

  dimension: costelem_po {
    type: yesno
    sql: ${TABLE}.costelem_po ;;
  }

  dimension: costelem_sys {
    type: yesno
    sql: ${TABLE}.costelem_sys ;;
  }

  dimension: costelem_type {
    type: string
    sql: ${TABLE}.costelem_type ;;
  }

  measure: count {
    type: count
    drill_fields: [costelem_id]
  }
}
