view: stdjrnlitem {
  sql_table_name: public.stdjrnlitem ;;

  dimension: stdjrnlitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.stdjrnlitem_id ;;
  }

  dimension: stdjrnlitem_accnt_id {
    type: number
    sql: ${TABLE}.stdjrnlitem_accnt_id ;;
  }

  dimension: stdjrnlitem_amount {
    type: number
    sql: ${TABLE}.stdjrnlitem_amount ;;
  }

  dimension: stdjrnlitem_notes {
    type: string
    sql: ${TABLE}.stdjrnlitem_notes ;;
  }

  dimension: stdjrnlitem_stdjrnl_id {
    type: number
    sql: ${TABLE}.stdjrnlitem_stdjrnl_id ;;
  }

  measure: count {
    type: count
    drill_fields: [stdjrnlitem_id]
  }
}
