view: vend {
  sql_table_name: public.vend ;;

  dimension: vend_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.vend_id ;;
  }

  dimension: vend_1099 {
    type: yesno
    sql: ${TABLE}.vend_1099 ;;
  }

  dimension: vend_accntnum {
    type: string
    sql: ${TABLE}.vend_accntnum ;;
  }

  dimension: vend_active {
    type: yesno
    sql: ${TABLE}.vend_active ;;
  }

  dimension: vend_address1 {
    type: string
    sql: ${TABLE}.vend_address1 ;;
  }

  dimension: vend_address2 {
    type: string
    sql: ${TABLE}.vend_address2 ;;
  }

  dimension: vend_address3 {
    type: string
    sql: ${TABLE}.vend_address3 ;;
  }

  dimension: vend_city {
    type: string
    sql: ${TABLE}.vend_city ;;
  }

  dimension: vend_comments {
    type: string
    sql: ${TABLE}.vend_comments ;;
  }

  dimension: vend_contact1 {
    type: string
    sql: ${TABLE}.vend_contact1 ;;
  }

  dimension: vend_contact2 {
    type: string
    sql: ${TABLE}.vend_contact2 ;;
  }

  dimension: vend_country {
    type: string
    sql: ${TABLE}.vend_country ;;
  }

  dimension: vend_curr_id {
    type: number
    sql: ${TABLE}.vend_curr_id ;;
  }

  dimension: vend_edicc {
    type: string
    sql: ${TABLE}.vend_edicc ;;
  }

  dimension: vend_ediemail {
    type: string
    sql: ${TABLE}.vend_ediemail ;;
  }

  dimension: vend_ediemailbody {
    type: string
    sql: ${TABLE}.vend_ediemailbody ;;
  }

  dimension: vend_edifilename {
    type: string
    sql: ${TABLE}.vend_edifilename ;;
  }

  dimension: vend_edisubject {
    type: string
    sql: ${TABLE}.vend_edisubject ;;
  }

  dimension: vend_email1 {
    type: string
    sql: ${TABLE}.vend_email1 ;;
  }

  dimension: vend_email2 {
    type: string
    sql: ${TABLE}.vend_email2 ;;
  }

  dimension: vend_emailpodelivery {
    type: yesno
    sql: ${TABLE}.vend_emailpodelivery ;;
  }

  dimension: vend_exported {
    type: yesno
    sql: ${TABLE}.vend_exported ;;
  }

  dimension: vend_fax1 {
    type: string
    sql: ${TABLE}.vend_fax1 ;;
  }

  dimension: vend_fax2 {
    type: string
    sql: ${TABLE}.vend_fax2 ;;
  }

  dimension: vend_fob {
    type: string
    sql: ${TABLE}.vend_fob ;;
  }

  dimension: vend_fobsource {
    type: string
    sql: ${TABLE}.vend_fobsource ;;
  }

  dimension_group: vend_lastpurchdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.vend_lastpurchdate ;;
  }

  dimension: vend_name {
    type: string
    sql: ${TABLE}.vend_name ;;
  }

  dimension: vend_number {
    type: string
    sql: ${TABLE}.vend_number ;;
  }

  dimension: vend_phone1 {
    type: string
    sql: ${TABLE}.vend_phone1 ;;
  }

  dimension: vend_phone2 {
    type: string
    sql: ${TABLE}.vend_phone2 ;;
  }

  dimension: vend_po {
    type: yesno
    sql: ${TABLE}.vend_po ;;
  }

  dimension: vend_pocomments {
    type: string
    sql: ${TABLE}.vend_pocomments ;;
  }

  dimension: vend_qualified {
    type: yesno
    sql: ${TABLE}.vend_qualified ;;
  }

  dimension: vend_restrictpurch {
    type: yesno
    sql: ${TABLE}.vend_restrictpurch ;;
  }

  dimension: vend_shipvia {
    type: string
    sql: ${TABLE}.vend_shipvia ;;
  }

  dimension: vend_state {
    type: string
    sql: ${TABLE}.vend_state ;;
  }

  dimension: vend_taxzone_id {
    type: number
    sql: ${TABLE}.vend_taxzone_id ;;
  }

  dimension: vend_terms_id {
    type: number
    sql: ${TABLE}.vend_terms_id ;;
  }

  dimension: vend_vendtype_id {
    type: number
    sql: ${TABLE}.vend_vendtype_id ;;
  }

  dimension: vend_zip {
    type: string
    sql: ${TABLE}.vend_zip ;;
  }

  measure: count {
    type: count
    drill_fields: [vend_id, vend_name, vend_edifilename, vendinfo.count]
  }
}
