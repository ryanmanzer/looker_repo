view: mrghist {
  sql_table_name: public.mrghist ;;

  dimension: mrghist_pkey_col {
    primary_key: yes
    type: string
    sql: ${TABLE}.mrghist_pkey_col ;;
  }

  dimension: mrghist_cntct_col {
    type: string
    sql: ${TABLE}.mrghist_cntct_col ;;
  }

  dimension: mrghist_cntct_id {
    type: number
    sql: ${TABLE}.mrghist_cntct_id ;;
  }

  dimension: mrghist_pkey_id {
    type: number
    sql: ${TABLE}.mrghist_pkey_id ;;
  }

  dimension: mrghist_table {
    type: string
    sql: ${TABLE}.mrghist_table ;;
  }

  measure: count {
    type: count
    drill_fields: [mrghist_pkey_col]
  }
}
