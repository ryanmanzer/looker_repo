view: costhist {
  sql_table_name: public.costhist ;;

  dimension: costhist_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.costhist_id ;;
  }

  dimension: costhist_costelem_id {
    type: number
    sql: ${TABLE}.costhist_costelem_id ;;
  }

  dimension_group: costhist {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.costhist_date ;;
  }

  dimension: costhist_item_id {
    type: number
    sql: ${TABLE}.costhist_item_id ;;
  }

  dimension: costhist_lowlevel {
    type: yesno
    sql: ${TABLE}.costhist_lowlevel ;;
  }

  dimension: costhist_newcost {
    type: number
    sql: ${TABLE}.costhist_newcost ;;
  }

  dimension: costhist_newcurr_id {
    type: number
    sql: ${TABLE}.costhist_newcurr_id ;;
  }

  dimension: costhist_oldcost {
    type: number
    sql: ${TABLE}.costhist_oldcost ;;
  }

  dimension: costhist_oldcurr_id {
    type: number
    sql: ${TABLE}.costhist_oldcurr_id ;;
  }

  dimension: costhist_type {
    type: string
    sql: ${TABLE}.costhist_type ;;
  }

  dimension: costhist_username {
    type: string
    sql: ${TABLE}.costhist_username ;;
  }

  measure: count {
    type: count
    drill_fields: [costhist_id, costhist_username]
  }
}
