view: stdjrnl {
  sql_table_name: public.stdjrnl ;;

  dimension: stdjrnl_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.stdjrnl_id ;;
  }

  dimension: stdjrnl_descrip {
    type: string
    sql: ${TABLE}.stdjrnl_descrip ;;
  }

  dimension: stdjrnl_name {
    type: string
    sql: ${TABLE}.stdjrnl_name ;;
  }

  dimension: stdjrnl_notes {
    type: string
    sql: ${TABLE}.stdjrnl_notes ;;
  }

  measure: count {
    type: count
    drill_fields: [stdjrnl_id, stdjrnl_name]
  }
}
