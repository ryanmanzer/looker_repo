view: trialbal {
  sql_table_name: public.trialbal ;;

  dimension: trialbal_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.trialbal_id ;;
  }

  dimension: trialbal_accnt_id {
    type: number
    sql: ${TABLE}.trialbal_accnt_id ;;
  }

  dimension: trialbal_beginning {
    type: number
    sql: ${TABLE}.trialbal_beginning ;;
  }

  dimension: trialbal_credits {
    type: number
    sql: ${TABLE}.trialbal_credits ;;
  }

  dimension: trialbal_debits {
    type: number
    sql: ${TABLE}.trialbal_debits ;;
  }

  dimension: trialbal_dirty {
    type: yesno
    sql: ${TABLE}.trialbal_dirty ;;
  }

  dimension: trialbal_ending {
    type: number
    sql: ${TABLE}.trialbal_ending ;;
  }

  dimension: trialbal_period_id {
    type: number
    sql: ${TABLE}.trialbal_period_id ;;
  }

  dimension: trialbal_yearend {
    type: number
    sql: ${TABLE}.trialbal_yearend ;;
  }

  measure: count {
    type: count
    drill_fields: [trialbal_id, trialbalsync.count]
  }
}
