view: shipto {
  sql_table_name: public.shipto ;;

  dimension: shipto_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.shipto_id ;;
  }

  dimension: shipto_active {
    type: yesno
    sql: ${TABLE}.shipto_active ;;
  }

  dimension: shipto_address1 {
    type: string
    sql: ${TABLE}.shipto_address1 ;;
  }

  dimension: shipto_address2 {
    type: string
    sql: ${TABLE}.shipto_address2 ;;
  }

  dimension: shipto_address3 {
    type: string
    sql: ${TABLE}.shipto_address3 ;;
  }

  dimension: shipto_city {
    type: string
    sql: ${TABLE}.shipto_city ;;
  }

  dimension: shipto_comments {
    type: string
    sql: ${TABLE}.shipto_comments ;;
  }

  dimension: shipto_commission {
    type: number
    sql: ${TABLE}.shipto_commission ;;
  }

  dimension: shipto_contact {
    type: string
    sql: ${TABLE}.shipto_contact ;;
  }

  dimension: shipto_country {
    type: string
    sql: ${TABLE}.shipto_country ;;
  }

  dimension: shipto_cust_id {
    type: number
    sql: ${TABLE}.shipto_cust_id ;;
  }

  dimension: shipto_default {
    type: yesno
    sql: ${TABLE}.shipto_default ;;
  }

  dimension: shipto_ediprofile_id {
    type: number
    sql: ${TABLE}.shipto_ediprofile_id ;;
  }

  dimension: shipto_email {
    type: string
    sql: ${TABLE}.shipto_email ;;
  }

  dimension: shipto_fax {
    type: string
    sql: ${TABLE}.shipto_fax ;;
  }

  dimension: shipto_name {
    type: string
    sql: ${TABLE}.shipto_name ;;
  }

  dimension: shipto_num {
    type: string
    sql: ${TABLE}.shipto_num ;;
  }

  dimension: shipto_phone {
    type: string
    sql: ${TABLE}.shipto_phone ;;
  }

  dimension: shipto_salesrep_id {
    type: number
    sql: ${TABLE}.shipto_salesrep_id ;;
  }

  dimension: shipto_shipchrg_id {
    type: number
    sql: ${TABLE}.shipto_shipchrg_id ;;
  }

  dimension: shipto_shipcomments {
    type: string
    sql: ${TABLE}.shipto_shipcomments ;;
  }

  dimension: shipto_shipform_id {
    type: number
    sql: ${TABLE}.shipto_shipform_id ;;
  }

  dimension: shipto_shipvia {
    type: string
    sql: ${TABLE}.shipto_shipvia ;;
  }

  dimension: shipto_shipzone_id {
    type: number
    sql: ${TABLE}.shipto_shipzone_id ;;
  }

  dimension: shipto_state {
    type: string
    sql: ${TABLE}.shipto_state ;;
  }

  dimension: shipto_taxzone_id {
    type: number
    sql: ${TABLE}.shipto_taxzone_id ;;
  }

  dimension: shipto_zipcode {
    type: string
    sql: ${TABLE}.shipto_zipcode ;;
  }

  measure: count {
    type: count
    drill_fields: [shipto_id, shipto_name, shiptoinfo.count]
  }
}
