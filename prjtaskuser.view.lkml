view: prjtaskuser {
  sql_table_name: public.prjtaskuser ;;

  dimension: prjtaskuser_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.prjtaskuser_id ;;
  }

  dimension: prjtaskuser_prjtask_id {
    type: number
    sql: ${TABLE}.prjtaskuser_prjtask_id ;;
  }

  dimension: prjtaskuser_username {
    type: string
    sql: ${TABLE}.prjtaskuser_username ;;
  }

  measure: count {
    type: count
    drill_fields: [prjtaskuser_id, prjtaskuser_username]
  }
}
