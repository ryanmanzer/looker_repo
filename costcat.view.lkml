view: costcat {
  sql_table_name: public.costcat ;;

  dimension: costcat_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.costcat_id ;;
  }

  dimension: costcat_adjustment_accnt_id {
    type: number
    sql: ${TABLE}.costcat_adjustment_accnt_id ;;
  }

  dimension: costcat_asset_accnt_id {
    type: number
    sql: ${TABLE}.costcat_asset_accnt_id ;;
  }

  dimension: costcat_code {
    type: string
    sql: ${TABLE}.costcat_code ;;
  }

  dimension: costcat_descrip {
    type: string
    sql: ${TABLE}.costcat_descrip ;;
  }

  dimension: costcat_exp_accnt_id {
    type: number
    sql: ${TABLE}.costcat_exp_accnt_id ;;
  }

  dimension: costcat_freight_accnt_id {
    type: number
    sql: ${TABLE}.costcat_freight_accnt_id ;;
  }

  dimension: costcat_invcost_accnt_id {
    type: number
    sql: ${TABLE}.costcat_invcost_accnt_id ;;
  }

  dimension: costcat_laboroverhead_accnt_id {
    type: number
    sql: ${TABLE}.costcat_laboroverhead_accnt_id ;;
  }

  dimension: costcat_liability_accnt_id {
    type: number
    sql: ${TABLE}.costcat_liability_accnt_id ;;
  }

  dimension: costcat_matusage_accnt_id {
    type: number
    sql: ${TABLE}.costcat_matusage_accnt_id ;;
  }

  dimension: costcat_mfgscrap_accnt_id {
    type: number
    sql: ${TABLE}.costcat_mfgscrap_accnt_id ;;
  }

  dimension: costcat_purchprice_accnt_id {
    type: number
    sql: ${TABLE}.costcat_purchprice_accnt_id ;;
  }

  dimension: costcat_scrap_accnt_id {
    type: number
    sql: ${TABLE}.costcat_scrap_accnt_id ;;
  }

  dimension: costcat_shipasset_accnt_id {
    type: number
    sql: ${TABLE}.costcat_shipasset_accnt_id ;;
  }

  dimension: costcat_toliability_accnt_id {
    type: number
    sql: ${TABLE}.costcat_toliability_accnt_id ;;
  }

  dimension: costcat_transform_accnt_id {
    type: number
    sql: ${TABLE}.costcat_transform_accnt_id ;;
  }

  dimension: costcat_wip_accnt_id {
    type: number
    sql: ${TABLE}.costcat_wip_accnt_id ;;
  }

  measure: count {
    type: count
    drill_fields: [costcat_id]
  }
}
