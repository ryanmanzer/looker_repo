view: shift {
  sql_table_name: public.shift ;;

  dimension: shift_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.shift_id ;;
  }

  dimension: shift_name {
    type: string
    sql: ${TABLE}.shift_name ;;
  }

  dimension: shift_number {
    type: string
    sql: ${TABLE}.shift_number ;;
  }

  measure: count {
    type: count
    drill_fields: [shift_id, shift_name]
  }
}
