view: saletype {
  sql_table_name: public.saletype ;;

  dimension: saletype_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.saletype_id ;;
  }

  dimension: saletype_active {
    type: yesno
    sql: ${TABLE}.saletype_active ;;
  }

  dimension: saletype_code {
    type: string
    sql: ${TABLE}.saletype_code ;;
  }

  dimension: saletype_default {
    type: yesno
    sql: ${TABLE}.saletype_default ;;
  }

  dimension: saletype_descr {
    type: string
    sql: ${TABLE}.saletype_descr ;;
  }

  measure: count {
    type: count
    drill_fields: [saletype_id]
  }
}
