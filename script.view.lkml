view: script {
  sql_table_name: public.script ;;

  dimension: script_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.script_id ;;
  }

  dimension: script_enabled {
    type: yesno
    sql: ${TABLE}.script_enabled ;;
  }

  dimension: script_name {
    type: string
    sql: ${TABLE}.script_name ;;
  }

  dimension: script_notes {
    type: string
    sql: ${TABLE}.script_notes ;;
  }

  dimension: script_order {
    type: number
    sql: ${TABLE}.script_order ;;
  }

  dimension: script_source {
    type: string
    sql: ${TABLE}.script_source ;;
  }

  measure: count {
    type: count
    drill_fields: [script_id, script_name]
  }
}
