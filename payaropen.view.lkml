view: payaropen {
  sql_table_name: public.payaropen ;;

  dimension: payaropen_amount {
    type: number
    sql: ${TABLE}.payaropen_amount ;;
  }

  dimension: payaropen_aropen_id {
    type: number
    sql: ${TABLE}.payaropen_aropen_id ;;
  }

  dimension: payaropen_ccpay_id {
    type: number
    sql: ${TABLE}.payaropen_ccpay_id ;;
  }

  dimension: payaropen_curr_id {
    type: number
    sql: ${TABLE}.payaropen_curr_id ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
