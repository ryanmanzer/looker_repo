view: rahist {
  sql_table_name: public.rahist ;;

  dimension: rahist_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.rahist_id ;;
  }

  dimension: rahist_amount {
    type: number
    sql: ${TABLE}.rahist_amount ;;
  }

  dimension: rahist_curr_id {
    type: number
    sql: ${TABLE}.rahist_curr_id ;;
  }

  dimension_group: rahist {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.rahist_date ;;
  }

  dimension: rahist_descrip {
    type: string
    sql: ${TABLE}.rahist_descrip ;;
  }

  dimension: rahist_itemsite_id {
    type: number
    sql: ${TABLE}.rahist_itemsite_id ;;
  }

  dimension: rahist_qty {
    type: number
    sql: ${TABLE}.rahist_qty ;;
  }

  dimension: rahist_rahead_id {
    type: number
    sql: ${TABLE}.rahist_rahead_id ;;
  }

  dimension: rahist_source {
    type: string
    sql: ${TABLE}.rahist_source ;;
  }

  dimension: rahist_source_id {
    type: number
    sql: ${TABLE}.rahist_source_id ;;
  }

  dimension: rahist_uom_id {
    type: number
    sql: ${TABLE}.rahist_uom_id ;;
  }

  measure: count {
    type: count
    drill_fields: [rahist_id]
  }
}
