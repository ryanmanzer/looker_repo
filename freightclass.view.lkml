view: freightclass {
  sql_table_name: public.freightclass ;;

  dimension: freightclass_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.freightclass_id ;;
  }

  dimension: freightclass_code {
    type: string
    sql: ${TABLE}.freightclass_code ;;
  }

  dimension: freightclass_descrip {
    type: string
    sql: ${TABLE}.freightclass_descrip ;;
  }

  measure: count {
    type: count
    drill_fields: [freightclass_id]
  }
}
