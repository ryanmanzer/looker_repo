view: incdtpriority {
  sql_table_name: public.incdtpriority ;;

  dimension: incdtpriority_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.incdtpriority_id ;;
  }

  dimension: incdtpriority_descrip {
    type: string
    sql: ${TABLE}.incdtpriority_descrip ;;
  }

  dimension: incdtpriority_name {
    type: string
    sql: ${TABLE}.incdtpriority_name ;;
  }

  dimension: incdtpriority_order {
    type: number
    sql: ${TABLE}.incdtpriority_order ;;
  }

  measure: count {
    type: count
    drill_fields: [incdtpriority_id, incdtpriority_name]
  }
}
