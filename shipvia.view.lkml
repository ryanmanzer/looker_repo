view: shipvia {
  sql_table_name: public.shipvia ;;

  dimension: shipvia_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.shipvia_id ;;
  }

  dimension: shipvia_code {
    type: string
    sql: ${TABLE}.shipvia_code ;;
  }

  dimension: shipvia_descrip {
    type: string
    sql: ${TABLE}.shipvia_descrip ;;
  }

  measure: count {
    type: count
    drill_fields: [shipvia_id]
  }
}
