view: atlasmap {
  sql_table_name: public.atlasmap ;;

  dimension: atlasmap_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.atlasmap_id ;;
  }

  dimension: atlasmap_atlas {
    type: string
    sql: ${TABLE}.atlasmap_atlas ;;
  }

  dimension: atlasmap_filter {
    type: string
    sql: ${TABLE}.atlasmap_filter ;;
  }

  dimension: atlasmap_filtertype {
    type: string
    sql: ${TABLE}.atlasmap_filtertype ;;
  }

  dimension: atlasmap_headerline {
    type: yesno
    sql: ${TABLE}.atlasmap_headerline ;;
  }

  dimension: atlasmap_map {
    type: string
    sql: ${TABLE}.atlasmap_map ;;
  }

  dimension: atlasmap_name {
    type: string
    sql: ${TABLE}.atlasmap_name ;;
  }

  measure: count {
    type: count
    drill_fields: [atlasmap_id, atlasmap_name]
  }
}
