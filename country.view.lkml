view: country {
  sql_table_name: public.country ;;

  dimension: country_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.country_id ;;
  }

  dimension: country_abbr {
    type: string
    sql: ${TABLE}.country_abbr ;;
  }

  dimension: country_curr_abbr {
    type: string
    sql: ${TABLE}.country_curr_abbr ;;
  }

  dimension: country_curr_name {
    type: string
    sql: ${TABLE}.country_curr_name ;;
  }

  dimension: country_curr_number {
    type: string
    sql: ${TABLE}.country_curr_number ;;
  }

  dimension: country_curr_symbol {
    type: string
    sql: ${TABLE}.country_curr_symbol ;;
  }

  dimension: country_name {
    type: string
    sql: ${TABLE}.country_name ;;
  }

  dimension: country_qt_number {
    type: number
    sql: ${TABLE}.country_qt_number ;;
  }

  measure: count {
    type: count
    drill_fields: [country_id, country_name, country_curr_name]
  }
}
