# README #

This project is my first attempt at setting up a Looker project that we can all collaborate on to explore this aspect of the Looker.

## Purpose ##

* Collaboration on data visualization
* Improved familiarity with Bitbucket as a tool to collaborate on code
* [Get better at using git](https://xkcd.com/1597/)
* [Write better commit messages](https://xkcd.com/1296/)
* Introduce some levity into the code development process (see above links)

## Projects - Dashboards ##

Providing current status updates on dashboard development

### New Products ###

This dashboard is intended to provide visualizations of data pertaining to the three new (or rebranded) SKUs released by Nordic Naturals on January 9, 2016.  It is in development and additional work by other developers (Shalini Mishra, Mark Timares) is highly encouraged.
