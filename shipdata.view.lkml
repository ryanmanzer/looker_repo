view: shipdata {
  sql_table_name: public.shipdata ;;

  dimension: shipdata_base_freight {
    type: number
    sql: ${TABLE}.shipdata_base_freight ;;
  }

  dimension: shipdata_base_freight_curr_id {
    type: number
    sql: ${TABLE}.shipdata_base_freight_curr_id ;;
  }

  dimension: shipdata_billing_option {
    type: string
    sql: ${TABLE}.shipdata_billing_option ;;
  }

  dimension: shipdata_cohead_number {
    type: string
    sql: ${TABLE}.shipdata_cohead_number ;;
  }

  dimension: shipdata_cosmisc_packnum_tracknum {
    type: string
    sql: ${TABLE}.shipdata_cosmisc_packnum_tracknum ;;
  }

  dimension: shipdata_cosmisc_tracknum {
    type: string
    sql: ${TABLE}.shipdata_cosmisc_tracknum ;;
  }

  dimension_group: shipdata_lastupdated {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.shipdata_lastupdated ;;
  }

  dimension: shipdata_package_type {
    type: string
    sql: ${TABLE}.shipdata_package_type ;;
  }

  dimension: shipdata_shiphead_number {
    type: string
    sql: ${TABLE}.shipdata_shiphead_number ;;
  }

  dimension: shipdata_shipper {
    type: string
    sql: ${TABLE}.shipdata_shipper ;;
  }

  dimension: shipdata_total_freight {
    type: number
    sql: ${TABLE}.shipdata_total_freight ;;
  }

  dimension: shipdata_total_freight_curr_id {
    type: number
    sql: ${TABLE}.shipdata_total_freight_curr_id ;;
  }

  dimension: shipdata_void_ind {
    type: string
    sql: ${TABLE}.shipdata_void_ind ;;
  }

  dimension: shipdata_weight {
    type: number
    sql: ${TABLE}.shipdata_weight ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
