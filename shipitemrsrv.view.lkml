view: shipitemrsrv {
  sql_table_name: public.shipitemrsrv ;;

  dimension: shipitemrsrv_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.shipitemrsrv_id ;;
  }

  dimension: shipitemrsrv_qty {
    type: number
    sql: ${TABLE}.shipitemrsrv_qty ;;
  }

  dimension: shipitemrsrv_shipitem_id {
    type: number
    sql: ${TABLE}.shipitemrsrv_shipitem_id ;;
  }

  measure: count {
    type: count
    drill_fields: [shipitemrsrv_id]
  }
}
