view: pohead {
  sql_table_name: public.pohead ;;

  dimension: pohead_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.pohead_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: pohead_agent_username {
    type: string
    sql: ${TABLE}.pohead_agent_username ;;
  }

  dimension: pohead_cohead_id {
    type: number
    sql: ${TABLE}.pohead_cohead_id ;;
  }

  dimension: pohead_comments {
    type: string
    sql: ${TABLE}.pohead_comments ;;
  }

  dimension: pohead_curr_id {
    type: number
    sql: ${TABLE}.pohead_curr_id ;;
  }

  dimension: pohead_dropship {
    type: yesno
    sql: ${TABLE}.pohead_dropship ;;
  }

  dimension: pohead_fob {
    type: string
    sql: ${TABLE}.pohead_fob ;;
  }

  dimension: pohead_freight {
    type: number
    sql: ${TABLE}.pohead_freight ;;
  }

  dimension: pohead_number {
    type: string
    sql: ${TABLE}.pohead_number ;;
  }

  dimension_group: pohead_orderdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.pohead_orderdate ;;
  }

  dimension: pohead_printed {
    type: yesno
    sql: ${TABLE}.pohead_printed ;;
  }

  dimension_group: pohead_released {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.pohead_released ;;
  }

  dimension: pohead_saved {
    type: yesno
    sql: ${TABLE}.pohead_saved ;;
  }

  dimension: pohead_shipto_cntct_email {
    type: string
    sql: ${TABLE}.pohead_shipto_cntct_email ;;
  }

  dimension: pohead_shipto_cntct_fax {
    type: string
    sql: ${TABLE}.pohead_shipto_cntct_fax ;;
  }

  dimension: pohead_shipto_cntct_first_name {
    type: string
    sql: ${TABLE}.pohead_shipto_cntct_first_name ;;
  }

  dimension: pohead_shipto_cntct_honorific {
    type: string
    sql: ${TABLE}.pohead_shipto_cntct_honorific ;;
  }

  dimension: pohead_shipto_cntct_id {
    type: number
    sql: ${TABLE}.pohead_shipto_cntct_id ;;
  }

  dimension: pohead_shipto_cntct_last_name {
    type: string
    sql: ${TABLE}.pohead_shipto_cntct_last_name ;;
  }

  dimension: pohead_shipto_cntct_middle {
    type: string
    sql: ${TABLE}.pohead_shipto_cntct_middle ;;
  }

  dimension: pohead_shipto_cntct_phone {
    type: string
    sql: ${TABLE}.pohead_shipto_cntct_phone ;;
  }

  dimension: pohead_shipto_cntct_suffix {
    type: string
    sql: ${TABLE}.pohead_shipto_cntct_suffix ;;
  }

  dimension: pohead_shipto_cntct_title {
    type: string
    sql: ${TABLE}.pohead_shipto_cntct_title ;;
  }

  dimension: pohead_shiptoaddress1 {
    type: string
    sql: ${TABLE}.pohead_shiptoaddress1 ;;
  }

  dimension: pohead_shiptoaddress2 {
    type: string
    sql: ${TABLE}.pohead_shiptoaddress2 ;;
  }

  dimension: pohead_shiptoaddress3 {
    type: string
    sql: ${TABLE}.pohead_shiptoaddress3 ;;
  }

  dimension: pohead_shiptoaddress_id {
    type: number
    sql: ${TABLE}.pohead_shiptoaddress_id ;;
  }

  dimension: pohead_shiptocity {
    type: string
    sql: ${TABLE}.pohead_shiptocity ;;
  }

  dimension: pohead_shiptocountry {
    type: string
    sql: ${TABLE}.pohead_shiptocountry ;;
  }

  dimension: pohead_shiptoname {
    type: string
    sql: ${TABLE}.pohead_shiptoname ;;
  }

  dimension: pohead_shiptostate {
    type: string
    sql: ${TABLE}.pohead_shiptostate ;;
  }

  dimension: pohead_shiptozipcode {
    type: string
    sql: ${TABLE}.pohead_shiptozipcode ;;
  }

  dimension: pohead_shipvia {
    type: string
    sql: ${TABLE}.pohead_shipvia ;;
  }

  dimension: pohead_status {
    type: string
    sql: ${TABLE}.pohead_status ;;
  }

  dimension: pohead_taxtype_id {
    type: number
    sql: ${TABLE}.pohead_taxtype_id ;;
  }

  dimension: pohead_taxzone_id {
    type: number
    sql: ${TABLE}.pohead_taxzone_id ;;
  }

  dimension: pohead_terms_id {
    type: number
    sql: ${TABLE}.pohead_terms_id ;;
  }

  dimension: pohead_vend_cntct_email {
    type: string
    sql: ${TABLE}.pohead_vend_cntct_email ;;
  }

  dimension: pohead_vend_cntct_fax {
    type: string
    sql: ${TABLE}.pohead_vend_cntct_fax ;;
  }

  dimension: pohead_vend_cntct_first_name {
    type: string
    sql: ${TABLE}.pohead_vend_cntct_first_name ;;
  }

  dimension: pohead_vend_cntct_honorific {
    type: string
    sql: ${TABLE}.pohead_vend_cntct_honorific ;;
  }

  dimension: pohead_vend_cntct_id {
    type: number
    sql: ${TABLE}.pohead_vend_cntct_id ;;
  }

  dimension: pohead_vend_cntct_last_name {
    type: string
    sql: ${TABLE}.pohead_vend_cntct_last_name ;;
  }

  dimension: pohead_vend_cntct_middle {
    type: string
    sql: ${TABLE}.pohead_vend_cntct_middle ;;
  }

  dimension: pohead_vend_cntct_phone {
    type: string
    sql: ${TABLE}.pohead_vend_cntct_phone ;;
  }

  dimension: pohead_vend_cntct_suffix {
    type: string
    sql: ${TABLE}.pohead_vend_cntct_suffix ;;
  }

  dimension: pohead_vend_cntct_title {
    type: string
    sql: ${TABLE}.pohead_vend_cntct_title ;;
  }

  dimension: pohead_vend_id {
    type: number
    sql: ${TABLE}.pohead_vend_id ;;
  }

  dimension: pohead_vendaddr_id {
    type: number
    sql: ${TABLE}.pohead_vendaddr_id ;;
  }

  dimension: pohead_vendaddress1 {
    type: string
    sql: ${TABLE}.pohead_vendaddress1 ;;
  }

  dimension: pohead_vendaddress2 {
    type: string
    sql: ${TABLE}.pohead_vendaddress2 ;;
  }

  dimension: pohead_vendaddress3 {
    type: string
    sql: ${TABLE}.pohead_vendaddress3 ;;
  }

  dimension: pohead_vendcity {
    type: string
    sql: ${TABLE}.pohead_vendcity ;;
  }

  dimension: pohead_vendcountry {
    type: string
    sql: ${TABLE}.pohead_vendcountry ;;
  }

  dimension: pohead_vendstate {
    type: string
    sql: ${TABLE}.pohead_vendstate ;;
  }

  dimension: pohead_vendzipcode {
    type: string
    sql: ${TABLE}.pohead_vendzipcode ;;
  }

  dimension: pohead_warehous_id {
    type: number
    sql: ${TABLE}.pohead_warehous_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      pohead_id,
      pohead_vend_cntct_last_name,
      pohead_shipto_cntct_first_name,
      pohead_shipto_cntct_last_name,
      pohead_shiptoname,
      pohead_agent_username,
      pohead_vend_cntct_first_name
    ]
  }
}
