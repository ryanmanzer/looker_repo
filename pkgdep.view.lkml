view: pkgdep {
  sql_table_name: public.pkgdep ;;

  dimension: pkgdep_pkghead_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.pkgdep_pkghead_id ;;
  }

  dimension: pkgdep_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.pkgdep_id ;;
  }

  dimension: pkgdep_parent_pkghead_id {
    type: number
    sql: ${TABLE}.pkgdep_parent_pkghead_id ;;
  }

  measure: count {
    type: count
    drill_fields: [pkgdep_pkghead_id, pkgdep.pkgdep_pkghead_id, pkgdep.count]
  }
}
