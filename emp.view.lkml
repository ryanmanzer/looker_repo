view: emp {
  sql_table_name: public.emp ;;

  dimension: emp_mgr_emp_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.emp_mgr_emp_id ;;
  }

  dimension: emp_active {
    type: yesno
    sql: ${TABLE}.emp_active ;;
  }

  dimension: emp_cntct_id {
    type: number
    sql: ${TABLE}.emp_cntct_id ;;
  }

  dimension: emp_code {
    type: string
    sql: ${TABLE}.emp_code ;;
  }

  dimension: emp_dept_id {
    type: number
    sql: ${TABLE}.emp_dept_id ;;
  }

  dimension: emp_extrate {
    type: number
    sql: ${TABLE}.emp_extrate ;;
  }

  dimension: emp_extrate_period {
    type: string
    sql: ${TABLE}.emp_extrate_period ;;
  }

  dimension: emp_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.emp_id ;;
  }

  dimension: emp_image_id {
    type: number
    sql: ${TABLE}.emp_image_id ;;
  }

  dimension: emp_name {
    type: string
    sql: ${TABLE}.emp_name ;;
  }

  dimension: emp_notes {
    type: string
    sql: ${TABLE}.emp_notes ;;
  }

  dimension: emp_number {
    type: string
    sql: ${TABLE}.emp_number ;;
  }

  dimension: emp_shift_id {
    type: number
    sql: ${TABLE}.emp_shift_id ;;
  }

  dimension_group: emp_startdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.emp_startdate ;;
  }

  dimension: emp_username {
    type: string
    sql: ${TABLE}.emp_username ;;
  }

  dimension: emp_wage {
    type: number
    sql: ${TABLE}.emp_wage ;;
  }

  dimension: emp_wage_curr_id {
    type: number
    sql: ${TABLE}.emp_wage_curr_id ;;
  }

  dimension: emp_wage_period {
    type: string
    sql: ${TABLE}.emp_wage_period ;;
  }

  dimension: emp_wage_type {
    type: string
    sql: ${TABLE}.emp_wage_type ;;
  }

  dimension: emp_warehous_id {
    type: number
    sql: ${TABLE}.emp_warehous_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      emp_mgr_emp_id,
      emp_name,
      emp_username,
      emp.emp_name,
      emp.emp_mgr_emp_id,
      emp.emp_username,
      emp.count
    ]
  }
}
