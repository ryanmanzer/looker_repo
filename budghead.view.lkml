view: budghead {
  sql_table_name: public.budghead ;;

  dimension: budghead_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.budghead_id ;;
  }

  dimension: budghead_descrip {
    type: string
    sql: ${TABLE}.budghead_descrip ;;
  }

  dimension: budghead_name {
    type: string
    sql: ${TABLE}.budghead_name ;;
  }

  measure: count {
    type: count
    drill_fields: [budghead_id, budghead_name]
  }
}
