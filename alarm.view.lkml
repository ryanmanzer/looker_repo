view: alarm {
  sql_table_name: public.alarm ;;

  dimension: alarm_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.alarm_id ;;
  }

  dimension: alarm_creator {
    type: string
    sql: ${TABLE}.alarm_creator ;;
  }

  dimension: alarm_email {
    type: yesno
    sql: ${TABLE}.alarm_email ;;
  }

  dimension: alarm_email_recipient {
    type: string
    sql: ${TABLE}.alarm_email_recipient ;;
  }

  dimension: alarm_event {
    type: yesno
    sql: ${TABLE}.alarm_event ;;
  }

  dimension: alarm_event_recipient {
    type: string
    sql: ${TABLE}.alarm_event_recipient ;;
  }

  dimension: alarm_number {
    type: string
    sql: ${TABLE}.alarm_number ;;
  }

  dimension: alarm_source {
    type: string
    sql: ${TABLE}.alarm_source ;;
  }

  dimension: alarm_source_id {
    type: number
    sql: ${TABLE}.alarm_source_id ;;
  }

  dimension: alarm_sysmsg {
    type: yesno
    sql: ${TABLE}.alarm_sysmsg ;;
  }

  dimension: alarm_sysmsg_recipient {
    type: string
    sql: ${TABLE}.alarm_sysmsg_recipient ;;
  }

  dimension_group: alarm {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.alarm_time ;;
  }

  dimension: alarm_time_offset {
    type: number
    sql: ${TABLE}.alarm_time_offset ;;
  }

  dimension: alarm_time_qualifier {
    type: string
    sql: ${TABLE}.alarm_time_qualifier ;;
  }

  dimension_group: alarm_trigger {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.alarm_trigger ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [alarm_id]
  }
}
