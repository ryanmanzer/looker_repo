view: incdtseverity {
  sql_table_name: public.incdtseverity ;;

  dimension: incdtseverity_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.incdtseverity_id ;;
  }

  dimension: incdtseverity_descrip {
    type: string
    sql: ${TABLE}.incdtseverity_descrip ;;
  }

  dimension: incdtseverity_name {
    type: string
    sql: ${TABLE}.incdtseverity_name ;;
  }

  dimension: incdtseverity_order {
    type: number
    sql: ${TABLE}.incdtseverity_order ;;
  }

  measure: count {
    type: count
    drill_fields: [incdtseverity_id, incdtseverity_name]
  }
}
