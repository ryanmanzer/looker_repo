view: charuse {
  sql_table_name: public.charuse ;;

  dimension: charuse_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.charuse_id ;;
  }

  dimension: charuse_char_id {
    type: number
    sql: ${TABLE}.charuse_char_id ;;
  }

  dimension_group: charuse_created {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.charuse_created ;;
  }

  dimension_group: charuse_last_modified {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.charuse_last_modified ;;
  }

  dimension: charuse_target_type {
    type: string
    sql: ${TABLE}.charuse_target_type ;;
  }

  measure: count {
    type: count
    drill_fields: [charuse_id]
  }
}
