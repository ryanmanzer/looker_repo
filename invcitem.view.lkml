view: invcitem {
  sql_table_name: public.invcitem ;;

  dimension: invcitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.invcitem_id ;;
  }

  dimension: invcitem_billed {
    type: number
    sql: ${TABLE}.invcitem_billed ;;
  }

  dimension: invcitem_coitem_id {
    type: number
    sql: ${TABLE}.invcitem_coitem_id ;;
  }

  dimension: invcitem_custpn {
    type: string
    sql: ${TABLE}.invcitem_custpn ;;
  }

  dimension: invcitem_custprice {
    type: number
    sql: ${TABLE}.invcitem_custprice ;;
  }

  dimension: invcitem_descrip {
    type: string
    sql: ${TABLE}.invcitem_descrip ;;
  }

  dimension: invcitem_invchead_id {
    type: number
    sql: ${TABLE}.invcitem_invchead_id ;;
  }

  dimension: invcitem_item_id {
    type: number
    sql: ${TABLE}.invcitem_item_id ;;
  }

  dimension: invcitem_linenumber {
    type: number
    sql: ${TABLE}.invcitem_linenumber ;;
  }

  dimension: invcitem_notes {
    type: string
    sql: ${TABLE}.invcitem_notes ;;
  }

  dimension: invcitem_number {
    type: string
    sql: ${TABLE}.invcitem_number ;;
  }

  dimension: invcitem_ordered {
    type: number
    sql: ${TABLE}.invcitem_ordered ;;
  }

  dimension: invcitem_price {
    type: number
    sql: ${TABLE}.invcitem_price ;;
  }

  dimension: invcitem_price_invuomratio {
    type: number
    sql: ${TABLE}.invcitem_price_invuomratio ;;
  }

  dimension: invcitem_price_uom_id {
    type: number
    sql: ${TABLE}.invcitem_price_uom_id ;;
  }

  dimension: invcitem_qty_invuomratio {
    type: number
    sql: ${TABLE}.invcitem_qty_invuomratio ;;
  }

  dimension: invcitem_qty_uom_id {
    type: number
    sql: ${TABLE}.invcitem_qty_uom_id ;;
  }

  dimension: invcitem_rev_accnt_id {
    type: number
    sql: ${TABLE}.invcitem_rev_accnt_id ;;
  }

  dimension: invcitem_salescat_id {
    type: number
    sql: ${TABLE}.invcitem_salescat_id ;;
  }

  dimension: invcitem_taxtype_id {
    type: number
    sql: ${TABLE}.invcitem_taxtype_id ;;
  }

  dimension: invcitem_updateinv {
    type: yesno
    sql: ${TABLE}.invcitem_updateinv ;;
  }

  dimension: invcitem_warehous_id {
    type: number
    sql: ${TABLE}.invcitem_warehous_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [invcitem_id, invoiceitem.count]
  }
}
