view: apopen {
  sql_table_name: public.apopen ;;

  dimension: apopen_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.apopen_id ;;
  }

  dimension: apopen_accnt_id {
    type: number
    sql: ${TABLE}.apopen_accnt_id ;;
  }

  dimension: apopen_amount {
    type: number
    sql: ${TABLE}.apopen_amount ;;
  }

  dimension_group: apopen_closedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.apopen_closedate ;;
  }

  dimension: apopen_curr_id {
    type: number
    sql: ${TABLE}.apopen_curr_id ;;
  }

  dimension: apopen_curr_rate {
    type: number
    sql: ${TABLE}.apopen_curr_rate ;;
  }

  dimension: apopen_discount {
    type: yesno
    sql: ${TABLE}.apopen_discount ;;
  }

  dimension: apopen_discountable_amount {
    type: number
    sql: ${TABLE}.apopen_discountable_amount ;;
  }

  dimension_group: apopen_distdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.apopen_distdate ;;
  }

  dimension_group: apopen_docdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.apopen_docdate ;;
  }

  dimension: apopen_docnumber {
    type: string
    sql: ${TABLE}.apopen_docnumber ;;
  }

  dimension: apopen_doctype {
    type: string
    sql: ${TABLE}.apopen_doctype ;;
  }

  dimension_group: apopen_duedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.apopen_duedate ;;
  }

  dimension: apopen_invcnumber {
    type: string
    sql: ${TABLE}.apopen_invcnumber ;;
  }

  dimension: apopen_journalnumber {
    type: number
    sql: ${TABLE}.apopen_journalnumber ;;
  }

  dimension: apopen_notes {
    type: string
    sql: ${TABLE}.apopen_notes ;;
  }

  dimension: apopen_open {
    type: yesno
    sql: ${TABLE}.apopen_open ;;
  }

  dimension: apopen_paid {
    type: number
    value_format_name: id
    sql: ${TABLE}.apopen_paid ;;
  }

  dimension: apopen_ponumber {
    type: string
    sql: ${TABLE}.apopen_ponumber ;;
  }

  dimension: apopen_posted {
    type: yesno
    sql: ${TABLE}.apopen_posted ;;
  }

  dimension: apopen_reference {
    type: string
    sql: ${TABLE}.apopen_reference ;;
  }

  dimension: apopen_status {
    type: string
    sql: ${TABLE}.apopen_status ;;
  }

  dimension: apopen_terms_id {
    type: number
    sql: ${TABLE}.apopen_terms_id ;;
  }

  dimension: apopen_username {
    type: string
    sql: ${TABLE}.apopen_username ;;
  }

  dimension: apopen_vend_id {
    type: number
    sql: ${TABLE}.apopen_vend_id ;;
  }

  dimension: apopen_void {
    type: yesno
    sql: ${TABLE}.apopen_void ;;
  }

  measure: count {
    type: count
    drill_fields: [apopen_id, apopen_username, apmemo.count]
  }
}
