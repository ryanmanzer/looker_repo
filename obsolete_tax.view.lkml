view: obsolete_tax {
  sql_table_name: public.obsolete_tax ;;

  dimension: tax_code {
    type: string
    sql: ${TABLE}.tax_code ;;
  }

  dimension: tax_cumulative {
    type: yesno
    sql: ${TABLE}.tax_cumulative ;;
  }

  dimension: tax_descrip {
    type: string
    sql: ${TABLE}.tax_descrip ;;
  }

  dimension: tax_freight {
    type: yesno
    sql: ${TABLE}.tax_freight ;;
  }

  dimension: tax_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.tax_id ;;
  }

  dimension: tax_ratea {
    type: number
    sql: ${TABLE}.tax_ratea ;;
  }

  dimension: tax_rateb {
    type: number
    sql: ${TABLE}.tax_rateb ;;
  }

  dimension: tax_ratec {
    type: number
    sql: ${TABLE}.tax_ratec ;;
  }

  dimension: tax_sales_accnt_id {
    type: number
    sql: ${TABLE}.tax_sales_accnt_id ;;
  }

  dimension: tax_salesb_accnt_id {
    type: number
    sql: ${TABLE}.tax_salesb_accnt_id ;;
  }

  dimension: tax_salesc_accnt_id {
    type: number
    sql: ${TABLE}.tax_salesc_accnt_id ;;
  }

  measure: count {
    type: count
    drill_fields: [tax.tax_id]
  }
}
