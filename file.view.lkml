view: file {
  sql_table_name: public.file ;;

  dimension: file_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.file_id ;;
  }

  dimension: file_descrip {
    type: string
    sql: ${TABLE}.file_descrip ;;
  }

  dimension: file_stream {
    type: string
    sql: ${TABLE}.file_stream ;;
  }

  dimension: file_title {
    type: string
    sql: ${TABLE}.file_title ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [file_id]
  }
}
