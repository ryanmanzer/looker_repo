view: checkhead {
  sql_table_name: public.checkhead ;;

  dimension: checkhead_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.checkhead_id ;;
  }

  dimension: checkhead_ach_batch {
    type: string
    sql: ${TABLE}.checkhead_ach_batch ;;
  }

  dimension: checkhead_alt_curr_rate {
    type: number
    sql: ${TABLE}.checkhead_alt_curr_rate ;;
  }

  dimension: checkhead_amount {
    type: number
    sql: ${TABLE}.checkhead_amount ;;
  }

  dimension: checkhead_bankaccnt_id {
    type: number
    sql: ${TABLE}.checkhead_bankaccnt_id ;;
  }

  dimension_group: checkhead_checkdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.checkhead_checkdate ;;
  }

  dimension: checkhead_curr_id {
    type: number
    sql: ${TABLE}.checkhead_curr_id ;;
  }

  dimension: checkhead_curr_rate {
    type: number
    sql: ${TABLE}.checkhead_curr_rate ;;
  }

  dimension: checkhead_deleted {
    type: yesno
    sql: ${TABLE}.checkhead_deleted ;;
  }

  dimension: checkhead_expcat_id {
    type: number
    sql: ${TABLE}.checkhead_expcat_id ;;
  }

  dimension: checkhead_for {
    type: string
    sql: ${TABLE}.checkhead_for ;;
  }

  dimension: checkhead_journalnumber {
    type: number
    sql: ${TABLE}.checkhead_journalnumber ;;
  }

  dimension: checkhead_misc {
    type: yesno
    sql: ${TABLE}.checkhead_misc ;;
  }

  dimension: checkhead_notes {
    type: string
    sql: ${TABLE}.checkhead_notes ;;
  }

  dimension: checkhead_number {
    type: number
    sql: ${TABLE}.checkhead_number ;;
  }

  dimension: checkhead_posted {
    type: yesno
    sql: ${TABLE}.checkhead_posted ;;
  }

  dimension: checkhead_printed {
    type: yesno
    sql: ${TABLE}.checkhead_printed ;;
  }

  dimension: checkhead_rec {
    type: yesno
    sql: ${TABLE}.checkhead_rec ;;
  }

  dimension: checkhead_recip_id {
    type: number
    sql: ${TABLE}.checkhead_recip_id ;;
  }

  dimension: checkhead_recip_type {
    type: string
    sql: ${TABLE}.checkhead_recip_type ;;
  }

  dimension: checkhead_replaced {
    type: yesno
    sql: ${TABLE}.checkhead_replaced ;;
  }

  dimension: checkhead_void {
    type: yesno
    sql: ${TABLE}.checkhead_void ;;
  }

  measure: count {
    type: count
    drill_fields: [checkhead_id]
  }
}
