view: xsltmap {
  sql_table_name: public.xsltmap ;;

  dimension: xsltmap_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.xsltmap_id ;;
  }

  dimension: xsltmap_doctype {
    type: string
    sql: ${TABLE}.xsltmap_doctype ;;
  }

  dimension: xsltmap_export {
    type: string
    sql: ${TABLE}.xsltmap_export ;;
  }

  dimension: xsltmap_import {
    type: string
    sql: ${TABLE}.xsltmap_import ;;
  }

  dimension: xsltmap_name {
    type: string
    sql: ${TABLE}.xsltmap_name ;;
  }

  dimension: xsltmap_system {
    type: string
    sql: ${TABLE}.xsltmap_system ;;
  }

  measure: count {
    type: count
    drill_fields: [xsltmap_id, xsltmap_name]
  }
}
