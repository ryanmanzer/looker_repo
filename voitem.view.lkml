view: voitem {
  sql_table_name: public.voitem ;;

  dimension: voitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.voitem_id ;;
  }

  dimension: voitem_close {
    type: yesno
    sql: ${TABLE}.voitem_close ;;
  }

  dimension: voitem_freight {
    type: number
    sql: ${TABLE}.voitem_freight ;;
  }

  dimension: voitem_poitem_id {
    type: number
    sql: ${TABLE}.voitem_poitem_id ;;
  }

  dimension: voitem_qty {
    type: number
    sql: ${TABLE}.voitem_qty ;;
  }

  dimension: voitem_taxtype_id {
    type: number
    sql: ${TABLE}.voitem_taxtype_id ;;
  }

  dimension: voitem_vohead_id {
    type: number
    sql: ${TABLE}.voitem_vohead_id ;;
  }

  measure: count {
    type: count
    drill_fields: [voitem_id]
  }
}
