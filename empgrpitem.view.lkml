view: empgrpitem {
  sql_table_name: public.empgrpitem ;;

  dimension: empgrpitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.empgrpitem_id ;;
  }

  dimension: empgrpitem_emp_id {
    type: number
    sql: ${TABLE}.empgrpitem_emp_id ;;
  }

  dimension: empgrpitem_empgrp_id {
    type: number
    sql: ${TABLE}.empgrpitem_empgrp_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [empgrpitem_id]
  }
}
