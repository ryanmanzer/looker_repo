view: whsezone {
  sql_table_name: public.whsezone ;;

  dimension: whsezone_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.whsezone_id ;;
  }

  dimension: whsezone_descrip {
    type: string
    sql: ${TABLE}.whsezone_descrip ;;
  }

  dimension: whsezone_name {
    type: string
    sql: ${TABLE}.whsezone_name ;;
  }

  dimension: whsezone_warehous_id {
    type: number
    sql: ${TABLE}.whsezone_warehous_id ;;
  }

  measure: count {
    type: count
    drill_fields: [whsezone_id, whsezone_name]
  }
}
