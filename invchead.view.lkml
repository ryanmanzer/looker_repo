view: invchead {
  sql_table_name: public.invchead ;;

  dimension: invchead_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.invchead_id ;;
  }

  dimension: invchead_billto_address1 {
    type: string
    sql: ${TABLE}.invchead_billto_address1 ;;
  }

  dimension: invchead_billto_address2 {
    type: string
    sql: ${TABLE}.invchead_billto_address2 ;;
  }

  dimension: invchead_billto_address3 {
    type: string
    sql: ${TABLE}.invchead_billto_address3 ;;
  }

  dimension: invchead_billto_city {
    type: string
    sql: ${TABLE}.invchead_billto_city ;;
  }

  dimension: invchead_billto_country {
    type: string
    sql: ${TABLE}.invchead_billto_country ;;
  }

  dimension: invchead_billto_name {
    type: string
    sql: ${TABLE}.invchead_billto_name ;;
  }

  dimension: invchead_billto_phone {
    type: string
    sql: ${TABLE}.invchead_billto_phone ;;
  }

  dimension: invchead_billto_state {
    type: string
    sql: ${TABLE}.invchead_billto_state ;;
  }

  dimension: invchead_billto_zipcode {
    type: string
    sql: ${TABLE}.invchead_billto_zipcode ;;
  }

  dimension: invchead_commission {
    type: number
    sql: ${TABLE}.invchead_commission ;;
  }

  dimension: invchead_curr_id {
    type: number
    sql: ${TABLE}.invchead_curr_id ;;
  }

  dimension: invchead_cust_id {
    type: number
    sql: ${TABLE}.invchead_cust_id ;;
  }

  dimension: invchead_fob {
    type: string
    sql: ${TABLE}.invchead_fob ;;
  }

  dimension: invchead_freight {
    type: number
    sql: ${TABLE}.invchead_freight ;;
  }

  dimension_group: invchead_gldistdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.invchead_gldistdate ;;
  }

  dimension_group: invchead_invcdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.invchead_invcdate ;;
  }

  dimension: invchead_invcnumber {
    type: string
    sql: ${TABLE}.invchead_invcnumber ;;
  }

  dimension: invchead_misc_accnt_id {
    type: number
    sql: ${TABLE}.invchead_misc_accnt_id ;;
  }

  dimension: invchead_misc_amount {
    type: number
    sql: ${TABLE}.invchead_misc_amount ;;
  }

  dimension: invchead_misc_descrip {
    type: string
    sql: ${TABLE}.invchead_misc_descrip ;;
  }

  dimension: invchead_notes {
    type: string
    sql: ${TABLE}.invchead_notes ;;
  }

  dimension_group: invchead_orderdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.invchead_orderdate ;;
  }

  dimension: invchead_ordernumber {
    type: string
    sql: ${TABLE}.invchead_ordernumber ;;
  }

  dimension: invchead_payment {
    type: number
    sql: ${TABLE}.invchead_payment ;;
  }

  dimension: invchead_paymentref {
    type: string
    sql: ${TABLE}.invchead_paymentref ;;
  }

  dimension: invchead_ponumber {
    type: string
    sql: ${TABLE}.invchead_ponumber ;;
  }

  dimension: invchead_posted {
    type: yesno
    sql: ${TABLE}.invchead_posted ;;
  }

  dimension: invchead_printed {
    type: yesno
    sql: ${TABLE}.invchead_printed ;;
  }

  dimension: invchead_prj_id {
    type: number
    sql: ${TABLE}.invchead_prj_id ;;
  }

  dimension: invchead_recurring {
    type: yesno
    sql: ${TABLE}.invchead_recurring ;;
  }

  dimension: invchead_recurring_interval {
    type: number
    sql: ${TABLE}.invchead_recurring_interval ;;
  }

  dimension: invchead_recurring_invchead_id {
    type: number
    sql: ${TABLE}.invchead_recurring_invchead_id ;;
  }

  dimension: invchead_recurring_type {
    type: string
    sql: ${TABLE}.invchead_recurring_type ;;
  }

  dimension_group: invchead_recurring_until {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.invchead_recurring_until ;;
  }

  dimension: invchead_salesrep_id {
    type: number
    sql: ${TABLE}.invchead_salesrep_id ;;
  }

  dimension: invchead_saletype_id {
    type: number
    sql: ${TABLE}.invchead_saletype_id ;;
  }

  dimension: invchead_shipchrg_id {
    type: number
    sql: ${TABLE}.invchead_shipchrg_id ;;
  }

  dimension_group: invchead_shipdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.invchead_shipdate ;;
  }

  dimension: invchead_shipto_address1 {
    type: string
    sql: ${TABLE}.invchead_shipto_address1 ;;
  }

  dimension: invchead_shipto_address2 {
    type: string
    sql: ${TABLE}.invchead_shipto_address2 ;;
  }

  dimension: invchead_shipto_address3 {
    type: string
    sql: ${TABLE}.invchead_shipto_address3 ;;
  }

  dimension: invchead_shipto_city {
    type: string
    sql: ${TABLE}.invchead_shipto_city ;;
  }

  dimension: invchead_shipto_country {
    type: string
    sql: ${TABLE}.invchead_shipto_country ;;
  }

  dimension: invchead_shipto_id {
    type: number
    sql: ${TABLE}.invchead_shipto_id ;;
  }

  dimension: invchead_shipto_name {
    type: string
    sql: ${TABLE}.invchead_shipto_name ;;
  }

  dimension: invchead_shipto_phone {
    type: string
    sql: ${TABLE}.invchead_shipto_phone ;;
  }

  dimension: invchead_shipto_state {
    type: string
    sql: ${TABLE}.invchead_shipto_state ;;
  }

  dimension: invchead_shipto_zipcode {
    type: string
    sql: ${TABLE}.invchead_shipto_zipcode ;;
  }

  dimension: invchead_shipvia {
    type: string
    sql: ${TABLE}.invchead_shipvia ;;
  }

  dimension: invchead_shipzone_id {
    type: number
    sql: ${TABLE}.invchead_shipzone_id ;;
  }

  dimension: invchead_taxzone_id {
    type: number
    sql: ${TABLE}.invchead_taxzone_id ;;
  }

  dimension: invchead_terms_id {
    type: number
    sql: ${TABLE}.invchead_terms_id ;;
  }

  dimension: invchead_void {
    type: yesno
    sql: ${TABLE}.invchead_void ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [invchead_id, invchead_shipto_name, invchead_billto_name]
  }
}
