view: rahead {
  sql_table_name: public.rahead ;;

  dimension: rahead_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.rahead_id ;;
  }

  dimension_group: rahead_authdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.rahead_authdate ;;
  }

  dimension: rahead_billtoaddress1 {
    type: string
    sql: ${TABLE}.rahead_billtoaddress1 ;;
  }

  dimension: rahead_billtoaddress2 {
    type: string
    sql: ${TABLE}.rahead_billtoaddress2 ;;
  }

  dimension: rahead_billtoaddress3 {
    type: string
    sql: ${TABLE}.rahead_billtoaddress3 ;;
  }

  dimension: rahead_billtocity {
    type: string
    sql: ${TABLE}.rahead_billtocity ;;
  }

  dimension: rahead_billtocountry {
    type: string
    sql: ${TABLE}.rahead_billtocountry ;;
  }

  dimension: rahead_billtoname {
    type: string
    sql: ${TABLE}.rahead_billtoname ;;
  }

  dimension: rahead_billtostate {
    type: string
    sql: ${TABLE}.rahead_billtostate ;;
  }

  dimension: rahead_billtozip {
    type: string
    sql: ${TABLE}.rahead_billtozip ;;
  }

  dimension: rahead_calcfreight {
    type: yesno
    sql: ${TABLE}.rahead_calcfreight ;;
  }

  dimension: rahead_cohead_warehous_id {
    type: number
    sql: ${TABLE}.rahead_cohead_warehous_id ;;
  }

  dimension: rahead_commission {
    type: number
    sql: ${TABLE}.rahead_commission ;;
  }

  dimension_group: rahead_created {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.rahead_created ;;
  }

  dimension: rahead_creator {
    type: string
    sql: ${TABLE}.rahead_creator ;;
  }

  dimension: rahead_creditmethod {
    type: string
    sql: ${TABLE}.rahead_creditmethod ;;
  }

  dimension: rahead_curr_id {
    type: number
    sql: ${TABLE}.rahead_curr_id ;;
  }

  dimension: rahead_cust_id {
    type: number
    sql: ${TABLE}.rahead_cust_id ;;
  }

  dimension: rahead_custponumber {
    type: string
    sql: ${TABLE}.rahead_custponumber ;;
  }

  dimension: rahead_disposition {
    type: string
    sql: ${TABLE}.rahead_disposition ;;
  }

  dimension_group: rahead_expiredate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.rahead_expiredate ;;
  }

  dimension: rahead_freight {
    type: number
    sql: ${TABLE}.rahead_freight ;;
  }

  dimension: rahead_headcredited {
    type: yesno
    sql: ${TABLE}.rahead_headcredited ;;
  }

  dimension: rahead_incdt_id {
    type: number
    sql: ${TABLE}.rahead_incdt_id ;;
  }

  dimension_group: rahead_lastupdated {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.rahead_lastupdated ;;
  }

  dimension: rahead_misc {
    type: number
    sql: ${TABLE}.rahead_misc ;;
  }

  dimension: rahead_misc_accnt_id {
    type: number
    sql: ${TABLE}.rahead_misc_accnt_id ;;
  }

  dimension: rahead_misc_descrip {
    type: string
    sql: ${TABLE}.rahead_misc_descrip ;;
  }

  dimension: rahead_new_cohead_id {
    type: number
    sql: ${TABLE}.rahead_new_cohead_id ;;
  }

  dimension: rahead_notes {
    type: string
    sql: ${TABLE}.rahead_notes ;;
  }

  dimension: rahead_number {
    type: string
    sql: ${TABLE}.rahead_number ;;
  }

  dimension: rahead_orig_cohead_id {
    type: number
    sql: ${TABLE}.rahead_orig_cohead_id ;;
  }

  dimension: rahead_printed {
    type: yesno
    sql: ${TABLE}.rahead_printed ;;
  }

  dimension: rahead_prj_id {
    type: number
    sql: ${TABLE}.rahead_prj_id ;;
  }

  dimension: rahead_rsncode_id {
    type: number
    sql: ${TABLE}.rahead_rsncode_id ;;
  }

  dimension: rahead_salesrep_id {
    type: number
    sql: ${TABLE}.rahead_salesrep_id ;;
  }

  dimension: rahead_saletype_id {
    type: number
    sql: ${TABLE}.rahead_saletype_id ;;
  }

  dimension: rahead_shipto_address1 {
    type: string
    sql: ${TABLE}.rahead_shipto_address1 ;;
  }

  dimension: rahead_shipto_address2 {
    type: string
    sql: ${TABLE}.rahead_shipto_address2 ;;
  }

  dimension: rahead_shipto_address3 {
    type: string
    sql: ${TABLE}.rahead_shipto_address3 ;;
  }

  dimension: rahead_shipto_city {
    type: string
    sql: ${TABLE}.rahead_shipto_city ;;
  }

  dimension: rahead_shipto_country {
    type: string
    sql: ${TABLE}.rahead_shipto_country ;;
  }

  dimension: rahead_shipto_id {
    type: number
    sql: ${TABLE}.rahead_shipto_id ;;
  }

  dimension: rahead_shipto_name {
    type: string
    sql: ${TABLE}.rahead_shipto_name ;;
  }

  dimension: rahead_shipto_state {
    type: string
    sql: ${TABLE}.rahead_shipto_state ;;
  }

  dimension: rahead_shipto_zipcode {
    type: string
    sql: ${TABLE}.rahead_shipto_zipcode ;;
  }

  dimension: rahead_shipzone_id {
    type: number
    sql: ${TABLE}.rahead_shipzone_id ;;
  }

  dimension: rahead_taxtype_id {
    type: number
    sql: ${TABLE}.rahead_taxtype_id ;;
  }

  dimension: rahead_taxzone_id {
    type: number
    sql: ${TABLE}.rahead_taxzone_id ;;
  }

  dimension: rahead_timing {
    type: string
    sql: ${TABLE}.rahead_timing ;;
  }

  dimension: rahead_warehous_id {
    type: number
    sql: ${TABLE}.rahead_warehous_id ;;
  }

  measure: count {
    type: count
    drill_fields: [rahead_id, rahead_billtoname, rahead_shipto_name]
  }
}
