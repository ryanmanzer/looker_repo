view: cohist {
  sql_table_name: public.cohist ;;

  dimension: cohist_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.cohist_id ;;
  }

  dimension: cohist_billtoaddress1 {
    type: string
    sql: ${TABLE}.cohist_billtoaddress1 ;;
  }

  dimension: cohist_billtoaddress2 {
    type: string
    sql: ${TABLE}.cohist_billtoaddress2 ;;
  }

  dimension: cohist_billtoaddress3 {
    type: string
    sql: ${TABLE}.cohist_billtoaddress3 ;;
  }

  dimension: cohist_billtocity {
    type: string
    sql: ${TABLE}.cohist_billtocity ;;
  }

  dimension: cohist_billtoname {
    type: string
    sql: ${TABLE}.cohist_billtoname ;;
  }

  dimension: cohist_billtostate {
    type: string
    sql: ${TABLE}.cohist_billtostate ;;
  }

  dimension: cohist_billtozip {
    type: string
    sql: ${TABLE}.cohist_billtozip ;;
  }

  dimension: cohist_cohead_ccpay_id {
    type: number
    sql: ${TABLE}.cohist_cohead_ccpay_id ;;
  }

  dimension: cohist_commission {
    type: number
    sql: ${TABLE}.cohist_commission ;;
  }

  dimension: cohist_commissionpaid {
    type: yesno
    sql: ${TABLE}.cohist_commissionpaid ;;
  }

  dimension: cohist_curr_id {
    type: number
    sql: ${TABLE}.cohist_curr_id ;;
  }

  dimension: cohist_cust_id {
    type: number
    sql: ${TABLE}.cohist_cust_id ;;
  }

  dimension: cohist_doctype {
    type: string
    sql: ${TABLE}.cohist_doctype ;;
  }

  dimension_group: cohist_duedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cohist_duedate ;;
  }

  dimension: cohist_imported {
    type: yesno
    sql: ${TABLE}.cohist_imported ;;
  }

  dimension_group: cohist_invcdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cohist_invcdate ;;
  }

  dimension: cohist_invcnumber {
    type: string
    sql: ${TABLE}.cohist_invcnumber ;;
  }

  dimension: cohist_itemsite_id {
    type: number
    sql: ${TABLE}.cohist_itemsite_id ;;
  }

  dimension: cohist_misc_descrip {
    type: string
    sql: ${TABLE}.cohist_misc_descrip ;;
  }

  dimension: cohist_misc_id {
    type: number
    sql: ${TABLE}.cohist_misc_id ;;
  }

  dimension: cohist_misc_type {
    type: string
    sql: ${TABLE}.cohist_misc_type ;;
  }

  dimension_group: cohist_orderdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cohist_orderdate ;;
  }

  dimension: cohist_ordernumber {
    type: string
    sql: ${TABLE}.cohist_ordernumber ;;
  }

  dimension: cohist_ponumber {
    type: string
    sql: ${TABLE}.cohist_ponumber ;;
  }

  dimension_group: cohist_promisedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cohist_promisedate ;;
  }

  dimension: cohist_qtyshipped {
    type: number
    sql: ${TABLE}.cohist_qtyshipped ;;
  }

  dimension: cohist_salesrep_id {
    type: number
    sql: ${TABLE}.cohist_salesrep_id ;;
  }

  dimension: cohist_saletype_id {
    type: number
    sql: ${TABLE}.cohist_saletype_id ;;
  }

  dimension: cohist_sequence {
    type: number
    sql: ${TABLE}.cohist_sequence ;;
  }

  dimension_group: cohist_shipdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cohist_shipdate ;;
  }

  dimension: cohist_shipto_id {
    type: number
    sql: ${TABLE}.cohist_shipto_id ;;
  }

  dimension: cohist_shiptoaddress1 {
    type: string
    sql: ${TABLE}.cohist_shiptoaddress1 ;;
  }

  dimension: cohist_shiptoaddress2 {
    type: string
    sql: ${TABLE}.cohist_shiptoaddress2 ;;
  }

  dimension: cohist_shiptoaddress3 {
    type: string
    sql: ${TABLE}.cohist_shiptoaddress3 ;;
  }

  dimension: cohist_shiptocity {
    type: string
    sql: ${TABLE}.cohist_shiptocity ;;
  }

  dimension: cohist_shiptoname {
    type: string
    sql: ${TABLE}.cohist_shiptoname ;;
  }

  dimension: cohist_shiptostate {
    type: string
    sql: ${TABLE}.cohist_shiptostate ;;
  }

  dimension: cohist_shiptozip {
    type: string
    sql: ${TABLE}.cohist_shiptozip ;;
  }

  dimension: cohist_shipvia {
    type: string
    sql: ${TABLE}.cohist_shipvia ;;
  }

  dimension: cohist_shipzone_id {
    type: number
    sql: ${TABLE}.cohist_shipzone_id ;;
  }

  dimension: cohist_taxtype_id {
    type: number
    sql: ${TABLE}.cohist_taxtype_id ;;
  }

  dimension: cohist_taxzone_id {
    type: number
    sql: ${TABLE}.cohist_taxzone_id ;;
  }

  dimension: cohist_unitcost {
    type: number
    sql: ${TABLE}.cohist_unitcost ;;
  }

  dimension: cohist_unitprice {
    type: number
    sql: ${TABLE}.cohist_unitprice ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [cohist_id, cohist_billtoname, cohist_shiptoname, saleshistory.count, saleshistorymisc.count]
  }

  dimension: line_price {
     type: number
    sql: ${cohist_qtyshipped} * ${cohist_unitprice} ;;
   }
}
