view: itemalias {
  sql_table_name: public.itemalias ;;

  dimension: itemalias_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.itemalias_id ;;
  }

  dimension: itemalias_comments {
    type: string
    sql: ${TABLE}.itemalias_comments ;;
  }

  dimension: itemalias_crmacct_id {
    type: number
    sql: ${TABLE}.itemalias_crmacct_id ;;
  }

  dimension: itemalias_descrip1 {
    type: string
    sql: ${TABLE}.itemalias_descrip1 ;;
  }

  dimension: itemalias_descrip2 {
    type: string
    sql: ${TABLE}.itemalias_descrip2 ;;
  }

  dimension: itemalias_item_id {
    type: number
    sql: ${TABLE}.itemalias_item_id ;;
  }

  dimension: itemalias_number {
    type: string
    sql: ${TABLE}.itemalias_number ;;
  }

  dimension: itemalias_usedescrip {
    type: yesno
    sql: ${TABLE}.itemalias_usedescrip ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [itemalias_id]
  }
}
