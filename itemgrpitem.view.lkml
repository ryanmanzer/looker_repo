view: itemgrpitem {
  sql_table_name: public.itemgrpitem ;;

  dimension: itemgrpitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.itemgrpitem_id ;;
  }

  dimension: itemgrpitem_item_id {
    type: number
    sql: ${TABLE}.itemgrpitem_item_id ;;
  }

  dimension: itemgrpitem_item_type {
    type: string
    sql: ${TABLE}.itemgrpitem_item_type ;;
  }

  dimension: itemgrpitem_itemgrp_id {
    type: number
    sql: ${TABLE}.itemgrpitem_itemgrp_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [itemgrpitem_id]
  }
}
