view: invoiceitem {
  sql_table_name: public.invoiceitem ;;

  dimension: baseextprice {
    type: number
    sql: ${TABLE}.baseextprice ;;
  }

  dimension: cohead_number {
    type: string
    sql: ${TABLE}.cohead_number ;;
  }

  dimension: extprice {
    type: number
    sql: ${TABLE}.extprice ;;
  }

  dimension: invcitem_billed {
    type: number
    sql: ${TABLE}.invcitem_billed ;;
  }

  dimension: invcitem_coitem_id {
    type: number
    sql: ${TABLE}.invcitem_coitem_id ;;
  }

  dimension: invcitem_custpn {
    type: string
    sql: ${TABLE}.invcitem_custpn ;;
  }

  dimension: invcitem_custprice {
    type: number
    sql: ${TABLE}.invcitem_custprice ;;
  }

  dimension: invcitem_descrip {
    type: string
    sql: ${TABLE}.invcitem_descrip ;;
  }

  dimension: invcitem_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.invcitem_id ;;
  }

  dimension: invcitem_invchead_id {
    type: number
    sql: ${TABLE}.invcitem_invchead_id ;;
  }

  dimension: invcitem_item_id {
    type: number
    sql: ${TABLE}.invcitem_item_id ;;
  }

  dimension: invcitem_linenumber {
    type: number
    sql: ${TABLE}.invcitem_linenumber ;;
  }

  dimension: invcitem_notes {
    type: string
    sql: ${TABLE}.invcitem_notes ;;
  }

  dimension: invcitem_number {
    type: string
    sql: ${TABLE}.invcitem_number ;;
  }

  dimension: invcitem_ordered {
    type: number
    sql: ${TABLE}.invcitem_ordered ;;
  }

  dimension: invcitem_price {
    type: number
    sql: ${TABLE}.invcitem_price ;;
  }

  dimension: invcitem_price_invuomratio {
    type: number
    sql: ${TABLE}.invcitem_price_invuomratio ;;
  }

  dimension: invcitem_price_uom_id {
    type: number
    sql: ${TABLE}.invcitem_price_uom_id ;;
  }

  dimension: invcitem_qty_invuomratio {
    type: number
    sql: ${TABLE}.invcitem_qty_invuomratio ;;
  }

  dimension: invcitem_qty_uom_id {
    type: number
    sql: ${TABLE}.invcitem_qty_uom_id ;;
  }

  dimension: invcitem_rev_accnt_id {
    type: number
    sql: ${TABLE}.invcitem_rev_accnt_id ;;
  }

  dimension: invcitem_salescat_id {
    type: number
    sql: ${TABLE}.invcitem_salescat_id ;;
  }

  dimension: invcitem_taxtype_id {
    type: number
    sql: ${TABLE}.invcitem_taxtype_id ;;
  }

  dimension: invcitem_updateinv {
    type: yesno
    sql: ${TABLE}.invcitem_updateinv ;;
  }

  dimension: invcitem_warehous_id {
    type: number
    sql: ${TABLE}.invcitem_warehous_id ;;
  }

  dimension: itemsite_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.itemsite_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: qty {
    type: number
    sql: ${TABLE}.qty ;;
  }

  dimension: tax {
    type: number
    sql: ${TABLE}.tax ;;
  }

  dimension: unitcost {
    type: number
    sql: ${TABLE}.unitcost ;;
  }

  dimension: unitprice {
    type: number
    sql: ${TABLE}.unitprice ;;
  }

  measure: count {
    type: count
    drill_fields: [itemsite.itemsite_supply_itemsite_id, invcitem.invcitem_id]
  }
}
