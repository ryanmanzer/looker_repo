view: cmnttype {
  sql_table_name: public.cmnttype ;;

  dimension: cmnttype_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.cmnttype_id ;;
  }

  dimension: cmnttype_descrip {
    type: string
    sql: ${TABLE}.cmnttype_descrip ;;
  }

  dimension: cmnttype_editable {
    type: yesno
    sql: ${TABLE}.cmnttype_editable ;;
  }

  dimension: cmnttype_name {
    type: string
    sql: ${TABLE}.cmnttype_name ;;
  }

  dimension: cmnttype_order {
    type: number
    sql: ${TABLE}.cmnttype_order ;;
  }

  dimension: cmnttype_sys {
    type: yesno
    sql: ${TABLE}.cmnttype_sys ;;
  }

  dimension: cmnttype_usedin {
    type: string
    sql: ${TABLE}.cmnttype_usedin ;;
  }

  measure: count {
    type: count
    drill_fields: [cmnttype_id, cmnttype_name]
  }
}
