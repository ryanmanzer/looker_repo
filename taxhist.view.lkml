view: taxhist {
  sql_table_name: public.taxhist ;;

  dimension: taxhist_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.taxhist_id ;;
  }

  dimension: taxhist_amount {
    type: number
    sql: ${TABLE}.taxhist_amount ;;
  }

  dimension: taxhist_basis {
    type: number
    sql: ${TABLE}.taxhist_basis ;;
  }

  dimension: taxhist_basis_tax_id {
    type: number
    sql: ${TABLE}.taxhist_basis_tax_id ;;
  }

  dimension: taxhist_curr_id {
    type: number
    sql: ${TABLE}.taxhist_curr_id ;;
  }

  dimension: taxhist_curr_rate {
    type: number
    sql: ${TABLE}.taxhist_curr_rate ;;
  }

  dimension_group: taxhist_distdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.taxhist_distdate ;;
  }

  dimension_group: taxhist_docdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.taxhist_docdate ;;
  }

  dimension: taxhist_journalnumber {
    type: number
    sql: ${TABLE}.taxhist_journalnumber ;;
  }

  dimension: taxhist_parent_id {
    type: number
    sql: ${TABLE}.taxhist_parent_id ;;
  }

  dimension: taxhist_percent {
    type: number
    sql: ${TABLE}.taxhist_percent ;;
  }

  dimension: taxhist_sequence {
    type: number
    sql: ${TABLE}.taxhist_sequence ;;
  }

  dimension: taxhist_tax {
    type: number
    sql: ${TABLE}.taxhist_tax ;;
  }

  dimension: taxhist_tax_id {
    type: number
    sql: ${TABLE}.taxhist_tax_id ;;
  }

  dimension: taxhist_taxtype_id {
    type: number
    sql: ${TABLE}.taxhist_taxtype_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      taxhist_id,
      apopentax.count,
      aropentax.count,
      asohisttax.count,
      cmheadtax.count,
      cmitemtax.count,
      cobilltax.count,
      cobmisctax.count,
      cohisttax.count,
      invcheadtax.count,
      invcitemtax.count,
      toheadtax.count,
      toitemtax.count,
      voheadtax.count,
      voitemtax.count
    ]
  }
}
