view: itemuom {
  sql_table_name: public.itemuom ;;

  dimension: itemuom_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.itemuom_id ;;
  }

  dimension: itemuom_itemuomconv_id {
    type: number
    sql: ${TABLE}.itemuom_itemuomconv_id ;;
  }

  dimension: itemuom_uomtype_id {
    type: number
    sql: ${TABLE}.itemuom_uomtype_id ;;
  }

  measure: count {
    type: count
    drill_fields: [itemuom_id]
  }
}
