view: lang {
  sql_table_name: public.lang ;;

  dimension: lang_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.lang_id ;;
  }

  dimension: lang_abbr2 {
    type: string
    sql: ${TABLE}.lang_abbr2 ;;
  }

  dimension: lang_abbr3 {
    type: string
    sql: ${TABLE}.lang_abbr3 ;;
  }

  dimension: lang_name {
    type: string
    sql: ${TABLE}.lang_name ;;
  }

  dimension: lang_qt_number {
    type: number
    sql: ${TABLE}.lang_qt_number ;;
  }

  measure: count {
    type: count
    drill_fields: [lang_id, lang_name]
  }
}
