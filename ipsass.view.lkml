view: ipsass {
  sql_table_name: public.ipsass ;;

  dimension: ipsass_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.ipsass_id ;;
  }

  dimension: ipsass_cust_id {
    type: number
    sql: ${TABLE}.ipsass_cust_id ;;
  }

  dimension: ipsass_custtype_id {
    type: number
    sql: ${TABLE}.ipsass_custtype_id ;;
  }

  dimension: ipsass_custtype_pattern {
    type: string
    sql: ${TABLE}.ipsass_custtype_pattern ;;
  }

  dimension: ipsass_ipshead_id {
    type: number
    sql: ${TABLE}.ipsass_ipshead_id ;;
  }

  dimension: ipsass_shipto_id {
    type: number
    sql: ${TABLE}.ipsass_shipto_id ;;
  }

  dimension: ipsass_shipto_pattern {
    type: string
    sql: ${TABLE}.ipsass_shipto_pattern ;;
  }

  measure: count {
    type: count
    drill_fields: [ipsass_id]
  }
}
