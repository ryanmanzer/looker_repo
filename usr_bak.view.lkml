view: usr_bak {
  sql_table_name: public.usr_bak ;;

  dimension: usr_active {
    type: yesno
    sql: ${TABLE}.usr_active ;;
  }

  dimension: usr_agent {
    type: yesno
    sql: ${TABLE}.usr_agent ;;
  }

  dimension: usr_email {
    type: string
    sql: ${TABLE}.usr_email ;;
  }

  dimension: usr_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.usr_id ;;
  }

  dimension: usr_initials {
    type: string
    sql: ${TABLE}.usr_initials ;;
  }

  dimension: usr_locale_id {
    type: number
    sql: ${TABLE}.usr_locale_id ;;
  }

  dimension: usr_passwd {
    type: string
    sql: ${TABLE}.usr_passwd ;;
  }

  dimension: usr_propername {
    type: string
    sql: ${TABLE}.usr_propername ;;
  }

  dimension: usr_username {
    type: string
    sql: ${TABLE}.usr_username ;;
  }

  dimension: usr_window {
    type: string
    sql: ${TABLE}.usr_window ;;
  }

  measure: count {
    type: count
    drill_fields: [usr_username, usr_propername, usr.usr_id, usr.usr_username, usr.usr_propername]
  }
}
