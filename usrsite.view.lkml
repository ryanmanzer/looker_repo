view: usrsite {
  sql_table_name: public.usrsite ;;

  dimension: usrsite_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.usrsite_id ;;
  }

  dimension: usrsite_username {
    type: string
    sql: ${TABLE}.usrsite_username ;;
  }

  dimension: usrsite_warehous_id {
    type: number
    sql: ${TABLE}.usrsite_warehous_id ;;
  }

  measure: count {
    type: count
    drill_fields: [usrsite_id, usrsite_username]
  }
}
