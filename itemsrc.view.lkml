view: itemsrc {
  sql_table_name: public.itemsrc ;;

  dimension: itemsrc_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.itemsrc_id ;;
  }

  dimension: itemsrc_active {
    type: yesno
    sql: ${TABLE}.itemsrc_active ;;
  }

  dimension: itemsrc_comments {
    type: string
    sql: ${TABLE}.itemsrc_comments ;;
  }

  dimension: itemsrc_contrct_id {
    type: number
    sql: ${TABLE}.itemsrc_contrct_id ;;
  }

  dimension: itemsrc_contrct_max {
    type: number
    sql: ${TABLE}.itemsrc_contrct_max ;;
  }

  dimension: itemsrc_contrct_min {
    type: number
    sql: ${TABLE}.itemsrc_contrct_min ;;
  }

  dimension: itemsrc_default {
    type: yesno
    sql: ${TABLE}.itemsrc_default ;;
  }

  dimension_group: itemsrc_effective {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.itemsrc_effective ;;
  }

  dimension_group: itemsrc_expires {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.itemsrc_expires ;;
  }

  dimension: itemsrc_invvendoruomratio {
    type: number
    sql: ${TABLE}.itemsrc_invvendoruomratio ;;
  }

  dimension: itemsrc_item_id {
    type: number
    sql: ${TABLE}.itemsrc_item_id ;;
  }

  dimension: itemsrc_leadtime {
    type: number
    sql: ${TABLE}.itemsrc_leadtime ;;
  }

  dimension: itemsrc_manuf_item_descrip {
    type: string
    sql: ${TABLE}.itemsrc_manuf_item_descrip ;;
  }

  dimension: itemsrc_manuf_item_number {
    type: string
    sql: ${TABLE}.itemsrc_manuf_item_number ;;
  }

  dimension: itemsrc_manuf_name {
    type: string
    sql: ${TABLE}.itemsrc_manuf_name ;;
  }

  dimension: itemsrc_minordqty {
    type: number
    sql: ${TABLE}.itemsrc_minordqty ;;
  }

  dimension: itemsrc_multordqty {
    type: number
    sql: ${TABLE}.itemsrc_multordqty ;;
  }

  dimension: itemsrc_ranking {
    type: number
    sql: ${TABLE}.itemsrc_ranking ;;
  }

  dimension: itemsrc_upccode {
    type: string
    sql: ${TABLE}.itemsrc_upccode ;;
  }

  dimension: itemsrc_vend_id {
    type: number
    sql: ${TABLE}.itemsrc_vend_id ;;
  }

  dimension: itemsrc_vend_item_descrip {
    type: string
    sql: ${TABLE}.itemsrc_vend_item_descrip ;;
  }

  dimension: itemsrc_vend_item_number {
    type: string
    sql: ${TABLE}.itemsrc_vend_item_number ;;
  }

  dimension: itemsrc_vend_uom {
    type: string
    sql: ${TABLE}.itemsrc_vend_uom ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  measure: count {
    type: count
    drill_fields: [itemsrc_id, itemsrc_manuf_name]
  }
}
