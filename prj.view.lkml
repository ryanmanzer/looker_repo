view: prj {
  sql_table_name: public.prj ;;

  dimension: prj_recurring_prj_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.prj_recurring_prj_id ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension_group: prj_assigned {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.prj_assigned_date ;;
  }

  dimension: prj_cntct_id {
    type: number
    sql: ${TABLE}.prj_cntct_id ;;
  }

  dimension_group: prj_completed {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.prj_completed_date ;;
  }

  dimension: prj_crmacct_id {
    type: number
    sql: ${TABLE}.prj_crmacct_id ;;
  }

  dimension: prj_descrip {
    type: string
    sql: ${TABLE}.prj_descrip ;;
  }

  dimension_group: prj_due {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.prj_due_date ;;
  }

  dimension: prj_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.prj_id ;;
  }

  dimension: prj_name {
    type: string
    sql: ${TABLE}.prj_name ;;
  }

  dimension: prj_number {
    type: string
    sql: ${TABLE}.prj_number ;;
  }

  dimension: prj_owner_username {
    type: string
    sql: ${TABLE}.prj_owner_username ;;
  }

  dimension: prj_po {
    type: yesno
    sql: ${TABLE}.prj_po ;;
  }

  dimension: prj_prjtype_id {
    type: number
    sql: ${TABLE}.prj_prjtype_id ;;
  }

  dimension: prj_so {
    type: yesno
    sql: ${TABLE}.prj_so ;;
  }

  dimension_group: prj_start {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.prj_start_date ;;
  }

  dimension: prj_status {
    type: string
    sql: ${TABLE}.prj_status ;;
  }

  dimension: prj_username {
    type: string
    sql: ${TABLE}.prj_username ;;
  }

  dimension: prj_wo {
    type: yesno
    sql: ${TABLE}.prj_wo ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      prj_recurring_prj_id,
      prj_name,
      prj_owner_username,
      prj_username,
      prj.prj_name,
      prj.prj_owner_username,
      prj.prj_username,
      prj.prj_recurring_prj_id,
      flaccnt.count,
      prj.count
    ]
  }
}
