view: recurtype {
  sql_table_name: public.recurtype ;;

  dimension: recurtype_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.recurtype_id ;;
  }

  dimension: recurtype_copyargs {
    type: string
    sql: ${TABLE}.recurtype_copyargs ;;
  }

  dimension: recurtype_copyfunc {
    type: string
    sql: ${TABLE}.recurtype_copyfunc ;;
  }

  dimension: recurtype_delfunc {
    type: string
    sql: ${TABLE}.recurtype_delfunc ;;
  }

  dimension: recurtype_donecheck {
    type: string
    sql: ${TABLE}.recurtype_donecheck ;;
  }

  dimension: recurtype_limit {
    type: string
    sql: ${TABLE}.recurtype_limit ;;
  }

  dimension: recurtype_schedcol {
    type: string
    sql: ${TABLE}.recurtype_schedcol ;;
  }

  dimension: recurtype_table {
    type: string
    sql: ${TABLE}.recurtype_table ;;
  }

  dimension: recurtype_type {
    type: string
    sql: ${TABLE}.recurtype_type ;;
  }

  measure: count {
    type: count
    drill_fields: [recurtype_id]
  }
}
