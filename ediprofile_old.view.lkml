view: ediprofile_old {
  sql_table_name: public.ediprofile_old ;;

  dimension: ediprofile_emailhtml {
    type: yesno
    sql: ${TABLE}.ediprofile_emailhtml ;;
  }

  dimension: ediprofile_id {
    type: number
    sql: ${TABLE}.ediprofile_id ;;
  }

  dimension: ediprofile_name {
    type: string
    sql: ${TABLE}.ediprofile_name ;;
  }

  dimension: ediprofile_notes {
    type: string
    sql: ${TABLE}.ediprofile_notes ;;
  }

  dimension: ediprofile_option1 {
    type: string
    sql: ${TABLE}.ediprofile_option1 ;;
  }

  dimension: ediprofile_option2 {
    type: string
    sql: ${TABLE}.ediprofile_option2 ;;
  }

  dimension: ediprofile_option3 {
    type: string
    sql: ${TABLE}.ediprofile_option3 ;;
  }

  dimension: ediprofile_option4 {
    type: string
    sql: ${TABLE}.ediprofile_option4 ;;
  }

  dimension: ediprofile_option5 {
    type: string
    sql: ${TABLE}.ediprofile_option5 ;;
  }

  dimension: ediprofile_type {
    type: string
    sql: ${TABLE}.ediprofile_type ;;
  }

  measure: count {
    type: count
    drill_fields: [ediprofile_name]
  }
}
