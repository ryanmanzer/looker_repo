view: subaccnttype {
  sql_table_name: public.subaccnttype ;;

  dimension: subaccnttype_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.subaccnttype_id ;;
  }

  dimension: subaccnttype_accnt_type {
    type: string
    sql: ${TABLE}.subaccnttype_accnt_type ;;
  }

  dimension: subaccnttype_code {
    type: string
    sql: ${TABLE}.subaccnttype_code ;;
  }

  dimension: subaccnttype_descrip {
    type: string
    sql: ${TABLE}.subaccnttype_descrip ;;
  }

  measure: count {
    type: count
    drill_fields: [subaccnttype_id]
  }
}
