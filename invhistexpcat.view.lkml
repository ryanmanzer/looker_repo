view: invhistexpcat {
  sql_table_name: public.invhistexpcat ;;

  dimension: invhistexpcat_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.invhistexpcat_id ;;
  }

  dimension: invhistexpcat_expcat_id {
    type: number
    sql: ${TABLE}.invhistexpcat_expcat_id ;;
  }

  dimension: invhistexpcat_invhist_id {
    type: number
    sql: ${TABLE}.invhistexpcat_invhist_id ;;
  }

  measure: count {
    type: count
    drill_fields: [invhistexpcat_id]
  }
}
