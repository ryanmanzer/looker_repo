view: ccbank {
  sql_table_name: public.ccbank ;;

  dimension: ccbank_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.ccbank_id ;;
  }

  dimension: ccbank_bankaccnt_id {
    type: number
    sql: ${TABLE}.ccbank_bankaccnt_id ;;
  }

  dimension: ccbank_ccard_type {
    type: string
    sql: ${TABLE}.ccbank_ccard_type ;;
  }

  measure: count {
    type: count
    drill_fields: [ccbank_id]
  }
}
