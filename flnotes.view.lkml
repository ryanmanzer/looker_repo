view: flnotes {
  sql_table_name: public.flnotes ;;

  dimension: flnotes_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.flnotes_id ;;
  }

  dimension: flnotes_flhead_id {
    type: number
    sql: ${TABLE}.flnotes_flhead_id ;;
  }

  dimension: flnotes_notes {
    type: string
    sql: ${TABLE}.flnotes_notes ;;
  }

  dimension: flnotes_period_id {
    type: number
    sql: ${TABLE}.flnotes_period_id ;;
  }

  measure: count {
    type: count
    drill_fields: [flnotes_id]
  }
}
