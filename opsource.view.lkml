view: opsource {
  sql_table_name: public.opsource ;;

  dimension: opsource_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.opsource_id ;;
  }

  dimension: opsource_descrip {
    type: string
    sql: ${TABLE}.opsource_descrip ;;
  }

  dimension: opsource_name {
    type: string
    sql: ${TABLE}.opsource_name ;;
  }

  measure: count {
    type: count
    drill_fields: [opsource_id, opsource_name]
  }
}
