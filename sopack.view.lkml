view: sopack {
  sql_table_name: public.sopack ;;

  dimension: sopack_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.sopack_id ;;
  }

  dimension: sopack_cosmisc_id {
    type: number
    sql: ${TABLE}.sopack_cosmisc_id ;;
  }

  dimension: sopack_printed {
    type: yesno
    sql: ${TABLE}.sopack_printed ;;
  }

  dimension: sopack_sohead_id {
    type: number
    sql: ${TABLE}.sopack_sohead_id ;;
  }

  measure: count {
    type: count
    drill_fields: [sopack_id]
  }
}
