view: saleshistory {
  sql_table_name: public.saleshistory ;;

  dimension: basecommission {
    type: number
    sql: ${TABLE}.basecommission ;;
  }

  dimension: basecommission_xtnumericrole {
    type: string
    sql: ${TABLE}.basecommission_xtnumericrole ;;
  }

  dimension: baseextprice {
    type: number
    sql: ${TABLE}.baseextprice ;;
  }

  dimension: baseextprice_xtnumericrole {
    type: string
    sql: ${TABLE}.baseextprice_xtnumericrole ;;
  }

  dimension: baseunitprice {
    type: number
    sql: ${TABLE}.baseunitprice ;;
  }

  dimension: baseunitprice_xtnumericrole {
    type: string
    sql: ${TABLE}.baseunitprice_xtnumericrole ;;
  }

  dimension: classcode_code {
    type: string
    sql: ${TABLE}.classcode_code ;;
  }

  dimension: classcode_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.classcode_id ;;
  }

  dimension: cohead_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.cohead_id ;;
  }

  dimension: cohist_billtoaddress1 {
    type: string
    sql: ${TABLE}.cohist_billtoaddress1 ;;
  }

  dimension: cohist_billtoaddress2 {
    type: string
    sql: ${TABLE}.cohist_billtoaddress2 ;;
  }

  dimension: cohist_billtoaddress3 {
    type: string
    sql: ${TABLE}.cohist_billtoaddress3 ;;
  }

  dimension: cohist_billtocity {
    type: string
    sql: ${TABLE}.cohist_billtocity ;;
  }

  dimension: cohist_billtoname {
    type: string
    sql: ${TABLE}.cohist_billtoname ;;
  }

  dimension: cohist_billtostate {
    type: string
    sql: ${TABLE}.cohist_billtostate ;;
  }

  dimension: cohist_billtozip {
    type: string
    sql: ${TABLE}.cohist_billtozip ;;
  }

  dimension: cohist_cohead_ccpay_id {
    type: number
    sql: ${TABLE}.cohist_cohead_ccpay_id ;;
  }

  dimension: cohist_commission {
    type: number
    sql: ${TABLE}.cohist_commission ;;
  }

  dimension: cohist_commission_xtnumericrole {
    type: string
    sql: ${TABLE}.cohist_commission_xtnumericrole ;;
  }

  dimension: cohist_commissionpaid {
    type: yesno
    sql: ${TABLE}.cohist_commissionpaid ;;
  }

  dimension: cohist_curr_id {
    type: number
    sql: ${TABLE}.cohist_curr_id ;;
  }

  dimension: cohist_cust_id {
    type: number
    sql: ${TABLE}.cohist_cust_id ;;
  }

  dimension: cohist_doctype {
    type: string
    sql: ${TABLE}.cohist_doctype ;;
  }

  dimension_group: cohist_duedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cohist_duedate ;;
  }

  dimension: cohist_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.cohist_id ;;
  }

  dimension: cohist_imported {
    type: yesno
    sql: ${TABLE}.cohist_imported ;;
  }

  dimension_group: cohist_invcdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cohist_invcdate ;;
  }

  dimension: cohist_invcnumber {
    type: string
    sql: ${TABLE}.cohist_invcnumber ;;
  }

  dimension: cohist_itemsite_id {
    type: number
    sql: ${TABLE}.cohist_itemsite_id ;;
  }

  dimension: cohist_misc_descrip {
    type: string
    sql: ${TABLE}.cohist_misc_descrip ;;
  }

  dimension: cohist_misc_id {
    type: number
    sql: ${TABLE}.cohist_misc_id ;;
  }

  dimension: cohist_misc_type {
    type: string
    sql: ${TABLE}.cohist_misc_type ;;
  }

  dimension_group: cohist_orderdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cohist_orderdate ;;
  }

  dimension: cohist_ordernumber {
    type: string
    sql: ${TABLE}.cohist_ordernumber ;;
  }

  dimension: cohist_ponumber {
    type: string
    sql: ${TABLE}.cohist_ponumber ;;
  }

  dimension_group: cohist_promisedate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cohist_promisedate ;;
  }

  dimension: cohist_qtyshipped {
    type: number
    sql: ${TABLE}.cohist_qtyshipped ;;
  }

  dimension: cohist_qtyshipped_xtnumericrole {
    type: string
    sql: ${TABLE}.cohist_qtyshipped_xtnumericrole ;;
  }

  dimension: cohist_salesrep_id {
    type: number
    sql: ${TABLE}.cohist_salesrep_id ;;
  }

  dimension: cohist_saletype_id {
    type: number
    sql: ${TABLE}.cohist_saletype_id ;;
  }

  dimension: cohist_sequence {
    type: number
    sql: ${TABLE}.cohist_sequence ;;
  }

  dimension_group: cohist_shipdate {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.cohist_shipdate ;;
  }

  dimension: cohist_shipto_id {
    type: number
    sql: ${TABLE}.cohist_shipto_id ;;
  }

  dimension: cohist_shiptoaddress1 {
    type: string
    sql: ${TABLE}.cohist_shiptoaddress1 ;;
  }

  dimension: cohist_shiptoaddress2 {
    type: string
    sql: ${TABLE}.cohist_shiptoaddress2 ;;
  }

  dimension: cohist_shiptoaddress3 {
    type: string
    sql: ${TABLE}.cohist_shiptoaddress3 ;;
  }

  dimension: cohist_shiptocity {
    type: string
    sql: ${TABLE}.cohist_shiptocity ;;
  }

  dimension: cohist_shiptoname {
    type: string
    sql: ${TABLE}.cohist_shiptoname ;;
  }

  dimension: cohist_shiptostate {
    type: string
    sql: ${TABLE}.cohist_shiptostate ;;
  }

  dimension: cohist_shiptozip {
    type: string
    sql: ${TABLE}.cohist_shiptozip ;;
  }

  dimension: cohist_shipvia {
    type: string
    sql: ${TABLE}.cohist_shipvia ;;
  }

  dimension: cohist_shipzone_id {
    type: number
    sql: ${TABLE}.cohist_shipzone_id ;;
  }

  dimension: cohist_taxtype_id {
    type: number
    sql: ${TABLE}.cohist_taxtype_id ;;
  }

  dimension: cohist_taxzone_id {
    type: number
    sql: ${TABLE}.cohist_taxzone_id ;;
  }

  dimension: cohist_unitcost {
    type: number
    sql: ${TABLE}.cohist_unitcost ;;
  }

  dimension: cohist_unitcost_xtnumericrole {
    type: string
    sql: ${TABLE}.cohist_unitcost_xtnumericrole ;;
  }

  dimension: cohist_unitprice {
    type: number
    sql: ${TABLE}.cohist_unitprice ;;
  }

  dimension: cohist_unitprice_xtnumericrole {
    type: string
    sql: ${TABLE}.cohist_unitprice_xtnumericrole ;;
  }

  dimension: currabbr {
    type: string
    sql: ${TABLE}.currabbr ;;
  }

  dimension: cust_curr_id {
    type: number
    sql: ${TABLE}.cust_curr_id ;;
  }

  dimension: cust_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.cust_id ;;
  }

  dimension: cust_name {
    type: string
    sql: ${TABLE}.cust_name ;;
  }

  dimension: cust_number {
    type: string
    sql: ${TABLE}.cust_number ;;
  }

  dimension: custextprice {
    type: number
    sql: ${TABLE}.custextprice ;;
  }

  dimension: custextprice_xtnumericrole {
    type: string
    sql: ${TABLE}.custextprice_xtnumericrole ;;
  }

  dimension: custtype_code {
    type: string
    sql: ${TABLE}.custtype_code ;;
  }

  dimension: custtype_descrip {
    type: string
    sql: ${TABLE}.custtype_descrip ;;
  }

  dimension: custtype_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.custtype_id ;;
  }

  dimension: custunitprice {
    type: number
    sql: ${TABLE}.custunitprice ;;
  }

  dimension: custunitprice_xtnumericrole {
    type: string
    sql: ${TABLE}.custunitprice_xtnumericrole ;;
  }

  dimension: extcost {
    type: number
    sql: ${TABLE}.extcost ;;
  }

  dimension: extcost_xtnumericrole {
    type: string
    sql: ${TABLE}.extcost_xtnumericrole ;;
  }

  dimension: extprice {
    type: number
    sql: ${TABLE}.extprice ;;
  }

  dimension: extprice_xtnumericrole {
    type: string
    sql: ${TABLE}.extprice_xtnumericrole ;;
  }

  dimension: item_descrip1 {
    type: string
    sql: ${TABLE}.item_descrip1 ;;
  }

  dimension: item_descrip2 {
    type: string
    sql: ${TABLE}.item_descrip2 ;;
  }

  dimension: item_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.item_id ;;
  }

  dimension: item_number {
    type: string
    sql: ${TABLE}.item_number ;;
  }

  dimension: itemdescription {
    type: string
    sql: ${TABLE}.itemdescription ;;
  }

  dimension: itemnumber {
    type: string
    sql: ${TABLE}.itemnumber ;;
  }

  dimension: itemsite_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.itemsite_id ;;
  }

  dimension: margin {
    type: number
    sql: ${TABLE}.margin ;;
  }

  dimension: margin_xtnumericrole {
    type: string
    sql: ${TABLE}.margin_xtnumericrole ;;
  }

  dimension: marginpercent {
    type: number
    sql: ${TABLE}.marginpercent ;;
  }

  dimension: marginpercent_xtnumericrole {
    type: string
    sql: ${TABLE}.marginpercent_xtnumericrole ;;
  }

  dimension: obj_uuid {
    type: string
    sql: ${TABLE}.obj_uuid ;;
  }

  dimension: prodcat_code {
    type: string
    sql: ${TABLE}.prodcat_code ;;
  }

  dimension: prodcat_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.prodcat_id ;;
  }

  dimension: salesrep_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.salesrep_id ;;
  }

  dimension: salesrep_name {
    type: string
    sql: ${TABLE}.salesrep_name ;;
  }

  dimension: salesrep_number {
    type: string
    sql: ${TABLE}.salesrep_number ;;
  }

  dimension: saletype_code {
    type: string
    sql: ${TABLE}.saletype_code ;;
  }

  dimension: saletype_descr {
    type: string
    sql: ${TABLE}.saletype_descr ;;
  }

  dimension: shipzone_descrip {
    type: string
    sql: ${TABLE}.shipzone_descrip ;;
  }

  dimension: shipzone_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.shipzone_id ;;
  }

  dimension: shipzone_name {
    type: string
    sql: ${TABLE}.shipzone_name ;;
  }

  dimension: warehous_code {
    type: string
    sql: ${TABLE}.warehous_code ;;
  }

  dimension: warehous_descrip {
    type: string
    sql: ${TABLE}.warehous_descrip ;;
  }

  dimension: warehous_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.warehous_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      cohist_billtoname,
      cohist_shiptoname,
      cust_name,
      salesrep_name,
      shipzone_name,
      cohist.cohist_id,
      cohist.cohist_billtoname,
      cohist.cohist_shiptoname,
      cohead.cohead_shipto_cntct_first_name,
      cohead.cohead_shipto_cntct_last_name,
      cohead.cohead_billto_cntct_first_name,
      cohead.cohead_billto_cntct_last_name,
      cohead.cohead_id,
      cohead.cohead_shiptoname,
      cohead.cohead_billtoname,
      cust.cust_id,
      cust.cust_name,
      cust.cust_edifilename,
      custtype.custtype_id,
      salesrep.salesrep_name,
      salesrep.salesrep_id,
      shipzone.shipzone_name,
      shipzone.shipzone_id,
      itemsite.itemsite_supply_itemsite_id,
      warehous.warehous_id,
      item.item_id,
      prodcat.prodcat_id,
      classcode.classcode_id
    ]
  }
}
