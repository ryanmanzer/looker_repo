view: ediform_old {
  sql_table_name: public.ediform_old ;;

  dimension: ediform_ediprofile_id {
    type: number
    sql: ${TABLE}.ediform_ediprofile_id ;;
  }

  dimension: ediform_file {
    type: string
    sql: ${TABLE}.ediform_file ;;
  }

  dimension: ediform_id {
    type: number
    sql: ${TABLE}.ediform_id ;;
  }

  dimension: ediform_option1 {
    type: string
    sql: ${TABLE}.ediform_option1 ;;
  }

  dimension: ediform_option2 {
    type: string
    sql: ${TABLE}.ediform_option2 ;;
  }

  dimension: ediform_output {
    type: string
    sql: ${TABLE}.ediform_output ;;
  }

  dimension: ediform_query {
    type: string
    sql: ${TABLE}.ediform_query ;;
  }

  dimension: ediform_type {
    type: string
    sql: ${TABLE}.ediform_type ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
