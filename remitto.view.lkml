view: remitto {
  sql_table_name: public.remitto ;;

  dimension: remitto_address1 {
    type: string
    sql: ${TABLE}.remitto_address1 ;;
  }

  dimension: remitto_address2 {
    type: string
    sql: ${TABLE}.remitto_address2 ;;
  }

  dimension: remitto_address3 {
    type: string
    sql: ${TABLE}.remitto_address3 ;;
  }

  dimension: remitto_citystatezip {
    type: string
    sql: ${TABLE}.remitto_citystatezip ;;
  }

  dimension: remitto_country {
    type: string
    sql: ${TABLE}.remitto_country ;;
  }

  dimension: remitto_name {
    type: string
    sql: ${TABLE}.remitto_name ;;
  }

  dimension: remitto_phone {
    type: string
    sql: ${TABLE}.remitto_phone ;;
  }

  measure: count {
    type: count
    drill_fields: [remitto_name]
  }
}
