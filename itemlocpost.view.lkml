view: itemlocpost {
  sql_table_name: public.itemlocpost ;;

  dimension: itemlocpost_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.itemlocpost_id ;;
  }

  dimension: itemlocpost_glseq {
    type: number
    sql: ${TABLE}.itemlocpost_glseq ;;
  }

  dimension: itemlocpost_itemlocseries {
    type: number
    sql: ${TABLE}.itemlocpost_itemlocseries ;;
  }

  measure: count {
    type: count
    drill_fields: [itemlocpost_id]
  }
}
