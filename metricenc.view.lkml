view: metricenc {
  sql_table_name: public.metricenc ;;

  dimension: metricenc_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.metricenc_id ;;
  }

  dimension: metricenc_module {
    type: string
    sql: ${TABLE}.metricenc_module ;;
  }

  dimension: metricenc_name {
    type: string
    sql: ${TABLE}.metricenc_name ;;
  }

  dimension: metricenc_value {
    type: string
    sql: ${TABLE}.metricenc_value ;;
  }

  measure: count {
    type: count
    drill_fields: [metricenc_id, metricenc_name]
  }
}
