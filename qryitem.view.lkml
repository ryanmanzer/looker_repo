view: qryitem {
  sql_table_name: public.qryitem ;;

  dimension: qryitem_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.qryitem_id ;;
  }

  dimension: qryitem_detail {
    type: string
    sql: ${TABLE}.qryitem_detail ;;
  }

  dimension: qryitem_group {
    type: string
    sql: ${TABLE}.qryitem_group ;;
  }

  dimension: qryitem_name {
    type: string
    sql: ${TABLE}.qryitem_name ;;
  }

  dimension: qryitem_notes {
    type: string
    sql: ${TABLE}.qryitem_notes ;;
  }

  dimension: qryitem_order {
    type: number
    sql: ${TABLE}.qryitem_order ;;
  }

  dimension: qryitem_qryhead_id {
    type: number
    sql: ${TABLE}.qryitem_qryhead_id ;;
  }

  dimension: qryitem_src {
    type: string
    sql: ${TABLE}.qryitem_src ;;
  }

  dimension_group: qryitem_updated {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.qryitem_updated ;;
  }

  dimension: qryitem_username {
    type: string
    sql: ${TABLE}.qryitem_username ;;
  }

  measure: count {
    type: count
    drill_fields: [qryitem_id, qryitem_name, qryitem_username]
  }
}
