view: vendtype {
  sql_table_name: public.vendtype ;;

  dimension: vendtype_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.vendtype_id ;;
  }

  dimension: vendtype_code {
    type: string
    sql: ${TABLE}.vendtype_code ;;
  }

  dimension: vendtype_descrip {
    type: string
    sql: ${TABLE}.vendtype_descrip ;;
  }

  measure: count {
    type: count
    drill_fields: [vendtype_id]
  }
}
