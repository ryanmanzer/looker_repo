view: bankadj {
  sql_table_name: public.bankadj ;;

  dimension: bankadj_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.bankadj_id ;;
  }

  dimension: bankadj_amount {
    type: number
    sql: ${TABLE}.bankadj_amount ;;
  }

  dimension: bankadj_bankaccnt_id {
    type: number
    sql: ${TABLE}.bankadj_bankaccnt_id ;;
  }

  dimension: bankadj_bankadjtype_id {
    type: number
    sql: ${TABLE}.bankadj_bankadjtype_id ;;
  }

  dimension_group: bankadj_created {
    type: time
    timeframes: [time, date, week, month]
    sql: ${TABLE}.bankadj_created ;;
  }

  dimension: bankadj_curr_id {
    type: number
    sql: ${TABLE}.bankadj_curr_id ;;
  }

  dimension: bankadj_curr_rate {
    type: number
    sql: ${TABLE}.bankadj_curr_rate ;;
  }

  dimension_group: bankadj {
    type: time
    timeframes: [date, week, month]
    convert_tz: no
    sql: ${TABLE}.bankadj_date ;;
  }

  dimension: bankadj_docnumber {
    type: string
    sql: ${TABLE}.bankadj_docnumber ;;
  }

  dimension: bankadj_notes {
    type: string
    sql: ${TABLE}.bankadj_notes ;;
  }

  dimension: bankadj_posted {
    type: yesno
    sql: ${TABLE}.bankadj_posted ;;
  }

  dimension: bankadj_sequence {
    type: number
    sql: ${TABLE}.bankadj_sequence ;;
  }

  dimension: bankadj_username {
    type: string
    sql: ${TABLE}.bankadj_username ;;
  }

  measure: count {
    type: count
    drill_fields: [bankadj_id, bankadj_username]
  }
}
