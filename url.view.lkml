view: url {
  sql_table_name: public.url ;;

  dimension: url_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.url_id ;;
  }

  dimension: url_source {
    type: string
    sql: ${TABLE}.url_source ;;
  }

  dimension: url_source_id {
    type: number
    sql: ${TABLE}.url_source_id ;;
  }

  dimension: url_stream {
    type: string
    sql: ${TABLE}.url_stream ;;
  }

  dimension: url_title {
    type: string
    sql: ${TABLE}.url_title ;;
  }

  dimension: url_url {
    type: string
    sql: ${TABLE}.url_url ;;
  }

  measure: count {
    type: count
    drill_fields: [url_id, urlinfo.count]
  }
}
