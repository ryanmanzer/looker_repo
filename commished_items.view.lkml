view: commished_items {
  sql_table_name: public.commished_items ;;

  dimension: item_descrip1 {
    type: string
    sql: ${TABLE}.item_descrip1 ;;
  }

  dimension: item_number {
    type: string
    sql: ${TABLE}.item_number ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
